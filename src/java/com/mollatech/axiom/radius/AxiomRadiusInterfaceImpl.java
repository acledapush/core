package com.mollatech.axiom.radius;

//import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.mobiletrust.Location;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Geofence;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Otptokens;
import com.mollatech.axiom.nucleus.db.Sessions;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.GeoFenceUtils;
import com.mollatech.axiom.nucleus.db.connector.GeoTrackingUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import com.mollatech.axiom.nucleus.db.connector.management.MobileTrustManagment;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.PushbasedauthenticationManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import com.mollatech.axiom.nucleus.settings.TokenSettings;
import static com.mollatech.axiom.v2.core.MobileTrustInterface.LOGIN;
import com.mollatech.axiom.v2.core.utils.AxiomStatus;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import org.hibernate.Session;

//@WebService(endpointInterface = "com.mollatech.axiom.v2.core.AxiomCoreInterface")
@WebService(endpointInterface = "com.mollatech.axiom.radius.AxiomRadiusInterface")
public class AxiomRadiusInterfaceImpl implements AxiomRadiusInterface {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AxiomRadiusInterfaceImpl.class.getName());

    public static int AxiomRadiusServerStatus;
    //private static Sessions gs_session = null;
    //private static Channels gs_channels = null;
    @Resource
    WebServiceContext wsContext;

    private int CheckCredentials(String channelid, String loginid, String loginpassword) {
        try {

            SettingsManagement setManagement = new SettingsManagement();
            try {

                ChannelProfile channelprofileObj = null;
                Object channelpobj = setManagement.getSettingInner(channelid, SettingsManagement.CHANNELPROFILE_SETTING, 1);

                if (channelpobj == null) {
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                } else {
                    channelprofileObj = (ChannelProfile) channelpobj;
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                }
            } catch (Exception ex) {
            }

            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                return -1;
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(channelid);
            if (channel == null) {
                return -2;
            }

            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);

            log.debug("Client IP = " + req.getRemoteAddr());

            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        SessionFactoryUtil suOperators = new SessionFactoryUtil(SessionFactoryUtil.operators);
                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            return -8;
                        }

                        suTemplate.close();
                        sTemplate.close();
                        return -8;
                    }
                    return -8;
                }
            }

            //String resultStr = "Failure";
            //int retValue = -1;
            SessionManagement sManagement = new SessionManagement();
            int iCredentialMatched = sManagement.CheckCredentials(channelid, loginid, loginpassword, req.getSession().getId());

            //AuditManagement audit = new AuditManagement();
            if (iCredentialMatched == 0) {
                return 0;
            }

            return -10;
        } catch (Exception e) {
            e.printStackTrace();
            return -11;
        }
    }

    @Override
    public AxiomRadiusConfiguration GetConfigrationSettings(String channelid, String loginid, String password) {
//        String strDebug = null;
        SettingsManagement setManagement = new SettingsManagement();
        try {

            int iResult = CheckCredentials(channelid, loginid, password);
            if (iResult != 0) {
                return null;
            }

            //AxiomRadiusConfiguration radiusConfigObj = null;
            Object radiuspobj = setManagement.getSettingInner(channelid, SettingsManagement.Radius, 1);

            if (radiuspobj == null) {
                //AxiomRadiusClient rClient = new AxiomRadiusClient();
                return null;

            } else {
                com.mollatech.axiom.nucleus.settings.AxiomRadiusConfiguration radiusConfigObj = (com.mollatech.axiom.nucleus.settings.AxiomRadiusConfiguration) radiuspobj;
                com.mollatech.axiom.radius.AxiomRadiusConfiguration radius = new com.mollatech.axiom.radius.AxiomRadiusConfiguration();
                radius.accountEnabled = radiusConfigObj.isAccountEnabled();
                radius.authIp = radiusConfigObj.getAuthIp();
                radius.authPort = radiusConfigObj.getAuthPort();
                radius.authEnabled = radiusConfigObj.isAuthEnabled();
                radius.ldapValidate = radiusConfigObj.isLdapValidate();
                radius.axiomValidate = radiusConfigObj.isAxiomValidate();
                radius.ldapSearchPath = radiusConfigObj.getLdapSearchPath();
                radius.accountIp = radiusConfigObj.getAccountIp();
                radius.accountPort = radiusConfigObj.getAccountPort();
                radius.ldapServerIp = radiusConfigObj.getLdapServerIp();
                radius.ldapServerPort = radiusConfigObj.getLdapServerPort();
                radius.ldapServerUsername = radiusConfigObj.getLdapServerUsername();
                radius.ldapServerPassword = radiusConfigObj.getLdapServerPassword();
                radius.sessionId = radiusConfigObj.getSessionId();
                radius.statusTimeinterval = LoadSettings.g_sSettings.getProperty("connector.status.check.time");

                radius.radiusClient = new com.mollatech.axiom.radius.AxiomRadiusClient[radiusConfigObj.getRadiusClient().length];
//                radius.radiusClient = new new AxiomRadiusClient[radiusConfigObj.getRadiusClient().length];

                com.mollatech.axiom.nucleus.settings.AxiomRadiusClient[] aradiusClient = new com.mollatech.axiom.nucleus.settings.AxiomRadiusClient[radiusConfigObj.getRadiusClient().length];
                aradiusClient = radiusConfigObj.getRadiusClient();
                for (int i = 0; i < radiusConfigObj.getRadiusClient().length; i++) {
                    radius.radiusClient[i] = new com.mollatech.axiom.radius.AxiomRadiusClient();
                    radius.radiusClient[i].radiusClientAuthtype = aradiusClient[i].getRadiusClientAuthtype();
                    radius.radiusClient[i].radiusClientDisplayname = aradiusClient[i].getRadiusClientDisplayname();
                    radius.radiusClient[i].radiusClientSecretkey = aradiusClient[i].getRadiusClientSecretkey();
                    radius.radiusClient[i].radiusClientIp = aradiusClient[i].getRadiusClientIp();
                    radius.radiusClient[i]._dayRestriction = aradiusClient[i].getDayRestriction();
                    radius.radiusClient[i]._timeFromInHour = aradiusClient[i].getTimeFromInHour();
                    radius.radiusClient[i]._timeToInHour = aradiusClient[i].getTimeToInHour();
                    radius.radiusClient[i]._timfromampm = aradiusClient[i].getTimfromampm();
                    radius.radiusClient[i]._timetoampm = aradiusClient[i].getTimetoampm();

                }

                //radiusConfigObj = (AxiomRadiusConfiguration) radiuspobj;
                return radius;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;

    }

    private String OpenSession(String channelid, String loginid, String loginpassword) {
        try {

            String strDebug = null;
            SettingsManagement setManagement = new SettingsManagement();
            try {

                ChannelProfile channelprofileObj = null;
                Object channelpobj = setManagement.getSettingInner(channelid, SettingsManagement.CHANNELPROFILE_SETTING, 1);

                if (channelpobj == null) {
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                } else {
                    channelprofileObj = (ChannelProfile) channelpobj;
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                }
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("OpenSession::loginpassword::" + loginpassword);
                    System.out.println("OpenSession::channelid::" + channelid);
                    System.out.println("OpenSession::loginid::" + loginid);
                }
            } catch (Exception ex) {
            }

            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                System.out.println("OpenSession::License Error::" + iResult);
                return null;
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(channelid);
            if (channel == null) {
                return null;
            }

            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            //System.out.println("Client IP = " + req.getRemoteAddr());
            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        SessionFactoryUtil suOperators = new SessionFactoryUtil(SessionFactoryUtil.operators);
                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            return null;
                        }

                        suTemplate.close();
                        sTemplate.close();
                        return null;
                    }
                    return null;
                }
            }

//            int result = setManagement.checkIP(channelid, req.getRemoteAddr());
//            if (checkIp != 1) {
//                return null;
//            }
            String resultStr = "Failure";
            int retValue = -1;
            SessionManagement sManagement = new SessionManagement();
            String sessionId = sManagement.OpenSessionForWS(channelid, loginid, loginpassword, req.getSession().getId());

            AuditManagement audit = new AuditManagement();
            if (sessionId != null) {
                retValue = 0;
                resultStr = "Success";
                audit.AddAuditTrail(sessionId, channelid, loginid,
                        req.getRemoteAddr(),
                        channel.getName(), loginid,
                        loginid, new Date(), "Open Session", resultStr, retValue,
                        "Login", "", "Open Session successfully with Session Id =" + sessionId, "SESSION",
                        loginid);

            } else if (sessionId == null) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), loginid, req.getRemoteAddr(), channel.getName(), loginid,
                        loginid, new Date(), "Open Session", resultStr, retValue,
                        "Login", "", "Failed To Open Session", "SESSION",
                        loginid);

            }

            return sessionId;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private AxiomStatus CloseSession(String sessionid) {
        try {

            String strDebug = null;
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("CloseSession::sessionid::" + sessionid);
                }
            } catch (Exception ex) {
            }

            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
                return aStatus;
            }

            SessionManagement sManagement = new SessionManagement();
            Sessions session = sManagement.getSessionById(sessionid);
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;

            if (session != null) {

                //System.out.println("Client IP = " + req.getRemoteAddr());        
                SettingsManagement setManagement = new SettingsManagement();
//                int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
//                if (result != 1) {
//                    aStatus.error = "INVALID IP REQUEST";
//                    aStatus.errorcode = -8;
//                    //  return aStatus;
//                }
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return null;
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }
                int retValue = sManagement.CloseSession(sessionid);

                aStatus.error = "ERROR";
                aStatus.errorcode = retValue;
                if (retValue == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                }

            } else if (session == null) {
                return aStatus;
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                return aStatus;
            }
            AuditManagement audit = new AuditManagement();
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                    session.getLoginid(), new Date(), "Close Session", aStatus.error, aStatus.errorcode,
                    "Login", "", "Close Session successfully with Session Id =" + sessionid, "SESSION",
                    session.getLoginid());
            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public AxiomRadiusStatus VerifyRadiusCredential(String channelid, String loginid, String loginpassword, String username, String password, String otp, String type) {

        try {
            int iResult = CheckCredentials(channelid, loginid, loginpassword);
            if (iResult != 0) {
                AxiomRadiusStatus ars = new AxiomRadiusStatus();
                ars.errorcode = -100;
                ars.error = "Client Authentication failed!!!";
                return ars;
            }

            AxiomRadiusStatus ars = new AxiomRadiusStatus();
            String sessionid = OpenSession(channelid, loginid, loginpassword);
            if (sessionid == null) {
                ars.errorcode = -101;
                ars.error = "Session could not be created!!!";
                return ars;
            }

            ars = VerifyRadiusCredentialInner(sessionid, username, password, otp, type);

            CloseSession(sessionid);

            return ars;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public int Ping(String channelid) {

        AxiomRadiusServerStatus = 1;
        SettingsManagement setManagement = new SettingsManagement();
        try {
            //AxiomRadiusConfiguration radiusConfigObj = null;
            Date lastupdate = setManagement.getLastUpdate(channelid, SettingsManagement.Radius, 1);
            if (lastupdate == null) {
                //AxiomRadiusClient rClient = new AxiomRadiusClient();
                return -1;
            } else {
                long timestamp = lastupdate.getTime();
                long diff = System.currentTimeMillis() - timestamp;
                if (diff < 600000) {
                    return 1;
                }
                //radiusConfigObj = (AxiomRadiusConfiguration) radiuspobj;
                return -2;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return -1;
    }

    public AxiomRadiusStatus VerifyRadiusCredentialInner(String sessionId, String username, String password, String otp, String type) {

        String strDebug = null;

        try {

            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("VerifyRadiusCredentialInner::sessionId::" + sessionId);
                System.out.println("VerifyRadiusCredentialInner::username::" + username);
                System.out.println("VerifyRadiusCredentialInner::password::" + password);
                System.out.println("VerifyRadiusCredentialInner::otp::" + otp);
                System.out.println("VerifyRadiusCredentialInner::type::" + type);
            }
        } catch (Exception ex) {
        }

        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionId);
        SettingsManagement setManagement = new SettingsManagement();
        String channelid = session.getChannelid();

        UserManagement user = new UserManagement();
        String userid = null;
        AuthUser authuser = user.CheckUserByType(sessionId, channelid, username, UserManagement.FULLNAME);
        if (authuser != null) {
            userid = authuser.getUserId();
        }
        String strException = "";

        com.mollatech.axiom.nucleus.settings.AxiomRadiusConfiguration radiusConfigObj = null;

        try {

            Object radiuspobj = setManagement.getSettingInner(channelid, SettingsManagement.Radius, 1);

            if (radiuspobj == null) {
                AxiomRadiusClient rClient = new AxiomRadiusClient();
            } else {
                radiusConfigObj = (com.mollatech.axiom.nucleus.settings.AxiomRadiusConfiguration) radiuspobj;

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (type.equalsIgnoreCase("password")) {

            if (radiusConfigObj.isAxiomValidate() == true) {
                try {
                    strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                    if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                        System.out.println("VerifyPassword::sessionId::" + sessionId);
                        System.out.println("VerifyPassword::userid::" + userid);
                        System.out.println("VerifyPassword::password::" + password);
                    }
                } catch (Exception ex) {
                }

                int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
//                aStatus.error = "Licence is invalid";
//                aStatus.errorcode = -100;
//                return aStatus;
//            }

//            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_PASSWORD) != 0) {
//                AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
//                aStatus.error = "This feature is not available in this license!!!";
//                aStatus.errorcode = -101;
//                return aStatus;
//            }
//            SessionManagement sManagement = new SessionManagement();
//            AuditManagement audit = new AuditManagement();
//            MessageContext mc = wsContext.getMessageContext();
//            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//            Sessions session = sManagement.getSessionById(sessionId);
                AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
                if (session == null) {
                    aStatus.error = "Invalid Session";
                    aStatus.errorcode = -14;
                    return aStatus;
                }

                UserManagement uManagement = new UserManagement();

                int retValue = -1;
                aStatus.error = "ERROR";
                aStatus.errorcode = retValue;

                if (session == null) {
                    return aStatus;
                }
                ChannelManagement cManagement = new ChannelManagement();
                Channels channel = cManagement.getChannelByID(session.getChannelid());
                if (channel == null) {
                    return aStatus;
                }
                if (session != null) {

//                SettingsManagement setManagement = new SettingsManagement();
//                String channelid = session.getChannelid();
                    if (channel == null) {
                        return null;
                    }
                    Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                    if (ipobj != null) {
                        GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                        int checkIp = 1;
                        if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                            checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                        } else {
                            checkIp = 1;
                        }
                        if (iObj.ipstatus == 0 && checkIp != 1) {
                            if (iObj.ipalertstatus == 0) {
                                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                                Session sTemplate = suTemplate.openSession();
                                TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                                Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                                OperatorsManagement oManagement = new OperatorsManagement();
                                Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                                if (aOperator != null) {
                                    String[] emailList = new String[aOperator.length - 1];
                                    for (int i = 1; i < aOperator.length; i++) {
                                        emailList[i - 1] = aOperator[i].getEmailid();
                                    }
                                    if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                        ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                        String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                        String strsubject = templatesObj.getSubject();
                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                        if (strmessageBody != null) {
                                            // Date date = new Date();
                                            strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                            strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                            strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                            strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                            strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                        }

                                        SendNotification send = new SendNotification();
                                        AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                                emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                        AxiomRadiusStatus axxStatus = new AxiomRadiusStatus();
                                        axxStatus.error = axStatus.strStatus;
                                        axxStatus.errorcode = axStatus.iStatus;

                                    }

                                    suTemplate.close();
                                    sTemplate.close();
                                    aStatus.error = "INVALID IP REQUEST";
                                    aStatus.errorcode = -8;
                                    return aStatus;
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                    }

                    AuthUser aUser = uManagement.verifyPassword(sessionId, session.getChannelid(), userid, password);

                    if (aUser != null) {
//                         //adding in for push notification >> 27th nov 2014
//                        
//                            //1. detect if push is enabled for this user 
//                            //2. send the push notification 
//                            SendNotification sNotification = new SendNotification();
//                            PushNotificationDeviceManagement pManagement = new PushNotificationDeviceManagement();
//                            String pushMessage = null;
//                            pushMessage = "Are you requesting for login from " + req.getRemoteAddr()  + " address. Please react accordingly!!!";
//                            
//                            //write into the DB
//                            PushbasedauthenticationManagement pnauth = new PushbasedauthenticationManagement();
//                            String nid = pnauth.AddPushMessages(session.getSessionid(), session.getChannelid(), userid, pushMessage);
//
//
//                            Registerdevicepush rDevice = pManagement.GetRegisterPushDeviceByUserID(session.getSessionid(), channelid, userid);
//                            if (rDevice != null) {
//                                JSONObject json = new JSONObject();
//                                json.put("_nid", nid);
//                                json.put("_message", pushMessage);
//                                json.put("_userid", userid);
//                                String messageToSend = json.toString();
//                                
//                                sNotification.SendOnPush(channelid, rDevice.getGoogleregisterid(), rDevice.getGooglemeailid(), messageToSend, rDevice.getType());
//                            }
//                            
//                            //3. check if loop for DB for certain time
//                            if (nid != null) {
//                                boolean bResponded = false;
//                                int iTimeToWait = 2 * 1000;
//                                int iTotalResult = 5;
//                                do {
//                                    try {
//                                        Thread.sleep(iTimeToWait);
//                                        iTotalResult--;
//                                        if (iTotalResult == 0) {
//                                            bResponded = true;
//                                        }
//
//                                        int iResponse = pnauth.CheckForResponse(session.getSessionid(), session.getChannelid(), userid, nid);
//
//                                        if (iResponse == 1) {
//
//                                            aStatus.error = "SUCCESS";
//                                            aStatus.errorcode = 0;
//                                            bResponded = true;
//
//                                        } else if (iResponse == -1) {
//                                            aStatus.error = "Authentication Failure";
//                                            aStatus.errorcode = -1;
//                                        }
//                                    } catch (InterruptedException ex) {
//                                        ex.printStackTrace();
//                                    }
//
//                                } while (bResponded == true);
//
//                            }
//
//                            pnauth = null;

                        //end of addition
                        aStatus.error = "SUCCESS";
                        aStatus.errorcode = 0;
                    }

                }

                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                        channel.getName(),
                        session.getLoginid(), session.getLoginid(),
                        new Date(),
                        "Verify Password", aStatus.error, aStatus.errorcode,
                        "User Management",
                        "",
                        "",
                        "PASSWORD", userid);
                return aStatus;
            } else if (radiusConfigObj.isLdapValidate() == true) {
                try {
                    strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                    if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                        System.out.println("VerifyPassword::sessionId::" + sessionId);
                        System.out.println("VerifyPassword::userid::" + userid);
                        System.out.println("VerifyPassword::password::" + password);
                    }
                } catch (Exception ex) {
                }

                int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
//                aStatus.error = "Licence is invalid";
//                aStatus.errorcode = -100;
//                return aStatus;
//            }

//            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_PASSWORD) != 0) {
//                AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
//                aStatus.error = "This feature is not available in this license!!!";
//                aStatus.errorcode = -101;
//                return aStatus;
//            }
//            SessionManagement sManagement = new SessionManagement();
//            AuditManagement audit = new AuditManagement();
//            MessageContext mc = wsContext.getMessageContext();
//            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//            Sessions session = sManagement.getSessionById(sessionId);
                AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
                if (session == null) {
                    aStatus.error = "Invalid Session";
                    aStatus.errorcode = -14;
                    return aStatus;
                }

                int retValue = -1;
                aStatus.error = "ERROR";
                aStatus.errorcode = retValue;

                if (session == null) {
                    return aStatus;
                }
                ChannelManagement cManagement = new ChannelManagement();
                Channels channel = cManagement.getChannelByID(session.getChannelid());
                if (channel == null) {
                    return aStatus;
                }
                if (session != null) {

//                SettingsManagement setManagement = new SettingsManagement();
//                String channelid = session.getChannelid();
                    if (channel == null) {
                        return null;
                    }
                    Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                    if (ipobj != null) {
                        GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                        int checkIp = 1;
                        if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                            checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                        } else {
                            checkIp = 1;
                        }
                        if (iObj.ipstatus == 0 && checkIp != 1) {
                            if (iObj.ipalertstatus == 0) {
                                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                                Session sTemplate = suTemplate.openSession();
                                TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                                Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                                OperatorsManagement oManagement = new OperatorsManagement();
                                Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                                if (aOperator != null) {
                                    String[] emailList = new String[aOperator.length - 1];
                                    for (int i = 1; i < aOperator.length; i++) {
                                        emailList[i - 1] = aOperator[i].getEmailid();
                                    }
                                    if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                        ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                        String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                        String strsubject = templatesObj.getSubject();
                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                        if (strmessageBody != null) {
                                            // Date date = new Date();
                                            strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                            strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                            strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                            strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                            strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                        }

                                        SendNotification send = new SendNotification();
                                        AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                                emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                        AxiomRadiusStatus axxStatus = new AxiomRadiusStatus();
                                        axxStatus.error = axStatus.strStatus;
                                        axxStatus.errorcode = axStatus.iStatus;

                                    }

                                    suTemplate.close();
                                    sTemplate.close();
                                    aStatus.error = "INVALID IP REQUEST";
                                    aStatus.errorcode = -8;
                                    return aStatus;
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                    }

                    //new addition for ACLEDA AD 
                    //                    New Code Added...
                    DirContext context = null;
                    Hashtable env = null;
                    env = new Hashtable();
                    String provider_url = "ldap://".concat(radiusConfigObj.getLdapServerIp()).concat(":").concat(Integer.toString(radiusConfigObj.getLdapServerPort()));
                    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
                    env.put(Context.SECURITY_AUTHENTICATION, "simple");
                    env.put(Context.SECURITY_PRINCIPAL, username);
                    env.put(Context.SECURITY_CREDENTIALS, password);
                    env.put(Context.PROVIDER_URL, provider_url);

                    //end of addition
//                    String provider_url = "ldap://".concat(radiusConfigObj.getLdapServerIp()).concat(":").concat(Integer.toString(radiusConfigObj.getLdapServerPort()));
//                    String security_principal = radiusConfigObj.getLdapSearchInitial().concat("=").concat(username).concat(",").concat(radiusConfigObj.getLdapSearchPath());
//
//                    DirContext context = null;
//                    Properties properties = new Properties();
//                    properties.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
//                    properties.put(Context.PROVIDER_URL, provider_url);
//                    properties.put(Context.SECURITY_AUTHENTICATION, "simple");
//                    properties.put(Context.SECURITY_PRINCIPAL, security_principal);
//                    properties.put(Context.SECURITY_CREDENTIALS, password);
                    try {
                        context = new InitialDirContext(env);

                        //adding in for push notification >> 27th nov 2014
//                        if (context != null) {
//                            //1. detect if push is enabled for this user 
//                            //2. send the push notification 
//                            SendNotification sNotification = new SendNotification();
//                            PushNotificationDeviceManagement pManagement = new PushNotificationDeviceManagement();
//                            String pushMessage = null;
//                            pushMessage = "Are you requesting for login from " + req.getRemoteAddr()  + " address. Please react accordingly!!!";
//                            
//                            //write into the DB
//                            PushbasedauthenticationManagement pnauth = new PushbasedauthenticationManagement();
//                            String nid = pnauth.AddPushMessages(session.getSessionid(), session.getChannelid(), userid, pushMessage);
//
//
//                            Registerdevicepush rDevice = pManagement.GetRegisterPushDeviceByUserID(session.getSessionid(), channelid, userid);
//                            if (rDevice != null) {
//                                JSONObject json = new JSONObject();
//                                json.put("_nid", nid);
//                                json.put("_message", pushMessage);
//                                json.put("_userid", userid);
//                                String messageToSend = json.toString();
//                                
//                                sNotification.SendOnPush(channelid, rDevice.getGoogleregisterid(), rDevice.getGooglemeailid(), messageToSend, rDevice.getType());
//                            }
//                            
//                            //3. check if loop for DB for certain time
//                            if (nid != null) {
//                                boolean bResponded = false;
//                                int iTimeToWait = 2 * 1000;
//                                int iTotalResult = 5;
//                                do {
//                                    try {
//                                        Thread.sleep(iTimeToWait);
//                                        iTotalResult--;
//                                        if (iTotalResult == 0) {
//                                            bResponded = true;
//                                        }
//
//                                        int iResponse = pnauth.CheckForResponse(session.getSessionid(), session.getChannelid(), userid, nid);
//
//                                        if (iResponse == 1) {
//
//                                            aStatus.error = "SUCCESS";
//                                            aStatus.errorcode = 0;
//                                            bResponded = true;
//
//                                        } else if (iResponse == -1) {
//                                            aStatus.error = "Authentication Failure";
//                                            aStatus.errorcode = -1;
//                                        }
//                                    } catch (InterruptedException ex) {
//                                        ex.printStackTrace();
//                                    }
//
//                                } while (bResponded == true);
//
//                            }
//
//                            pnauth = null;
//                        }
                        //end of addition
                        if (context != null) {
                            aStatus.error = "SUCCESS";
                            aStatus.errorcode = 0;
                        }
                    } catch (NamingException e) {
                        e.printStackTrace();
                        if (context == null) {
                            aStatus.error = "Authentication Failure";
                            aStatus.errorcode = -1;
                        }
                    }

                }

                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                        channel.getName(),
                        session.getLoginid(), session.getLoginid(),
                        new Date(),
                        "Verify Password", aStatus.error, aStatus.errorcode,
                        "LDAP Database",
                        "",
                        "",
                        "PASSWORD", userid);
                return aStatus;
            }

        } else if (type.equalsIgnoreCase("otp")) {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("VerifyOTP::sessionId::" + sessionId);
                    System.out.println("VerifyOTP::userid::" + userid);
                    System.out.println("VerifyOTP::otp::" + otp);
                }
            } catch (Exception ex) {
            }

            int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
//                aStatus.error = "Licence is invalid";
//                aStatus.errorcode = -100;
//                return aStatus;
//            }

//            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_OOB) != 0
//                    && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_HARDWARE) != 0
//                    && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
//                AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
//                aStatus.error = "This feature is not available in this license!!!";
//                aStatus.errorcode = -101;
//                return aStatus;
//            }
            try {

//            int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                AxiomStatus aStatus = new AxiomStatus();
//                aStatus.error = "Licence Not Valid";
//                aStatus.errorcode = -10;
//                return aStatus;
//            }
//                SessionManagement sManagement = new SessionManagement();
//                AuditManagement audit = new AuditManagement();
//                MessageContext mc = wsContext.getMessageContext();
//                HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//                Sessions session = sManagement.getSessionById(sessionId);
                if (session == null) {
                    AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
                    aStatus.error = "Invalid Session";
                    aStatus.errorcode = -14;
                    return aStatus;
                }

                if (sessionId == null || userid == null
                        || sessionId.isEmpty() == true || userid.isEmpty() == true
                        || otp == null || otp.isEmpty() == true) {
                    AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
                    aStatus.error = "Invalid Data";
                    aStatus.errorcode = -11;
                    return aStatus;
                }

                AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
                int retValue = -1;
                aStatus.error = "ERROR";
                aStatus.errorcode = retValue;
                ChannelManagement cManagement = new ChannelManagement();
                Channels channel = cManagement.getChannelByID(session.getChannelid());
                if (channel == null) {
                    aStatus.error = "Invalid Channel";
                    aStatus.errorcode = -15;
                    return aStatus;
                }
                OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                if (session != null) {

//                    SettingsManagement setManagement = new SettingsManagement();
//
//                    String channelid = session.getChannelid();
                    Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                    if (ipobj != null) {
                        GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                        int checkIp = 1;
                        if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                            checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                        } else {
                            checkIp = 1;
                        }
                        if (iObj.ipstatus == 0 && checkIp != 1) {
                            if (iObj.ipalertstatus == 0) {
                                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                                Session sTemplate = suTemplate.openSession();
                                TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                                Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                                OperatorsManagement oManagementObj = new OperatorsManagement();
                                Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                                if (aOperator != null) {
                                    String[] emailList = new String[aOperator.length - 1];
                                    for (int i = 1; i < aOperator.length; i++) {
                                        emailList[i - 1] = aOperator[i].getEmailid();
                                    }
                                    if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                        ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                        String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                        String strsubject = templatesObj.getSubject();
                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                        if (strmessageBody != null) {
                                            // Date date = new Date();
                                            strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                            strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                            strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                            strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                            strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                        }

                                        SendNotification send = new SendNotification();
                                        AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                                emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                    }

                                    suTemplate.close();
                                    sTemplate.close();
                                    aStatus.error = "INVALID IP REQUEST";
                                    aStatus.errorcode = -8;
                                    return aStatus;
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;

                            }
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                    }
                    //System.out.println("Before VerifyOTP >> " + otp);
                    //removed to support Geo OTP
                    //retValue = oManagement.VerifyOTP(session.getChannelid(), userid, sessionId, otp);
                    //end of remove

                    if (otp.contains(":")) {
                        MobileTrustManagment mManagment = new MobileTrustManagment();
                        UtilityFunctions utility = new UtilityFunctions();

                        String codes[] = otp.split(":");
                        String newOtp = codes[2];
                        String longitude = codes[1];
                        String lattitude = codes[0];

                        Location srvLoc = mManagment.getLocationByLongAndLat(longitude, lattitude);

//                        String oldLatiLong = otp.substring(0, otp.indexOf(":"));
//                        String[] latilong = utility.DecodeGEOCode(oldLatiLong);
//                        String newOtp = otp.substring(otp.indexOf(":") + 1, otp.length());
                        //Location srvLoc = mManagment.getLocationByLongAndLat(latilong[1], latilong[0]);
                        SessionFactoryUtil geouSession = new SessionFactoryUtil(SessionFactoryUtil.geotrack);
                        Session geoSession = geouSession.openSession();
                        GeoTrackingUtils geoUtil = new GeoTrackingUtils(geouSession, geoSession);
                        SessionFactoryUtil guSession = new SessionFactoryUtil(SessionFactoryUtil.geofence);
                        Session gSession = guSession.openSession();
                        GeoFenceUtils gUtil = new GeoFenceUtils(guSession, gSession);
                        try {

                            //int val = geoUtil.addGeotrack(channel.getChannelid(), userid, latilong[1], latilong[0], req.getRemoteAddr(), srvLoc.city, srvLoc.state, srvLoc.country, srvLoc.zipcode, LOGIN, new Date());
                            int val = geoUtil.addGeotrack(channel.getChannelid(), userid, longitude, lattitude, req.getRemoteAddr(), srvLoc.city, srvLoc.state, srvLoc.country, srvLoc.zipcode, LOGIN, new Date());
                            Geofence gFence = gUtil.getGeofence(userid, channel.getChannelid());
                            if (gFence == null) {
                                aStatus.errorcode = -7;
                            }

                            boolean b3434 = false;
                            String locationUser = null;
                            if (gFence != null) {
                                //b3434 = gFence.getRoamingAllowed();
                                b3434 = (gFence.getRoamingAllowed() != 0);
                                locationUser = gFence.getHomeCountry();
                            }

                            if (b3434 == false) {
                                if (srvLoc.country.equalsIgnoreCase(locationUser)) {
                                    retValue = 0;
                                } else {
                                    retValue = -12;
                                }
                            } else {

                                String[] foreignCountryList = null;
                                if (gFence.getForeignCountry() != null) {
                                    foreignCountryList = gFence.getForeignCountry().split(",");
                                }
                                if (foreignCountryList != null && foreignCountryList.length > 0) {
                                    for (int i = 0; i < foreignCountryList.length; i++) {
                                        if (srvLoc.country.equalsIgnoreCase(foreignCountryList[i])) {

                                            long nowDate = new Date().getTime();
                                            //System.out.println("Current Time::"+nowDate);
                                            //System.out.println("Start Time::"+gFence.getStartDateForRoming().getTime() );
                                            //System.out.println("End Time::"+gFence.getEndDateForRoming().getTime() );

                                            Long startDiff = nowDate - gFence.getStartDateForRoming().getTime();
                                            Long endDiff = gFence.getEndDateForRoming().getTime() - nowDate;
                                            if (startDiff < 0 || endDiff < 0) {
                                                retValue = -11;
                                            }
                                            retValue = 0;
                                        }
                                    }
                                } else {
                                    retValue = -13;
                                }

//                                if (gFence.getForeignCountry().equalsIgnoreCase(srvLoc.country)) {
//                                    Long startDiff = new Date().getTime() - gFence.getStartDateForRoming().getTime();
//                                    Long endDiff = gFence.getEndDateForRoming().getTime() - new Date().getTime();
//                                    if (startDiff < 0 || endDiff < 0) {
//                                        retValue = -11;
//                                    }
//                                    retValue = 0;
//                                } else {
//                                    retValue = -12;
//                                }
                            }
                        } finally {
                            geoSession.close();
                            geouSession.close();
                            gSession.close();
                            guSession.close();
                        }
                        if (retValue == 0) {
                            retValue = oManagement.VerifyOTP(session.getChannelid(), userid, sessionId, newOtp);
                        }
                    } else {
                        retValue = oManagement.VerifyOTP(session.getChannelid(), userid, sessionId, otp);
                    }

                    if (retValue == 0) {
                        aStatus.error = "SUCCESS";
                        aStatus.errorcode = 0;
                         //adding in for push notification >> 27th nov 2014

//                            //1. detect if push is enabled for this user 
//                            //2. send the push notification 
//                            SendNotification sNotification = new SendNotification();
//                            PushNotificationDeviceManagement pManagement = new PushNotificationDeviceManagement();
//                            String pushMessage = null;
//                            pushMessage = "Are you requesting for login from " + req.getRemoteAddr()  + " address. Please react accordingly!!!";
//                            
//                            //write into the DB
//                            PushbasedauthenticationManagement pnauth = new PushbasedauthenticationManagement();
//                            String nid = pnauth.AddPushMessages(session.getSessionid(), session.getChannelid(), userid, pushMessage);
//
//
//                            Registerdevicepush rDevice = pManagement.GetRegisterPushDeviceByUserID(session.getSessionid(), channelid, userid);
//                            if (rDevice != null) {
//                                JSONObject json = new JSONObject();
//                                json.put("_nid", nid);
//                                json.put("_message", pushMessage);
//                                json.put("_userid", userid);
//                                String messageToSend = json.toString();
//                                
//                                sNotification.SendOnPush(channelid, rDevice.getGoogleregisterid(), rDevice.getGooglemeailid(), messageToSend, rDevice.getType());
//                            }
//                            
//                            //3. check if loop for DB for certain time
//                            if (nid != null) {
//                                boolean bResponded = false;
//                                int iTimeToWait = 2 * 1000;
//                                int iTotalResult = 5;
//                                do {
//                                    try {
//                                        Thread.sleep(iTimeToWait);
//                                        iTotalResult--;
//                                        if (iTotalResult == 0) {
//                                            bResponded = true;
//                                        }
//
//                                        int iResponse = pnauth.CheckForResponse(session.getSessionid(), session.getChannelid(), userid, nid);
//
//                                        if (iResponse == 1) {
//
//                                            aStatus.error = "SUCCESS";
//                                            aStatus.errorcode = 0;
//                                            bResponded = true;
//
//                                        } else if (iResponse == -1) {
//                                            aStatus.error = "Authentication Failure";
//                                            aStatus.errorcode = -1;
//                                        }
//                                    } catch (InterruptedException ex) {
//                                        ex.printStackTrace();
//                                    }
//
//                                } while (bResponded == true);
//
//                            }
//
//                            pnauth = null;
                        //end of addition
                    } else if (retValue == -9) {
                        aStatus.error = "One Time Password is expired...";
                        aStatus.errorcode = -9;
                    } //added for new error codes 2nd april 2014
                    else if (retValue == -8) {
                        aStatus.error = "One Time Password is already consumed!!!";
                        aStatus.errorcode = -16;

                    } else if (retValue == -17) {
                        aStatus.error = "User/Token is not found!!!";
                        aStatus.errorcode = -17;

                    } else if (retValue == -22) {
                        aStatus.error = "Token is locked!!!";
                        aStatus.errorcode = -22;
                    }

                    //end of addition
                    //System.out.println("VerifyOTP result::" + retValue + " for OTP::" + otp);
                }

                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                        channel.getName(),
                        session.getLoginid(), session.getLoginid(),
                        new Date(),
                        "VERIFY OTP", aStatus.error, aStatus.errorcode,
                        "Token Management",
                        "OTP=" + otp,
                        "",
                        "OTPTOKENS", userid);

//            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                    session.getLoginid(), session.getLoginid(), new Date(), "Verify OTP ", aStatus.error, aStatus.errorcode,
//                    "Token Management", "OTP = ******", " OTP = ******",
//                    "Verify OTP TOKEN", userid);
                return aStatus;

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (type.equalsIgnoreCase("both")) {

//            SessionManagement sManagement = new SessionManagement();
//            AuditManagement audit = new AuditManagement();
//            MessageContext mc = wsContext.getMessageContext();
//            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//            Sessions session = sManagement.getSessionById(sessionId);
//            SettingsManagement setManagement = new SettingsManagement();
//            String channelid = session.getChannelid();
            TokenSettings tokenObj = null;
            Object obj = setManagement.getSettingInner(channelid, SettingsManagement.Token, 1);
            tokenObj = (TokenSettings) obj;

            OTPTokenManagement otpmanage = new OTPTokenManagement(channelid);
            Otptokens softObj = null;
            Otptokens hardObj = null;
            Otptokens oobObj = null;
            String newPasword = null;
            String newOTP = null;
            softObj = otpmanage.getOtpObjByUserId(channelid, userid, 1);
            hardObj = otpmanage.getOtpObjByUserId(channelid, userid, 2);
            oobObj = otpmanage.getOtpObjByUserId(channelid, userid, 3);

            if (softObj != null) {

                newPasword = password.substring(0, password.length() - (tokenObj.getOtpLengthSWToken()));
                newOTP = password.substring(password.length() - tokenObj.getOtpLengthSWToken(), password.length());

                try {
                    strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                    if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                        System.out.println("VerifyOTP::sessionId::" + sessionId);
                        System.out.println("VerifyOTP::userid::" + userid);
                        System.out.println("VerifyOTP::otp::" + otp);
                    }
                } catch (Exception ex) {
                }

                int iResult = AxiomProtect.ValidateLicense();
//                if (iResult != 0) {
//                    AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
//                    aStatus.error = "Licence is invalid";
//                    aStatus.errorcode = -100;
//                    return aStatus;
//                }

//                if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_OOB) != 0
//                        && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_HARDWARE) != 0
//                        && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
//                    AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
//                    aStatus.error = "This feature is not available in this license!!!";
//                    aStatus.errorcode = -101;
//                    return aStatus;
//                }
                try {

//            int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                AxiomStatus aStatus = new AxiomStatus();
//                aStatus.error = "Licence Not Valid";
//                aStatus.errorcode = -10;
//                return aStatus;
//            }
//                    SessionManagement sManagement = new SessionManagement();
//                    AuditManagement audit = new AuditManagement();
//                    MessageContext mc = wsContext.getMessageContext();
//                    HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//                    Sessions session = sManagement.getSessionById(sessionId);
                    if (session == null) {
                        AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
                        aStatus.error = "Invalid Session";
                        aStatus.errorcode = -14;
                        return aStatus;
                    }

//                    if (sessionId == null || userid == null
//                            || sessionId.isEmpty() == true || userid.isEmpty() == true
//                            || otp == null || otp.isEmpty() == true) {
//                        AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
//                        aStatus.error = "Invalid Data";
//                        aStatus.errorcode = -11;
//                        return aStatus;
//                    }
                    AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
                    int retValue = -1;
                    aStatus.error = "ERROR";
                    aStatus.errorcode = retValue;
                    ChannelManagement cManagement = new ChannelManagement();
                    Channels channel = cManagement.getChannelByID(session.getChannelid());
                    if (channel == null) {
                        aStatus.error = "Invalid Channel";
                        aStatus.errorcode = -15;
                        return aStatus;
                    }
                    OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                    if (session != null) {

//                        SettingsManagement setManagement = new SettingsManagement();
//
//                        String channelid = session.getChannelid();
                        Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                        if (ipobj != null) {
                            GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                            int checkIp = 1;
                            if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                                checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                            } else {
                                checkIp = 1;
                            }
                            if (iObj.ipstatus == 0 && checkIp != 1) {
                                if (iObj.ipalertstatus == 0) {
                                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                                    Session sTemplate = suTemplate.openSession();
                                    TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                                    Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                                    OperatorsManagement oManagementObj = new OperatorsManagement();
                                    Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                                    if (aOperator != null) {
                                        String[] emailList = new String[aOperator.length - 1];
                                        for (int i = 1; i < aOperator.length; i++) {
                                            emailList[i - 1] = aOperator[i].getEmailid();
                                        }
                                        if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                            ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                            String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                            String strsubject = templatesObj.getSubject();
                                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                            if (strmessageBody != null) {
                                                // Date date = new Date();
                                                strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                                strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                                strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                                strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                                strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                            }

                                            SendNotification send = new SendNotification();
                                            AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                                    emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                        }

                                        suTemplate.close();
                                        sTemplate.close();
                                        aStatus.error = "INVALID IP REQUEST";
                                        aStatus.errorcode = -8;
                                        return aStatus;
                                    }

                                    suTemplate.close();
                                    sTemplate.close();
                                    aStatus.error = "INVALID IP REQUEST";
                                    aStatus.errorcode = -8;
                                    return aStatus;

                                }
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                        }
                        //System.out.println("Before VerifyOTP >> " + otp);
                        retValue = oManagement.VerifyOTPByType(channelid, userid, sessionId, newOTP, OTPTokenManagement.SOFTWARE_TOKEN);

                        if (retValue == -1 && oobObj != null) {

                            newPasword = password.substring(0, (password.length() - tokenObj.getOtpLengthOOBToken()));
                            newOTP = password.substring(password.length() - tokenObj.getOtpLengthOOBToken(), password.length());

                            retValue = oManagement.VerifyOTPByType(channelid, userid, sessionId, newOTP, OTPTokenManagement.OOB_TOKEN);
                        }
                        if (retValue == -1 && hardObj != null) {
                            newPasword = password.substring(0, (password.length() - 6));
                            newOTP = password.substring(password.length() - 6, password.length());
                            retValue = oManagement.VerifyOTPByType(channelid, userid, sessionId, newOTP, OTPTokenManagement.HARDWARE_TOKEN);
                        }

                        if (retValue == 0) {

                            try {
                                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                                    System.out.println("VerifyPassword::sessionId::" + sessionId);
                                    System.out.println("VerifyPassword::userid::" + userid);
                                    System.out.println("VerifyPassword::password::" + password);
                                }

                            } catch (Exception ex) {
                            }

//                            int iResult = AxiomProtect.ValidateLicense();
//                            if (iResult != 0) {
////                                AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
//                                aStatus.error = "Licence is invalid";
//                                aStatus.errorcode = -100;
//                                return aStatus;
//                            }
//                            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_PASSWORD) != 0) {
////                                AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
//                                aStatus.error = "This feature is not available in this license!!!";
//                                aStatus.errorcode = -101;
//                                return aStatus;
//                            }
                            if (session == null) {
                                aStatus.error = "Invalid Session";
                                aStatus.errorcode = -14;
                                return aStatus;
                            }
                            UserManagement uManagement = new UserManagement();

                            aStatus.error = "ERROR";
                            aStatus.errorcode = retValue;

                            if (session == null) {
                                return aStatus;
                            }

                            if (channel == null) {
                                return aStatus;
                            }

                            if (session != null) {

                                if (radiusConfigObj.isAxiomValidate() == true) {

                                    AuthUser aUser = uManagement.verifyPassword(sessionId, session.getChannelid(), userid, newPasword);
                                    if (aUser != null) {
                                        aStatus.error = "SUCCESS";
                                        aStatus.errorcode = 0;

                                        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                                channel.getName(),
                                                session.getLoginid(), session.getLoginid(),
                                                new Date(),
                                                "Verify Password", aStatus.error, aStatus.errorcode,
                                                "User Management",
                                                "",
                                                "",
                                                "PASSWORD", userid);
                                        return aStatus;

                                    } else if (aUser == null) {
                                        aStatus.error = "ERROR";
                                        aStatus.errorcode = -1;

                                        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                                channel.getName(),
                                                session.getLoginid(), session.getLoginid(),
                                                new Date(),
                                                "Verify Password", aStatus.error, aStatus.errorcode,
                                                "User Management",
                                                "",
                                                "",
                                                "PASSWORD", userid);
                                        return aStatus;

                                    }
                                } else if (radiusConfigObj.isLdapValidate() == true) {
                                    DirContext context = null;
                                    Hashtable env = new Hashtable();
                                    String provider_url = "ldap://".concat(radiusConfigObj.getLdapServerIp()).concat(":").concat(Integer.toString(radiusConfigObj.getLdapServerPort()));
                                    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
                                    env.put(Context.SECURITY_AUTHENTICATION, "simple");
                                    env.put(Context.SECURITY_PRINCIPAL, userid);
                                    env.put(Context.SECURITY_CREDENTIALS, newPasword);
                                    env.put(Context.PROVIDER_URL, provider_url);

                                    try {
                                        context = new InitialDirContext(env);
                                        if (context != null) {
                                            aStatus.error = "SUCCESS";
                                            aStatus.errorcode = 0;

                                            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                                    channel.getName(),
                                                    session.getLoginid(), session.getLoginid(),
                                                    new Date(),
                                                    "Verify Password", aStatus.error, aStatus.errorcode,
                                                    "User Management",
                                                    "",
                                                    "",
                                                    "PASSWORD", userid);
                                        }
                                    } catch (NamingException e) {
                                        e.printStackTrace();
                                        if (context == null) {
                                            aStatus.error = "Authentication Failure";
                                            aStatus.errorcode = -1;

                                            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                                    channel.getName(),
                                                    session.getLoginid(), session.getLoginid(),
                                                    new Date(),
                                                    "Verify Password", aStatus.error, aStatus.errorcode,
                                                    "User Management",
                                                    "",
                                                    "",
                                                    "PASSWORD", userid);
                                        }
                                    }

                                }

//                                AuthUser aUser = uManagement.verifyPassword(sessionId, session.getChannelid(), userid, newPasword);
//
//                                if (aUser != null) {
//                                    aStatus.error = "SUCCESS";
//                                    aStatus.errorcode = 0;
//
//                                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
//                                            channel.getName(),
//                                            session.getLoginid(), session.getLoginid(),
//                                            new Date(),
//                                            "Verify Password", aStatus.error, aStatus.errorcode,
//                                            "User Management",
//                                            "",
//                                            "",
//                                            "PASSWORD", userid);
//                                    return aStatus;
//
//                                } else if (aUser == null) {
//                                    aStatus.error = "ERROR";
//                                    aStatus.errorcode = -1;
//
//                                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
//                                            channel.getName(),
//                                            session.getLoginid(), session.getLoginid(),
//                                            new Date(),
//                                            "Verify Password", aStatus.error, aStatus.errorcode,
//                                            "User Management",
//                                            "",
//                                            "",
//                                            "PASSWORD", userid);
//                                    return aStatus;
//
//                                }
                            }

                        } else if (retValue == -9) {
                            aStatus.error = "One Time Password is expired...";
                            aStatus.errorcode = -9;
                        } //added for new error codes 2nd april 2014
                        else if (retValue == -8) {
                            aStatus.error = "One Time Password is already consumed!!!";
                            aStatus.errorcode = -16;

                        } else if (retValue == -17) {
                            aStatus.error = "User/Token is not found!!!";
                            aStatus.errorcode = -17;

                        } else if (retValue == -22) {
                            aStatus.error = "Token is locked!!!";
                            aStatus.errorcode = -22;
                        }

                        //end of addition
                        //System.out.println("VerifyOTP result::" + retValue + " for OTP::" + otp);
                    }

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                            channel.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(),
                            "VERIFY OTP", aStatus.error, aStatus.errorcode,
                            "Token Management",
                            "OTP=" + otp,
                            "",
                            "OTPTOKENS", userid);

//            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                    session.getLoginid(), session.getLoginid(), new Date(), "Verify OTP ", aStatus.error, aStatus.errorcode,
//                    "Token Management", "OTP = ******", " OTP = ******",
//                    "Verify OTP TOKEN", userid);
                    return aStatus;

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            if (oobObj != null && softObj == null) {

                newPasword = password.substring(0, (password.length() - tokenObj.getOtpLengthOOBToken()));
                newOTP = password.substring((password.length() - tokenObj.getOtpLengthOOBToken()), password.length());

                try {
                    strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                    if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                        System.out.println("VerifyOTP::sessionId::" + sessionId);
                        System.out.println("VerifyOTP::userid::" + userid);
                        System.out.println("VerifyOTP::otp::" + otp);
                    }
                } catch (Exception ex) {
                }

                int iResult = AxiomProtect.ValidateLicense();
//                if (iResult != 0) {
//                    AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
//                    aStatus.error = "Licence is invalid";
//                    aStatus.errorcode = -100;
//                    return aStatus;
//                }

//                if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_OOB) != 0
//                        && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_HARDWARE) != 0
//                        && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
//                    AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
//                    aStatus.error = "This feature is not available in this license!!!";
//                    aStatus.errorcode = -101;
//                    return aStatus;
//                }
                try {

//            int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                AxiomStatus aStatus = new AxiomStatus();
//                aStatus.error = "Licence Not Valid";
//                aStatus.errorcode = -10;
//                return aStatus;
//            }
//                    SessionManagement sManagement = new SessionManagement();
//                    AuditManagement audit = new AuditManagement();
//                    MessageContext mc = wsContext.getMessageContext();
//                    HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//                    Sessions session = sManagement.getSessionById(sessionId);
                    if (session == null) {
                        AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
                        aStatus.error = "Invalid Session";
                        aStatus.errorcode = -14;
                        return aStatus;
                    }

//                    if (sessionId == null || userid == null
//                            || sessionId.isEmpty() == true || userid.isEmpty() == true
//                            || otp == null || otp.isEmpty() == true) {
//                        AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
//                        aStatus.error = "Invalid Data";
//                        aStatus.errorcode = -11;
//                        return aStatus;
//                    }
                    AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
                    int retValue = -1;
                    aStatus.error = "ERROR";
                    aStatus.errorcode = retValue;
                    ChannelManagement cManagement = new ChannelManagement();
                    Channels channel = cManagement.getChannelByID(session.getChannelid());
                    if (channel == null) {
                        aStatus.error = "Invalid Channel";
                        aStatus.errorcode = -15;
                        return aStatus;
                    }
                    OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                    if (session != null) {

//                        SettingsManagement setManagement = new SettingsManagement();
//
//                        String channelid = session.getChannelid();
                        Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                        if (ipobj != null) {
                            GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                            int checkIp = 1;
                            if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                                checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                            } else {
                                checkIp = 1;
                            }
                            if (iObj.ipstatus == 0 && checkIp != 1) {
                                if (iObj.ipalertstatus == 0) {
                                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                                    Session sTemplate = suTemplate.openSession();
                                    TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                                    Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                                    OperatorsManagement oManagementObj = new OperatorsManagement();
                                    Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                                    if (aOperator != null) {
                                        String[] emailList = new String[aOperator.length - 1];
                                        for (int i = 1; i < aOperator.length; i++) {
                                            emailList[i - 1] = aOperator[i].getEmailid();
                                        }
                                        if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                            ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                            String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                            String strsubject = templatesObj.getSubject();
                                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                            if (strmessageBody != null) {
                                                // Date date = new Date();
                                                strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                                strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                                strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                                strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                                strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                            }

                                            SendNotification send = new SendNotification();
                                            AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                                    emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                        }

                                        suTemplate.close();
                                        sTemplate.close();
                                        aStatus.error = "INVALID IP REQUEST";
                                        aStatus.errorcode = -8;
                                        return aStatus;
                                    }

                                    suTemplate.close();
                                    sTemplate.close();
                                    aStatus.error = "INVALID IP REQUEST";
                                    aStatus.errorcode = -8;
                                    return aStatus;

                                }
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                        }
                        //System.out.println("Before VerifyOTP >> " + otp);
                        retValue = oManagement.VerifyOTPByType(channelid, userid, sessionId, newOTP, OTPTokenManagement.OOB_TOKEN);

                        if (retValue == -1 && oobObj != null) {

                            newPasword = password.substring(0, password.length() - 6);
                            newOTP = password.substring(password.length() - 6, password.length());

                            retValue = oManagement.VerifyOTPByType(channelid, userid, sessionId, newOTP, OTPTokenManagement.HARDWARE_TOKEN);
                        }

                        if (retValue == 0) {

                            try {
                                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                                    System.out.println("VerifyPassword::sessionId::" + sessionId);
                                    System.out.println("VerifyPassword::userid::" + userid);
                                    System.out.println("VerifyPassword::password::" + newPasword);
                                }

                            } catch (Exception ex) {
                            }

//                            int iResult = AxiomProtect.ValidateLicense();
//                            if (iResult != 0) {
////                                AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
//                                aStatus.error = "Licence is invalid";
//                                aStatus.errorcode = -100;
//                                return aStatus;
//                            }
//                            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_PASSWORD) != 0) {
////                                AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
//                                aStatus.error = "This feature is not available in this license!!!";
//                                aStatus.errorcode = -101;
//                                return aStatus;
//                            }
                            if (session == null) {
                                aStatus.error = "Invalid Session";
                                aStatus.errorcode = -14;
                                return aStatus;
                            }
                            UserManagement uManagement = new UserManagement();

                            aStatus.error = "ERROR";
                            aStatus.errorcode = retValue;

                            if (session == null) {
                                return aStatus;
                            }

                            if (channel == null) {
                                return aStatus;
                            }

                            if (session != null) {

                                if (radiusConfigObj.isAxiomValidate() == true) {

                                    AuthUser aUser = uManagement.verifyPassword(sessionId, session.getChannelid(), userid, newPasword);
                                    if (aUser != null) {
                                        aStatus.error = "SUCCESS";
                                        aStatus.errorcode = 0;

                                        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                                channel.getName(),
                                                session.getLoginid(), session.getLoginid(),
                                                new Date(),
                                                "Verify Password", aStatus.error, aStatus.errorcode,
                                                "User Management",
                                                "",
                                                "",
                                                "PASSWORD", userid);
                                        return aStatus;

                                    } else if (aUser == null) {
                                        aStatus.error = "ERROR";
                                        aStatus.errorcode = -1;

                                        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                                channel.getName(),
                                                session.getLoginid(), session.getLoginid(),
                                                new Date(),
                                                "Verify Password", aStatus.error, aStatus.errorcode,
                                                "User Management",
                                                "",
                                                "",
                                                "PASSWORD", userid);
                                        return aStatus;

                                    }
                                }
                                if (radiusConfigObj.isLdapValidate() == true) {
                                    DirContext context = null;
                                    Hashtable env = new Hashtable();
                                    String provider_url = "ldap://".concat(radiusConfigObj.getLdapServerIp()).concat(":").concat(Integer.toString(radiusConfigObj.getLdapServerPort()));
                                    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
                                    env.put(Context.SECURITY_AUTHENTICATION, "simple");
                                    env.put(Context.SECURITY_PRINCIPAL, userid);
                                    env.put(Context.SECURITY_CREDENTIALS, newPasword);
                                    env.put(Context.PROVIDER_URL, provider_url);

                                    try {
                                        context = new InitialDirContext(env);
                                        if (context != null) {
                                            aStatus.error = "SUCCESS";
                                            aStatus.errorcode = 0;

                                            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                                    channel.getName(),
                                                    session.getLoginid(), session.getLoginid(),
                                                    new Date(),
                                                    "Verify Password", aStatus.error, aStatus.errorcode,
                                                    "User Management",
                                                    "",
                                                    "",
                                                    "PASSWORD", userid);
                                        }
                                    } catch (NamingException e) {
                                        e.printStackTrace();
                                        if (context == null) {
                                            aStatus.error = "Authentication Failure";
                                            aStatus.errorcode = -1;

                                            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                                    channel.getName(),
                                                    session.getLoginid(), session.getLoginid(),
                                                    new Date(),
                                                    "Verify Password", aStatus.error, aStatus.errorcode,
                                                    "User Management",
                                                    "",
                                                    "",
                                                    "PASSWORD", userid);
                                        }
                                    }

                                }

//                                AuthUser aUser = uManagement.verifyPassword(sessionId, session.getChannelid(), userid, newPasword);
//
//                                if (aUser != null) {
//                                    aStatus.error = "SUCCESS";
//                                    aStatus.errorcode = 0;
//
//                                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
//                                            channel.getName(),
//                                            session.getLoginid(), session.getLoginid(),
//                                            new Date(),
//                                            "Verify Password", aStatus.error, aStatus.errorcode,
//                                            "User Management",
//                                            "",
//                                            "",
//                                            "PASSWORD", userid);
//                                    return aStatus;
//
//                                } else if (aUser == null) {
//                                    aStatus.error = "ERROR";
//                                    aStatus.errorcode = -1;
//
//                                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
//                                            channel.getName(),
//                                            session.getLoginid(), session.getLoginid(),
//                                            new Date(),
//                                            "Verify Password", aStatus.error, aStatus.errorcode,
//                                            "User Management",
//                                            "",
//                                            "",
//                                            "PASSWORD", userid);
//                                    return aStatus;
//
//                                }
                            }

                        } else if (retValue == -9) {
                            aStatus.error = "One Time Password is expired...";
                            aStatus.errorcode = -9;
                        } //added for new error codes 2nd april 2014
                        else if (retValue == -8) {
                            aStatus.error = "One Time Password is already consumed!!!";
                            aStatus.errorcode = -16;

                        } else if (retValue == -17) {
                            aStatus.error = "User/Token is not found!!!";
                            aStatus.errorcode = -17;

                        } else if (retValue == -22) {
                            aStatus.error = "Token is locked!!!";
                            aStatus.errorcode = -22;
                        }

                        //end of addition
                        //System.out.println("VerifyOTP result::" + retValue + " for OTP::" + otp);
                    }

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                            channel.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(),
                            "VERIFY OTP", aStatus.error, aStatus.errorcode,
                            "Token Management",
                            "OTP=" + otp,
                            "",
                            "OTPTOKENS", userid);

//            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                    session.getLoginid(), session.getLoginid(), new Date(), "Verify OTP ", aStatus.error, aStatus.errorcode,
//                    "Token Management", "OTP = ******", " OTP = ******",
//                    "Verify OTP TOKEN", userid);
                    return aStatus;

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            if (hardObj != null && oobObj == null && softObj == null) {
                try {
                    strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                    if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                        System.out.println("VerifyOTP::sessionId::" + sessionId);
                        System.out.println("VerifyOTP::userid::" + userid);
                        System.out.println("VerifyOTP::otp::" + otp);
                    }
                } catch (Exception ex) {
                }

                int iResult = AxiomProtect.ValidateLicense();
                if (iResult != 0) {
                    AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
                    aStatus.error = "Licence is invalid";
                    aStatus.errorcode = -100;
                    return aStatus;
                }

                if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_OOB) != 0
                        && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_HARDWARE) != 0
                        && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
                    AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
                    aStatus.error = "This feature is not available in this license!!!";
                    aStatus.errorcode = -101;
                    return aStatus;
                }

                try {

//            int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                AxiomStatus aStatus = new AxiomStatus();
//                aStatus.error = "Licence Not Valid";
//                aStatus.errorcode = -10;
//                return aStatus;
//            }
//                    SessionManagement sManagement = new SessionManagement();
//                    AuditManagement audit = new AuditManagement();
//                    MessageContext mc = wsContext.getMessageContext();
//                    HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//                    Sessions session = sManagement.getSessionById(sessionId);
                    if (session == null) {
                        AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
                        aStatus.error = "Invalid Session";
                        aStatus.errorcode = -14;
                        return aStatus;
                    }

//                    if (sessionId == null || userid == null
//                            || sessionId.isEmpty() == true || userid.isEmpty() == true
//                            || otp == null || otp.isEmpty() == true) {
//                        AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
//                        aStatus.error = "Invalid Data";
//                        aStatus.errorcode = -11;
//                        return aStatus;
//                    }
                    AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
                    int retValue = -1;
                    aStatus.error = "ERROR";
                    aStatus.errorcode = retValue;
                    ChannelManagement cManagement = new ChannelManagement();
                    Channels channel = cManagement.getChannelByID(session.getChannelid());
                    if (channel == null) {
                        aStatus.error = "Invalid Channel";
                        aStatus.errorcode = -15;
                        return aStatus;
                    }
                    OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                    if (session != null) {

//                        SettingsManagement setManagement = new SettingsManagement();
//
//                        String channelid = session.getChannelid();
                        Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                        if (ipobj != null) {
                            GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                            int checkIp = 1;
                            if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                                checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                            } else {
                                checkIp = 1;
                            }
                            if (iObj.ipstatus == 0 && checkIp != 1) {
                                if (iObj.ipalertstatus == 0) {
                                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                                    Session sTemplate = suTemplate.openSession();
                                    TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                                    Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                                    OperatorsManagement oManagementObj = new OperatorsManagement();
                                    Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                                    if (aOperator != null) {
                                        String[] emailList = new String[aOperator.length - 1];
                                        for (int i = 1; i < aOperator.length; i++) {
                                            emailList[i - 1] = aOperator[i].getEmailid();
                                        }
                                        if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                            ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                            String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                            String strsubject = templatesObj.getSubject();
                                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                            if (strmessageBody != null) {
                                                // Date date = new Date();
                                                strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                                strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                                strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                                strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                                strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                            }

                                            SendNotification send = new SendNotification();
                                            AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                                    emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                        }

                                        suTemplate.close();
                                        sTemplate.close();
                                        aStatus.error = "INVALID IP REQUEST";
                                        aStatus.errorcode = -8;
                                        return aStatus;
                                    }

                                    suTemplate.close();
                                    sTemplate.close();
                                    aStatus.error = "INVALID IP REQUEST";
                                    aStatus.errorcode = -8;
                                    return aStatus;

                                }
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                        }
                        //System.out.println("Before VerifyOTP >> " + otp);

                        newPasword = password.substring(0, password.length() - 6);
                        newOTP = password.substring(password.length() - 6, password.length());
                        retValue = oManagement.VerifyOTPByType(channelid, userid, sessionId, newOTP, OTPTokenManagement.HARDWARE_TOKEN);

                        if (retValue == 0) {

                            try {
                                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                                    System.out.println("VerifyPassword::sessionId::" + sessionId);
                                    System.out.println("VerifyPassword::userid::" + userid);
                                    System.out.println("VerifyPassword::password::" + newPasword);
                                }

                            } catch (Exception ex) {
                            }

//                            int iResult = AxiomProtect.ValidateLicense();
//                            if (iResult != 0) {
////                                AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
//                                aStatus.error = "Licence is invalid";
//                                aStatus.errorcode = -100;
//                                return aStatus;
//                            }
//                            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_PASSWORD) != 0) {
////                                AxiomRadiusStatus aStatus = new AxiomRadiusStatus();
//                                aStatus.error = "This feature is not available in this license!!!";
//                                aStatus.errorcode = -101;
//                                return aStatus;
//                            }
                            if (session == null) {
                                aStatus.error = "Invalid Session";
                                aStatus.errorcode = -14;
                                return aStatus;
                            }
                            UserManagement uManagement = new UserManagement();

                            aStatus.error = "ERROR";
                            aStatus.errorcode = retValue;

                            if (session == null) {
                                return aStatus;
                            }

                            if (channel == null) {
                                return aStatus;
                            }

                            if (session != null) {

                                if (radiusConfigObj.isAxiomValidate() == true) {

                                    AuthUser aUser = uManagement.verifyPassword(sessionId, session.getChannelid(), userid, newPasword);
                                    if (aUser != null) {
                                        aStatus.error = "SUCCESS";
                                        aStatus.errorcode = 0;

                                        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                                channel.getName(),
                                                session.getLoginid(), session.getLoginid(),
                                                new Date(),
                                                "Verify Password", aStatus.error, aStatus.errorcode,
                                                "User Management",
                                                "",
                                                "",
                                                "PASSWORD", userid);
                                        return aStatus;

                                    } else if (aUser == null) {
                                        aStatus.error = "ERROR";
                                        aStatus.errorcode = -1;

                                        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                                channel.getName(),
                                                session.getLoginid(), session.getLoginid(),
                                                new Date(),
                                                "Verify Password", aStatus.error, aStatus.errorcode,
                                                "User Management",
                                                "",
                                                "",
                                                "PASSWORD", userid);
                                        return aStatus;

                                    }
                                }
                                if (radiusConfigObj.isLdapValidate() == true) {
                                    DirContext context = null;
                                    Hashtable env = new Hashtable();
                                    String provider_url = "ldap://".concat(radiusConfigObj.getLdapServerIp()).concat(":").concat(Integer.toString(radiusConfigObj.getLdapServerPort()));
                                    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
                                    env.put(Context.SECURITY_AUTHENTICATION, "simple");
                                    env.put(Context.SECURITY_PRINCIPAL, userid);
                                    env.put(Context.SECURITY_CREDENTIALS, newPasword);
                                    env.put(Context.PROVIDER_URL, provider_url);

                                    try {
                                        context = new InitialDirContext(env);
                                        if (context != null) {
                                            aStatus.error = "SUCCESS";
                                            aStatus.errorcode = 0;

                                            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                                    channel.getName(),
                                                    session.getLoginid(), session.getLoginid(),
                                                    new Date(),
                                                    "Verify Password", aStatus.error, aStatus.errorcode,
                                                    "User Management",
                                                    "",
                                                    "",
                                                    "PASSWORD", userid);
                                        }
                                    } catch (NamingException e) {
                                        e.printStackTrace();
                                        if (context == null) {
                                            aStatus.error = "Authentication Failure";
                                            aStatus.errorcode = -1;

                                            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                                    channel.getName(),
                                                    session.getLoginid(), session.getLoginid(),
                                                    new Date(),
                                                    "Verify Password", aStatus.error, aStatus.errorcode,
                                                    "User Management",
                                                    "",
                                                    "",
                                                    "PASSWORD", userid);
                                        }
                                    }

                                }

//                                AuthUser aUser = uManagement.verifyPassword(sessionId, session.getChannelid(), userid, newPasword);
//
//                                if (aUser != null) {
//                                    aStatus.error = "SUCCESS";
//                                    aStatus.errorcode = 0;
//
//                                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
//                                            channel.getName(),
//                                            session.getLoginid(), session.getLoginid(),
//                                            new Date(),
//                                            "Verify Password", aStatus.error, aStatus.errorcode,
//                                            "User Management",
//                                            "",
//                                            "",
//                                            "PASSWORD", userid);
//                                    return aStatus;
//
//                                } else if (aUser == null) {
//                                    aStatus.error = "ERROR";
//                                    aStatus.errorcode = -1;
//
//                                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
//                                            channel.getName(),
//                                            session.getLoginid(), session.getLoginid(),
//                                            new Date(),
//                                            "Verify Password", aStatus.error, aStatus.errorcode,
//                                            "User Management",
//                                            "",
//                                            "",
//                                            "PASSWORD", userid);
//                                    return aStatus;
//
//                                }
                            }

                        } else if (retValue == -9) {
                            aStatus.error = "One Time Password is expired...";
                            aStatus.errorcode = -9;
                        } //added for new error codes 2nd april 2014
                        else if (retValue == -8) {
                            aStatus.error = "One Time Password is already consumed!!!";
                            aStatus.errorcode = -16;

                        } else if (retValue == -17) {
                            aStatus.error = "User/Token is not found!!!";
                            aStatus.errorcode = -17;

                        } else if (retValue == -22) {
                            aStatus.error = "Token is locked!!!";
                            aStatus.errorcode = -22;
                        }

                        //end of addition
                        //System.out.println("VerifyOTP result::" + retValue + " for OTP::" + otp);
                    }

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                            channel.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(),
                            "VERIFY OTP", aStatus.error, aStatus.errorcode,
                            "Token Management",
                            "OTP=" + otp,
                            "",
                            "OTPTOKENS", userid);

//            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                    session.getLoginid(), session.getLoginid(), new Date(), "Verify OTP ", aStatus.error, aStatus.errorcode,
//                    "Token Management", "OTP = ******", " OTP = ******",
//                    "Verify OTP TOKEN", userid);
                    return aStatus;

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }

        return null;

    }

//    //added by vikram for making radius inside core and remove from ourside
//    private static String g_channelID = null;
//
//    private String GetChannelID() {
//
//        if (g_channelID == null) {
//
//            SessionFactoryUtil suChannel = null;// new SessionFactoryUtil(SessionFactoryUtil.channels);
//            Session sChannel = null; //suChannel.openSession();
//
//            try {
//                suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
//                sChannel = suChannel.openSession();
//
//                MessageContext mc = wsContext.getMessageContext();
//                ServletContext sc = (ServletContext) mc.get(MessageContext.SERVLET_CONTEXT);
//                String _channelName = sc.getContextPath();
//                _channelName = _channelName.replaceAll("/", "");
//
//                if (_channelName.compareTo("core") == 0) {
//                    _channelName = "face";
//                }
//
//                ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
//                Channels channel = cUtil.getChannel(_channelName);
//                g_channelID = channel.getChannelid();
//                sChannel.close();
//                suChannel.close();
//            } catch (Exception e) {
//                sChannel.close();
//                suChannel.close();
//                e.printStackTrace();
//            }
//        }
//
//        return g_channelID;
//    }
//    //end of addition
//
//    public AxiomRadiusStatus VerifyRadiusCredentialDirect(String username, String password, String otp, String type) {
//
//        String channelid = GetChannelID();
//        AxiomRadiusStatus ars = new AxiomRadiusStatus();
//        try {
//
//            SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
//            Session sRemoteAcess = suRemoteAcess.openSession();
//            RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
//            String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channelid);
//            if (credentialInfo == null) {
//                ars.errorcode = -100;
//                ars.error = "Remote Access is not configured properly!!";
//                sRemoteAcess.close();
//                suRemoteAcess.close();
//                return ars;
//            }
//            sRemoteAcess.close();
//            suRemoteAcess.close();
//
////            int iResult = CheckCredentials(channelid, loginid, loginpassword);
////            if (iResult != 0) {
////                AxiomRadiusStatus ars = new AxiomRadiusStatus();
////                ars.errorcode = -100;
////                ars.error = "Client Authentication failed!!!";
////                return ars;
////            }
////
////            AxiomRadiusStatus ars = new AxiomRadiusStatus();
////            String sessionid = OpenSession(channelid, loginid, loginpassword);
////            if (sessionid == null) {
////                ars.errorcode = -101;
////                ars.error = "Session could not be created!!!";
////                return ars;
////            }
//            ars = VerifyRadiusCredentialInner(sessionid, username, password, otp, type);
//
//            //CloseSession(sessionid);
//            return ars;
//        } catch (Exception e) {
//            return null;
//        }
//    }
//    
    

}
