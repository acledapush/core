package com.mollatech.axiom.radius;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

//Service Endpoint Interface
//@WebService
@WebService(name = "axiomradius")
public interface AxiomRadiusInterface {
    
    @WebMethod
    public AxiomRadiusConfiguration GetConfigrationSettings(@WebParam(name = "channelid") String channelid, @WebParam(name = "loginid") String loginid, @WebParam(name = "password") String password);
    
    @WebMethod
    public AxiomRadiusStatus VerifyRadiusCredential(@WebParam(name = "channelid") String channelid, @WebParam(name = "loginid") String loginid, @WebParam(name = "loginpassword") String loginpassword, @WebParam(name = "username") String username, @WebParam(name = "password") String password,@WebParam(name = "otp") String otp,@WebParam(name = "type") String type);
    
    @WebMethod
    public int Ping(@WebParam(name = "channelid") String channelid);
}
