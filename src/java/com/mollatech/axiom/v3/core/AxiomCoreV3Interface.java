/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v3.core;

import com.mollatech.axiom.common.utils.InitTransactionPackage;
import com.mollatech.axiom.v2.core.utils.AxiomException;
import com.mollatech.axiom.v2.core.utils.AxiomTokenDetails;
import com.mollatech.axiom.v2.core.utils.AxiomStatus;
import com.mollatech.axiom.v2.core.utils.AxiomUser;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author pramodchaudhari
 */
@WebService(name = "axiomv3")
public interface AxiomCoreV3Interface {

    @WebMethod
    public String OpenSession(@WebParam(name = "channelid") String channelid, @WebParam(name = "loginid") String loginid, @WebParam(name = "loginpassword") String loginpassword) throws AxiomException;

    @WebMethod
    public AxiomStatus CloseSession(@WebParam(name = "sessionid") String sessionid);

    @WebMethod
    public AxiomStatus CreateUser(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "aUser") AxiomUser aUser, @WebParam(name = "tokenSrno") String tokenSrno, @WebParam(name = "defaultTokentype") Integer default_token_type);

    @WebMethod
    public AxiomUser GetUserBy(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "type") int type, @WebParam(name = "searchFor") String searchFor) throws AxiomException;

    @WebMethod
    public AxiomStatus ResetUser(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type);

    @WebMethod
    public AxiomStatus VerifyOTP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "otp") String otp);

    @WebMethod
    public AxiomStatus ChangeTokenStatus(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "tokenSerialno") String tokenSerialno, @WebParam(name = "type") int type, @WebParam(name = "status") int status);

    @WebMethod
    public AxiomStatus DeleteUser(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid);

    @WebMethod
    public AxiomStatus ChangeUserDetails(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "axiomuser") AxiomUser axiomUser, @WebParam(name = "defaultTokentype") int default_token_type);

    @WebMethod
    public AxiomStatus ResendActivationCode(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "Category") int Category, @WebParam(name = "SubCategory") int SubCategory, @WebParam(name = "type") int type);

    @WebMethod
    public AxiomStatus ActivatePushAuthentication(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "devicePayload") String devicepayload, @WebParam(name = "regCode") String regCode);

    @WebMethod
    public AxiomStatus InitTransaction(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "InitTransactionPackage") InitTransactionPackage packagepaload, @WebParam(name = "integrityCheckString") String integrityCheckString) throws AxiomException;

    @WebMethod
    public AxiomStatus UpdateTransactionStatus(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "transactionId") String transactionId, @WebParam(name = "integrityCheckString") String integrityCheckString, @WebParam(name = "devicepayload") String devicepayload) throws AxiomException;

    @WebMethod
    public AxiomStatus GetTransactionStatus(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "txId") String txId, @WebParam(name = "integrityCheckString") String integrityCheckString) throws AxiomException;

    @WebMethod
    public AxiomTokenDetails[] GetUserTokens(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid) throws AxiomException;

    @WebMethod
    public AxiomStatus ValidateRegistrationCode(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "regCode") String regcode, @WebParam(name = "deviceid") String deviceid, @WebParam(name = "tokenType") Integer tokenType) throws AxiomException;

    @WebMethod
    public AxiomStatus ReplaceToken(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "oldTokenSerialno") String oldTokenSerialno, @WebParam(name = "newTokenSerialno") String newTokenSerialno) throws AxiomException;
    
    @WebMethod
    public AxiomStatus ResyncToken(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "category") Integer category, @WebParam(name = "otp1") String otp1,  @WebParam(name = "otp2") String otp2) throws AxiomException;

}
