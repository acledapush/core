/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v3.core;

import com.mollatech.axiom.common.utils.InitTransactionPackage;
import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.connector.user.TokenStatusDetails;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.crypto.HASH;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Errormessages;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Otptokens;
import com.mollatech.axiom.nucleus.db.Pkitokens;
import com.mollatech.axiom.nucleus.db.Registerdevicepush;
import com.mollatech.axiom.nucleus.db.Registrationcodetrail;
import com.mollatech.axiom.nucleus.db.Securephrase;
import com.mollatech.axiom.nucleus.db.Sessions;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.Trusteddevice;
import com.mollatech.axiom.nucleus.db.Txdetails;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import com.mollatech.axiom.nucleus.db.connector.management.CryptoManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ErrorMessageManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PushNotificationDeviceManagement;
import com.mollatech.axiom.nucleus.db.connector.management.RegistrationCodeTrailManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SecurePhraseManagment;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TrustDeviceManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TxManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import com.mollatech.axiom.nucleus.settings.MobileTrustSettings;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import com.mollatech.axiom.nucleus.settings.TokenSettings;
import com.mollatech.axiom.v2.core.utils.AxiomException;
import com.mollatech.axiom.v2.core.utils.AxiomStatus;
import com.mollatech.axiom.v2.core.utils.AxiomTokenDetails;
import com.mollatech.axiom.v2.core.utils.AxiomUser;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author pramodchaudhari
 */
@WebService(endpointInterface = "com.mollatech.axiom.v3.core.AxiomCoreV3Interface")
public class AxiomCoreV3InterfaceImpl implements AxiomCoreV3Interface {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AxiomCoreV3InterfaceImpl.class.getName());
    @Resource
    WebServiceContext wsContext;
    private static HashMap g_ValidSessions;
    private final static int ANDROID = 1;
    private final static int IOS = 2;
    private final static int Webtrust = 3;

    private static String g_channelID = null;
    public static final int OTP_TOKEN = 1;
    public static final int PKI_TOKEN = 2;
    public static final int IMG_AUTH = 3;
    private int GETUSER_BY_USERID = 1;
    private int GETUSER_BY_PHONENUMBER = 2;
    private int GETUSER_BY_EMAILID = 3;
    private int GETUSER_BY_FULLNAME = 4;
    final String itemtype = "OTPTOKENS";
    private int RESET_USER_PASSWORD = 1;
    private int RESET_USER_TOKEN_OOB = 2;
    private int RESET_USER_TOKEN_SOFTWARE = 3;
    private int RESET_USER_TOKEN_HARDWARE = 4;
    private int RESET_USER_CERT = 5;
    private int RESET_USER_PKI_TOKEN_SOFTWARE = 6;
    private int RESET_USER_PKI_TOKEN_HARDWARE = 7;
    private int OTP_TOKEN_OUTOFBAND = 3;
    public int OTP_TOKEN_SOFTWARE = 1;
    private int OTP_TOKEN_HARDWARE = 2;
    private int OTP_TOKEN_OUTOFBAND_SMS = 1;
    private int OTP_TOKEN_OUTOFBAND_VOICE = 2;
    private int OTP_TOKEN_OUTOFBAND_USSD = 3;
    private int OTP_TOKEN_OUTOFBAND_EMAIL = 4;
    private int OTP_TOKEN_OUTOFBAND_PUSH = 5;
    private int OTP_TOKEN_SOFTWARE_MOBILE = 2;
    private int OTP_TOKEN_SOFTWARE_PC = 3;
    private int OTP_TOKEN_SOFTWARE_WEB = 1;
    private int OTP_TOKEN_HARDWARE_MINI = 1;
    private int OTP_TOKEN_HARDWARE_CR = 2;
    private int TOKEN_STATUS_UNASSIGNED = -10;
    private int TOKEN_STATUS_ACTIVE = 1;
    private int TOKEN_STATUS_SUSPENDED = -2;
    private String strACTIVE = "ACTIVE";
    private String strSUSPENDED = "SUSPENDED";
    private String strUNASSIGNED = "UNASSIGNED";
    private int SEND_MESSAGE_PENDING_STATE = 2;
    private String SEND_MESSAGE_PENDING_STATE_STRING = "PENDING";
    private int SEND_MESSAGE_BLOCKED_STATE = -5;
    private String SEND_MESSAGE_BLOCKED_STATE_STRING = "BLOCKED";
    private int SEND_MESSAGE_INVALID_DATA = -4;
    private String SEND_MESSAGE_INVALID_DATA_STRING = "INVALIDDATA";
    private int INVALID_IP = -3;
    private int INVALID_REMOTEUSER = -4;
    private int INVALID_LICENSE = -5;
    private int EPINSMS = 1;
    private int EPINIVR = 2;    

    private String GetChannelID() {

        if (g_channelID == null) {

            SessionFactoryUtil suChannel = null;// new SessionFactoryUtil(SessionFactoryUtil.channels);
            Session sChannel = null; //suChannel.openSession();

            try {
                suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
                sChannel = suChannel.openSession();
                MessageContext mc = wsContext.getMessageContext();
                ServletContext sc = (ServletContext) mc.get(MessageContext.SERVLET_CONTEXT);
                String _channelName = sc.getContextPath();
                _channelName = _channelName.replaceAll("/", "");
                if (_channelName.compareTo("core") == 0) {
                    _channelName = "face";
                }
                ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
                Channels channel = cUtil.getChannel(_channelName);
                g_channelID = channel.getChannelid();
                sChannel.close();
                suChannel.close();
            } catch (Exception e) {
                sChannel.close();
                suChannel.close();
                e.printStackTrace();
            }
        }

        return g_channelID;
    }

    @Override
    public String OpenSession(String channelid, String loginid, String loginpassword) throws AxiomException {

        try {
            log.info("OpenSession Entered");

            String strDebug = null;
            SettingsManagement setManagement = new SettingsManagement();
            try {

                if (channelid == null || channelid.isEmpty() == true
                        || loginid == null || loginid.isEmpty() == true
                        || loginpassword == null || loginpassword.isEmpty() == true) {
                    log.error("OpenSession got null parameter!!!");
                    return null;
                }
                SessionFactoryUtil suChannel = null;// new SessionFactoryUtil(SessionFactoryUtil.channels);
                Session sChannel = null; //suChannel.openSession();
                suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
                sChannel = suChannel.openSession();
                ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
                MessageContext mc = wsContext.getMessageContext();
                ServletContext sc = (ServletContext) mc.get(MessageContext.SERVLET_CONTEXT);
                String _channelName = sc.getContextPath();
                _channelName = _channelName.replaceAll("/", "");
                String modifiedChannelName = "";
                if (_channelName.equals("core")) {
                    modifiedChannelName = "face";
                }
                Channels _channeltoCompare = cUtil.getChannelbyID(channelid);
                if (_channeltoCompare.getName().equals(modifiedChannelName)) {
                    g_channelID = _channeltoCompare.getChannelid();
                    System.out.println("ChannelId" + g_channelID);
                } else {
                    g_channelID = null;
                }
                ChannelProfile channelprofileObj = null;
                Object channelpobj = setManagement.getSettingInner(channelid, SettingsManagement.CHANNELPROFILE_SETTING, 1);

                if (channelpobj == null) {
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                } else {
                    channelprofileObj = (ChannelProfile) channelpobj;
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                }
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    log.debug("OpenSession::loginpassword having length::" + loginpassword.length());
                    log.debug("OpenSession::channelid::" + channelid);
                    log.debug("OpenSession::loginid::" + loginid);
                }
            } catch (Exception ex) {
            }

            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                log.debug("OpenSession::License Error::" + iResult);
                log.info("OpenSession exiting with errr");
                //throws new AxiomProtect("ValidateLicense");
                return null;
            }
            AxiomStatus aStatus = new AxiomStatus();
            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//            aStatus.error = "ERROR";
            aStatus.errorcode = -1;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(channelid);
            if (channel == null) {
                aStatus.errorcode = -102;
                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return null;
            }

            MessageContext mc = wsContext.getMessageContext();

            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            //log.debug("Client IP = " + req.getRemoteAddr());
            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        SessionFactoryUtil suOperators = new SessionFactoryUtil(SessionFactoryUtil.operators);
                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            return null;
                        }

                        suTemplate.close();
                        sTemplate.close();
                        return null;
                    }
                    return null;
                }
            }

            String resultStr = "Failure";
            int retValue = -1;
            SessionManagement sManagement = new SessionManagement();
            String sessionId = sManagement.OpenSessionForWS(channelid, loginid, loginpassword, req.getSession().getId());

            AuditManagement audit = new AuditManagement();
            if (sessionId != null) {
                retValue = 0;
                resultStr = "Success";
                audit.AddAuditTrail(sessionId, channelid, loginid,
                        req.getRemoteAddr(),
                        channel.getName(), loginid,
                        loginid, new Date(), "Open Session", resultStr, retValue,
                        "Login", "", "Open Session successfully with Session Id =" + sessionId, "SESSION",
                        loginid);

            } else if (sessionId == null) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), loginid, req.getRemoteAddr(), channel.getName(), loginid,
                        loginid, new Date(), "Open Session", resultStr, retValue,
                        "Login", "", "Failed To Open Session", "SESSION",
                        loginid);

            }
            if (sessionId == null) {
                throw new AxiomException("Remote Access Disabled or LoginId or Password is wrong..!!!");

            }
            return sessionId;
        } catch (Exception e) {
            e.printStackTrace();
            throw new AxiomException("Remote Access Disabled or LoginId or Password is wrong..!!!");
        }

    }

    public AxiomStatus CloseSession(String sessionid) {
        try {

            String strDebug = null;
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    log.debug("CloseSession::sessionid::" + sessionid);
                }
            } catch (Exception ex) {
            }

            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
                AxiomStatus aStatus = new AxiomStatus();
//                aStatus.error = "Licence is invalid";

                aStatus.errorcode = -100;
                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }

            SessionManagement sManagement = new SessionManagement();
            Sessions session = sManagement.getSessionById(sessionid);
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            AxiomStatus aStatus = new AxiomStatus();
            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//            aStatus.error = "ERROR";
            aStatus.errorcode = -1;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();

            if (session != null) {

                //log.debug("Client IP = " + req.getRemoteAddr());        
                SettingsManagement setManagement = new SettingsManagement();
//                int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
//                if (result != 1) {
//                    aStatus.error = "INVALID IP REQUEST";
//                    aStatus.errorcode = -8;
//                    //  return aStatus;
//                }
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    aStatus.errorcode = -102;
                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                    aStatus.error = errmsg.getUsermessage();
                    return null;
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
//                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                                aStatus.error = errmsg.getUsermessage();
                                return aStatus;
                            }

                            suTemplate.close();
                            sTemplate.close();
//                            aStatus.error = "INVALID IP REQUEST";

                            aStatus.errorcode = -8;
                            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                            aStatus.error = errmsg.getUsermessage();

                            return aStatus;
                        }
//                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                        aStatus.error = errmsg.getUsermessage();
                        return aStatus;
                    }
                }

                int retValue = sManagement.CloseSession(sessionid);

//                aStatus.error = "ERROR";
                aStatus.errorcode = retValue;
                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                if (retValue == 0) {
//                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                    aStatus.error = errmsg.getUsermessage();

                }

            } else if (session == null) {
                aStatus.errorcode = -113;
                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                aStatus.errorcode = -102;
                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }
            AuditManagement audit = new AuditManagement();
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                    session.getLoginid(), new Date(), "Close Session", errmsg.getErrorMessage(), aStatus.errorcode,
                    "Login", "", "Close Session successfully with Session Id =" + sessionid, "SESSION",
                    session.getLoginid());
            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public AxiomUser GetUserBy(String sessionId, int type, String searchFor) throws AxiomException {

        log.debug("Started");
        String strDebug = null;
        AuthUser authUser = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("GetUserBy::sessionId::" + sessionId));
                log.debug((Object) ("GetUserBy::type::" + type));
                log.debug((Object) ("GetUserBy::searchFor::" + searchFor));
            }
        } catch (Exception var5_5) {
            // empty catch block
        }
        try {
            UserManagement uManagement = new UserManagement();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            String resultStr = "Failure";
            int retValue = -1;
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionId);
            AxiomUser aUser = null;
            if (sessionId == null) {
                throw new Exception("Session ID is empty!!!");
            }
            if (session == null) {
                throw new Exception("Session ID is invalid/expired!!!");
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                if (channel == null) {
                    return null;
                }
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                throw new Exception("IP is not allowed!!!");
                            }
                            suTemplate.close();
                            sTemplate.close();
                            throw new Exception("IP is not allowed!!!");
                        }
                        throw new Exception("IP is not allowed!!!");
                    }
                }
                String userFlag = LoadSettings.g_sSettings.getProperty("check.user");

                log.debug("GetUserBy :: CheckUserByType() called");
                authUser = userFlag.equals("1") ? uManagement.CheckUserByType(sessionId, session.getChannelid(), searchFor, 4) : uManagement.CheckUserByType(sessionId, session.getChannelid(), searchFor, type);

                log.debug(" GetUserBy :: CheckUserByType() ended");
                if (authUser != null) {
                    aUser = new AxiomUser();
                    aUser.email = authUser.email;
                    aUser.iAttempts = authUser.iAttempts;
                    aUser.lCreatedOn = authUser.lCreatedOn;
                    aUser.lLastAccessOn = authUser.lLastAccessOn;
                    aUser.phoneNo = authUser.phoneNo;
                    aUser.statePassword = authUser.statePassword;
                    aUser.userId = authUser.userId;
                    aUser.userName = authUser.userName;

                }
            }                        
            if (aUser != null) {
                retValue = 0;
                resultStr = "Success";                               
                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "GET USER", resultStr, retValue, "User Management", "Name=" + aUser.userName + ",Phone=" + aUser.phoneNo + ",Email=" + aUser.email, "", "USER", aUser.userId);
            } else if (aUser == null) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "GET USER", resultStr, retValue, "User Management", "", "", "USER", "");
            }
            log.debug("ended");
            return aUser;
        } catch (Exception e) {
            e.printStackTrace();
            throw new AxiomException(e.getMessage());
        }

    }

    @Override
    public AxiomStatus ResetUser(String sessionId, String userid, int type) {

        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("ResetUser::sessionId::" + sessionId));
                log.debug((Object) ("ResetUser::userid::" + userid));
                log.debug((Object) ("ResetUser::type::" + type));
            }
        } catch (Exception ex) {

        }
        try {
            SessionManagement sManagement = new SessionManagement();
            Sessions session = sManagement.getSessionById(sessionId);
            AuditManagement audit = new AuditManagement();
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "ERROR";
            aStatus.errorcode = -9;
            int retValue = -1;
            String strCategory = "";
            if (type == OTPTokenManagement.SOFTWARE_TOKEN) {
                strCategory = "SOFTWARE_TOKEN";
            } else if (type == OTPTokenManagement.HARDWARE_TOKEN) {
                strCategory = "HARDWARE_TOKEN";
            } else if (type == OTPTokenManagement.OOB_TOKEN) {
                strCategory = "OOB_TOKEN";
            }
            String strSubCategory = "";
            if (session == null) {
                return aStatus;
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (session != null) {
                OTPTokenManagement oManagement;
                MessageContext mc = this.wsContext.getMessageContext();
                HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                if (channel == null) {
                    return null;
                }
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement2 = new OperatorsManagement();
                            Operators[] aOperator = oManagement2.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }
                if (type == this.RESET_USER_PASSWORD) {
                    UserManagement uManagement = new UserManagement();
                    retValue = uManagement.resetPassword(sessionId, session.getChannelid(), userid);
                    if (retValue == 0) {
                        aStatus.error = "SUCCESS";
                        aStatus.errorcode = 0;
                    } else {
                        aStatus.errorcode = -1;
                    }
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "RESET", aStatus.error, aStatus.errorcode, "User Management", "Old Password = ******", "New Password = ******", "PASSWORD", userid);
                    return aStatus;
                }
                if (type == this.RESET_USER_TOKEN_OOB) {
                    oManagement = new OTPTokenManagement(session.getChannelid());
                    TokenStatusDetails[] tokens = oManagement.getTokenList(sessionId, session.getChannelid(), userid);
                    if (tokens == null) {
                        aStatus.errorcode = -2;
                        aStatus.error = "Desired OOB Token is not assigned";
                        return aStatus;
                    }
                    TokenStatusDetails tokenSelected = null;
                    for (int i = 0; i < tokens.length; ++i) {
                        if (tokens[i].Catrgory != 3) {
                            continue;
                        }
                        tokenSelected = tokens[i];
                        break;
                    }
                    if (tokenSelected == null) {
                        aStatus.errorcode = -2;
                        aStatus.error = "Desired OOB Token is not assigned";
                        return aStatus;
                    }
                    if (tokenSelected.SubCategory == this.OTP_TOKEN_OUTOFBAND_SMS) {
                        strSubCategory = "OOB__SMS_TOKEN";
                    } else if (tokenSelected.SubCategory == this.OTP_TOKEN_OUTOFBAND_USSD) {
                        strSubCategory = "OOB__USSD_TOKEN";
                    } else if (tokenSelected.SubCategory == this.OTP_TOKEN_OUTOFBAND_VOICE) {
                        strSubCategory = "OOB__VOICE_TOKEN";
                    } else if (tokenSelected.SubCategory == this.OTP_TOKEN_OUTOFBAND_EMAIL) {
                        strSubCategory = "OOB__EMAIL_TOKEN";
                    } else if (tokenSelected.SubCategory == this.OTP_TOKEN_OUTOFBAND_PUSH) {
                        strSubCategory = "OOB__PUSH_TOKEN";
                    }
                    retValue = oManagement.ChangeStatus(sessionId,session.getChannelid(),userid,  this.TOKEN_STATUS_UNASSIGNED, 3, tokenSelected.SubCategory);
                    if (retValue == 0) {
                        aStatus.error = "SUCCESS";
                        aStatus.errorcode = 0;
                    } else {
                        aStatus.errorcode = -2;
                    }
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "RESET", aStatus.error, aStatus.errorcode, "Token Management", "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status, "New Status =" + this.strUNASSIGNED, "OOBTOKEN", userid);
                } else if (type == this.RESET_USER_TOKEN_SOFTWARE) {
                    oManagement = new OTPTokenManagement(session.getChannelid());
                    TokenStatusDetails[] tokens = oManagement.getTokenList(sessionId, session.getChannelid(), userid);
                    TokenStatusDetails tokenSelected = null;
                    for (int i = 0; i < tokens.length; ++i) {
                        if (tokens[i].Catrgory != 1) {
                            continue;
                        }
                        tokenSelected = tokens[i];
                        break;
                    }
                    if (tokenSelected == null) {
                        aStatus.errorcode = -2;
                        aStatus.error = "Desired Software Token is not assigned";
                        return aStatus;
                    }
                    if (tokenSelected.SubCategory == this.OTP_TOKEN_SOFTWARE_WEB) {
                        strSubCategory = "SW_WEB_TOKEN";
                    } else if (tokenSelected.SubCategory == this.OTP_TOKEN_SOFTWARE_MOBILE) {
                        strSubCategory = "SW_MOBILE_TOKEN";
                    } else if (tokenSelected.SubCategory == this.OTP_TOKEN_SOFTWARE_PC) {
                        strSubCategory = "SW_PC_TOKEN";
                    }
                    retValue = oManagement.ChangeStatus(sessionId, userid, session.getChannelid(), this.TOKEN_STATUS_UNASSIGNED, 1, tokenSelected.SubCategory);
                    if (retValue == 0) {
                        aStatus.error = "SUCCESS";
                        aStatus.errorcode = 0;
                    } else {
                        aStatus.errorcode = -2;
                    }
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "RESET", aStatus.error, aStatus.errorcode, "Token Management", "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status, "New Status =" + this.strUNASSIGNED, "SOFTWARETOKEN", userid);
                } else if (type == this.RESET_USER_TOKEN_HARDWARE) {
                    oManagement = new OTPTokenManagement(session.getChannelid());
                    TokenStatusDetails[] tokens = oManagement.getTokenList(sessionId, session.getChannelid(), userid);
                    if (tokens == null) {
                        aStatus.errorcode = -2;
                        aStatus.error = "Desired Hardware Token is not assigned";
                        return aStatus;
                    }
                    TokenStatusDetails tokenSelected = null;
                    for (int i = 0; i < tokens.length; ++i) {
                        if (tokens[i].Catrgory != 2) {
                            continue;
                        }
                        tokenSelected = tokens[i];
                        break;
                    }
                    if (tokenSelected == null) {
                        aStatus.errorcode = -2;
                        aStatus.error = "Desired Hardware Token is not assigned";
                        return aStatus;
                    }
                    if (tokenSelected.SubCategory == this.OTP_TOKEN_HARDWARE_CR) {
                        strSubCategory = "HW_CR_TOKEN";
                    } else if (tokenSelected.SubCategory == this.OTP_TOKEN_HARDWARE_MINI) {
                        strSubCategory = "HW_MINI_TOKEN";
                    }
                    retValue = oManagement.ChangeStatus(sessionId, userid, session.getChannelid(), this.TOKEN_STATUS_UNASSIGNED, 2, tokenSelected.SubCategory);
                    if (retValue == 0) {
                        aStatus.error = "SUCCESS";
                        aStatus.errorcode = 0;
                    } else {
                        aStatus.errorcode = -2;
                    }
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "RESET", aStatus.error, aStatus.errorcode, "Token Management", "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status, "New Status =" + this.strUNASSIGNED, "HARDWARETOKEN", userid);
                }
            }
            return aStatus;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public AxiomStatus VerifyOTP(String sessionId, String userid, String otp) {

        log.debug("started");
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("VerifyOTP::sessionId::" + sessionId));
                log.debug((Object) ("VerifyOTP::userid::" + userid));
                log.debug((Object) ("VerifyOTP::otp::" + otp));
            }
        } catch (Exception var5_5) {
            // empty catch block
        }
        try {
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                return aStatus;
            }
            if (sessionId == null || userid == null || sessionId.isEmpty() || userid.isEmpty() || otp == null || otp.isEmpty()) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Data";
                aStatus.errorcode = -11;
                return aStatus;
            }
            AxiomStatus aStatus = new AxiomStatus();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            ChannelManagement cManagement = new ChannelManagement();
            log.debug("getChannelByID Started");
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            log.debug("getChannelByID Ended");
            if (channel == null) {
                aStatus.error = "Invalid Channel";
                aStatus.errorcode = -15;
                return aStatus;
            }
            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
            if (session != null) {
//                SettingsManagement setManagement = new SettingsManagement();
//                String channelid = session.getChannelid();
//                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
//                if (ipobj != null) {
//                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                    int checkIp = 1;
//                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
//                    if (iObj.ipstatus == 0 && checkIp != 1) {
//                        if (iObj.ipalertstatus == 0) {
//                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
//                            Session sTemplate = suTemplate.openSession();
//                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
//                            OperatorsManagement oManagementObj = new OperatorsManagement();
//                            Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
//                            if (aOperator != null) {
//                                String[] emailList = new String[aOperator.length - 1];
//                                for (int i = 1; i < aOperator.length; ++i) {
//                                    emailList[i - 1] = aOperator[i].getEmailid();
//                                }
//                                if (templatesObj.getStatus() == 1) {
//                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
//                                    String strsubject = templatesObj.getSubject();
//                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                    if (strmessageBody != null) {
//                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                    }
//                                    SendNotification send = new SendNotification();
//                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                }
//                                suTemplate.close();
//                                sTemplate.close();
//                                aStatus.error = "INVALID IP REQUEST";
//                                aStatus.errorcode = -8;
//                                return aStatus;
//                            }
//                            suTemplate.close();
//                            sTemplate.close();
//                            aStatus.error = "INVALID IP REQUEST";
//                            aStatus.errorcode = -8;
//                            return aStatus;
//                        }
//                        aStatus.error = "INVALID IP REQUEST";
//                        aStatus.errorcode = -8;
//                        return aStatus;
//                    }
//                }
                log.debug("VerifyOTP :: VerifyOTP() called");
                if ((retValue = oManagement.VerifyOTP(session.getChannelid(), userid, sessionId, otp)) == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                    log.debug((Object) ("otp:" + otp + " :Verified Sucessfully"));
                } else if (retValue == -9) {
                    aStatus.error = "One Time Password is expired...";
                    aStatus.errorcode = -9;
                } else if (retValue == -8) {
                    aStatus.error = "One Time Password is already consumed!!!";
                    aStatus.errorcode = -16;
                } else if (retValue == -17) {
                    aStatus.error = "User/Token is not found!!!";
                    aStatus.errorcode = -17;
                } else if (retValue == -22) {
                    aStatus.error = "Token is locked!!!";
                    aStatus.errorcode = -22;
                }
                log.debug("VerifyOTP :: VerifyOTP() ended");
            }
            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "VERIFY OTP", aStatus.error, aStatus.errorcode, "Token Management", "OTP=" + otp, "", "OTPTOKENS", userid);
            log.debug("ended");
            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public AxiomStatus ChangeTokenStatus(String sessionid, String userid, String tokenSerialno, int type, int status) {
        String strDebug = null;
        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
        Errormessages errorMsg = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");

            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.info("Started");
                log.info("sessionid::" + sessionid);
                log.info("userid::" + userid);
                log.info("type::" + type);
                log.info("status::" + status);

            }
        } catch (Exception ex) {
            log.debug(ex.getMessage());
        }
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            // aStatus.error = "Licence is invalid";
            aStatus.errorcode = -100;
            errorMsg = errmsgObj.LoadAuditErrorUsingErrorCode(g_channelID, aStatus.errorcode);
            aStatus.error = errorMsg.getErrorMessage();
            return aStatus;
        }

        try {
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionid);
            if (session == null) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                return aStatus;
            }
            if (sessionid == null || userid == null || sessionid.isEmpty() || userid.isEmpty()) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Data";
                aStatus.errorcode = -11;
                return aStatus;
            }
            AxiomStatus aStatus = new AxiomStatus();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            ChannelManagement cManagement = new ChannelManagement();
            log.debug("getChannelByID Started");
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            log.debug("getChannelByID Ended");
            if (channel == null) {
                aStatus.error = "Invalid Channel";
                aStatus.errorcode = -15;
                return aStatus;
            }
            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagementObj = new OperatorsManagement();
                            Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }

                OTPTokenManagement otpMgmt = new OTPTokenManagement(channelid);
                Otptokens otpObj = otpMgmt.getOtpObjByUserId(channelid, userid, type);
                if (status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
                    retValue = otpMgmt.AssignToken(sessionid, channelid, userid, type, OTPTokenManagement.OOB_PUSH_TOKEN, "");
                } else {
                    retValue = otpMgmt.ChangeStatus(channelid, userid, status, type, type);
                }

                if (retValue == -3) {
                    aStatus = new AxiomStatus();
                    aStatus.error = "Token does not exists";
                    aStatus.errorcode = -3;
                    //return aStatus;

                } else if (retValue == -6) {
                    aStatus = new AxiomStatus();
                    aStatus.error = "Token Already have requested status..!!!";
                    aStatus.errorcode = -6;
                    //return aStatus;

                } else if (retValue == -20) {
                    aStatus = new AxiomStatus();
                    aStatus.error = "Token Secret can not be empty..!!!";
                    aStatus.errorcode = -20;
                    //return aStatus;

                } else if (retValue == 0) {

                    aStatus = new AxiomStatus();
                    aStatus.error = "Token Status Changed Successfully..!!!";
                    aStatus.errorcode = 0;
                    //return aStatus;
                }else if (retValue == -4) {
                    aStatus = new AxiomStatus();
                    aStatus.error = "Token is already assign!!!";
                    aStatus.errorcode = -4;
                    //return aStatus;
                }

                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                        channel.getName(),
                        session.getLoginid(), session.getLoginid(),
                        new Date(),
                        "CHANGE STATUS", aStatus.error, aStatus.errorcode,
                        "Token Management",
                        "Current Status=" + otpObj.getStatus(),
                        "New Status=" + status,
                        "OTPTOKENS", userid);
                return aStatus;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public AxiomStatus DeleteUser(String sessionId, String userid) {

        try {
            String strDebug = null;
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    log.debug((Object) ("DeleteUser::sessionId::" + sessionId));
                    log.debug((Object) ("DeleteUser::userid::" + userid));
                }
            } catch (Exception var4_5) {
                // empty catch block
            }
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                return aStatus;
            }
            UserManagement uManagement = new UserManagement();
            AxiomStatus aStatus = new AxiomStatus();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            AuthUser oldUser = null;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                ChannelManagement cManagement = new ChannelManagement();
                String channelid = session.getChannelid();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    aStatus.error = "INVALID CHANNEL";
                    aStatus.errorcode = -3;
                    return aStatus;
                }
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }
                if ((retValue = uManagement.DeleteUser(sessionId, session.getChannelid(), userid)) == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                }
                oldUser = uManagement.getUser(sessionId, session.getChannelid(), userid);
            }
            String oldUserName = "";
            String oldPhoneNo = "";
            String oldEmailID = "";
            String oldPasswordState = "";
            if (oldUser != null) {
                oldUserName = oldUser.getUserName();
                oldPhoneNo = oldUser.getPhoneNo();
                oldEmailID = oldUser.getEmail();
                oldPasswordState = "" + oldUser.getStatePassword();
            }
            String itemtype = "USERPASSWORD";
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (retValue == 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "Delete User", aStatus.error, aStatus.errorcode, "User Management", "User Name = " + oldUserName + "User Phone =" + oldPhoneNo + "User Email =" + oldEmailID + "State Of Password =" + oldPasswordState, "User Deleted Successfully", itemtype, "");
                return aStatus;
            }
            String reason = "User Deletion Failed";
            reason = retValue == -10 ? reason + "User is assigned token/certificate, therefore cannot be removed!!!" : reason + "!!!";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "Delete User", aStatus.error, aStatus.errorcode, "User Management", "User Name = " + oldUserName + "User Phone =" + oldPhoneNo + "User Email =" + oldEmailID + "State Of Password =" + oldPasswordState, reason, itemtype, "");
            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public AxiomStatus ChangeUserDetails(String sessionId, AxiomUser au, int default_token_type) {

        log.info("entered ChangeUserDetails()");
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug("ChangeUserDetails::sessionId::" + sessionId);
                log.debug("ChangeUserDetails::userid::" + au.userId);
                log.debug("ChangeUserDetails::fullname::" + au.userName);
                log.debug("ChangeUserDetails::phone::" + au.phoneNo);
                log.debug("ChangeUserDetails::email::" + au.email);
                log.debug("ChangeUserDetails::default_token_type::" + default_token_type);
            }
        } catch (Exception ex) {
            log.error(ex);
        }

        System.out.println("ChangeUserDetails::All::" + au.toString());

        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//            aStatus.error = "Licence is invalid";
            aStatus.errorcode = -100;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            return aStatus;
        }

        if (sessionId == null || sessionId.isEmpty() || au == null) {
            AxiomStatus aStatus = new AxiomStatus();
            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
            aStatus.errorcode = -11;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            return aStatus;

        }

        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionId);
        if (session == null) {
            AxiomStatus aStatus = new AxiomStatus();
            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//            aStatus.error = "Invalid Session";
            aStatus.errorcode = -114;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            return aStatus;
        }

        UserManagement uManagement = new UserManagement();
        AxiomStatus aStatus = new AxiomStatus();
        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
        int retValue = -1;
        aStatus.errorcode = retValue;
        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
        aStatus.error = errmsg.getUsermessage();
        String channelid = session.getChannelid();
        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(channelid);
        if (channel == null) {
            aStatus.errorcode = -102;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            return aStatus;
        }

        AxiomStatus as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
        if (as.errorcode != 0) {
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
            as.error = errmsg.getUsermessage();
            return as;
        }

        AuthUser axObj = uManagement.getUser(channelid, au.userId);
//        System.out.println(axObj.toString());
        //AuthUser mTObj = new AuthUser();

        if (au.userId != null && !au.userId.isEmpty()) {
            axObj.setUserId(au.userId);
        }
        if (au.userName != null && !au.userName.isEmpty()) {
            axObj.setUserName(au.userName);
        }
        if (au.phoneNo != null && !au.phoneNo.isEmpty()) {
            axObj.setPhoneNo(au.phoneNo);
        }
        if (au.email != null && !au.email.isEmpty()) {
            axObj.setEmail(au.email);
        }
        if (au.organisation != null && !au.organisation.isEmpty()) {
            axObj.setOrganisation(au.organisation);
        }
        if (au.userIdentity != null && !au.userIdentity.isEmpty()) {
            axObj.setUserIdentity(au.userIdentity);
        }
        if (au.idType != null && !au.idType.isEmpty()) {
            axObj.setIdType(au.idType);
        }
        if (au.organisationUnit != null && !au.organisationUnit.isEmpty()) {
            axObj.setOrganisationUnit(au.organisationUnit);
        }
        if (au.country != null && !au.country.isEmpty()) {
            axObj.setCountry(au.country);
        }
        if (au.location != null && !au.location.isEmpty()) {
            axObj.setLocation(au.location);
        }
        if (au.street != null && !au.street.isEmpty()) {
            axObj.setStreet(au.street);
        }
        if (au.designation != null && !au.designation.isEmpty()) {
            axObj.setDesignation(au.designation);
        }
        
        retValue = uManagement.EditUserForPush(sessionId, session.getChannelid(),
                axObj.getUserId(),
                axObj.getUserName(),
                axObj.getPhoneNo(),
                axObj.getEmail(),
                0,
                axObj.getOrganisation(), axObj.getUserIdentity(), axObj.getIdType(),
                axObj.getOrganisationUnit(), axObj.getCountry(), axObj.getLocation(), axObj.getStreet(),
                axObj.getDesignation(),default_token_type);
        System.out.println("retValue::" + retValue);
        if (retValue == 0) {
            aStatus.errorcode = 0;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        } else if (retValue == 1) {
            aStatus.errorcode = 8;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        } else if (retValue == -2) {
            aStatus.errorcode = -2;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        } else {
            aStatus.errorcode = retValue;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        }

        String itemtype = "USERPASSWORD";
        String strStatus = "";
        if (au.status == 0) {
            strStatus = "ACTIVE_STATUS";
        }
        if (au.status == 1) {
            strStatus = "SUSPENDED";
        }

        String clientip = "";

        if (aStatus.errorcode == 0) {
            String cip = req.getRemoteAddr();
            if (clientip == null || clientip.isEmpty() == true) {
            } else {
                cip = clientip;
            }
            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
                    cip, channel.getName(),
                    session.getLoginid(), session.getLoginid(), new Date(), "Edit User", "SUCCESS", aStatus.errorcode,
                    "User Management",
                    //"User Name = " + oldUserName + ", User Phone =" + oldPhoneNo + ", User Email =" + oldEmailID + ", State=" + stroldStatus,
                    "",
                    au.toString(),
                    itemtype, au.userId);
        } else {
            String cip = req.getRemoteAddr();
            if (clientip == null || clientip.isEmpty() == true) {
            } else {
                cip = clientip;
            }
            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
                    cip, channel.getName(),
                    session.getLoginid(), session.getLoginid(), new Date(), "Edit User", "ERROR", aStatus.errorcode,
                    "User Management",
                    //"User Name = " + oldUserName + ", User Phone =" + oldPhoneNo + ", User Email =" + oldEmailID + ", State=" + stroldStatus,
                    "",
                    au.toString(),
                    itemtype, au.userId);

        }

        log.info("Exited ChangeUserDetails()");

        return aStatus;

    }

    @Override
    public AxiomStatus ResendActivationCode(String sessionid, String userid, int Category, int SubCategory, int type) {

        String strDebug = null;
        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug("ResendActivationCode::sessionid::" + sessionid);
                log.debug("ResendActivationCode::userid::" + userid);
                log.debug("ResendActivationCode::type::" + type);
                log.debug("ResendActivationCode::Category::" + Category);
                log.debug("ResendActivationCode::SubCategory::" + SubCategory);

            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }

        int iResult = AxiomProtect.ValidateLicense();
        String clientip = "";
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();

//            aStatus.error = "Licence is invalid";
            aStatus.errorcode = -100;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            return aStatus;
        }

        if (sessionid == null || sessionid.isEmpty()) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.errorcode = -114;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            return aStatus;
        }
        if (userid == null || userid.isEmpty()) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.errorcode = -114;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            return aStatus;

        }
        AxiomStatus aStatus = new AxiomStatus();

        SessionManagement sManagement = new SessionManagement();
        UserManagement uManagement = new UserManagement();
        CryptoManagement cyManagement = new CryptoManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);

        int retValue = -1;
//        aStatus.error = "ERROR";
        aStatus.errorcode = -1;
        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
        aStatus.error = errmsg.getUsermessage();
        if (session == null) {
//            aStatus.error = "Invalid Session";
            aStatus.errorcode = -114;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            return aStatus;
        }

        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(session.getChannelid());
        if (channel == null) {
//            aStatus.error = "Invalid Channel";
            aStatus.errorcode = -102;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            return aStatus;
        }
        SettingsManagement setManagement = new SettingsManagement();
        String channelid = session.getChannelid();
        ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
        if (channelProfileObj != null) {
            int retVal = sManagement.getServerStatus(channelProfileObj);
            if (retVal != 0) {
//                aStatus.error = "Channel Profile is not properly configured";
                aStatus.errorcode = -82;
                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }
        }

        AuthUser aUser = uManagement.getUser(sessionid, channel.getChannelid(), userid);
        if (aUser == null) {
//            aStatus.error = "User is invalid";
            aStatus.errorcode = -60;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            return aStatus;
        }

        AxiomStatus as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
        if (as.errorcode != 0) {
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
            as.error = errmsg.getUsermessage();
            return as;
        }

        as = SendRegistrationCode(session, channel, userid, OTP_TOKEN, req, clientip, type);
        return as;

    }

    private AxiomStatus SendRegistrationCode(Sessions session, Channels channel, String userid, int type, HttpServletRequest req, String clientip, Integer notificationType) {
        String strDebug = null;
        
        log.info("#SendRegistrationCode# :userid  :" + userid);
        if (type == OTP_TOKEN) {

            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//                aStatus.error = "OTP Token feature is not available in this license!!!";
                aStatus.errorcode = -104;
                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();

                return aStatus;
            }
        } else if (type == PKI_TOKEN) {

            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_SOFTWARE) != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//                aStatus.error = "PKI Token feature is not available in this license!!!";
                aStatus.errorcode = -105;
                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }
        }

        AuditManagement audit = new AuditManagement();

        AxiomStatus aStatus = new AxiomStatus();
        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
        int retValue = -1;
//        aStatus.error = "ERROR";
        aStatus.errorcode = -1;
        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
        aStatus.error = errmsg.getUsermessage();

        if (session == null) {
//            aStatus.error = "Session Invalid!!!";
            aStatus.errorcode = -114;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();

            return aStatus;
        }

        String strCategory = "OOB_TOKEN";

        if (channel == null) {
//            aStatus.error = "Invalid Channel";
            aStatus.errorcode = -102;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();

            return aStatus;
        }

        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());

        SettingsManagement setManagement = new SettingsManagement();
        //if (tSettings.isbSilentCall() == false) {
        String regCode = null;
        Otptokens oToken = null;
        Pkitokens pToken = null;
        Securephrase sPhrase = null;
        int subtype = 0;
        PKITokenManagement pManagement = new PKITokenManagement();
        SecurePhraseManagment sManagment = new SecurePhraseManagment();
        TokenSettings token = (TokenSettings) setManagement.getSetting(session.getSessionid(), session.getChannelid(), SettingsManagement.Token, SettingsManagement.PREFERENCE_ONE);
        if (token == null) {
//            aStatus.error = "OTP Token Settings is not configured!!!";
            aStatus.errorcode = -85;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();

            return aStatus;
        }
        SimpleDateFormat sdfExpiry = new SimpleDateFormat("hh:mm");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, token.getRegistrationValidity());
        Date expiry = cal.getTime();
        boolean isoobTokenChecked = false;
        if (type == OTP_TOKEN) {

            oToken = oManagement.getOtpObjByUserId(session.getSessionid(), session.getChannelid(), userid, OTPTokenManagement.SOFTWARE_TOKEN);
            
            if(oToken == null){
                oToken = oManagement.getOtpObjByUserId(session.getSessionid(), session.getChannelid(), userid, OTPTokenManagement.OOB_TOKEN);
                isoobTokenChecked = true;
            }
            if (oToken == null) {
//                aStatus.error = "Desired Software Token is not assigned!!!";
                aStatus.errorcode = -32;
                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                if(isoobTokenChecked){
                    aStatus.error = "Desired OOB Token is not assigned!!!";
                }else{
                    aStatus.error = errmsg.getUsermessage();
                }

                String cip = req.getRemoteAddr();
                if (clientip == null || clientip.isEmpty() == true) {
                } else {
                    cip = clientip;
                }

                audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(), cip, channel.getName(),
                        session.getLoginid(), session.getLoginid(), new Date(), "Check OTP Token", "ERROR", aStatus.errorcode,
                        "Token Management", "",
                        errmsg.getErrorMessage(),
                        "OTPTOKENS", userid);
                return aStatus;
            }

            subtype = oToken.getSubcategory();
            regCode = oManagement.generateRegistrationCodeForPush(session.getSessionid(), session.getChannelid(), userid, OTP_TOKEN_OUTOFBAND);

            if (regCode == null) {
//                aStatus.error = "Registration Code cannot be generate";
                aStatus.errorcode = -24;
                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();

                String cip = req.getRemoteAddr();
                if (clientip == null || clientip.isEmpty() == true) {
                } else {
                    cip = clientip;
                }

                audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(), cip, channel.getName(),
                        session.getLoginid(), session.getLoginid(), new Date(), "Generate Registration Code", "ERROR", aStatus.errorcode,
                        "Token Management", "",
                        errmsg.getErrorMessage(),
                        "OTPTOKENS", userid);
                return aStatus;
            }

            //String strSWOTPType = LoadSettings.g_sSettings.getProperty("sw.otp.type");
//            if (strSWOTPType != null && strSWOTPType.compareToIgnoreCase("simple") == 0) { // we are forcing it to be time based for mobile token.
//                regCode = "1" + regCode;
//            }
            //log.debug("REGCODE =" + regCode);

        } else if (type == PKI_TOKEN) {
            pToken = pManagement.getPkiObjByUserid(session.getChannelid(), userid, PKITokenManagement.SOFTWARE_TOKEN);
            if (pToken == null) {
//                aStatus.error = "Desired Software Token is not assigned!!!";
                aStatus.errorcode = -32;

                audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                        session.getLoginid(), session.getLoginid(), new Date(), "Send Registration Code ", errmsg.getErrorMessage(), aStatus.errorcode,
                        "PKI Token Management", "", "Category = " + strCategory + "regcode =" + "******",
                        "PKITOKEN", userid);
                return aStatus;

            }
            subtype = pToken.getCategory();
            regCode = pManagement.generateRegistrationCode(session.getSessionid(), session.getChannelid(), userid, PKITokenManagement.SOFTWARE_TOKEN);
        }

        if (regCode == null) {
            String cip = req.getRemoteAddr();
            if (clientip == null || clientip.isEmpty() == true) {
            } else {
                cip = clientip;
            }
            aStatus.errorcode = -24;
            audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(), clientip, channel.getName(),
                    session.getLoginid(), session.getLoginid(), new Date(), "Send Registration Code", "ERROR", aStatus.errorcode,
                    "Image Authentication Management",
                    "",
                    "Image Auth Reg Code is null",
                    "IMAGEAUTH", userid);
        } else {

            UserManagement userObj = new UserManagement();
            AuthUser user = null;
            user = userObj.getUser(session.getSessionid(), channel.getChannelid(), userid);

            Templates temp = null;
            TemplateManagement tObj = new TemplateManagement();
            if (type == OTP_TOKEN || type == PKI_TOKEN) {
                temp = tObj.LoadbyName(session.getSessionid(), channel.getChannelid(), TemplateNames.MOBILE_SOFTWARE_TOKEN_REGISTER_TEMPLATE);
            } else if (type == IMG_AUTH) {
                temp = tObj.LoadbyName(session.getSessionid(), channel.getChannelid(), TemplateNames.IMAGE_AUTH_REGISTER_TEMPLATE);
            }
            ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date d = new Date();
            String tmessage = (String) UtilityFunctions.deserializeFromObject(bais);
            tmessage = tmessage.replaceAll("#name#", user.getUserName());
            tmessage = tmessage.replaceAll("#channel#", channel.getName());
            tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
            tmessage = tmessage.replaceAll("#regcode#", regCode);
            tmessage = tmessage.replaceAll("#expiry#", sdfExpiry.format(expiry));

            if (subtype == OTPTokenManagement.SW_WEB_TOKEN) {
                tmessage = tmessage.replaceAll("#tokentype#", "WEB");
            }

            if (type == OTP_TOKEN || type == PKI_TOKEN) {
                if (subtype == OTPTokenManagement.SW_MOBILE_TOKEN) {
                    tmessage = tmessage.replaceAll("#tokentype#", "MOBILE");

                }
                if (subtype == OTPTokenManagement.SW_PC_TOKEN) {
                    tmessage = tmessage.replaceAll("#tokentype#", "PC");

                }
                if (subtype == OTPTokenManagement.OOB_PUSH_TOKEN) {
                    tmessage = tmessage.replaceAll("#tokentype#", "PUSH");
                }
            }
            if (type == IMG_AUTH) {
                if (subtype == OTPTokenManagement.SW_MOBILE_TOKEN) {
                    tmessage = tmessage.replaceAll("#tokentype#", "MOBILE");

                }
                if (subtype == OTPTokenManagement.SW_PC_TOKEN) {
                    tmessage = tmessage.replaceAll("#tokentype#", "PC");

                }
            } else {
                tmessage = tmessage.replaceAll("#tokentype#", "");
            }
            SendNotification send = new SendNotification();
            AXIOMStatus smsStatus = new AXIOMStatus();
            AXIOMStatus emailStatus = new AXIOMStatus();
            if (temp.getStatus() == tObj.ACTIVE_STATUS) {
                aStatus.errorcode = SEND_MESSAGE_PENDING_STATE;
//                aStatus.error = "PENDING";
                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                aStatus.regCodeMessage = tmessage;
                aStatus.regcode = regCode;
                if(notificationType == SendNotification.SMS){
                    smsStatus = send.SendOnMobileNoWaiting(channel.getChannelid(), user.phoneNo, tmessage, 1, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                }
                if(notificationType == SendNotification.EMAIL){
                    //for email 
                    emailStatus = send.SendEmail(channel.getChannelid(), user.getEmail(), "REGISTRATION CODE", tmessage, null, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                }
                if(notificationType == 2){
                    smsStatus = send.SendOnMobileNoWaiting(channel.getChannelid(), user.phoneNo, tmessage, 1, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                    emailStatus = send.SendEmail(channel.getChannelid(), user.getEmail(), "REGISTRATION CODE", tmessage, null, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                }
                //end for email
            }

            if (aStatus != null) {
                if (aStatus.errorcode == SEND_MESSAGE_PENDING_STATE) {
                    RegistrationCodeTrailManagement rManagement = new RegistrationCodeTrailManagement();
                    String registrationcode = UtilityFunctions.Bas64SHA1(regCode);
                    rManagement.addRegCodeTrail(session.getChannelid(), userid, registrationcode, RegistrationCodeTrailManagement.SENT, RegistrationCodeTrailManagement.OTPTOKEN);
                }
            }

            retValue = 0;
//            aStatus.error = "SUCCESS";
            aStatus.errorcode = 0;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();

            aStatus.regCodeMessage = tmessage;
            aStatus.regcode = regCode;
            if (type == IMG_AUTH) {
                audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getChannelid(),
                        req.getRemoteAddr(), channel.getName(),
                        session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code",
                        "success", 0, "IMAGE AUTHENTICATION", "", "Registration Code = ******" /*strRegCode*/, "IMAGAUTH", userid);
            } else {
                audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getChannelid(),
                        req.getRemoteAddr(), channel.getName(),
                        session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code",
                        "success", 0, "Token Management", "", "Registration Code = ******" /*strRegCode*/, itemtype, userid);
            }
        }
        if (type == IMG_AUTH) {
            audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getChannelid(),
                    req.getRemoteAddr(), channel.getName(),
                    session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code",
                    errmsg.getErrorMessage(), aStatus.errorcode, "IMAGE AUTHENTICATION", "", "Registration Code = ******" /*strRegCode*/, "IMAGAUTH", userid);
        } else {
            audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                    session.getLoginid(), session.getLoginid(), new Date(), "Send Registration Code", errmsg.getErrorMessage(), aStatus.errorcode,
                    "Token Management", "", "Category = " + strCategory + "regcode =" + "******",
                    itemtype, userid);
        }
        return aStatus;
    }

    @Override
    public AxiomStatus ActivatePushAuthentication(String sessionid, String userid, String devicepayload, String regCode) {
        String strDebug = null;
        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
        AxiomStatus aStatus = new AxiomStatus();
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug("ActivatePushAuthentication::sessionid::" + sessionid);
                log.debug("ActivatePushAuthentication::userid::" + userid);
                log.debug("ActivatePushAuthentication::devicepayload::" + devicepayload);
                log.debug("ActivatePushAuthentication::regCode::" + regCode);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }

        int iResult = AxiomProtect.ValidateLicense();
        String clientip = "";
        try {
            if (iResult != 0) {

                aStatus.errorcode = -100;
                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }

            if (sessionid == null || sessionid.isEmpty()) {

                aStatus.errorcode = -114;
                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }

            if (userid == null || userid.isEmpty() == true || devicepayload == null || devicepayload.isEmpty() == true || regCode == null || regCode.isEmpty() == true || sessionid == null || sessionid.isEmpty()
                    == true) {

                aStatus.errorcode = -114;
                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;

            }

            SessionManagement sManagement = new SessionManagement();
            UserManagement uManagement = new UserManagement();
            CryptoManagement cyManagement = new CryptoManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionid);

            int retValue = -1;
//        aStatus.error = "ERROR";
            aStatus.errorcode = -1;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            if (session == null) {
//            aStatus.error = "Invalid Session";
                aStatus.errorcode = -114;
                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
//            aStatus.error = "Invalid Channel";
                aStatus.errorcode = -102;
                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }
            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
            if (channelProfileObj != null) {
                int retVal = sManagement.getServerStatus(channelProfileObj);
                if (retVal != 0) {
//                aStatus.error = "Channel Profile is not properly configured";
                    aStatus.errorcode = -82;
                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                    aStatus.error = errmsg.getUsermessage();
                    return aStatus;
                }
            }

            AuthUser aUser = uManagement.getUser(channelid, userid);

            String deviceToken = "";
            int _devicetype = 0;
            String deviceId = "";
            String deviceName =null;
            try {
                JSONObject jsonDevicePayload = new JSONObject(devicepayload);
                if(jsonDevicePayload.has("_devicetoken")){
                    deviceToken = jsonDevicePayload.getString("_devicetoken");
                }else{
                    aStatus.error = "Invalid inputs for Push registration";
                    aStatus.errorcode = -67;
                    return aStatus;
                }
                if(jsonDevicePayload.has("_devicetype")){
                    _devicetype = Integer.parseInt(jsonDevicePayload.getString("_devicetype"));
                }else{
                    aStatus.error = "Invalid inputs for Push registration";
                    aStatus.errorcode = -67;
                    return aStatus;
                }
                if(jsonDevicePayload.has("_deviceId")){
                    deviceId = jsonDevicePayload.getString("_deviceId");
                }else{
                    aStatus.error = "Invalid inputs for Push registration";
                    aStatus.errorcode = -67;
                    return aStatus;
                }
                if(jsonDevicePayload.has("_deviceName")){
                    deviceName = jsonDevicePayload.getString("_deviceName");
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            if (deviceToken != null && deviceToken.isEmpty()) {

                aStatus.error = "Invalid inputs for Push registration";
                aStatus.errorcode = -67;
                return aStatus;
            }
            if (_devicetype == 0) {

                aStatus.error = "Invalid inputs for Push registration";
                aStatus.errorcode = -67;
                return aStatus;
            }
            
            PushNotificationDeviceManagement pManagement = new PushNotificationDeviceManagement();
            OTPTokenManagement otmMgt = new OTPTokenManagement(channelid);
            int result = -1;
            Registerdevicepush registerdevicepush = pManagement.GetRegisterPushDeviceByUserID(sessionid, channel.getChannelid(), userid);

            if (registerdevicepush != null) {
                if (!deviceToken.equals(registerdevicepush.getGoogleregisterid())) {
                    
                    Trusteddevice td = new Trusteddevice();
                    td.setChannelid(session.getChannelid());
                    td.setCreatedOn(new Date());
                    td.setUserid(userid);
                    td.setDeviceid(deviceId);
                    td.setOsdetails(deviceName);
                    td.setVisibleKey(channel.getVisiblekey());
                    td.setAxiomid(userid);
                    td.setDeviceno(deviceId);
                    td.setLastupdatedOn(new Date());
                    td.setMacaddress(deviceId);
                    result = new TrustDeviceManagement().addDevicedetails(sessionid, td);
                    if (result == 0) {
                        registerdevicepush.setGoogleregisterid(deviceToken);
                        registerdevicepush.setUserid(userid);
                        registerdevicepush.setType(_devicetype);
                        result = pManagement.ChangeRegisterPushDevice(sessionid, channel.getChannelid(), userid, registerdevicepush);
                    
                        if (result == 0) {
                            aStatus.error = "Push device registered Successfully..!!!";
                            aStatus.errorcode = 0;
                            return aStatus;
                        }
                    }else{
                        aStatus.error = "Failed to register for trust device..!!!";
                        aStatus.errorcode = -70;
                    }
                } else {
                    aStatus.error = "Push device registered Successfully..!!!";
                    aStatus.errorcode = 0;
                    return aStatus;
                }

            }
            Trusteddevice td = new Trusteddevice();
            td.setChannelid(session.getChannelid());
            td.setCreatedOn(new Date());
            td.setUserid(userid);
            td.setDeviceid(deviceId);
            td.setOsdetails(deviceName);
            td.setVisibleKey(channel.getVisiblekey());
            td.setAxiomid(userid);
            td.setDeviceno(deviceId);
            td.setLastupdatedOn(new Date());
            td.setMacaddress(deviceId);
            result = new TrustDeviceManagement().addDevicedetails(sessionid, td);
            if(result == 0){
                result = pManagement.AddRegisterPushDevice(sessionid, channel.getChannelid(), OTPTokenManagement.TOKEN_STATUS_ACTIVE, aUser.getEmail(), deviceToken, aUser.getUserId(), _devicetype, new Date(), new Date());
                //(ACTIVE, emailid, gcm_regid, String.valueOf(uId), ANDROID, new Date(), new Date());
                    if (result == 0) {
                        result = otmMgt.ChangeStatus(channelid, userid, OTPTokenManagement.TOKEN_STATUS_ACTIVE, OTPTokenManagement.OOB_TOKEN, OTPTokenManagement.OOB_PUSH_TOKEN);

                        if (result == 0) {
                            aStatus.error = "Push device registered Successfully..!!!";
                            aStatus.errorcode = 0;

                        } else {

                            aStatus.error = "Failed to change token Status..!!!";
                            aStatus.errorcode = -69;

                        }

                    } else {
                        aStatus.error = "Failed to register device for Push..!!!";
                        aStatus.errorcode = -68;
                    }
            }else{
                aStatus.error = "Failed to register for trust device..!!!";
                aStatus.errorcode = -70;
            }
            audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                    session.getLoginid(), session.getLoginid(), new Date(), "Activate Push Authentication", errmsg.getErrorMessage(), aStatus.errorcode,
                    "Token Management", "", "Category = " + OTPTokenManagement.OOB_TOKEN + "regcode =" + "******",
                    itemtype, userid);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return aStatus;
    }

    @Override
    public AxiomStatus InitTransaction(String sessionid, String userid, InitTransactionPackage packagepaload, String integrityCheckString) throws AxiomException {
        String strDebug = null;
        Sessions session = null;
        int retValue = -1;
        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
        AxiomStatus aStatus = new AxiomStatus();
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug("::sessionid::" + sessionid);
                log.debug("::userid::" + userid);
                log.debug("::InitTransactionPackage::" + packagepaload.getMessage());
                log.debug("::integrityCheckString::" + integrityCheckString);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }

        int iResult = AxiomProtect.ValidateLicense();
        String clientip = "";

        try {
            if (iResult != 0) {

                aStatus.errorcode = -100;
                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }

            if (sessionid == null || sessionid.isEmpty()) {

                aStatus.errorcode = -114;
                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }

            if (userid == null || userid.isEmpty() == true || packagepaload == null) {

                aStatus.errorcode = -114;
                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;

            }

            SessionManagement sManagement = new SessionManagement();
            UserManagement uManagement = new UserManagement();
            CryptoManagement cyManagement = new CryptoManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            session = sManagement.getSessionById(sessionid);

//        aStatus.error = "ERROR";
            aStatus.errorcode = -1;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            if (session == null) {
//            aStatus.error = "Invalid Session";
                aStatus.errorcode = -114;
                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                aStatus.errorcode = -102;
                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }
            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
            if (channelProfileObj != null) {
                int retVal = sManagement.getServerStatus(channelProfileObj);
                if (retVal != 0) {
                    aStatus.errorcode = -82;
                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                    aStatus.error = errmsg.getUsermessage();
                    return aStatus;
                }
            }
            OTPTokenManagement tokenManagement = new OTPTokenManagement(session.getChannelid());            
            Otptokens tokenDetails = tokenManagement.getOtpObjByUserId(session.getChannelid(), userid, OTPTokenManagement.OOB_TOKEN);
            if(tokenDetails == null){
                aStatus.errorcode = -83;                
                aStatus.error = "Desired OOB Token not assigned.";
                return aStatus;
            }
            if(tokenDetails.getCategory() != OTPTokenManagement.OOB_PUSH_TOKEN){
                aStatus.errorcode = -84;                
                aStatus.error = "Desired OOB Push Token not assigned.";
                return aStatus;
            }
            if(tokenDetails.getStatus() != OTPTokenManagement.TOKEN_STATUS_ACTIVE){
                aStatus.errorcode = -85;                
                aStatus.error = "Desired OOB Push Token is not Active.";
                return aStatus;
            }
            TxManagement trxMgmt = new TxManagement();
            Txdetails txDet = new Txdetails();
            JSONObject jsonData = null;
            txDet.setChannelid(session.getChannelid());
            if (packagepaload.getUserid() != null && packagepaload.getUserid().isEmpty() == false) {
                txDet.setUserid(packagepaload.getChannelid());
            }
            txDet.setCreatedOn(new Date());
            Calendar cal = Calendar.getInstance();
            if (packagepaload.getExpirytimeInMins() != 0) {
                cal.add(Calendar.MINUTE, packagepaload.getExpirytimeInMins());
                txDet.setExpiredOn(cal.getTime());
            }
            if (packagepaload.getFriendlyMsg() != null && packagepaload.getFriendlyMsg().isEmpty() == false) {
                txDet.setTxmsg(packagepaload.getFriendlyMsg());
            }
            UUID uuid = UUID.randomUUID();
            AXIOMStatus aPushStatus = null;
            String hashedID = HASH.getStringFromSHA1("" + new Date().getTime() + Thread.currentThread().getId() + uuid.toString());
            txDet.setType(1);
            txDet.setTransactionId(hashedID);
            txDet.setStatus(OTPTokenManagement.TOKEN_STATUS_ASSIGNED);
            txDet.setSessionid(sessionid);
            retValue = trxMgmt.addTxDetails(txDet);
            int countFoSentMessages = 0;
            if (retValue != 0) {
                aStatus.errorcode = -70;
                aStatus.error = "Failed to Initiate Transaction";
                return aStatus;
            } else {
                SendNotification sendNot = new SendNotification();
                PushNotificationDeviceManagement pushDevMgmt = new PushNotificationDeviceManagement();
                UserManagement userMgmt = new UserManagement();
                jsonData = new JSONObject();
                jsonData.put("TransactionId", hashedID);
                AuthUser auser = userMgmt.getUser(g_channelID, userid);
                if(auser == null){
                    aStatus.errorcode = -60;
                    aStatus.error = "User not found";
                    return aStatus;
                }
                Registerdevicepush[] regPushDevices = pushDevMgmt.GetRegisterPushDeviceByEmailID(sessionid, g_channelID, auser.email);
                if (regPushDevices != null && regPushDevices.length != 0) {
                    for (int i = 0; i < regPushDevices.length; i++) {
                        if(packagepaload.getType()==IOS){                            
                            aPushStatus = sendNot.SendOnPushIOS(g_channelID, regPushDevices[i].getGoogleregisterid(), auser.getEmail(), packagepaload.getMessage(), regPushDevices[i].getType());
                        }else{
                            aPushStatus = sendNot.SendOnPush(g_channelID, regPushDevices[i].getGoogleregisterid(), auser.getEmail(), packagepaload.getMessage(), regPushDevices[i].getType());
                        }
                        if (aPushStatus.iStatus == 0 || aPushStatus.iStatus == 2) {
                            countFoSentMessages = countFoSentMessages + 1;
                        }
                    }
                }

            }
            if (countFoSentMessages != 0) {
                aStatus.errorcode = 0;
                aStatus.error = "success";
                aStatus.regCodeMessage = "";
                if (jsonData != null) {
                    aStatus.data = jsonData.toString();
                }
                return aStatus;
            } else {
                aStatus.errorcode = -72;
                aStatus.error = "Transaction Initiated but failed to send push message...!!";
                aStatus.regCodeMessage = "";
                if (jsonData != null) {
                    aStatus.data = jsonData.toString();
                }
                return aStatus;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public AxiomStatus UpdateTransactionStatus(String sessionid, String transactionId, String integrityCheckString, String devicepayload) throws AxiomException {

        String strDebug = null;
        Sessions session = null;
        int retValue = -1;
        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
        AxiomStatus aStatus = new AxiomStatus();
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug("::sessionid::" + sessionid);
                log.debug("::transactionId::" + transactionId);
                log.debug("::devicepayload::" + devicepayload);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }

        int iResult = AxiomProtect.ValidateLicense();
        String clientip = "";

        try {
            if (iResult != 0) {

                aStatus.errorcode = -100;
                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }

            if (sessionid == null || sessionid.isEmpty()) {

                aStatus.errorcode = -114;
                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }

            if (transactionId == null || transactionId.isEmpty() == true || devicepayload == null || devicepayload.isEmpty() == true) {

                aStatus.errorcode = -114;
                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;

            }

            SessionManagement sManagement = new SessionManagement();
            UserManagement uManagement = new UserManagement();
            CryptoManagement cyManagement = new CryptoManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            session = sManagement.getSessionById(sessionid);

//        aStatus.error = "ERROR";
            aStatus.errorcode = -1;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            if (session == null) {
//            aStatus.error = "Invalid Session";
                aStatus.errorcode = -114;
                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                aStatus.errorcode = -102;
                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }
            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
            if (channelProfileObj != null) {
                int retVal = sManagement.getServerStatus(channelProfileObj);
                if (retVal != 0) {
                    aStatus.errorcode = -82;
                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                    aStatus.error = errmsg.getUsermessage();
                    return aStatus;
                }
            }
            JSONObject json = new JSONObject(devicepayload);
            String _transactionResponse = json.getString("transactionResponse");
            String _deviceId = json.getString("_deviceid");
            TxManagement trxMgmt = new TxManagement();
            Txdetails trxDetails = trxMgmt.getTxDetailsByTxId(transactionId);
            if (trxDetails != null) {
                if (trxDetails.getExpiredOn().getTime() < new Date().getTime()) {
                    trxDetails.setStatus(TxManagement.EXPIRED);

                } else if (_transactionResponse != null && _transactionResponse.isEmpty() == false) {
                    if (Integer.parseInt(_transactionResponse) == TxManagement.APPROVED) {
                        trxDetails.setStatus(TxManagement.APPROVED);
                    }
                    if (Integer.parseInt(_transactionResponse) == TxManagement.DENIED) {
                        trxDetails.setStatus(TxManagement.DENIED);
                    }
                }
                trxDetails.setDeviceid(_deviceId);
                trxDetails.setUpdatedOn(new Date());
                retValue = trxMgmt.editTxDetails(trxDetails);
                if (retValue == 0) {
                    aStatus.errorcode = 0;
                    aStatus.error = "success";
                    return aStatus;
                }
            } else {
                aStatus.errorcode = -68;
                aStatus.error = "Invalid transaction Id...!!!";
                return aStatus;

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;

    }

    @Override
    public AxiomStatus GetTransactionStatus(String sessionId, String txId, String integrityCheckString) throws AxiomException {
        String strDebug = null;
        Sessions session = null;
        int retValue = -1;
        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
        AxiomStatus aStatus = new AxiomStatus();
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug("::sessionid::" + sessionId);
                log.debug("::transactionId::" + txId);
                log.debug("::integrityCheckString::" + integrityCheckString);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
        int iResult = AxiomProtect.ValidateLicense();
        String clientip = "";
        try {
            if (iResult != 0) {
                aStatus.errorcode = -100;
                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }

            if (sessionId == null || sessionId.isEmpty()) {

                aStatus.errorcode = -114;
                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }

            if (txId == null || txId.isEmpty() == true || txId == null || txId.isEmpty() == true) {

                aStatus.errorcode = -114;
                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;

            }
            SessionManagement sManagement = new SessionManagement();
            UserManagement uManagement = new UserManagement();
            CryptoManagement cyManagement = new CryptoManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            session = sManagement.getSessionById(sessionId);
            aStatus.errorcode = -1;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            if (session == null) {
                aStatus.errorcode = -114;
                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                aStatus.errorcode = -102;
                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }
            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
            if (channelProfileObj != null) {
                int retVal = sManagement.getServerStatus(channelProfileObj);
                if (retVal != 0) {
                    aStatus.errorcode = -82;
                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                    aStatus.error = errmsg.getUsermessage();
                    return aStatus;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public AxiomTokenDetails[] GetUserTokens(String sessionid, String userid) throws AxiomException {
        log.debug("started");
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("GetUserTokens::sessionId::" + sessionid));
                log.debug((Object) ("GetUserTokens::userid::" + userid));
            }
        } catch (Exception var4_4) {
            // empty catch block
        }
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = this.wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
        Sessions session = sManagement.getSessionById(sessionid);
        if (session == null) {
            throw new AxiomException("Invalid Session!!!");
        }
        TokenStatusDetails[] tDetails = null;
        AxiomTokenDetails[] atDetails = null;
        String resultStr = "Failure";
        int retValue = -1;
        ChannelManagement cManagement = new ChannelManagement();
        log.debug("getChannelByID :Strted");
        Channels channel = cManagement.getChannelByID(session.getChannelid());
          log.debug("getChannelByID :Ended");
        if (channel == null) {
            throw new AxiomException("Invalid Channel!!!");
        }
        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
        log.debug("");
         if (session != null) {
            log.debug("GetUserTokens :: getTokenList() called");
            if ((tDetails = oManagement.getTokenList(sessionid, session.getChannelid(), userid)) != null && tDetails.length > 0) {
                atDetails = new AxiomTokenDetails[tDetails.length];
                for (int i = 0; i < tDetails.length; ++i) {
                    atDetails[i] = new AxiomTokenDetails();
                    atDetails[i].attempts = tDetails[i].Attempts;
                    atDetails[i].category = tDetails[i].Catrgory;
                    atDetails[i].status = tDetails[i].Status;
                    atDetails[i].createOn = tDetails[i].createOn;
                    atDetails[i].subcategory = tDetails[i].SubCategory;
                    atDetails[i].lastaccessOn = tDetails[i].lastaccessOn;
                    atDetails[i].serialnumber = tDetails[i].serialnumber;
                }
            }
            log.debug("GetUserTokens :: getTokenList() ended");
        }
        if (atDetails != null) {
            retValue = 0;
            resultStr = "Success";
        } else if (atDetails == null) {
            // empty if block
        }
        log.debug("ended");
        return atDetails;
    }

    @Override
    public AxiomStatus ValidateRegistrationCode(String sessionid, String userid, String regcode, String deviceid, Integer tokenType) throws AxiomException {
        String strDebug = null;        
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug(String.format("##ValidateRegistrationCode :" + "sessionid" + sessionid));
                log.debug(String.format("##ValidateRegistrationCode :" + "username" + userid));
                log.debug(String.format("##ValidateRegistrationCode :" + "regcode" + regcode));
                log.debug(String.format("##ValidateRegistrationCode :" + "deviceid" + deviceid)); 
                log.debug(String.format("##ValidateRegistrationCode :" + "tokenType" + tokenType)); 
            }
        } catch (Exception ex) {
            log.error(String.format("##ValidateRegistrationCode : [%s] - exception caught when calling ValidateRegistrationCode() - [%s]", ex.getMessage()));
            ex.printStackTrace();
        }

        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//            aStatus.error = "Licence is invalid";
            aStatus.errorcode = -100;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            return aStatus;
        }
        AxiomStatus aStatus = new AxiomStatus();
        try{
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);

        
        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
        
        if (session == null) {
            log.debug("Invalid Session");
//            aStatus.error = "Invalid Session";
            aStatus.errorcode = -114;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            return aStatus;
        }
        if (userid == null || regcode == null || deviceid == null || tokenType == null               
                || userid.isEmpty() == true || regcode.isEmpty() == true || deviceid.isEmpty() == true
                ) {
            AxiomStatus as = new AxiomStatus();
            as.errorcode = -11;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
            as.error = errmsg.getUsermessage();
            return as;
        }
        Channels channel = new ChannelManagement().getChannelByID(session.getChannelid());
        String channelid = channel.getChannelid();
        SettingsManagement setManagement = new SettingsManagement();
        Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
        if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                throw new Exception("IP is not allowed!!!");
                            }
                            suTemplate.close();
                            sTemplate.close();
                            throw new Exception("IP is not allowed!!!");
                        }
                        throw new Exception("IP is not allowed!!!");
                    }
                }
            Object setObj = setManagement.getSetting(channelid, SettingsManagement.Token, SettingsManagement.PREFERENCE_ONE);
            TokenSettings tSeetings = null;
            if (setObj != null) {
                if (setObj instanceof TokenSettings) {
                    tSeetings = (TokenSettings) setObj;
                }
            }
            String lengthOfOTP = "6";
            if (tSeetings != null) {
                lengthOfOTP = "" + tSeetings.getOtpLengthSWToken();
            }
            OTPTokenManagement tManagement = new OTPTokenManagement(channelid);
            Otptokens otpObj = tManagement.getOtpObjByUserId(channelid, userid, tokenType);
            if (otpObj == null) {                
                aStatus.errorcode = -2;                
                aStatus.error = "Registration Code Not Found/Already Used!!!";
                return aStatus;   
            } 
            System.err.println("Reg code " + otpObj.getRegcode());
            String strRegCode = otpObj.getRegcode();
            String registrationcode = UtilityFunctions.Bas64SHA1(strRegCode);
            RegistrationCodeTrailManagement rManagement = new RegistrationCodeTrailManagement();
            Registrationcodetrail regcodetaril = rManagement.getRegistrationcodetrail(channelid, registrationcode);
            if (regcodetaril == null) {                                         
                aStatus.errorcode = -1;                
                aStatus.error = "Registration Code Not Found/Already Used!!!";
                return aStatus;                    
            }
//            if (registrationcode.equals(regcode)) {

                int res = rManagement.validateRegisterCodeV2(channelid, regcodetaril.getUserid(), registrationcode,tokenType);
                if (res == 0) {
                    //System.out.println("In change RegTrail   ");
                    rManagement.changeRegCodeTrail(channelid, regcodetaril.getUserid(), strRegCode, deviceid, RegistrationCodeTrailManagement.USED, RegistrationCodeTrailManagement.OTPTOKEN);
                }
                if (res == -13) {                    
                    aStatus.errorcode = -13;                
                    aStatus.error = "Token Not found";
                    return aStatus;                      
                } else if (res == -18) {                    
                    aStatus.errorcode = -18;                
                    aStatus.error = "Registration code has expired.";
                    return aStatus;  
                }
                if (res == 0) {
                    aStatus.errorcode = 0;                
                    aStatus.error = "Registration code validate successfully!!!";
                    return aStatus;                      

                } else {
                    aStatus.errorcode = -10;                
                    aStatus.error = "Invalid registartion code( errorcode  :" + res + ")!!!";
                    return aStatus;                      
                }
//            }
//            else {
//                aStatus.errorcode = -10;                
//                aStatus.error = "Invalid Registration Code/Token Not Found";
//                return aStatus;                 
//            }

        }catch(Exception e){
            log.error(String.format("##ValidateRegistrationCode : [%s] - exception caught when calling ValidateRegistrationCode() - [%s]", e.getMessage()));
            e.printStackTrace();
        }
        return aStatus;    
    }

    private AxiomStatus CheckIPAndSendAlert(Channels channel, String cip) {

        AxiomStatus mStatus = new AxiomStatus();
        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();

        SettingsManagement setManagement = new SettingsManagement();

        Object ipobj = setManagement.getSettingInner(channel.getChannelid(), SettingsManagement.GlobalSettings, 1);
        if (ipobj != null) {
            GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
            int checkIp = 1;
            if (cip.compareTo("127.0.0.1") != 0) {
                checkIp = setManagement.checkIP(channel.getChannelid(), cip);
            } else {
                checkIp = 1;
            }
            String channelid = GetChannelID();
            if (iObj.ipstatus == 0 && checkIp != 1) {
                if (iObj.ipalertstatus == 0) {
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                    Session sTemplate = suTemplate.openSession();
                    TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                    Templates templatesObj = tUtil.loadbyName(channel.getChannelid(), TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                    OperatorsManagement oManagement = new OperatorsManagement();
                    Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                    if (aOperator != null) {
                        String[] emailList = new String[aOperator.length - 1];
                        for (int i = 1; i < aOperator.length; i++) {
                            emailList[i - 1] = aOperator[i].getEmailid();
                        }
                        if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                            ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                            String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                            String strsubject = templatesObj.getSubject();
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            if (strmessageBody != null) {
                                // Date date = new Date();
                                strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                strmessageBody = strmessageBody.replaceAll("#filterword#", cip);
                            }

                            SendNotification send = new SendNotification();
                            AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                    emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            AxiomStatus mxStatus = new AxiomStatus();
                            mxStatus.error = axStatus.strStatus;
                        }

                        suTemplate.close();
                        sTemplate.close();
//                        mStatus.error = "INVALID IP REQUEST";
                        mStatus.errorcode = -8;

                        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(channelid, mStatus.errorcode);
                        mStatus.error = errmsg.getUsermessage();
                        return mStatus;
                    }

                    suTemplate.close();
                    sTemplate.close();
//                    mStatus.error = "INVALID IP REQUEST";
                    mStatus.errorcode = -8;
                    Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(channelid, mStatus.errorcode);
                    mStatus.error = errmsg.getUsermessage();
                    return mStatus;
                }
//                mStatus.error = "INVALID IP REQUEST";
                mStatus.errorcode = -8;
                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(channelid, mStatus.errorcode);
                mStatus.error = errmsg.getUsermessage();
                return mStatus;
            }
        }
        return mStatus;
    }

    @Override
    public AxiomStatus CreateUser(String sessionid, AxiomUser aUser, String tokenSrno, Integer default_token_type) {
        String strDebug = null;
        int bTokenActivation = -1;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug(String.format("##CreateAPUser :" + "sessionid" + sessionid));
                log.debug(String.format("##CreateAPUser :" + "username" + aUser.userName));
                log.debug(String.format("##CreateAPUser :" + "phoneNo" + aUser.phoneNo));
                log.debug(String.format("##CreateAPUser :" + "email" + aUser.email));
                log.debug(String.format("##CreateAPUser :" + "userId" + aUser.userId));
                log.debug(String.format("##CreateAPUser :" + "idType" + aUser.idType));
                log.debug(String.format("##CreateAPUser :" + "location" + aUser.location));
                log.debug(String.format("##CreateAPUser :" + "organisation" + aUser.organisation));
                log.debug(String.format("##CreateAPUser :" + "organisationUnit" + aUser.organisationUnit));
                log.debug(String.format("##CreateAPUser :" + "street" + aUser.street));
                log.debug(String.format("##CreateAPUser :" + "userIdentity" + aUser.userIdentity));
                log.debug(String.format("##CreateAPUser :" + "country" + aUser.country));
                log.debug(String.format("##CreateAPUser :" + "designation" + aUser.designation));
                log.debug(String.format("##CreateAPUser :" + "default_token_type" + default_token_type));
                log.debug(String.format("##CreateAPUser :" + "tokenSrno" + tokenSrno));
            }
        } catch (Exception ex) {
            log.error(String.format("##CreateAPUser : [%s] - exception caught when calling CreateAPUser() - [%s]", ex.getMessage()));
            ex.printStackTrace();
        }

        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//            aStatus.error = "Licence is invalid";
            aStatus.errorcode = -100;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            return aStatus;
        }

        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);

        AxiomStatus aStatus = new AxiomStatus();
        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();

        if (session == null) {
            log.debug("Invalid Session");
//            aStatus.error = "Invalid Session";
            aStatus.errorcode = -114;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            return aStatus;
        }

        if (aUser.email == null || aUser.phoneNo == null
                || aUser.userName == null || aUser.userId == null
                || aUser.email.isEmpty() == true || aUser.phoneNo.isEmpty() == true
                || aUser.userName.isEmpty() == true || aUser.userIdentity.isEmpty() == true
                || aUser.userIdentity == null
                || aUser.country == null
                || aUser.country.isEmpty() == true
                || aUser.userIdentity.isEmpty() == true) {
            AxiomStatus as = new AxiomStatus();
            as.errorcode = -11;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
            as.error = errmsg.getUsermessage();
            return as;
        }

        if (aUser.idType == null || aUser.idType.isEmpty() == true) {
            aUser.idType = "ic"; //this is hardcoded to ic even though it can be passport/ic
        }
        if (aUser.designation == null || aUser.designation.isEmpty() == true) {
            aUser.designation = "";
        }

        if (aUser.organisation == null || aUser.organisation.isEmpty() == true) {
            aUser.organisation = "";
        }

        if (aUser.organisation == null || aUser.organisation.isEmpty() == true) {
            aUser.organisation = "";
        }

        if (aUser.street == null || aUser.street.isEmpty() == true) {
            aUser.street = "";
        }

        if (aUser.location == null || aUser.location.isEmpty() == true) {
            aUser.location = "";
        }

        UserManagement uManagement = new UserManagement();
        int retValue = -1;
        aStatus.errorcode = retValue;
        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
        aStatus.error = errmsg.getUsermessage();
        SettingsManagement setManagement = new SettingsManagement();
        String channelid = session.getChannelid();
        log.debug("##CreateUser" + "ChannelId" + channelid);
        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(channelid);
        if (channel == null) {
            log.debug("Channel is Null");
            aStatus.errorcode = -102;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            return aStatus;
        }

        AxiomStatus as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
        if (as.errorcode != 0) {
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
            System.out.println("error code: " + as.errorcode);
            as.error = errmsg.getUsermessage();
            return as;
        }

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_PASSWORD) != 0) {
//            aStatus.error = "This feature is not available in this license!!!";
            aStatus.errorcode = -103;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();

            return aStatus;
        }
        int iUserCount = uManagement.getCountOfLicenseUser(channel.getChannelid());
        int licensecount = AxiomProtect.GetUsersAllowed(); //AxiomProtect Function here now default 0. 
        if (licensecount == -998) {
            //unlimited licensing 
        } else if (iUserCount >= licensecount) {
//            aStatus.error = "User Addition already reached its limit as per this license!!!";
            aStatus.errorcode = -107;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();

            return aStatus;
        }

        retValue = uManagement.CreateUserV2(sessionid, session.getChannelid(), aUser.userName, aUser.phoneNo, aUser.email, aUser.userId, 0, aUser.organisation, aUser.organisationUnit, aUser.idType, aUser.organisationUnit, aUser.country, aUser.location, aUser.street, aUser.designation, tokenSrno, default_token_type);
        log.debug("##CreateUserV2 user creation retvalue" + retValue);

        if (retValue >= 0) {
            aStatus.error = "SUCCESS";
            aStatus.errorcode = 0;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
            if (tokenSrno.isEmpty() == false) {
                retValue = oManagement.AssignToken(sessionid, channelid, aUser.userId, OTPTokenManagement.HARDWARE_TOKEN, OTPTokenManagement.HW_MINI_TOKEN, tokenSrno);
                AuditManagement ad = new AuditManagement();
                ad.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "Assign OTP Token", "", retValue, "Token Management", "", "Category=" + OTPTokenManagement.HARDWARE_TOKEN + ",Subcategory=" + OTPTokenManagement.HW_MINI_TOKEN, "OTPTOKENS", aUser.userId);
                if (retValue == 0) {

                    if (retValue == 0) {
                        retValue = oManagement.ChangeStatus(channelid, aUser.userId, OTPTokenManagement.TOKEN_STATUS_ACTIVE, OTPTokenManagement.HARDWARE_TOKEN, OTPTokenManagement.HW_CR_TOKEN);
                        ad = new AuditManagement();
                        bTokenActivation = -2;
                        ad.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "Change OTP Token", "", retValue, "Token Management", "", "Old Status=" + OTPTokenManagement.TOKEN_STATUS_ASSIGNED + ",New Status=" + OTPTokenManagement.TOKEN_STATUS_ACTIVE, "OTPTOKENS", aUser.userId);

                    }
                }
            }

            retValue = oManagement.AssignToken(sessionid, channelid, aUser.userId, OTPTokenManagement.OOB_TOKEN, OTPTokenManagement.OOB_PUSH_TOKEN, "");
            
            if(retValue == 0){
                bTokenActivation = -2;
            }
            if (retValue == 1) {
                aStatus.errorcode = 1;
                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                log.debug("createUser:User created succesfully but email could not be sent!!!" + retValue);
            }

        } else if (retValue == -244) {

            aStatus.errorcode = retValue;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();

        } else if (retValue == -241) {
            aStatus.errorcode = retValue;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();

        } else if (retValue == -242) {
            aStatus.errorcode = retValue;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();

        } else if (retValue == -240) {
            aStatus.errorcode = retValue;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        } else if (retValue == -2) {
            aStatus.errorcode = retValue;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        } else if (retValue == 0) {
            aStatus.errorcode = retValue;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        }

        String strStatus = "";
        if (aUser.status == 0) {
            strStatus = "ACTIVE_STATUS";
        }
        if (aUser.status == 1) {
            strStatus = "SUSPENDED";
        }

        if (aStatus.errorcode == 0) {
            String cip = req.getRemoteAddr();
            AuditManagement a = new AuditManagement();
            a.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                    cip, channel.getName(),
                    session.getLoginid(),
                    session.getLoginid(),
                    new Date(), "Create User", "SUCCESS",
                    aStatus.errorcode,
                    "User Management", "",
                    "Name=" + aUser.userName + ",Phone=" + aUser.phoneNo + ",Email=" + aUser.email + ",State=" + strStatus,
                    "USERPASSWORD", aUser.userId);

        } else {
            String cip = req.getRemoteAddr();

            AuditManagement a = new AuditManagement();
            a.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                    cip, channel.getName(),
                    session.getLoginid(), session.getLoginid(),
                    new Date(), "Create User", "ERROR", aStatus.errorcode,
                    "User Management", "",
                    "Name=" + aUser.userName + ",Phone=" + aUser.phoneNo + ",Email=" + aUser.email + ",State=" + strStatus + ",Error:" + errmsg.getErrorMessage(),
                    "USERPASSWORD", aUser.userId);
        }

        String cip = req.getRemoteAddr();

        if (bTokenActivation == -1) {
            int retValue1 = uManagement.DeleteUser(sessionid, channelid, aUser.userId);
            log.error("DeleteUser result is =" + retValue1);

            if (retValue1 == 0) {
                AuditManagement ad = new AuditManagement();
                ad.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                        cip, channel.getName(),
                        session.getLoginid(), session.getLoginid(),
                        new Date(),
                        "Delete User(roolback)",
                        "SUCCESS", aStatus.errorcode,
                        "User Management",
                        "Name=" + aUser.userName + ",userid=" + aUser.userId,
                        "Deleted user to rollback as failed certificate issance. userid was " + aUser.userId,
                        "USERPASSWORD",
                        aUser.userId);
                aStatus.errorcode = -5;
                aStatus.error = "User Deleted cause of  failed to assign token";
            }
        }

        return aStatus;

    }

    @Override
    public AxiomStatus ReplaceToken(String sessionid, String userid, String oldTokenSerialno, String newTokenSerialno) throws AxiomException {
        String strDebug = null;        
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug(String.format("##ReplaceToken :" + "sessionid" + sessionid));
                log.debug(String.format("##ReplaceToken :" + "userid" + userid));
                log.debug(String.format("##ReplaceToken :" + "oldTokenSerialno" + oldTokenSerialno));
                log.debug(String.format("##ReplaceToken :" + "newTokenSerialno" + newTokenSerialno));                
            }
        } catch (Exception ex) {
            log.error(String.format("##ReplaceToken : [%s] - exception caught when calling ReplaceToken() - [%s]", ex.getMessage()));
            ex.printStackTrace();
        }
        
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//            aStatus.error = "Licence is invalid";
            aStatus.errorcode = -100;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            return aStatus;
        }

        
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);

        AxiomStatus aStatus = new AxiomStatus();
        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
         if (session == null) {
            log.debug("Invalid Session");
//            aStatus.error = "Invalid Session";
            aStatus.errorcode = -114;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            return aStatus;
        }
       
        Channels channel = new ChannelManagement().getChannelByID(session.getChannelid());
        AxiomStatus as = new AxiomStatus();
        if (userid == null || oldTokenSerialno == null
                || newTokenSerialno == null
                || userid.isEmpty() == true || oldTokenSerialno.isEmpty() == true
                || newTokenSerialno.isEmpty() == true) {
            
            as.errorcode = -11;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
            as.error = errmsg.getUsermessage();
            return as;
        }
        OTPTokenManagement oManagement = new OTPTokenManagement(channel.getChannelid());       
        Otptokens oldtoken = oManagement.getOtpObjByUserId(sessionid, channel.getChannelid(), userid, OTPTokenManagement.HARDWARE_TOKEN);

        if (oldtoken == null) {
            as.errorcode = -12;
            as.error = "Token not found!!!";
           
        }
        int status = 0;
        status = oManagement.ChangeStatus(channel.getChannelid(), userid, OTPTokenManagement.TOKEN_STATUS_UNASSIGNED, OTPTokenManagement.HARDWARE_TOKEN, oldtoken.getSubcategory());
        log.debug("ChangeStatus :: "+status);
        int retValue = -1;
        if (status == 0 || status == -6) {
            String resultString = "SUCCESS";
            AuditManagement a = new AuditManagement();
            a.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                    req.getRemoteAddr(), channel.getName(),
                    session.getLoginid(), session.getLoginid(), new Date(), "Replace Token (Change Status)",
                    resultString, status, "Token Management",
                    "", 
                    "category=" + OTPTokenManagement.HARDWARE_TOKEN + ", subcategory=" + oldtoken.getSubcategory() + ",iTokenStatus=" + OTPTokenManagement.TOKEN_STATUS_UNASSIGNED,
                    "OTPTOKENS", 
                    userid);
            
            retValue = oManagement.AssignToken(sessionid, channel.getChannelid(), userid, OTPTokenManagement.HARDWARE_TOKEN, oldtoken.getSubcategory(), newTokenSerialno);
            log.debug("AssignToken :: "+retValue);
        }
        
        if (retValue == 0) {
            String resultString = "SUCCESS";
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                    req.getRemoteAddr(), channel.getName(),
                    session.getLoginid(), session.getLoginid(), new Date(), "Replace Token (Assign Token)",
                    resultString, retValue, "Token Management",
                    "", 
                    "category=" + OTPTokenManagement.HARDWARE_TOKEN + ", subcategory=" + oldtoken.getSubcategory() + ",_newTokenSerialNo=" + newTokenSerialno,
                    "OTPTOKENS", userid);
            as.errorcode = 0;
            as.error = "Token Replaced Successfully.";
            return as;
        }else {
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                    req.getRemoteAddr(), channel.getName(),
                    session.getLoginid(), session.getLoginid(), new Date(), 
                    "Replace Token",
                    "Error", retValue,
                    "Token Management",
                    "", 
                    "Failed to Replace Token",
                    "OTPTOKENS",
                    userid);            
            as.errorcode = -11;
            as.error = "Replace token failed!!!";
            return as;
        }        
    }

    @Override
    public AxiomStatus ResyncToken(String sessionid, String userid, Integer category, String otp1, String otp2) throws AxiomException {
        String strDebug = null;        
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug(String.format("##ResyncToken :" + "sessionid" + sessionid));
                log.debug(String.format("##ResyncToken :" + "userid" + userid));
                log.debug(String.format("##ResyncToken :" + "category" + category));
                log.debug(String.format("##ResyncToken :" + "otp1" + otp1));
                log.debug(String.format("##ResyncToken :" + "otp2" + otp2));
            }
        } catch (Exception ex) {
            log.error(String.format("##ResyncToken : [%s] - exception caught when calling ResyncToken() - [%s]", ex.getMessage()));
            ex.printStackTrace();
        }
        
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//            aStatus.error = "Licence is invalid";
            aStatus.errorcode = -100;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            return aStatus;
        }

        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);        
        AxiomStatus aStatus = new AxiomStatus();
        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
        if (session == null) {
            log.debug("Invalid Session");
//            aStatus.error = "Invalid Session";
            aStatus.errorcode = -114;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            return aStatus;
        }
       
        Channels channel = new ChannelManagement().getChannelByID(session.getChannelid());
        AxiomStatus as = new AxiomStatus();
        if (userid == null
                || category == null || otp1 == null || otp2 == null
                || userid.isEmpty() == true || otp1.isEmpty() == true
                || otp2.isEmpty() == true) {
            
            as.errorcode = -11;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
            as.error = errmsg.getUsermessage();
            return as;
        }        
        OTPTokenManagement oManagement = new OTPTokenManagement(channel.getChannelid());        
        Otptokens oldtoken = oManagement.getOtpObjByUserId(sessionid, channel.getChannelid(), userid, category);
        if (oldtoken == null) {                        
            as.errorcode = -11;            
            as.error = "Token not found!!!";
            return as;
        }                        
        int oldTimeDiff = oldtoken.getTimediff();
        int retValue = oManagement.resyncOTP(sessionid, channel.getChannelid(), userid, category, otp1, otp2);
        log.debug("resyncOTP :: "+retValue);
        Otptokens newtoken = oManagement.getOtpObjByUserId(sessionid, channel.getChannelid(), userid, category);
        int newTimeDiff = newtoken.getTimediff();
        if (retValue == 0) {
            String resultString = "SUCCESS";
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                    req.getRemoteAddr(), channel.getName(),
                    session.getLoginid(), session.getLoginid(), new Date(), "Resynchronize Token",
                    resultString, retValue, "Token Management",
                    "Old Time Difference =" + oldTimeDiff, "New Time Difference =" + newTimeDiff,
                    "OTPTOKENS", 
                    userid);
            as.errorcode = 0;            
            as.error = "Token Resynchronization Successful";           
        }else{            
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                    session.getLoginid(), channel.getName(),
                    session.getLoginid(), session.getLoginid(), new Date(), 
                    "Resynchronize Token",
                    "ERROR", retValue, 
                    "Token Management",
                    "Old Time Difference =" + oldTimeDiff, 
                    "Failed to resynchronize Token",
                    "OTPTOKENS", 
                    userid);
            as.errorcode = -10;            
            as.error = "Token Resynchronization Failed";            
        }        
        return as;
    }
}
