package com.mollatech.axiom.v2.core;

import com.mollatech.axiom.connector.communication.RemoteSigningInfo;
import com.mollatech.axiom.connector.epin.QuestionsAndAnswers;
import com.mollatech.axiom.connector.user.AxiomChallengeResponse;
import com.mollatech.axiom.v2.core.utils.AxiomException;
import com.mollatech.axiom.v2.core.utils.AxiomData;
import com.mollatech.axiom.v2.core.utils.AxiomMessage;
import com.mollatech.axiom.v2.core.utils.AxiomStatus;
import com.mollatech.axiom.v2.core.utils.AxiomTokenDetails;
import com.mollatech.axiom.v2.core.utils.AxiomUser;
import com.mollatech.axiom.v2.core.utils.ConnectorStatus;
import com.mollatech.axiom.v2.core.utils.MessageReport;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

//Service Endpoint Interface
//@WebService
@WebService(name = "axiom")
public interface AxiomCoreInterface {

    @WebMethod
    public String OpenSession(@WebParam(name = "channelid") String channelid, @WebParam(name = "loginid") String loginid, @WebParam(name = "loginpassword") String loginpassword);

    @WebMethod
    public AxiomStatus CloseSession(@WebParam(name = "sessionid") String sessionid);

    @WebMethod
    public AxiomStatus CreateUser(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "fullname") String fullname, @WebParam(name = "phone") String phone, @WebParam(name = "email") String email, @WebParam(name = "userid") String userid);

    @WebMethod
    public AxiomUser GetUserBy(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "type") int type, @WebParam(name = "searchFor") String searchFor) throws AxiomException;

    @WebMethod
    public AxiomStatus ResetUser(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type);

    @WebMethod
    public AxiomStatus AssignPassword(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "password") String password, @WebParam(name = "bSentToUser") boolean bSentToUser, @WebParam(name = "subcategory") int subcategory);

    @WebMethod
    public AxiomStatus VerifyPassword(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "password") String password);

    @WebMethod
    public AxiomStatus AssignToken(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type, @WebParam(name = "subtype") int subtype);

    @WebMethod
    public AxiomTokenDetails[] GetUserTokens(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid) throws AxiomException;

    @WebMethod
    public AxiomStatus SendOTP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid);

    @WebMethod
//    public AxiomStatus VerifyOTP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "otp") String otp,@WebParam(name = "ackno") String ackno);
    public AxiomStatus VerifyOTP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "otp") String otp);

    
    @WebMethod
    public AxiomStatus SendSignatureOTP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "data") String[] data);

    @WebMethod
    public AxiomStatus VerifySignatureOTP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "data") String[] data, @WebParam(name = "otp") String otp);

    @WebMethod
    public AxiomMessage[] SendMessages(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "msgs") AxiomMessage[] msgs, @WebParam(name = "bCheckContent") boolean bCheckContent, @WebParam(name = "speed") int speed, @WebParam(name = "type") int type) throws AxiomException;

    @WebMethod
    public AxiomStatus ChangeOTPTokenStatus(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "category") int category, @WebParam(name = "value") int value);

    @WebMethod
    public ConnectorStatus GetConnectorsStatus(@WebParam(name = "channelid") String channelid, @WebParam(name = "loginid") String loginid, @WebParam(name = "password") String password);

    @WebMethod
    public AxiomStatus AssignTokenAndGenerateCertificate(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type, @WebParam(name = "subtype") int subtype, @WebParam(name = "srno") String srno);

    @WebMethod
    //public AxiomData VerifyOTPAndSignTransaction(String sessionid, String userid, String otp, String dataToSign, int docType, boolean bNotifyUser);
    public AxiomData VerifyOTPAndSignTransaction(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "otp") String otp, @WebParam(name = "rsInfo") RemoteSigningInfo rsInfo, @WebParam(name = "bNotifyUser") boolean bNotifyUser);

    @WebMethod
    public AxiomData VerifySignature(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "dataToSign") String dataToSign, @WebParam(name = "envelopedData") String envelopedData, @WebParam(name = "docType") int docType);

    //For software token >> regcode = challenge and response is Hex(2 6digit OTPs). 
    @WebMethod
    public AxiomStatus ValidateRegistration(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "challenge") String challenge, @WebParam(name = "response") String response);

    @WebMethod
    public MessageReport[] FetchStatus(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "phonNo") String phonNo, @WebParam(name = "startDate") String startDate, @WebParam(name = "endDate") String endDate) throws AxiomException;

    @WebMethod
    //public AxiomStatus initiateEpinRequest(@WebParam(name = "sessionid")String sessionid,@WebParam(name = "phoneNumber")String phoneNumber);
    public AxiomStatus initiateRequestEPIN(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "inChannel") int inChannel);

    @WebMethod
    public void AddTrailRA(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "source") String source, @WebParam(name = "type") int type, @WebParam(name = "txDetails") String[][] txDetails, @WebParam(name = "ip") String ip, @WebParam(name = "requestheaders") String[][] requestheaders, @WebParam(name = "extendsDetails") String[][] extendsDetails);

    @WebMethod
    public int IsAdditionalAuthenticationNeededRA(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid);

    @WebMethod
    public QuestionsAndAnswers GetQuestionsEPIN(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid);

    @WebMethod
    public AxiomStatus verifyAnswersAndSendEPIN(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "QandA") QuestionsAndAnswers QandA);

    @WebMethod
    public AxiomStatus DeleteUser(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid);

    @WebMethod
    public AxiomStatus ChangeUserDetails(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "fullname") String fullname, @WebParam(name = "phone") String phone, @WebParam(name = "email") String email);

    @WebMethod
    public String EnforceSecurity(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "data") String data);

    @WebMethod
    public String ConsumeSecurity(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "data") String data);

    @WebMethod
    public AxiomStatus IsUserRegistered(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid);

    @WebMethod
    public AxiomChallengeResponse getQuestionsForRegistration(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "noOfQuestion") int noOfQuestion) throws AxiomException;

    @WebMethod
    public AxiomStatus registerAnswersForUser(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "QAndA") AxiomChallengeResponse QAndA);

    @WebMethod
    public AxiomStatus registerAnswersForUserAndGenerateCertificate(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "QAndA") AxiomChallengeResponse QAndA);

    @WebMethod
    public AxiomChallengeResponse getQuestionsForValidation(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid) throws AxiomException;

    @WebMethod
    public AxiomStatus ValidateUserAnswers(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "QAndA") AxiomChallengeResponse QAndA);

    @WebMethod
    public AxiomData ValidateUserAnswersAndSignTransaction(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "QAndA") AxiomChallengeResponse QAndA, @WebParam(name = "rsInfo") RemoteSigningInfo rsInfo, @WebParam(name = "bNotifyUser") boolean bNotifyUser) throws AxiomException;

    @WebMethod
    public AxiomStatus changeAnswersForUser(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "QAndA") AxiomChallengeResponse QAndA);

    @WebMethod
    public AxiomStatus ValidateUserAnswersAndResetUser(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "QAndA") AxiomChallengeResponse QAndA, @WebParam(name = "type") int type);

    @WebMethod
    public AxiomStatus ValidateUserAnswersAndChangePassword(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "password") String password, @WebParam(name = "QAndA") AxiomChallengeResponse QAndA, @WebParam(name = "bSendOnMobile") boolean bSendOnMobile, @WebParam(name = "type") int type);

    @WebMethod
    public AxiomStatus AddTokens(String channelid, String userid, String regcode, String secrete, String serialno);

    @WebMethod
    public AxiomStatus AddOperators(String operatorid, String channelid, String name, String phone, String emailid, String passsword, int roleid, int status, int currentAttempts);

    @WebMethod
    public AxiomStatus SendRegistrationCode(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type);

    @WebMethod
    public AxiomStatus ChangeTokenType(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type, @WebParam(name = "subType") int subType);

    @WebMethod
    public AxiomStatus AssignTokenV2(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "serialNo") String serialNo, @WebParam(name = "type") int type, @WebParam(name = "subtype") int subtype);

    @WebMethod
    public AxiomMessage GetArchivedMessage(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "messageId") int messageId) throws AxiomException;

    @WebMethod
    public AxiomMessage[] SendArchivedMessage(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "msgs") AxiomMessage[] msgs, @WebParam(name = "bCheckContent") boolean bCheckContent, @WebParam(name = "speed") int speed, @WebParam(name = "type") int type) throws AxiomException;

}
