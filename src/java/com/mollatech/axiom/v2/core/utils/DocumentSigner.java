/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.axiom.v2.core.utils;

/**
 *
 * @author Manoj Sherkhane
 */
public class DocumentSigner {
     public static final int RAW_DATA = 1;
    public static final int PDF = 2;

    public int type;
    public String dataToSign;
    public String clientLocation;
    public String reason;
    public String name;
    public String designation;
    public boolean bNotifyUser;
    public boolean bReturnSignedDocument;
    public boolean bAddTimestamp;
    public String referenceId;
    public String base64ImageData;
     public String qrCodeData;
}
