package com.mollatech.axiom.v2.core.utils;

public class AxiomStatus {

    public String error;
    public int errorcode;
    public String regCodeMessage;
    public String regcode;
    public String data="";

    public AxiomStatus() {
        error = "SUCCESS";
        errorcode = 0;
        regCodeMessage = "No Applicable";
        regcode = regCodeMessage;
        data="";
    }

}
