/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.core.utils;

import java.util.Date;

/**
 *
 * @author AMOL
 */
public class EasyCheckInSessions {

    private Integer elsid;
    private String easyloginId;
    private String appSesssionId;
    private String appUserid;
    private String appChannelid;
    private String appIp;
    private Integer requesterIndex;
    private Date createdon;
    private Integer status;
    private String errormsg;
    private String browser;
    private String agent;
    private String location;
    private String lattitude;
    private String longitude;

    public Integer getElsid() {
        return elsid;
    }

    public void setElsid(Integer elsid) {
        this.elsid = elsid;
    }

    public String getEasyloginId() {
        return easyloginId;
    }

    public void setEasyloginId(String easyloginId) {
        this.easyloginId = easyloginId;
    }

    public String getAppSesssionId() {
        return appSesssionId;
    }

    public void setAppSesssionId(String appSesssionId) {
        this.appSesssionId = appSesssionId;
    }

    public String getAppUserid() {
        return appUserid;
    }

    public void setAppUserid(String appUserid) {
        this.appUserid = appUserid;
    }

    public String getAppChannelid() {
        return appChannelid;
    }

    public void setAppChannelid(String appChannelid) {
        this.appChannelid = appChannelid;
    }

    public String getAppIp() {
        return appIp;
    }

    public void setAppIp(String appIp) {
        this.appIp = appIp;
    }

    public Integer getRequesterIndex() {
        return requesterIndex;
    }

    public void setRequesterIndex(Integer requesterIndex) {
        this.requesterIndex = requesterIndex;
    }

    public Date getCreatedon() {
        return createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public void setErrormsg(String errormsg) {
        this.errormsg = errormsg;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLattitude() {
        return lattitude;
    }

    public void setLattitude(String lattitude) {
        this.lattitude = lattitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
