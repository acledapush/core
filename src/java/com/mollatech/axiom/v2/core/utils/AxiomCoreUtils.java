/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.axiom.v2.core.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

/**
 *
 * @author Human
 */
public class AxiomCoreUtils {
    public static String[] SendDataInner(String link, String[][] headers, String[][] NameValue, boolean bSecure) throws Exception {

        SSLContext sslContext;
        String[] returnStrs = null;
        try {
            HttpsURLConnection.setDefaultHostnameVerifier(new com.mollatech.internal.handler.geolocation.AllVerifier());
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, new TrustManager[]{new com.mollatech.internal.handler.geolocation.AllTrustManager()}, null);
            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
        } catch (Exception e) {
            throw e;
        }

        URL url = new URL(link);
        String dataToSend = "";
        if (NameValue != null) {
            for (int i = 0; i < NameValue.length; i++) {
                String[] pair = NameValue[i];
                if (i == 0) {
                    dataToSend = URLEncoder.encode(pair[0], "UTF-8") + "=" + URLEncoder.encode(pair[1], "UTF-8");
                } else {
                    dataToSend = dataToSend + "&" + URLEncoder.encode(pair[0], "UTF-8") + "=" + URLEncoder.encode(pair[1], "UTF-8");
                }
            }
        }

        String text = "";
        BufferedReader reader = null;
        // Send data 
        try {
            URLConnection conn = url.openConnection();
            if (headers != null) {
                for (int i = 0; i < headers.length; i++) {
                    String[] header = headers[i];
                    conn.setRequestProperty(header[0], header[1]);
                }
            }
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(dataToSend);
            wr.flush();

            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            text = sb.toString();

            String contentType = conn.getHeaderField("Content-Type");
            //if( contentType.compareTo("application/json") == true ) {
            //}

            returnStrs = new String[2];
            returnStrs[0] = text;
            returnStrs[1] = contentType;
        } catch (Exception ex) {
        } finally {
            try {
                reader.close();
            } catch (Exception ex) {
            }
        }
        return returnStrs;
    }
}
