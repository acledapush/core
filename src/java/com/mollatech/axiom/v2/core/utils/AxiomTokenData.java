/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.axiom.v2.core.utils;

/**
 *
 * @author nilesh
 */
public class AxiomTokenData {
     public int OTPLength;
     public String OTPSecret;
     public int OTPduration;
     public int PublicWebTokenExpiryTime;
     public int invalidPinAttempt;
     
     public String certificate;
     public String certificatePrivateKey;
     public String certificatePublicKey;
     public String certificatePassword;
}
