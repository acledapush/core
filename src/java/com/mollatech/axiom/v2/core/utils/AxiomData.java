package com.mollatech.axiom.v2.core.utils;

public class AxiomData {
   public String signature;
   public String sErrorMessage;
   public int iErrorCode;
   public String archiveid;
   public String publicurl;
   public String referenceid;
   
}