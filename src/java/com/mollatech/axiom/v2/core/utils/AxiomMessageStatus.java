package com.mollatech.axiom.v2.core.utils;

public class AxiomMessageStatus {
    public String number;
    public String msgid;    
    public String error;
    public int errorcode;
}
