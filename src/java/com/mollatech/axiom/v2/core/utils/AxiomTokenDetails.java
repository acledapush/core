/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.core.utils;

/**
 *
 * @author vikramsareen
 */
public class AxiomTokenDetails {

    public int category;
    public int subcategory;
    public int attempts;
    public int status;
    public String serialnumber;
    public long createOn;
    public long lastaccessOn;
}
