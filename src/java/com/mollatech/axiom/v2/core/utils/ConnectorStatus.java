/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.core.utils;

/**
 *
 * @author mollatech2
 */
public class ConnectorStatus {
    // 0 is available 
    // -1 is down
    // -2 is not configured 

    public int SMSPrimary;
    public int SMSSecondary;
    public int USSDPrimary;
    public int USSDSecondary;
    public int VOICEPrimary;
    public int VOICESecondary;
    public int EMAILPrimary;
    public int EMAILSecondary;
    public int CertificateConnector;
    public String channelid;

    public ConnectorStatus() {
        SMSPrimary = -2;
        SMSSecondary = -2;
        USSDPrimary = -2;
        USSDSecondary = -2;
        VOICEPrimary = -2;
        VOICESecondary = -2;
        EMAILPrimary = -2;
        EMAILSecondary = -2;
        CertificateConnector = -2;

    }
}
