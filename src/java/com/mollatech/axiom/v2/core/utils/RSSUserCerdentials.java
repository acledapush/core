/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.core.utils;

/**
 *
 * @author Manoj Sherkhane
 */
public class RSSUserCerdentials {

    public RSSUserDetails rssUser;
    public AxiomCredentialDetails[] tokenDetails;
    public int errorCode;
    public String errorMsg;
    public GeoCredentials geoCredentials;
    public TwoWayAuthStatus twoWayAuthStatus;
}
