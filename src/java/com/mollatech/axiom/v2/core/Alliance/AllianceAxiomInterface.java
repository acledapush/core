/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.core.Alliance;

import com.mollatech.axiom.v2.core.utils.AxiomCredentialDetails;
import com.mollatech.axiom.v2.core.utils.AxiomData;
import com.mollatech.axiom.v2.core.utils.AxiomException;
import com.mollatech.axiom.v2.core.utils.AxiomStatus;
import com.mollatech.axiom.v2.core.utils.AxiomUser;
import com.mollatech.axiom.v2.core.utils.AxiomUserCerdentials;
import com.mollatech.axiom.v2.core.utils.MobileTrustStatus;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author Pramod Chaudhari
 */
@WebService(name = "AllianceAxiom")

public interface AllianceAxiomInterface {

    public int GETUSER_BY_USERID = 1;
    public int GETUSER_BY_PHONENUMBER = 2;
    public int GETUSER_BY_EMAILID = 3;
    public int GETUSER_BY_FULLNAME = 4;
    final String itemtype = "MOBILETRUST";
    public int RESET_USER_PASSWORD = 1;
    public int RESET_USER_TOKEN_OOB = 2;
    public int RESET_USER_TOKEN_SOFTWARE = 3;
    public int RESET_USER_TOKEN_HARDWARE = 4;

    public int OTP_TOKEN_SOFTWARE = 1;
    public int OTP_TOKEN_SOFTWARE_WEB = 1;
    public int OTP_TOKEN_SOFTWARE_MOBILE = 2;
    public int OTP_TOKEN_SOFTWARE_PC = 3;

    public int OTP_TOKEN_OUTOFBAND = 3;

    public int OTP_TOKEN_HARDWARE = 2;
    public int OTP_TOKEN_OUTOFBAND_SMS = 1;
    public int OTP_TOKEN_OUTOFBAND_VOICE = 2;
    public int OTP_TOKEN_OUTOFBAND_USSD = 3;
    public int OTP_TOKEN_OUTOFBAND_EMAIL = 4;
    public int OTP_TOKEN_HARDWARE_MINI = 1;
    public int OTP_TOKEN_HARDWARE_CR = 2;
    public int TOKEN_STATUS_UNASSIGNED = -10;
    public int TOKEN_STATUS_ACTIVE = 1;
    public int TOKEN_STATUS_SUSPENDED = -2;
    public int TOKEN_STATUS_PENDING = 0;

    public String strACTIVE = "ACTIVE";
    public String strSUSPENDED = "SUSPENDED";
    public String strPENDING = "PENDING";
    public String strUNASSIGNED = "UNASSIGNED";

    public int WRONG_ATTEMPT_DESTROY = 1;
    public int INTENTIONALLY_DESTROY = 2;
    public int FORCE_DESTROY = 3;

    public String USER_ASSIGN_PASSWORD_TEMPLATE_NAME = "User Password";

    public int SEND_MESSAGE_PENDING_STATE = 2;
    public String SEND_MESSAGE_PENDING_STATE_STRING = "PENDING";

    public int SEND_MESSAGE_BLOCKED_STATE = -5;
    public String SEND_MESSAGE_BLOCKED_STATE_STRING = "BLOCKED";
    public int SEND_MESSAGE_INVALID_DATA = -4;
    public String SEND_MESSAGE_INVALID_DATA_STRING = "INVALIDDATA";
    public final int LOGIN = 2;
    // AUTHENTICATE = 1
    public final int TRANSACTION = 3;
    public final int freeByCompany = 1;
    public final int freeByUserid = 2;
    public final int freeByBoth = 3;
    // AUTHORIZE = 2

    @WebMethod
    public String OpenSession(@WebParam(name = "channelid") String channelid, @WebParam(name = "loginid") String loginid, @WebParam(name = "loginpassword") String loginpassword) throws AxiomException;

    @WebMethod
    public AxiomStatus CloseSession(@WebParam(name = "sessionid") String sessionid);

    @WebMethod
    public AxiomStatus CheckTokenAvailability(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "serialnumber") String serialnumber, @WebParam(name = "clientip") String clientip);

    @WebMethod
    public AxiomStatus ReserveToken(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "serialnumber") String serialnumber, @WebParam(name = "companyName") String companyName, @WebParam(name = "clientip") String clientip);

    @WebMethod
    public AxiomStatus FreeToken(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "serialnumber") String serialnumber, @WebParam(name = "iLevel") int level, @WebParam(name = "clientip") String clientip);

    @WebMethod
    public AxiomStatus ChangeTokenStatus(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "Category") int Category, @WebParam(name = "SubCategory") int SubCategory, @WebParam(name = "status") int status, @WebParam(name = "serialnumber") String serialnumber, @WebParam(name = "clientip") String clientip);

    @WebMethod
    public AxiomStatus CreateUser(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "AxiomUser") AxiomUser axiomUser, @WebParam(name = "clientip") String clientip);

    @WebMethod
    public AxiomStatus ChangeUserStatus(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "Userid") String Userid, @WebParam(name = "status") int status, @WebParam(name = "clientip") String clientip);

    @WebMethod
    public AxiomStatus ChangeUserDetails(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "AxiomUser") AxiomUser axiomUser, @WebParam(name = "clientip") String clientip);

    @WebMethod
    public AxiomUserCerdentials[] GetUsers(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "type") int type, @WebParam(name = "searchFor") String[] searchFor, @WebParam(name = "clientip") String clientip) throws AxiomException;

    @WebMethod
    public AxiomCredentialDetails[] GetTokens(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "clientip") String clientip) throws AxiomException;

    @WebMethod
    public AxiomStatus ResyncToken(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "catgory") int catgory, @WebParam(name = "otp1") String otp1, @WebParam(name = "otp2") String otp2, @WebParam(name = "clientip") String clientip);

    @WebMethod
    public AxiomData VerifyCredentialsAndSignTransaction(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "SOTP") String SOTP, @WebParam(name = "challenge") String challenge, @WebParam(name = "Data") String[] Data, @WebParam(name = "type") int type, @WebParam(name = "PlusFactor") String PlusFactor, @WebParam(name = "clientip") String clientip);

    @WebMethod
    public String GenerateSignatureCode(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "Data") String[] Data, @WebParam(name = "clientip") String clientip) throws AxiomException;

    @WebMethod
    public AxiomStatus ResendActivationCode(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "Category") int Category, @WebParam(name = "SubCategory") int SubCategory, @WebParam(name = "type") int type, @WebParam(name = "clientip") String clientip);

    @WebMethod  
    public AxiomStatus GetPUKCode(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "serialnumber") String serialnumber, @WebParam(name = "unlockcodeFromToken") String unlockcodeFromToken, @WebParam(name = "clientip") String clientip);

    @WebMethod
    public AxiomStatus ping();

    @WebMethod
    public MobileTrustStatus ActivateSDKToken(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "SwTokenData") String SwTokenData, @WebParam(name = "clientip") String clientip);

    @WebMethod
    public String GetTimeStamp(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "clientip") String clientip) throws AxiomException; // client encrypt(server sign(timestamp, expiry time, session id,channel id, client hardware profile))

    @WebMethod
    public AxiomStatus ALertEvent(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "signData") String signData, @WebParam(name = "clientip") String clientip);
}
