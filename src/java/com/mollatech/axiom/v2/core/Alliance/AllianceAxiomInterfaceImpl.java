//package com.mollatech.axiom.v2.core.Alliance;
//
//import com.mollatech.axiom.common.utils.UtilityFunctions;
//import com.mollatech.axiom.connector.communication.AXIOMStatus;
//import com.mollatech.axiom.connector.communication.RemoteSigningInfo;
//import com.mollatech.axiom.connector.mobiletrust.Location;
//import com.mollatech.axiom.connector.user.AuthUser;
//import com.mollatech.axiom.connector.user.AxiomChallengeResponse;
//import com.mollatech.axiom.connector.user.TokenStatusDetails;
//import com.mollatech.axiom.mobiletrust.crypto.CryptoManager;
//import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
//import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
//import com.mollatech.axiom.nucleus.crypto.LoadSettings;
//import com.mollatech.axiom.nucleus.db.Certificates;
//import com.mollatech.axiom.nucleus.db.Channels;
//import com.mollatech.axiom.nucleus.db.Countrylist;
//import com.mollatech.axiom.nucleus.db.Errormessages;
//import com.mollatech.axiom.nucleus.db.Operators;
//import com.mollatech.axiom.nucleus.db.Otptokens;
//import com.mollatech.axiom.nucleus.db.Pkitokens;
//import com.mollatech.axiom.nucleus.db.Registrationcodetrail;
//import com.mollatech.axiom.nucleus.db.Securephrase;
//import com.mollatech.axiom.nucleus.db.Sessions;
//import com.mollatech.axiom.nucleus.db.Templates;
//import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
//import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
//import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
//import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.ChallengeResponsemanagement;
//import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.CryptoManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.ErrorMessageManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.LocationManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.MobileTrustManagment;
//import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
//import static com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement.TOKEN;
//import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.PDFSigningManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.RegistrationCodeTrailManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.SecurePhraseManagment;
//import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
//import com.mollatech.axiom.nucleus.db.operation.AxiomQuestionsAndAnswers;
//import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
//import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
//import com.mollatech.axiom.nucleus.settings.MobileTrustSettings;
//import com.mollatech.axiom.nucleus.settings.RootCertificateSettings;
//import com.mollatech.axiom.nucleus.settings.SendNotification;
//import com.mollatech.axiom.nucleus.settings.TokenSettings;
//import static com.mollatech.axiom.v2.core.rss.RSSCoreInterfaceImpl.IMG_AUTH;
//import static com.mollatech.axiom.v2.core.rss.RSSCoreInterfaceImpl.OTP_TOKEN;
//import static com.mollatech.axiom.v2.core.rss.RSSCoreInterfaceImpl.PKI_TOKEN;
//import com.mollatech.axiom.v2.core.utils.AxiomCredentialDetails;
//import com.mollatech.axiom.v2.core.utils.AxiomData;
//import com.mollatech.axiom.v2.core.utils.AxiomException;
//import com.mollatech.axiom.v2.core.utils.AxiomStatus;
//import com.mollatech.axiom.v2.core.utils.AxiomTokenDetails;
//import com.mollatech.axiom.v2.core.utils.AxiomUser;
//import com.mollatech.axiom.v2.core.utils.AxiomUserCerdentials;
//import com.mollatech.axiom.v2.core.utils.MobileTrustStatus;
//import com.mollatech.axiom.v2.core.utils.RSSUtils;
//
//import java.io.ByteArrayInputStream;
//import java.security.KeyFactory;
//import java.security.KeyPair;
//import java.security.KeyStore;
//import java.security.MessageDigest;
//import java.security.PrivateKey;
//import java.security.PublicKey;
//import java.security.interfaces.RSAPrivateCrtKey;
//import java.security.spec.RSAPublicKeySpec;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.Enumeration;
//import java.util.List;
//import javax.annotation.Resource;
//import javax.jws.WebService;
//import javax.servlet.ServletContext;
//import javax.servlet.http.HttpServletRequest;
//import javax.xml.ws.WebServiceContext;
//import javax.xml.ws.handler.MessageContext;
//import org.bouncycastle.util.encoders.Base64;
//import org.hibernate.Session;
//import org.json.JSONObject;
//
//@WebService(endpointInterface = "com.mollatech.axiom.v2.core.Alliance.AllianceAxiomInterface")
//public class AllianceAxiomInterfaceImpl implements AllianceAxiomInterface {
//
//    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AllianceAxiomInterfaceImpl.class.getName());
//
//    @Resource
//    WebServiceContext wsContext;
//
//    private static String g_channelID = null;
//
//    private String GetChannelID() {
//
//        if (g_channelID == null) {
//
//            SessionFactoryUtil suChannel = null;// new SessionFactoryUtil(SessionFactoryUtil.channels);
//            Session sChannel = null; //suChannel.openSession();
//
//            try {
//                suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
//                sChannel = suChannel.openSession();
//
//                MessageContext mc = wsContext.getMessageContext();
//                ServletContext sc = (ServletContext) mc.get(MessageContext.SERVLET_CONTEXT);
//                String _channelName = sc.getContextPath();
//                _channelName = _channelName.replaceAll("/", "");
//
//                if (_channelName.compareTo("core") == 0) {
//                    _channelName = "face";
//                }
//
//                ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
//                Channels channel = cUtil.getChannel(_channelName);
//                g_channelID = channel.getChannelid();
//                sChannel.close();
//                suChannel.close();
//            } catch (Exception e) {
//                sChannel.close();
//                suChannel.close();
//                e.printStackTrace();
//            }
//        }
//
//        return g_channelID;
//    }
//
//    private AxiomStatus CheckIPAndSendAlert(Channels channel, String cip) {
//
//        AxiomStatus aStatus = new AxiomStatus();
//        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//
//        SettingsManagement setManagement = new SettingsManagement();
//
//        Object ipobj = setManagement.getSettingInner(channel.getChannelid(), SettingsManagement.GlobalSettings, 1);
//        if (ipobj != null) {
//            GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//            int checkIp = 1;
//            if (cip.compareTo("127.0.0.1") != 0) {
//                checkIp = setManagement.checkIP(channel.getChannelid(), cip);
//            } else {
//                checkIp = 1;
//            }
//            String channelid = GetChannelID();
//            if (iObj.ipstatus == 0 && checkIp != 1) {
//                if (iObj.ipalertstatus == 0) {
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                    Session sTemplate = suTemplate.openSession();
//                    TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                    Templates templatesObj = tUtil.loadbyName(channel.getChannelid(), TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                    OperatorsManagement oManagement = new OperatorsManagement();
//                    Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                    if (aOperator != null) {
//                        String[] emailList = new String[aOperator.length - 1];
//                        for (int i = 1; i < aOperator.length; i++) {
//                            emailList[i - 1] = aOperator[i].getEmailid();
//                        }
//                        if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                            ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                            String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                            String strsubject = templatesObj.getSubject();
//                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                            if (strmessageBody != null) {
//                                // Date date = new Date();
//                                strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                strmessageBody = strmessageBody.replaceAll("#filterword#", cip);
//                            }
//
//                            SendNotification send = new SendNotification();
//                            AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                    emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                        }
//
//                        suTemplate.close();
//                        sTemplate.close();
////                        aStatus.error = "INVALID IP REQUEST";
//                        aStatus.errorcode = -8;
//
//                        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(channelid, aStatus.errorcode);
//                        aStatus.error = errmsg.getUsermessage();
//                        return aStatus;
//                    }
//
//                    suTemplate.close();
//                    sTemplate.close();
////                    aStatus.error = "INVALID IP REQUEST";
//                    aStatus.errorcode = -8;
//                    Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(channelid, aStatus.errorcode);
//                    aStatus.error = errmsg.getUsermessage();
//                    return aStatus;
//                }
////                aStatus.error = "INVALID IP REQUEST";
//                aStatus.errorcode = -8;
//                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(channelid, aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                return aStatus;
//            }
//        }
//        return aStatus;
//    }
//
//    @Override
//    public String OpenSession(String channelid, String loginid, String loginpassword) {
//        try {
//            log.info("OpenSession Entered");
//
//            String strDebug = null;
//            SettingsManagement setManagement = new SettingsManagement();
//            try {
//
//                ChannelProfile channelprofileObj = null;
//                Object channelpobj = setManagement.getSettingInner(channelid, SettingsManagement.CHANNELPROFILE_SETTING, 1);
//
//                if (channelpobj == null) {
//                    LoadSettings.LoadChannelProfile(channelprofileObj);
//                } else {
//                    channelprofileObj = (ChannelProfile) channelpobj;
//                    LoadSettings.LoadChannelProfile(channelprofileObj);
//                }
//                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                    log.debug("OpenSession::loginpassword::" + loginpassword);
//                    log.debug("OpenSession::channelid::" + channelid);
//                    log.debug("OpenSession::loginid::" + loginid);
//                }
//            } catch (Exception ex) {
//            }
//
//            int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                log.debug("OpenSession::License Error::" + iResult);
//                log.info("OpenSession exiting with errr");
//
//                //throws new AxiomProtect("ValidateLicense");
//            }
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "ERROR";
//            aStatus.errorcode = -2;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            ChannelManagement cManagement = new ChannelManagement();
//            Channels channel = cManagement.getChannelByID(channelid);
//            if (channel == null) {
//                aStatus.errorcode = -102;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                return null;
//            }
//
//            MessageContext mc = wsContext.getMessageContext();
//
//            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//            //log.debug("Client IP = " + req.getRemoteAddr());
//            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//            if (ipobj != null) {
//                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                int checkIp = 1;
//                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                } else {
//                    checkIp = 1;
//                }
//                if (iObj.ipstatus == 0 && checkIp != 1) {
//                    if (iObj.ipalertstatus == 0) {
//                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                        Session sTemplate = suTemplate.openSession();
//                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                        SessionFactoryUtil suOperators = new SessionFactoryUtil(SessionFactoryUtil.operators);
//                        OperatorsManagement oManagement = new OperatorsManagement();
//                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                        if (aOperator != null) {
//                            String[] emailList = new String[aOperator.length - 1];
//                            for (int i = 1; i < aOperator.length; i++) {
//                                emailList[i - 1] = aOperator[i].getEmailid();
//                            }
//                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                String strsubject = templatesObj.getSubject();
//                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                if (strmessageBody != null) {
//                                    // Date date = new Date();
//                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                }
//
//                                SendNotification send = new SendNotification();
//                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            return null;
//                        }
//
//                        suTemplate.close();
//                        sTemplate.close();
//                        return null;
//                    }
//                    return null;
//                }
//            }
//
////            int result = setManagement.checkIP(channelid, req.getRemoteAddr());
////            if (checkIp != 1) {
////                return null;
////            }
//            String resultStr = "Failure";
//            int retValue = -1;
//            SessionManagement sManagement = new SessionManagement();
//            String sessionId = sManagement.OpenSessionForWS(channelid, loginid, loginpassword, req.getSession().getId());
//
//            AuditManagement audit = new AuditManagement();
//            if (sessionId != null) {
//                retValue = 0;
//                resultStr = "Success";
//                audit.AddAuditTrail(sessionId, channelid, loginid,
//                        req.getRemoteAddr(),
//                        channel.getName(), loginid,
//                        loginid, new Date(), "Open Session", resultStr, retValue,
//                        "Login", "", "Open Session successfully with Session Id =" + sessionId, "SESSION",
//                        loginid);
//
//            } else if (sessionId == null) {
//                audit.AddAuditTrail(sessionId, channel.getChannelid(), loginid, req.getRemoteAddr(), channel.getName(), loginid,
//                        loginid, new Date(), "Open Session", resultStr, retValue,
//                        "Login", "", "Failed To Open Session", "SESSION",
//                        loginid);
//
//            }
//
//            return sessionId;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    @Override
//    public AxiomStatus CloseSession(String sessionid) {
//        try {
//
//            String strDebug = null;
//            try {
//                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                    log.debug("CloseSession::sessionid::" + sessionid);
//                }
//            } catch (Exception ex) {
//            }
//
//            int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//                AxiomStatus aStatus = new AxiomStatus();
////                aStatus.error = "Licence is invalid";
//
//                aStatus.errorcode = -100;
//                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                return aStatus;
//            }
//
//            SessionManagement sManagement = new SessionManagement();
//            Sessions session = sManagement.getSessionById(sessionid);
//            MessageContext mc = wsContext.getMessageContext();
//            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "ERROR";
//            aStatus.errorcode = -2;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//
//            if (session != null) {
//
//                //log.debug("Client IP = " + req.getRemoteAddr());        
//                SettingsManagement setManagement = new SettingsManagement();
////                int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
////                if (result != 1) {
////                    aStatus.error = "INVALID IP REQUEST";
////                    aStatus.errorcode = -8;
////                    //  return aStatus;
////                }
//                String channelid = session.getChannelid();
//                ChannelManagement cManagement = new ChannelManagement();
//                Channels channel = cManagement.getChannelByID(channelid);
//                if (channel == null) {
//                    aStatus.errorcode = -102;
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                    aStatus.error = errmsg.getUsermessage();
//                    return null;
//                }
//                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//                if (ipobj != null) {
//                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                    int checkIp = 1;
//                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                    } else {
//                        checkIp = 1;
//                    }
//                    if (iObj.ipstatus == 0 && checkIp != 1) {
//                        if (iObj.ipalertstatus == 0) {
//                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                            Session sTemplate = suTemplate.openSession();
//                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                            OperatorsManagement oManagement = new OperatorsManagement();
//                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                            if (aOperator != null) {
//                                String[] emailList = new String[aOperator.length - 1];
//                                for (int i = 1; i < aOperator.length; i++) {
//                                    emailList[i - 1] = aOperator[i].getEmailid();
//                                }
//                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                    String strsubject = templatesObj.getSubject();
//                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                    if (strmessageBody != null) {
//                                        // Date date = new Date();
//                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                    }
//
//                                    SendNotification send = new SendNotification();
//                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                }
//
//                                suTemplate.close();
//                                sTemplate.close();
////                                aStatus.error = "INVALID IP REQUEST";
//                                aStatus.errorcode = -8;
//                                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                                aStatus.error = errmsg.getUsermessage();
//                                return aStatus;
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
////                            aStatus.error = "INVALID IP REQUEST";
//
//                            aStatus.errorcode = -8;
//                            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                            aStatus.error = errmsg.getUsermessage();
//
//                            return aStatus;
//                        }
////                        aStatus.error = "INVALID IP REQUEST";
//                        aStatus.errorcode = -8;
//                        errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                        aStatus.error = errmsg.getUsermessage();
//                        return aStatus;
//                    }
//                }
//
//                int retValue = sManagement.CloseSession(sessionid);
//
////                aStatus.error = "ERROR";
//                aStatus.errorcode = retValue;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                if (retValue == 0) {
////                    aStatus.error = "SUCCESS";
//                    aStatus.errorcode = 0;
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                    aStatus.error = errmsg.getUsermessage();
//
//                }
//
//            } else if (session == null) {
//                aStatus.errorcode = -113;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                return aStatus;
//            }
//
//            ChannelManagement cManagement = new ChannelManagement();
//            Channels channel = cManagement.getChannelByID(session.getChannelid());
//            if (channel == null) {
//                aStatus.errorcode = -102;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                return aStatus;
//            }
//            AuditManagement audit = new AuditManagement();
//            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(),
//                    session.getLoginid(), new Date(), "Close Session", errmsg.getErrorMessage(), aStatus.errorcode,
//                    "Login", "", "Close Session successfully with Session Id =" + sessionid, "SESSION",
//                    session.getLoginid());
//            return aStatus;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    @Override
//    public AxiomStatus CheckTokenAvailability(String sessionid, String serialnumber, String clientip) {
//
//        String strDebug = null;
//
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.debug("CloseSession::sessionid::" + sessionid);
//            }
//        } catch (Exception ex) {
//            log.error(ex);
//        }
//
//        int iResult = AxiomProtect.ValidateLicense();
//
//        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "Licence is invalid";
//            aStatus.errorcode = -100;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//        SessionManagement sManagement = new SessionManagement();
//        Sessions session = sManagement.getSessionById(sessionid);
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
////            AxiomStatus aStatus = new AxiomStatus();
////            aStatus.error = "ERROR";
////            aStatus.errorcode = -2;
//
//        if (session != null) {
//
//            SettingsManagement setManagement = new SettingsManagement();
//            String channelid = session.getChannelid();
//            ChannelManagement cManagement = new ChannelManagement();
//            Channels channel = cManagement.getChannelByID(channelid);
//            if (channel == null) {
//                ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//                AxiomStatus aStatus = new AxiomStatus();
////                aStatus.error = "Channel is invalid";
//                aStatus.errorcode = -102;
//                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                return aStatus;
//            }
//
//            AxiomStatus as = null;
//            as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//            if (as.errorcode != 0) {
//                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
//                as.error = errmsg.getUsermessage();
//                return as;
//            }
//
//            OTPTokenManagement otptkmgmt = new OTPTokenManagement(channelid);
//            int result = otptkmgmt.getHardwareTokenAvialable(sessionid, channelid, serialnumber);
//            if (result == 0) {
//                AxiomStatus aStatus = new AxiomStatus();
//                aStatus.errorcode = 6;
//                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
////                aStatus.error = "Hardware token is Available";
//                try {
//                    AuditManagement audit = new AuditManagement();
//                    String cip = req.getRemoteAddr();
//                    if (clientip == null || clientip.isEmpty() == true) {
//                    } else {
//                        cip = clientip;
//                    }
//                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                            cip, channel.getName(),
//                            session.getLoginid(), session.getLoginid(), new Date(),
//                            "CHECKTOKEN", "SUCCESS", aStatus.errorcode,
//                            "Token Management",
//                            "",
//                            "Serial No:" + serialnumber,
//                            "OTPTOKENS", session.getLoginid());
//                    audit = null;
//                } catch (Exception e) {
//                }
//
//                return aStatus;
//            } else {
//                AxiomStatus aStatus = new AxiomStatus();
//                aStatus.errorcode = -40;
//                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
////                aStatus.error = "Hardware token is not Available";
//                try {
//                    AuditManagement audit = new AuditManagement();
//                    String cip = req.getRemoteAddr();
//                    if (clientip == null || clientip.isEmpty() == true) {
//                    } else {
//                        cip = clientip;
//                    }
//                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                            cip, channel.getName(),
//                            session.getLoginid(), session.getLoginid(), new Date(),
//                            "CHECKTOKEN", "ERROR", aStatus.errorcode,
//                            "Token Management",
//                            "",
//                            "Serial No:" + serialnumber + ",Error:" + errmsg.getErrorMessage(),
//                            "OTPTOKENS", session.getLoginid());
//                    audit = null;
//                } catch (Exception e) {
//                }
//                return aStatus;
//            }
//        }
//        return null;
//    }
//
//    @Override
//    public AxiomStatus ReserveToken(String sessionid, String serialnumber, String companyName, String clientip) {
//
//        String strDebug = null;
//
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.debug("CloseSession::sessionid::" + sessionid);
//            }
//        } catch (Exception ex) {
//            log.error(ex);
//        }
//
//        int iResult = AxiomProtect.ValidateLicense();
//        SessionManagement sManagement = new SessionManagement();
//        Sessions session = sManagement.getSessionById(sessionid);
//        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//
//            aStatus.errorcode = -100;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
////            aStatus.error = "Licence is invalid";
//
//            return aStatus;
//        }
//
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        AxiomStatus aStatus = new AxiomStatus();
//        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////        aStatus.error = "ERROR";
//        aStatus.errorcode = -2;
//        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//        aStatus.error = errmsg.getUsermessage();
//
//        if (session != null) {
//
//            SettingsManagement setManagement = new SettingsManagement();
//            String channelid = session.getChannelid();
//            ChannelManagement cManagement = new ChannelManagement();
//            Channels channel = cManagement.getChannelByID(channelid);
//            if (channel == null) {
//                aStatus.errorcode = -102;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                return aStatus;
//            }
//
//            AxiomStatus as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
//            if (as.errorcode != 0) {
//
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
//                as.error = errmsg.getUsermessage();
//
//                return as;
//            }
//
//            //Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//            OTPTokenManagement otptkmgmt = new OTPTokenManagement(channelid);
//
//            //check if already asssigned by a company
//            int result = otptkmgmt.reserverHardwareToken(sessionid, channelid, serialnumber, companyName);
//            if (result == 0) {
//                aStatus.errorcode = 4;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
////                aStatus.error = "Token Reserve Successfully";
//                try {
//                    AuditManagement audit = new AuditManagement();
//                    String cip = req.getRemoteAddr();
//                    if (clientip == null || clientip.isEmpty() == true) {
//                    } else {
//                        cip = clientip;
//                    }
//                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                            cip, channel.getName(),
//                            session.getLoginid(), session.getLoginid(), new Date(),
//                            "RESERVETOKEN", "SUCCESS", aStatus.errorcode,
//                            "Token Management",
//                            "Serial No:" + serialnumber + ",Company:" + companyName,
//                            "Serial No:" + serialnumber + ",Company:" + companyName,
//                            "OTPTOKENS", session.getLoginid());
//                    audit = null;
//                } catch (Exception e) {
//                }
//                return aStatus;
//            } else {
//
//                aStatus.errorcode = -70;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
////                aStatus.error = "Token reservation failed";
//                try {
//                    AuditManagement audit = new AuditManagement();
//                    String cip = req.getRemoteAddr();
//                    if (clientip == null || clientip.isEmpty() == true) {
//                    } else {
//                        cip = clientip;
//                    }
//                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                            cip, channel.getName(),
//                            session.getLoginid(), session.getLoginid(), new Date(),
//                            "RESERVETOKEN", "ERROR", aStatus.errorcode,
//                            "Token Management",
//                            "Serial No:" + serialnumber + ",Company:" + companyName,
//                            "Serial No:" + serialnumber + ",Company:" + companyName + "ERROR:" + errmsg.getErrorMessage(),
//                            "OTPTOKENS", session.getLoginid());
//                    audit = null;
//                } catch (Exception e) {
//                }
//                return aStatus;
//            }
//        }
//        return null;
//    }
//
//    @Override
//    public AxiomStatus FreeToken(String sessionid, String serialnumber, int level, String clientip) {
//        try {
//            String strDebug = null;
//
//            try {
//                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                    log.debug("CloseSession::sessionid::" + sessionid);
//                }
//            } catch (Exception ex) {
//                log.error(ex);
//            }
//
//            int iResult = AxiomProtect.ValidateLicense();
//            SessionManagement sManagement = new SessionManagement();
//            Sessions session = sManagement.getSessionById(sessionid);
//            if (iResult != 0) {
//                AxiomStatus aStatus = new AxiomStatus();
//                ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////                aStatus.error = "Licence is invalid";
//                aStatus.errorcode = -100;
//                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                return aStatus;
//            }
//
//            MessageContext mc = wsContext.getMessageContext();
//            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "ERROR";
//            aStatus.errorcode = -2;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//
//            if (session != null) {
//
//                SettingsManagement setManagement = new SettingsManagement();
//                String channelid = session.getChannelid();
//                ChannelManagement cManagement = new ChannelManagement();
//                Channels channel = cManagement.getChannelByID(channelid);
//                if (channel == null) {
//                    aStatus.errorcode = -102;
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                    aStatus.error = errmsg.getUsermessage();
//                    return aStatus;
//                }
//
//                AxiomStatus as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
//                if (as.errorcode != 0) {
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
//                    as.error = errmsg.getUsermessage();
//                    return as;
//                }
//
//                OTPTokenManagement otptkmgmt = new OTPTokenManagement(channelid);
//                int result = otptkmgmt.freeHardwareToken(sessionid, channelid, level, serialnumber);
//
//                if (result == 0) {
//                    aStatus.errorcode = 7;
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                    aStatus.error = errmsg.getUsermessage();
////                    aStatus.error = "Token has been released";
//                    try {
//                        AuditManagement audit = new AuditManagement();
//                        String cip = req.getRemoteAddr();
//                        if (clientip == null || clientip.isEmpty() == true) {
//                        } else {
//                            cip = clientip;
//                        }
//                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                                cip, channel.getName(),
//                                session.getLoginid(), session.getLoginid(), new Date(),
//                                "FREETOKEN", "SUCCESS", aStatus.errorcode,
//                                "Token Management",
//                                "Serial No:" + serialnumber + ",Level:" + level,
//                                "Serial No:" + serialnumber + ",Level:" + level,
//                                "OTPTOKENS", session.getLoginid());
//                        audit = null;
//                    } catch (Exception e) {
//                    }
//                    return aStatus;
//
//                } else if (result == -395) {
//                    aStatus.errorcode = -71;
////                    aStatus.error = "Token not available or not assigned for company ";
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                    aStatus.error = errmsg.getUsermessage();
//
//                } else if (result == -396) {
//                    aStatus.errorcode = -73;
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                    aStatus.error = errmsg.getUsermessage();
//
////                    aStatus.error = "Token not available or not assigned for User ";
//                } else {
//                    aStatus.errorcode = -74;
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                    aStatus.error = errmsg.getUsermessage();
//
////                    aStatus.error = "Token released failed";
//                }
//                try {
//                    AuditManagement audit = new AuditManagement();
//                    String cip = req.getRemoteAddr();
//                    if (clientip == null || clientip.isEmpty() == true) {
//                    } else {
//                        cip = clientip;
//                    }
//                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                            cip, channel.getName(),
//                            session.getLoginid(), session.getLoginid(), new Date(),
//                            "FREETOKEN", "ERROR", aStatus.errorcode,
//                            "Token Management",
//                            "",
//                            "Serial No:" + serialnumber + ",Level:" + level + "ERROR:" + errmsg.getErrorMessage(),
//                            "OTPTOKENS", session.getLoginid());
//                    audit = null;
//                } catch (Exception e) {
//                }
//                return aStatus;
//
//            }
//        } catch (Exception ex) {
//            log.error(ex);
//        }
//
//        return null;
//
//    }
//
//    @Override
//    public AxiomStatus ChangeTokenStatus(String sessionId, String userid, int Category, int SubCategory,
//            int status, String serialnumber, String clientip) {
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.debug("ResetUser::sessionId::" + sessionId);
//                log.debug("ResetUser::userid::" + userid);
//                log.debug("ResetUser::category::" + Category);
//                log.debug("ResetUser::SubCategory::" + SubCategory);
//                log.debug("ResetUser::Status::" + status);
//                log.debug("ResetUser::Serial no::" + serialnumber);
//            }
//        } catch (Exception ex) {
//        }
//
//        int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "Licence is invalid";
//            aStatus.errorcode = -100;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        SessionManagement sManagement = new SessionManagement();
//        Sessions session = sManagement.getSessionById(sessionId);
//        AxiomStatus aStatus = new AxiomStatus();
//        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////        aStatus.error = "ERROR";
//        aStatus.errorcode = -2;
//        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//        aStatus.error = errmsg.getUsermessage();
//
//        if (session == null) {
////            aStatus.error = "Session is invalid or null!!";
//            aStatus.errorcode = -113;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//
//            return aStatus;
//
//        }
//
//        int retValue = -1;
//        String strCategory = "";
//        if (Category == OTP_TOKEN_SOFTWARE) {
//            strCategory = "OTP_TOKEN_SOFTWARE";
//        } else if (Category == OTP_TOKEN_HARDWARE) {
//            strCategory = "OTP_TOKEN_HARDWARE";
//        } else if (Category == OTP_TOKEN_OUTOFBAND) {
//            strCategory = "OTP_TOKEN_OUTOFBAND";
//        }
//        else if(Category==5)
//        {
//            strCategory="CERTIFICATE_RENEWAL";
//        }
//        String strSubCategory = "";
//
//        ChannelManagement cManagement = new ChannelManagement();
//        Channels channel = cManagement.getChannelByID(session.getChannelid());
//
//        if (channel == null) {
//            aStatus.errorcode = -102;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        String channelid = session.getChannelid();
//
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        SettingsManagement setManagement = new SettingsManagement();
//
//        AxiomStatus as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
//        if (as.errorcode != 0) {
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
//            as.error = errmsg.getUsermessage();
//            return as;
//        }
//
////        if (strCategory.equals("OTP_TOKEN_OUTOFBAND")) {
////            String strStatus = "";
////            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
////            TokenStatusDetails tokenSelected = null;
////            if (status == 0) {
////
////                TokenStatusDetails[] tokens1 = oManagement.getTokenList(sessionId, session.getChannelid(), userid);
////                if (tokens1 != null) {
////                    for (int i = 0; i < tokens1.length; i++) {
////
////                        if (tokens1[i].Catrgory == OTPTokenManagement.OOB_TOKEN) {
////                            tokenSelected = tokens1[i];
////                            break;
////                        }
////                    }
////                    if (tokenSelected != null) {
////                        if (tokenSelected.Status == 0 || tokenSelected.Status == 1) {
////                            aStatus.errorcode = -5;
////                            aStatus.error = "Desired OOB token is already assigned!!";
////                            try {
////                                AuditManagement audit = new AuditManagement();
////                                String cip = req.getRemoteAddr();
////                                if (clientip == null || clientip.isEmpty() == true) {
////                                } else {
////                                    cip = clientip;
////                                }
////                                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
////                                        cip, channel.getName(),
////                                        session.getLoginid(), session.getLoginid(), new Date(),
////                                        "RESET", "ERROR", aStatus.errorcode,
////                                        "Token Management",
////                                        "",
////                                        "Category=" + Category + ",Subcategory=" + SubCategory + ",Current Status=" + status + ",Error=" + aStatus.error,
////                                        "OTPTOKENS", userid);
////                                audit = null;
////                            } catch (Exception e) {
////                            }
////                            return aStatus;
////
////                        }
////                    } else {
////                        retValue = oManagement.AssignToken(sessionId, session.getChannelid(), userid, OTP_TOKEN_OUTOFBAND, SubCategory, userid);
////                        String regCode = oManagement.generateRegistrationCode(sessionId, session.getChannelid(), userid, OTP_TOKEN_OUTOFBAND);
////                    }
////
////                } else {
////                    retValue = oManagement.AssignToken(sessionId, session.getChannelid(), userid, OTP_TOKEN_OUTOFBAND, SubCategory, userid);
////                    String regCode = oManagement.generateRegistrationCode(sessionId, session.getChannelid(), userid, OTP_TOKEN_OUTOFBAND);
////                }
////            } else {
////                TokenStatusDetails[] tokens = oManagement.getTokenList(sessionId, session.getChannelid(), userid);
////                for (int i = 0; i < tokens.length; i++) {
////                    if (tokens[i].Catrgory == OTPTokenManagement.OOB_TOKEN) {
////                        tokenSelected = tokens[i];
////                        break;
////                    }
////                }
////
////                if (tokenSelected == null) {
////                    //return ERROR;
////                    aStatus.errorcode = -2;
////                    aStatus.error = "Desired OOB Token is not assigned";
////                    try {
////                        AuditManagement audit = new AuditManagement();
////                        String cip = req.getRemoteAddr();
////                        if (clientip == null || clientip.isEmpty() == true) {
////                        } else {
////                            cip = clientip;
////                        }
////                        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
////                                cip, channel.getName(),
////                                session.getLoginid(), session.getLoginid(), new Date(),
////                                "RESET", "ERROR", aStatus.errorcode,
////                                "Token Management",
////                                "",
////                                "Category=" + Category + ",Subcategory=" + SubCategory + ",Current Status=" + status + ",Error=" + aStatus.error,
////                                "OTPTOKENS", userid);
////                        audit = null;
////                    } catch (Exception e) {
////                    }
////                    return aStatus;
////                }
////
////                if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_SMS) {
////                    strSubCategory = "OOB__SMS_TOKEN";
////                } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_USSD) {
////                    strSubCategory = "OOB__USSD_TOKEN";
////                } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_VOICE) {
////                    strSubCategory = "OOB__VOICE_TOKEN";
////                } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_EMAIL) {
////                    strSubCategory = "OOB__EMAIL_TOKEN";
////                }
////
////                if (tokenSelected.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
////                    strStatus = "TOKEN_STATUS_ACTIVE";
////                } else if (tokenSelected.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
////                    strStatus = "TOKEN_STATUS_ASSIGNED";
////                } else if (tokenSelected.Status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
////                    strStatus = "TOKEN_STATUS_LOCKEd";
////                } else if (tokenSelected.Status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
////                    strStatus = "TOKEN_STATUS_SUSPENDED";
////                } else if (tokenSelected.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
////                    strStatus = "TOKEN_STATUS_UNASSIGNED";
////                } else if (tokenSelected.Status == OTPTokenManagement.TOKEN_STATUS_LOST) {
////                    strStatus = "TOKEN_STATUS_LOST";
////                }
////
////                retValue = oManagement.ChangeStatus(sessionId, session.getChannelid(), userid, status, OTPTokenManagement.OOB_TOKEN, tokenSelected.SubCategory);
////            }
////            if (retValue == 0) {
////                aStatus.error = "SUCCESS";
////                aStatus.errorcode = 0;
////            } else if (retValue == -4) {
////                aStatus.error = "Token is already Active";
////                aStatus.errorcode = -4;
////                aStatus.errorcode = 0;
////            } else if (retValue == -2) {
////                aStatus.error = "Session not active";
////                aStatus.errorcode = -2;
////            } else if (retValue == -20) {
////                aStatus.error = "Token Secret is null/empty or tampered!!!";
////                aStatus.errorcode = -20;
////            } else if (retValue == -6) {
////                aStatus.error = "Desired Status is same as current status!!!";
////                aStatus.errorcode = -6;
////            } else if (retValue == -3) {
////                aStatus.error = "Token not found!!!";
////                aStatus.errorcode = -3;
////            } else {
////                aStatus.error = "Failed";
////                aStatus.errorcode = -2;
////            }
////
////            TokenStatusDetails[] tokens = oManagement.getTokenList(sessionId, session.getChannelid(), userid);
////            if (tokens != null) {
////
////                for (int i = 0; i < tokens.length; i++) {
////                    if (tokens[i].Catrgory == OTPTokenManagement.OOB_TOKEN) {
////                        tokenSelected = tokens[i];
////                        break;
////                    }
////                }
////            }
////
////            String strnewStatus = "";
////
////            if (status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
////                strnewStatus = "TOKEN_STATUS_ACTIVE";
////            } else if (status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
////                strnewStatus = "TOKEN_STATUS_ASSIGNED";
////            } else if (status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
////                strnewStatus = "TOKEN_STATUS_LOCKEd";
////            } else if (status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
////                strnewStatus = "TOKEN_STATUS_SUSPENDED";
////            } else if (status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
////                strnewStatus = "TOKEN_STATUS_UNASSIGNED";
////            } else if (status == OTPTokenManagement.TOKEN_STATUS_LOST) {
////                strnewStatus = "TOKEN_STATUS_LOST";
////            }
////
////            String resultMsg = "ERROR";
////            if (aStatus.errorcode == 0) {
////                resultMsg = "SUCCESS";
////                aStatus.error = "SUCCESS";
////            }
////
////            AuditManagement audit = new AuditManagement();
////            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
////                    req.getRemoteAddr(), channel.getName(),
////                    session.getLoginid(), session.getLoginid(), new Date(),
////                    "RESET", resultMsg, aStatus.errorcode,
////                    "Token Management",
////                    "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + strStatus,
////                    "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + strnewStatus + ",Result=" + aStatus.error,
////                    "OTPTOKENS", userid);
////
////        } 
//        if (strCategory.equals("OTP_TOKEN_SOFTWARE")) {
//
////            System.out.println("in software token");
//            String strStatus = "";
//
//            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//            TokenStatusDetails tokenSelected = null;
//            if (status == 0) { // this is for ASSIGN TOKEN
////                System.out.println("status is zero");
//                TokenStatusDetails[] tokens = oManagement.getTokenList(sessionId, session.getChannelid(), userid);
//                if (tokens != null) {
//                    for (int i = 0; i < tokens.length; i++) {
//                        if (tokens[i].Catrgory == OTPTokenManagement.SOFTWARE_TOKEN) {
//                            tokenSelected = tokens[i];
//                            break;
//                        }
//                    }
//                    if (tokenSelected != null) {
//
//                        if (tokenSelected.Status == 0 || tokenSelected.Status == 1) {
//                            aStatus.errorcode = -31;
//                            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                            aStatus.error = errmsg.getUsermessage();
////                            aStatus.error = "Desired token is already assigned..!!";
//                            try {
//                                AuditManagement audit = new AuditManagement();
//                                String cip = req.getRemoteAddr();
//                                if (clientip == null || clientip.isEmpty() == true) {
//                                } else {
//                                    cip = clientip;
//                                }
//                                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
//                                        cip, channel.getName(),
//                                        session.getLoginid(), session.getLoginid(), new Date(),
//                                        "RESET", "ERROR", aStatus.errorcode,
//                                        "Token Management",
//                                        "",
//                                        "Category=" + Category + ",Subcategory=" + SubCategory + ",Current Status=" + status + ",Error=" + errmsg.getErrorMessage(),
//                                        "OTPTOKENS", userid);
//                                audit = null;
//                            } catch (Exception e) {
//                            }
//
//                            return aStatus;
//
//                        }
//                    } else {
//                        retValue = oManagement.AssignToken(sessionId, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE, SubCategory, null);
////                        System.out.println("retValue is case of existing token but different type =" + retValue);
//                        String regCode = oManagement.generateRegistrationCode(sessionId, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE);
//
//                    }
//                } else {
////                    System.out.println("retValue is case of new user=" + retValue);
//                    retValue = oManagement.AssignToken(sessionId, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE, SubCategory, null);
//                    String regCode = oManagement.generateRegistrationCode(sessionId, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE);
//
//                }
//
//            } else {
//                TokenStatusDetails[] tokens = oManagement.getTokenList(sessionId, session.getChannelid(), userid);
//
//                for (int i = 0; i < tokens.length; i++) {
//                    if (tokens[i].Catrgory == OTPTokenManagement.SOFTWARE_TOKEN) {
//                        tokenSelected = tokens[i];
//                        break;
//                    }
//                }
//
//                if (tokenSelected == null) {
//                    //return ERROR;
//                    aStatus.errorcode = -32;
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                    aStatus.error = errmsg.getUsermessage();
////                    aStatus.error = "Desired Software Token is not assigned";
//                    try {
//                        AuditManagement audit = new AuditManagement();
//                        String cip = req.getRemoteAddr();
//                        if (clientip == null || clientip.isEmpty() == true) {
//                        } else {
//                            cip = clientip;
//                        }
//                        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
//                                cip, channel.getName(),
//                                session.getLoginid(), session.getLoginid(), new Date(),
//                                "RESET", "ERROR", aStatus.errorcode,
//                                "Token Management",
//                                "",
//                                "Category=" + Category + ",Subcategory=" + SubCategory + ",Current Status=" + status + ",Error=" + errmsg.getErrorMessage(),
//                                "OTPTOKENS", userid);
//                        audit = null;
//                    } catch (Exception e) {
//                    }
//                    return aStatus;
//
//                }
//
//                if (tokenSelected.SubCategory == OTP_TOKEN_SOFTWARE_WEB) {
//                    strSubCategory = "SW_WEB_TOKEN";
//                } else if (tokenSelected.SubCategory == OTP_TOKEN_SOFTWARE_MOBILE) {
//                    strSubCategory = "SW_MOBILE_TOKEN";
//                } else if (tokenSelected.SubCategory == OTP_TOKEN_SOFTWARE_PC) {
//                    strSubCategory = "SW_PC_TOKEN";
//                }
//
//                if (tokenSelected.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
//                    strStatus = "TOKEN_STATUS_ACTIVE";
//                } else if (tokenSelected.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
//                    strStatus = "TOKEN_STATUS_ASSIGNED";
//                } else if (tokenSelected.Status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
//                    strStatus = "TOKEN_STATUS_LOCKEd";
//                } else if (tokenSelected.Status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
//                    strStatus = "TOKEN_STATUS_SUSPENDED";
//                } else if (tokenSelected.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
//                    strStatus = "TOKEN_STATUS_UNASSIGNED";
//                } else if (tokenSelected.Status == OTPTokenManagement.TOKEN_STATUS_LOST) {
//                    strStatus = "TOKEN_STATUS_LOST";
//                }
//
//                retValue = oManagement.ChangeStatus(sessionId, session.getChannelid(), userid, status, OTPTokenManagement.SOFTWARE_TOKEN, tokenSelected.SubCategory);
//
//            }
//
//            System.out.println("retValue=" + retValue);
//
//            if (retValue == 0) {
////                aStatus.error = "SUCCESS";
//                aStatus.errorcode = 0;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//            } else if (retValue == -4) {
////                aStatus.error = "software Token is already Active";
//                aStatus.errorcode = -33;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//            } else if (retValue == -2) {
////                aStatus.error = "Session not active";
//                aStatus.errorcode = -114;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//            } else if (retValue == -20) {
////                aStatus.error = "Token Secret is null/empty or tampered!!!";
//                aStatus.errorcode = -49;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//            } else if (retValue == -6) {
////                aStatus.error = "Desired Status is same as current status!!!";
//                aStatus.errorcode = -34;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//            } else if (retValue == -3) {
////                aStatus.error = "software Token not found!!!";
//                aStatus.errorcode = -35;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//            } else {
////                aStatus.error = "Failed";
//                aStatus.errorcode = -2;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//            }
//
//            TokenStatusDetails[] tokens = oManagement.getTokenList(sessionId, session.getChannelid(), userid);
//            if (tokens != null) {
//                for (int i = 0; i < tokens.length; i++) {
//                    if (tokens[i].Catrgory == OTPTokenManagement.SOFTWARE_TOKEN) {
//                        tokenSelected = tokens[i];
//                        break;
//                    }
//                }
//            }
//            String strnewStatus = "";
//
//            if (status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
//                strnewStatus = "TOKEN_STATUS_ACTIVE";
//
//            } else if (status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
//                strnewStatus = "TOKEN_STATUS_ASSIGNED";
//
//            } else if (status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
//                strnewStatus = "TOKEN_STATUS_LOCKEd";
//
//            } else if (status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
//                strnewStatus = "TOKEN_STATUS_SUSPENDED";
//
//            } else if (status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
//                strnewStatus = "TOKEN_STATUS_UNASSIGNED";
//
//            } else if (status == OTPTokenManagement.TOKEN_STATUS_LOST) {
//                strnewStatus = "TOKEN_STATUS_LOST";
//            }
//
//            if (status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
//                strnewStatus = "TOKEN_STATUS_ACTIVE";
//            } else if (status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
//                strnewStatus = "TOKEN_STATUS_ASSIGNED";
//            } else if (status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
//                strnewStatus = "TOKEN_STATUS_LOCKEd";
//            } else if (status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
//                strnewStatus = "TOKEN_STATUS_SUSPENDED";
//            } else if (status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
//                strnewStatus = "TOKEN_STATUS_UNASSIGNED";
//            } else if (status == OTPTokenManagement.TOKEN_STATUS_LOST) {
//                strnewStatus = "TOKEN_STATUS_LOST";
//            }
//
//            String resultMsg = "ERROR";
//            if (aStatus.errorcode == 0) {
//                resultMsg = "SUCCESS";
//                aStatus.error = "SUCCESS";
//
//                if (status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
//                    //added for mobile trust removal also... 
//                    MobileTrustManagment mManagment = new MobileTrustManagment();
//                    int[] retValues = mManagment.eraseMobileTrustCredentialv2(sessionId, channel.getChannelid(), userid);
//                    if (retValues == null) {
//                        AuditManagement audit = new AuditManagement();
//                        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
//                                req.getRemoteAddr(), channel.getName(),
//                                session.getLoginid(), session.getLoginid(), new Date(),
//                                "RESET", "ERROR", aStatus.errorcode,
//                                "Mobile Trust Management",
//                                "",
//                                "Reset user credentials (certificate, pki, token, geofencing, trusted device) failed",
//                                "MOBILETRUST", userid);
//                    } else {
//
//                        aStatus.errorcode = -126;
//                        errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                        aStatus.error = errmsg.getUsermessage();
////                        aStatus.error = "Mobile Trust Acccount Reset Failed (" + retValue + ")";
//                        if (retValue == -3) {
////                            aStatus.error = "Mobile Trust Acccount was not present/active";
//                            aStatus.errorcode = -127;
//                            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                            aStatus.error = errmsg.getUsermessage();
//                        }
//
//                        //  return aStatus;
//                    }
//
//                    AuditManagement audit = new AuditManagement();
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
//                            req.getRemoteAddr(), channel.getName(),
//                            session.getLoginid(), session.getLoginid(), new Date(),
//                            "RESET", errmsg.getErrorMessage(), aStatus.errorcode,
//                            "MOBILETRUST",
//                            "ALL TOKENS AND CERTIFICATE",
//                            "New Status =" + strUNASSIGNED,
//                            "MOBILETRUST", userid);
//                    //end of addition
//                }
//
//            }
//
//            AuditManagement audit = new AuditManagement();
//            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
//                    req.getRemoteAddr(), channel.getName(),
//                    session.getLoginid(), session.getLoginid(), new Date(),
//                    "RESET", resultMsg, aStatus.errorcode,
//                    "Token Management",
//                    "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + strStatus,
//                    "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + strnewStatus + ",Result=" + errmsg.getErrorMessage(),
//                    "OTPTOKENS", userid);
//
//        } else if (strCategory.equals("OTP_TOKEN_HARDWARE")) {
//            String strStatus = "";
//            TokenStatusDetails tokenSelected = null;
//            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//            if (status == 0) {
//                TokenStatusDetails[] tokens1 = oManagement.getTokenList(sessionId, session.getChannelid(), userid);
//                if (tokens1 != null) {
//                    for (int i = 0; i < tokens1.length; i++) {
//
//                        if (tokens1[i].Catrgory == OTPTokenManagement.HARDWARE_TOKEN) {
//                            tokenSelected = tokens1[i];
//                            break;
//                        }
//                    }
//                    if (tokenSelected != null) {
//
//                        if (tokenSelected.Status == 0 || tokenSelected.Status == 1) {
//                            aStatus.errorcode = -41;
//                            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                            aStatus.error = errmsg.getUsermessage();
////                            aStatus.error = "Desired hardware token is already assigned..!!";
//                            try {
//                                AuditManagement audit = new AuditManagement();
//                                String cip = req.getRemoteAddr();
//                                if (clientip == null || clientip.isEmpty() == true) {
//                                } else {
//                                    cip = clientip;
//                                }
//                                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
//                                        cip, channel.getName(),
//                                        session.getLoginid(), session.getLoginid(), new Date(),
//                                        "RESET", "ERROR", aStatus.errorcode,
//                                        "Token Management",
//                                        "",
//                                        "Category=" + Category + ",Subcategory=" + SubCategory + ",Current Status=" + status + ",Error=" + errmsg.getErrorMessage(),
//                                        "OTPTOKENS", userid);
//                                audit = null;
//                            } catch (Exception e) {
//                            }
//
//                            return aStatus;
//
//                        }
//                    } else {
//
//                        retValue = oManagement.AssignToken(sessionId, session.getChannelid(), userid, OTP_TOKEN_HARDWARE, SubCategory, serialnumber);
//                        if (retValue == -2) {
//                            retValue = -3;
//                        }
//                    }
//
//                } else {
//
//                    retValue = oManagement.AssignToken(sessionId, session.getChannelid(), userid, OTP_TOKEN_HARDWARE, SubCategory, serialnumber);
//                    if (retValue == -2) {
//                        retValue = -3;
//                    }
//                }
//
//            } else {
//
//                TokenStatusDetails[] tokens = oManagement.getTokenList(sessionId, session.getChannelid(), userid);
//
//                if (tokens != null) {
//                    for (int i = 0; i < tokens.length; i++) {
//                        if (tokens[i].Catrgory == OTPTokenManagement.HARDWARE_TOKEN) {
//                            tokenSelected = tokens[i];
//                            break;
//                        }
//                    }
//                } else {
//                    aStatus.errorcode = -44;
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                    aStatus.error = errmsg.getUsermessage();
////                    aStatus.error = "No tokens Available for this user..!!";
//                    try {
//                        AuditManagement audit = new AuditManagement();
//                        String cip = req.getRemoteAddr();
//                        if (clientip == null || clientip.isEmpty() == true) {
//                        } else {
//                            cip = clientip;
//                        }
//                        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
//                                cip, channel.getName(),
//                                session.getLoginid(), session.getLoginid(), new Date(),
//                                "RESET", "ERROR", aStatus.errorcode,
//                                "Token Management",
//                                "",
//                                "Category=" + Category + ",Subcategory=" + SubCategory + ",Current Status=" + status + ",Error=" + errmsg.getErrorMessage(),
//                                "OTPTOKENS", userid);
//                        audit = null;
//                    } catch (Exception e) {
//                    }
//
//                    return aStatus;
//
//                }
//
//                if (tokenSelected == null) {
//                    //return ERROR;
//                    aStatus.errorcode = -42;
////                    aStatus.error = "Desired Hardware Token is not assigned";
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                    aStatus.error = errmsg.getUsermessage();
//                    try {
//                        AuditManagement audit = new AuditManagement();
//                        String cip = req.getRemoteAddr();
//                        if (clientip == null || clientip.isEmpty() == true) {
//                        } else {
//                            cip = clientip;
//                        }
//                        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
//                                cip, channel.getName(),
//                                session.getLoginid(), session.getLoginid(), new Date(),
//                                "RESET", "ERROR", aStatus.errorcode,
//                                "Token Management",
//                                "",
//                                "Category=" + Category + ",Subcategory=" + SubCategory + ",Current Status=" + status + ",Error=" + errmsg.getErrorMessage(),
//                                "OTPTOKENS", userid);
//                        audit = null;
//                    } catch (Exception e) {
//                    }
//
//                    return aStatus;
//                }
//
//                if (tokenSelected.SubCategory == OTP_TOKEN_HARDWARE_CR) {
//                    strSubCategory = "HW_CR_TOKEN";
//                } else if (tokenSelected.SubCategory == OTP_TOKEN_HARDWARE_MINI) {
//                    strSubCategory = "HW_MINI_TOKEN";
//                }
//
//                if (tokenSelected.Status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
//                    strStatus = "TOKEN_STATUS_ACTIVE";
//                } else if (tokenSelected.Status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
//                    strStatus = "TOKEN_STATUS_ASSIGNED";
//                } else if (tokenSelected.Status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
//                    strStatus = "TOKEN_STATUS_LOCKEd";
//                } else if (tokenSelected.Status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
//                    strStatus = "TOKEN_STATUS_SUSPENDED";
//                } else if (tokenSelected.Status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
//                    strStatus = "TOKEN_STATUS_UNASSIGNED";
//                } else if (tokenSelected.Status == OTPTokenManagement.TOKEN_STATUS_LOST) {
//                    strStatus = "TOKEN_STATUS_LOST";
//                }
//
//                retValue = oManagement.ChangeStatus(sessionId, session.getChannelid(), userid, status, OTPTokenManagement.HARDWARE_TOKEN, tokenSelected.SubCategory);
//            }
//
//            if (retValue == 0) {
////                aStatus.error = "SUCCESS";
//                aStatus.errorcode = 0;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//            } else if (retValue == -4) {
////                aStatus.error = "Hardware Token is already Active";
//                aStatus.errorcode = -43;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
////                aStatus.errorcode = 0;
//            } else if (retValue == -2) {
////                aStatus.error = "Session not active";
//                aStatus.errorcode = -114;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//            } else if (retValue == -20) {
////                aStatus.error = "Token Secret is null/empty or tampered!!!";
//                aStatus.errorcode = -49;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//            } else if (retValue == -6) {
////                aStatus.error = "Desired Status is same as current status!!!";
//                aStatus.errorcode = -34;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//            } else if (retValue == -3) {
////                aStatus.error = "Hardware Token not found!!!";
//                aStatus.errorcode = -45;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//            } else {
////                aStatus.error = "Failed";
//                aStatus.errorcode = -2;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//            }
//
//            TokenStatusDetails[] tokens = oManagement.getTokenList(sessionId, session.getChannelid(), userid);
//            if (tokens != null) {
//
//                for (int i = 0; i < tokens.length; i++) {
//                    if (tokens[i].Catrgory == OTPTokenManagement.HARDWARE_TOKEN) {
//                        tokenSelected = tokens[i];
//                        break;
//                    }
//                }
//            }
//
//            String strnewStatus = "";
//            if (status == OTPTokenManagement.TOKEN_STATUS_ACTIVE) {
//                strnewStatus = "TOKEN_STATUS_ACTIVE";
//            } else if (status == OTPTokenManagement.TOKEN_STATUS_ASSIGNED) {
//                strnewStatus = "TOKEN_STATUS_ASSIGNED";
//            } else if (status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
//                strnewStatus = "TOKEN_STATUS_LOCKEd";
//            } else if (status == OTPTokenManagement.TOKEN_STATUS_SUSPENDED) {
//                strnewStatus = "TOKEN_STATUS_SUSPENDED";
//            } else if (status == OTPTokenManagement.TOKEN_STATUS_UNASSIGNED) {
//                strnewStatus = "TOKEN_STATUS_UNASSIGNED";
//            } else if (status == OTPTokenManagement.TOKEN_STATUS_LOST) {
//                strnewStatus = "TOKEN_STATUS_LOST";
//
//            }
//
//            String resultMsg = "ERROR";
//            if (aStatus.errorcode == 0) {
//                resultMsg = "SUCCESS";
////                aStatus.error = "SUCCESS";
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//            }
//
//            AuditManagement audit = new AuditManagement();
//            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
//                    req.getRemoteAddr(), channel.getName(),
//                    session.getLoginid(), session.getLoginid(), new Date(),
//                    "RESET", resultMsg, aStatus.errorcode,
//                    "Token Management",
//                    "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + strStatus,
//                    "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + strnewStatus + ",Result=" + errmsg.getErrorMessage(),
//                    "OTPTOKENS", userid);
//        }
//      
//        else if(strCategory.equals("CERTIFICATE_RENEWAL"))
//        {    
//            String cip = req.getRemoteAddr();
//            if (clientip == null || clientip.isEmpty() == true) {
//            } else {
//                cip = clientip;
//            }
//            Date refDate = new Date();
//            CertificateManagement certMgmt = new CertificateManagement();
//            Certificates cert = certMgmt.getCertificate1(channelid, userid);
//            if (cert != null) {
//                if (cert.getExpirydatetime().getTime() > refDate.getTime()) {
//
//                    retValue = -129;
//
//                } else {
//                    
//                    retValue = certMgmt.RenewCertificatev2(channelid, userid, cert);
//
//                }
//
//            } else {
//                retValue = -5;
//            }
//
//            if (retValue == -5) {
//                aStatus.errorcode = -7;
//                aStatus.error = "Certificate is not issued for this user!!!";
//                AuditManagement audit = new AuditManagement();
//                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
//                        cip, channel.getName(),
//                        session.getLoginid(), session.getLoginid(),
//                        new Date(), "RenewCert",
//                        errmsg.getErrorMessage(), aStatus.errorcode,
//                        "Certificate Management",
//                        "",
//                        "Certicate Renewal failed " + userid,
//                        "PKITOKEN",
//                        userid);
//            }
//           else if (retValue == 0) {
//                aStatus.error = "Success";
//                aStatus.errorcode = 0;
//                AuditManagement audit = new AuditManagement();
//                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
//                        cip, channel.getName(),
//                        session.getLoginid(), session.getLoginid(),
//                        new Date(), "RenewCert",
//                        errmsg.getErrorMessage(), aStatus.errorcode,
//                        "Certificate Management",
//                        "",
//                        "Certicate Renewed Successfully " + userid,
//                        "PKITOKEN",
//                        userid);
//            }
//          else  if (retValue == -129) {
//                aStatus.errorcode = -129;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                 aStatus.error = errmsg.getUsermessage().replace("#expirydate#", "" + cert.getExpirydatetime());
//            }else {
//
//                aStatus.errorcode = -2;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//            }
//            
//        }
//
//        return aStatus;
//
//    }
//
//    @Override
//    public AxiomStatus CreateUser(String sessionid, AxiomUser aUser, String clientip) {
//        String strDebug = null;
//
//        int bCertificateSuccessful = -1;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.debug(String.format("##CreateAPUser :" + "sessionid" + sessionid));
//                log.debug(String.format("##CreateAPUser :" + "username" + aUser.userName));
//                log.debug(String.format("##CreateAPUser :" + "phoneNo" + aUser.phoneNo));
//                log.debug(String.format("##CreateAPUser :" + "email" + aUser.email));
//                log.debug(String.format("##CreateAPUser :" + "userId" + aUser.userId));
//                log.debug(String.format("##CreateAPUser :" + "idType" + aUser.idType));
//                log.debug(String.format("##CreateAPUser :" + "location" + aUser.location));
//                log.debug(String.format("##CreateAPUser :" + "organisation" + aUser.organisation));
//                log.debug(String.format("##CreateAPUser :" + "organisationUnit" + aUser.organisationUnit));
//                log.debug(String.format("##CreateAPUser :" + "street" + aUser.street));
//                log.debug(String.format("##CreateAPUser :" + "userIdentity" + aUser.userIdentity));
//                log.debug(String.format("##CreateAPUser :" + "country" + aUser.country));
//                log.debug(String.format("##CreateAPUser :" + "designation" + aUser.designation));
////                log.debug("CreateUser::sessionId::" + sessionid);
////                log.debug("CreateUser::fullname::" + aUser.userName);
////                log.debug("CreateUser::phone::" + aUser.phoneNo);
////                log.debug("CreateUser::email::" + aUser.email);
////                log.debug("CreateUser::userid::" + aUser.userId);
//            }
//        } catch (Exception ex) {
//            log.error(String.format("##CreateAPUser : [%s] - exception caught when calling CreateAPUser() - [%s]", ex.getMessage()));
//            ex.printStackTrace();
//        }
//
//        int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "Licence is invalid";
//            aStatus.errorcode = -100;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        SessionManagement sManagement = new SessionManagement();
//        AuditManagement audit = new AuditManagement();
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        Sessions session = sManagement.getSessionById(sessionid);
//
//        AxiomStatus aStatus = new AxiomStatus();
//        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//
//        if (session == null) {
//            log.debug("Invalid Session");
////            aStatus.error = "Invalid Session";
//            aStatus.errorcode = -114;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        if (aUser.email == null || aUser.phoneNo == null
//                || aUser.userName == null || aUser.userId == null
//                || aUser.email.isEmpty() == true || aUser.phoneNo.isEmpty() == true
//                || aUser.userName.isEmpty() == true || aUser.userId.isEmpty() == true
//                || aUser.userIdentity == null
//                || aUser.country == null
//                || aUser.country.isEmpty() == true
//                || aUser.userIdentity.isEmpty() == true) {
//            AxiomStatus as = new AxiomStatus();
////             ErrorMessageManagement errmsgObj1=new ErrorMessageManagement();
////            as.error = "Invalid Data (Email,Phone, Name, UserId, Country and/or UserIdentity cannot be empty/null.";
//            as.errorcode = -11;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
//            as.error = errmsg.getUsermessage();
//            return as;
//        }
//
//        if (aUser.surName == null || aUser.surName.isEmpty() == true) {
//            aUser.surName = "";
//        }
//
//        if (aUser.idType == null || aUser.idType.isEmpty() == true) {
//            aUser.idType = "ic"; //this is hardcoded to ic even though it can be passport/ic
//        }
//        if (aUser.designation == null || aUser.designation.isEmpty() == true) {
//            aUser.designation = "";
//        }
//
//        if (aUser.organisation == null || aUser.organisation.isEmpty() == true) {
//            aUser.organisation = "";
//        }
//
//        if (aUser.organisationUnit == null || aUser.organisationUnit.isEmpty() == true) {
//            aUser.organisationUnit = "";
//        }
//
//        if (aUser.street == null || aUser.street.isEmpty() == true) {
//            aUser.street = "";
//        }
//
//        if (aUser.location == null || aUser.location.isEmpty() == true) {
//            aUser.location = "";
//        }
//        if (aUser.expiryDateForcCertMigration == null || aUser.expiryDateForcCertMigration.isEmpty() == true) {
//            aUser.expiryDateForcCertMigration = "NA";
//        }
//
//        UserManagement uManagement = new UserManagement();
//        int retValue = -2;
////        aStatus.error = "ERROR";
//        aStatus.errorcode = retValue;
//        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//        aStatus.error = errmsg.getUsermessage();
//
//        SettingsManagement setManagement = new SettingsManagement();
//        String channelid = session.getChannelid();
//        log.debug("##CreateUser" + "ChannelId" + channelid);
//        ChannelManagement cManagement = new ChannelManagement();
//        Channels channel = cManagement.getChannelByID(channelid);
//        if (channel == null) {
//            log.debug("Channel is Null");
//            aStatus.errorcode = -102;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        AxiomStatus as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
//        if (as.errorcode != 0) {
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
//            as.error = errmsg.getUsermessage();
//            return as;
//        }
//
//        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_PASSWORD) != 0) {
////            aStatus.error = "This feature is not available in this license!!!";
//            aStatus.errorcode = -103;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//
//            return aStatus;
//        }
//        int iUserCount = uManagement.getCountOfLicenseUser(channel.getChannelid());
//        int licensecount = AxiomProtect.GetUsersAllowed(); //AxiomProtect Function here now default 0. 
//        if (licensecount == -998) {
//            //unlimited licensing 
//        } else if (iUserCount >= licensecount) {
////            aStatus.error = "User Addition already reached its limit as per this license!!!";
//            aStatus.errorcode = -107;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//
//            return aStatus;
//        }
//
//        retValue = uManagement.CreateUser(sessionid, session.getChannelid(), aUser.userName, aUser.phoneNo, aUser.email, aUser.userId, 0, aUser.organisation, aUser.userIdentity, aUser.idType, aUser.organisationUnit, aUser.country, aUser.location, aUser.street, aUser.designation);
//        log.debug("##CreateUser user creation retvalue" + retValue);
//
//        if (retValue >= 0) {
//            aStatus.error = "SUCCESS";
//            aStatus.errorcode = 0;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//
//            //certificate addition code
//            CertificateManagement cerManagement = new CertificateManagement();
//            String certificate = null;
//            if (aUser.expiryDateForcCertMigration.equals("NA") == true) {
//                certificate = cerManagement.GenerateCertificateFace(sessionid, session.getChannelid(), aUser.userId);
//            } else {
//
//                certificate = cerManagement.GenerateCertificateFaceForMigration(sessionid, session.getChannelid(), aUser.userId, aUser.expiryDateForcCertMigration);
//            }
//            log.debug(" the certificate generate " + certificate);
//
//            if (certificate != null) {
//                retValue = 0;
//                String cip = req.getRemoteAddr();
//                if (clientip == null || clientip.isEmpty() == true) {
//                } else {
//                    cip = clientip;
//                }
//                log.debug("user and certificate both are created!!!! ");
//                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                        cip, channel.getName(),
//                        session.getLoginid(), session.getLoginid(),
//                        new Date(), "Assign",
//                        errmsg.getErrorMessage(), aStatus.errorcode,
//                        "Certificate Management",
//                        "",
//                        "Certicate generated successfully to user " + aUser.userName,
//                        "PKITOKEN",
//                        aUser.userId);
//
//                bCertificateSuccessful = 0;
//            } else {
//                retValue = -250;
//                aStatus.errorcode = retValue;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//
////                aStatus.error = "Certificate could not assigned!!!";
//                String cip = req.getRemoteAddr();
//                if (clientip == null || clientip.isEmpty() == true) {
//                } else {
//                    cip = clientip;
//                }
//
//                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                        cip, channel.getName(),
//                        session.getLoginid(), session.getLoginid(),
//                        new Date(), "Assign",
//                        "ERROR", aStatus.errorcode,
//                        "Certificate Management", "",
//                        "Certicate assign failed for user " + aUser.userName,
//                        "PKITOKEN", aUser.userId);
//
//                log.debug("createUser & assign certificate:User created but certificate could not assigned!!!!" + retValue);
//
//                bCertificateSuccessful = -2;
//            }
//
//            if (retValue == 1) {
////                aStatus.error = "User created succesfully but alert could not be sent!!!";
//                aStatus.errorcode = 1;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                log.debug("createUser:User created succesfully but email could not be sent!!!" + retValue);
//            }
//
//        } else if (retValue == -244) {
//
//            aStatus.errorcode = retValue;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//
//        } else if (retValue == -241) {
//            aStatus.errorcode = retValue;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//
//        } else if (retValue == -242) {
//            aStatus.errorcode = retValue;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//
//        } else if (retValue == -240) {
//            aStatus.errorcode = retValue;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        }
//
//        String strStatus = "";
//        if (aUser.status == 0) {
//            strStatus = "ACTIVE_STATUS";
//        }
//        if (aUser.status == 1) {
//            strStatus = "SUSPENDED";
//        }
//
//        if (aStatus.errorcode == 0) {
//            String cip = req.getRemoteAddr();
//            if (clientip == null || clientip.isEmpty() == true) {
//            } else {
//                cip = clientip;
//            }
//            AuditManagement a = new AuditManagement();
//            a.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                    cip, channel.getName(),
//                    session.getLoginid(),
//                    session.getLoginid(),
//                    new Date(), "Create User", "SUCCESS",
//                    aStatus.errorcode,
//                    "User Management", "",
//                    "Name=" + aUser.userName + ",Phone=" + aUser.phoneNo + ",Email=" + aUser.email + ",State=" + strStatus,
//                    "USERPASSWORD", aUser.userId);
//
//        } else {
//            String cip = req.getRemoteAddr();
//            if (clientip == null || clientip.isEmpty() == true) {
//            } else {
//                cip = clientip;
//            }
//            AuditManagement a = new AuditManagement();
//            a.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                    cip, channel.getName(),
//                    session.getLoginid(), session.getLoginid(),
//                    new Date(), "Create User", "ERROR", aStatus.errorcode,
//                    "User Management", "",
//                    "Name=" + aUser.userName + ",Phone=" + aUser.phoneNo + ",Email=" + aUser.email + ",State=" + strStatus + ",Error:" + errmsg.getErrorMessage(),
//                    "USERPASSWORD", aUser.userId);
//        }
//
//        String cip = req.getRemoteAddr();
//        if (clientip == null || clientip.isEmpty() == true) {
//        } else {
//            cip = clientip;
//        }
//
//        if (bCertificateSuccessful == -2) {
//
//            //this means that suer was created but certificate issuance failed 
//            int retValue1 = uManagement.DeleteUser(sessionid, channelid, aUser.userId);
//            log.error("DeleteUser result is =" + retValue1);
//
//            if (retValue1 == 0) {
//                AuditManagement ad = new AuditManagement();
//                ad.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                        cip, channel.getName(),
//                        session.getLoginid(), session.getLoginid(),
//                        new Date(),
//                        "Delete User(roolback)",
//                        "SUCCESS", aStatus.errorcode,
//                        "User Management",
//                        "Name=" + aUser.userName + ",userid=" + aUser.userId,
//                        "Deleted user to rollback as failed certificate issance. userid was " + aUser.userId,
//                        "USERPASSWORD",
//                        aUser.userId);
//            } else {
//                AuditManagement ad = new AuditManagement();
//                ad.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                        cip, channel.getName(),
//                        session.getLoginid(), session.getLoginid(),
//                        new Date(),
//                        "Delete User(roolback)",
//                        "ERROR",
//                        retValue1,
//                        "User Management",
//                        "Name=" + aUser.userName + ",userid=" + aUser.userId,
//                        "Deleted user to rollback as failed certificate issuance. userid is " + aUser.userId,
//                        "USERPASSWORD",
//                        aUser.userId);
//            }
//        }
//
//        return aStatus;
//
//    }
//
//    @Override
//    public AxiomStatus ChangeUserStatus(String sessionid, String Userid, int status, String clientip) {
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.debug("CreateUser::sessionId::" + sessionid);
//                log.debug("CreateUser::fullname::" + Userid);
//                log.debug("CreateUser::status::" + status);
//
//            }
//        } catch (Exception ex) {
//            log.error(ex);
//        }
//        int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "Licence is invalid";
//            aStatus.errorcode = -100;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        SessionManagement sManagement = new SessionManagement();
//        AuditManagement audit = new AuditManagement();
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        Sessions session = sManagement.getSessionById(sessionid);
//        AxiomStatus aStatus = new AxiomStatus();
//        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//
//        if (session == null) {
////            aStatus.error = "Invalid Session";
//            aStatus.errorcode = -114;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        UserManagement uManagement = new UserManagement();
//        int retValue = -2;
////        aStatus.error = "ERROR";
//        aStatus.errorcode = retValue;
//
//        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//        aStatus.error = errmsg.getUsermessage();
//
//        SettingsManagement setManagement = new SettingsManagement();
//        String channelid = session.getChannelid();
//        ChannelManagement cManagement = new ChannelManagement();
//        Channels channel = cManagement.getChannelByID(channelid);
//        if (channel == null) {
//            log.debug("Channel is Null");
//            aStatus.errorcode = -102;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//
//            return aStatus;
//        }
//
//        AxiomStatus as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
//        if (as.errorcode != 0) {
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
//            as.error = errmsg.getUsermessage();
//            return as;
//        }
//
//        String strStatus = "";
//        String cip = req.getRemoteAddr();
//        if (clientip == null || clientip.isEmpty() == true) {
//        } else {
//            cip = clientip;
//        }
//
//        int retval = -1;
//        int tokenDeletionres = 0;
//        if (status == -99) {
//
//            OTPTokenManagement otpMgt = new OTPTokenManagement(channelid);
//            Otptokens[] userTokens = otpMgt.getTokenListv2(sessionid, channelid, Userid);
//            Boolean tokenUnassigned = false;
//            if(userTokens!=null)
//            {
//            
//            for (int i = 0; i < userTokens.length; i++) {
//                if (userTokens[i].getStatus() == 1 || userTokens[i].getStatus() == 0) {
//                    tokenDeletionres = otpMgt.ChangeStatus(channelid, Userid, -10, userTokens[i].getCategory(), userTokens[i].getSubcategory());
//                    if (tokenDeletionres == 0) {
//                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(),
//                                session.getLoginid(), new Date(), "Change Token Status", "Token Unassigned Successfully", tokenDeletionres,
//                                "Token Management", "", null, itemtype,
//                                session.getLoginid());
//                        tokenUnassigned = true;
//                    }
//                    
//                }
//            }}
//            else{
//                tokenUnassigned=true;
//            }
//            
//            if (tokenUnassigned == true) {
//                CertificateManagement certMgmt = new CertificateManagement();
//                PDFSigningManagement pdfSignMgmt = new PDFSigningManagement();
//                Certificates crt = certMgmt.getCertificate1(channelid, Userid);
//                if (crt != null) {
//                    retval = certMgmt.DeleteCert(channelid, Userid);
//                }
//                else
//                {
//                    retval =0;
//                }
//                if (retval == 0) {
//                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                            cip, channel.getName(),
//                            session.getLoginid(), session.getLoginid(),
//                            new Date(), "Change Status",
//                            "Certificate Deleted Successfully!!!", retValue,
//                            "Certificate Management", "",
//                            "New Status=" + strStatus,
//                            "USERCertificate", Userid);
//                    //to update all old AUdit record
//                    
//                    
//                    retValue = audit.UpdateAuditforDeletionRecovery(session.getLoginid(), channelid, Userid);
//
//                    if (retValue == 0) {
//                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                                cip, channel.getName(),
//                                session.getLoginid(), session.getLoginid(),
//                                new Date(), "Change Status",
//                                "Deleted User Audit Updated Successfully", retValue,
//                                "Audit Management", "",
//                                "New Status=" + strStatus,
//                                "USERPASSWORD", Userid);
//                        //to update all remote signature records
//                        retValue = pdfSignMgmt.UpdateRemoteSignature(Userid, channelid);
//
//                        if (retValue == 0) {
//                            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                                    cip, channel.getName(),
//                                    session.getLoginid(), session.getLoginid(),
//                                    new Date(), "Change Status",
//                                    "Remote SIgnature for deleted user updated Successfully!!", retValue,
//                                    "PdfSigningManagement", "",
//                                    "New Status=" + retValue,
//                                    "UpdateRemoteSignature", Userid);
//                                       //to delete user from system
//                                      }
//                            retValue = uManagement.DeleteUser(sessionid,channelid, Userid);
//                            if (retValue == 0) {
//                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                                        cip, channel.getName(),
//                                        session.getLoginid(), session.getLoginid(),
//                                        new Date(), "Change Status",
//                                        aStatus.error, aStatus.errorcode,
//                                        "User Management", "",
//                                        "New Status=" + strStatus,
//                                        "USERPASSWORD", Userid);
//
//                                
//                            }else{retValue=-139;retval=retValue;}
//                        
//                    }else{retValue=-123;retval=retValue;;}//User audit updatation failed
//
//                }else{retValue=-122;retval=retValue;}//cert deletion
//
//            }
//            else
//            {
//                retValue=-121;retval=retValue;//token deletion failed
//            }
//
//        } else {
//            retval = uManagement.ChangeStatus(sessionid, channelid, Userid, status);
//        }
//
//        if (retval == 0) {
////            aStatus.error = "SUCCESS";
//            aStatus.errorcode = 0;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//
//            if (status == 1) {
//                strStatus = "ACTIVE";
//
//            }
//            if (status == -1) {
//                strStatus = "SUSPENDED";
//
//            }
//            if (status == -99) {
//                strStatus = "DELETED";
//            }
//
//            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                    cip, channel.getName(),
//                    session.getLoginid(), session.getLoginid(),
//                    new Date(), "Change Status",
//                    aStatus.error, aStatus.errorcode,
//                    "User Management", "",
//                    "New Status=" + strStatus,
//                    "USERPASSWORD", Userid);
//
//            return aStatus;
//        }else if(retValue==-121)
//        {      
//            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                    cip, channel.getName(),
//                    session.getLoginid(), session.getLoginid(),
//                    new Date(), "Change Status",
//                    aStatus.error, aStatus.errorcode,
//                    "TokenManagement", "",
//                    "New Status=" + strStatus,
//                    "USERPASSWORD", Userid);
//             aStatus.errorcode = -121;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        }
//        else if(retValue==-122)
//        {      
//            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                    cip, channel.getName(),
//                    session.getLoginid(), session.getLoginid(),
//                    new Date(), "Change Status",
//                    aStatus.error, aStatus.errorcode,
//                    "CertificateManagment", "",
//                    "New Status=" + strStatus,
//                    "USERPASSWORD", Userid);
//             aStatus.errorcode = -122;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        }
//         else if(retValue==-123)
//        {      
//            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                    cip, channel.getName(),
//                    session.getLoginid(), session.getLoginid(),
//                    new Date(), "UserAuditUpdate",
//                    aStatus.error, aStatus.errorcode,
//                    "UserManagment", "",
//                    "New Status=" + strStatus,
//                    "USERPASSWORD", Userid);
//             aStatus.errorcode = -123;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        }
//            else if(retValue==-124)
//        {      
//            
//            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                    cip, channel.getName(),
//                    session.getLoginid(), session.getLoginid(),
//                    new Date(), "RemoteSIgnatureUpdateD",
//                    aStatus.error, aStatus.errorcode,
//                    "PdfSigningManagement", "",
//                    "New Status=" + strStatus,
//                    "USERPASSWORD", Userid);
//             aStatus.errorcode = -124;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        }
//        
//        else if (retval == -10) {
////            aStatus.error = "Failed to Update Status due to assigned tokens";
//            aStatus.errorcode = -204;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//
//        } else {
////            aStatus.error = "Failed to Update Status";
//            aStatus.errorcode = -203;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        }
//
//        if (clientip == null || clientip.isEmpty() == true) {
//        } else {
//            cip = clientip;
//        }
//        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                cip, channel.getName(),
//                session.getLoginid(), session.getLoginid(),
//                new Date(), "Change Status",
//                "ERROR", aStatus.errorcode,
//                "User Management", "",
//                errmsg.getErrorMessage(),
//                "USERPASSWORD",
//                Userid);
//
//        return aStatus;
//    }
//
//    @Override
//    public AxiomStatus ChangeUserDetails(String sessionId, AxiomUser au, String clientip) {
//
//        log.info("entered ChangeUserDetails()");
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.debug("ChangeUserDetails::sessionId::" + sessionId);
//                log.debug("ChangeUserDetails::userid::" + au.userId);
//                log.debug("ChangeUserDetails::fullname::" + au.userName);
//                log.debug("ChangeUserDetails::phone::" + au.phoneNo);
//                log.debug("ChangeUserDetails::email::" + au.email);
//            }
//        } catch (Exception ex) {
//            log.error(ex);
//        }
//
//        int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "Licence is invalid";
//            aStatus.errorcode = -100;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        SessionManagement sManagement = new SessionManagement();
//        AuditManagement audit = new AuditManagement();
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        Sessions session = sManagement.getSessionById(sessionId);
//        if (session == null) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "Invalid Session";
//            aStatus.errorcode = -114;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        UserManagement uManagement = new UserManagement();
//        AxiomStatus aStatus = new AxiomStatus();
//        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//        int retValue = -2;
////        aStatus.error = "ERROR";
//        aStatus.errorcode = retValue;
//        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//        aStatus.error = errmsg.getUsermessage();
//        AuthUser oldUser = null;
//
//        SettingsManagement setManagement = new SettingsManagement();
//        String channelid = session.getChannelid();
//        ChannelManagement cManagement = new ChannelManagement();
//        Channels channel = cManagement.getChannelByID(channelid);
//        if (channel == null) {
////            aStatus.error = "Channel is null";
//            aStatus.errorcode = -102;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        AxiomStatus as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
//        if (as.errorcode != 0) {
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
//            as.error = errmsg.getUsermessage();
//            return as;
//        }
//
//        AuthUser axObj = uManagement.getUser(channelid, au.userId);
//
//        if (au.userId != null) {
//            axObj.setUserId(au.userId);
//        }
//        if (au.userName != null) {
//            axObj.setUserName(au.userName);
//        }
//        if (au.phoneNo != null) {
//            axObj.setPhoneNo(au.phoneNo);
//        }
//        if (au.email != null) {
//            axObj.setEmail(au.email);
//        }
//        if (au.organisation != null) {
//            axObj.setOrganisation(au.organisation);
//        }
//        if (au.userIdentity != null) {
//            axObj.setUserIdentity(au.userIdentity);
//        }
//        if (au.idType != null) {
//            axObj.setIdType(au.idType);
//        }
//        if (au.organisationUnit != null) {
//            axObj.setOrganisationUnit(au.organisationUnit);
//        }
//        if (au.country != null) {
//            axObj.setCountry(au.country);
//        }
//        if (au.location != null) {
//            axObj.setLocation(au.location);
//        }
//        if (au.street != null) {
//            axObj.setStreet(au.street);
//        }
//        if (au.designation != null) {
//            axObj.setDesignation(au.designation);
//        }
//
//        retValue = uManagement.EditUser(sessionId, session.getChannelid(), axObj.userId, axObj.userName, axObj.phoneNo,
//                axObj.email, 0, axObj.organisation, axObj.userIdentity, axObj.idType,
//                axObj.organisationUnit, axObj.country, axObj.location, axObj.street,
//                axObj.designation);
//        if (retValue == 0) {
////            aStatus.error = "SUCCESS";
//            aStatus.errorcode = 0;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        } else if (retValue == 1) {
////            aStatus.error = "User Details Changed Succesfully but email could not be sent!!!";
//            aStatus.errorcode = 8;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        }
//        //return aStatus;
////            oldUser = uManagement.getUser(sessionId, session.getChannelid(), au.userId);
////
////            String oldUserName = "";
////            String oldPhoneNo = "";
////            String oldEmailID = "";
////            int oldstate = -1;
////            String oldPasswordState = "";
////
////            if (oldUser != null) {
////                oldUserName = oldUser.getUserName();
////                oldPhoneNo = oldUser.getPhoneNo();
////                oldEmailID = oldUser.getEmail();
////                oldstate = oldUser.getStatus();
////                oldPasswordState = "" + oldUser.getStatePassword();
////            }
//
//        String itemtype = "USERPASSWORD";
//        String strStatus = "";
//        if (au.status == 0) {
//            strStatus = "ACTIVE_STATUS";
//        }
//        if (au.status == 1) {
//            strStatus = "SUSPENDED";
//        }
//
////            String stroldStatus = "";
////            if (oldstate == 0) {
////                stroldStatus = "ACTIVE_STATUS";
////            } else if (oldstate == 1) {
////                stroldStatus = "SUSPENDED";
////            } else {
////                stroldStatus = " ";
////            }
//        if (aStatus.errorcode == 0) {
//            String cip = req.getRemoteAddr();
//            if (clientip == null || clientip.isEmpty() == true) {
//            } else {
//                cip = clientip;
//            }
//            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
//                    cip, channel.getName(),
//                    session.getLoginid(), session.getLoginid(), new Date(), "Edit User", "SUCCESS", aStatus.errorcode,
//                    "User Management",
//                    //"User Name = " + oldUserName + ", User Phone =" + oldPhoneNo + ", User Email =" + oldEmailID + ", State=" + stroldStatus,
//                    "",
//                    au.toString(),
//                    itemtype, au.userId);
//        } else {
//            String cip = req.getRemoteAddr();
//            if (clientip == null || clientip.isEmpty() == true) {
//            } else {
//                cip = clientip;
//            }
//            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
//                    cip, channel.getName(),
//                    session.getLoginid(), session.getLoginid(), new Date(), "Edit User", "ERROR", aStatus.errorcode,
//                    "User Management",
//                    //"User Name = " + oldUserName + ", User Phone =" + oldPhoneNo + ", User Email =" + oldEmailID + ", State=" + stroldStatus,
//                    "",
//                    au.toString(),
//                    itemtype, au.userId);
//
//        }
//
//        log.info("Exited ChangeUserDetails()");
//
//        return aStatus;
//
//    }
//
//    @Override
//    public AxiomUserCerdentials[] GetUsers(String sessionid, int type, String[] searchFor, String clientip) throws AxiomException {
//
//        log.info("entered GetUsers()");
//
//        String strDebug = null;
//        List<AxiomUserCerdentials> axiomUserList = new ArrayList<AxiomUserCerdentials>();
//        AxiomUserCerdentials[] aUCredentials = null;
//        AxiomUserCerdentials AxiomUCred = new AxiomUserCerdentials();
//        AxiomStatus aStatus = new AxiomStatus();
//        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.debug("GetUserBy::sessionId::" + sessionid);
//                log.debug("GetUserBy::type::" + type);
//                log.debug("GetUserBy::searchFor::" + searchFor);
//            }
//        } catch (Exception ex) {
//        }
//
//        int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//
////            AxiomUCred.errorMsg = aStatus.error = "Licence is invalid";
//            AxiomUCred.errorCode = aStatus.errorcode = -100;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            AxiomUCred.errorMsg = aStatus.error = errmsg.getUsermessage();
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
//
//        }
//
//        UserManagement uManagement = new UserManagement();
//        SessionManagement sManagement = new SessionManagement();
//        AuditManagement audit = new AuditManagement();
//
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        Sessions session = sManagement.getSessionById(sessionid);
////            AxiomUser aUser = null;
//        if (sessionid == null) {
//            aStatus.errorcode = -113;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
////            throw new AxiomException("Session ID is empty!!!");
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
//        }
//
//        if (session == null) {
//            aStatus.errorcode = -114;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
////            throw new AxiomException("Session ID is invalid/expired!!!");
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
//        }
//
//        ChannelManagement cManagement = new ChannelManagement();
//        Channels channel = cManagement.getChannelByID(session.getChannelid());
//
//        SettingsManagement setManagement = new SettingsManagement();
//        String channelid = session.getChannelid();
//
//        ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
//        if (channelProfileObj != null) {
//            int retVal = sManagement.getServerStatus(channelProfileObj);
//            if (retVal != 0) {
//                aStatus.errorcode = -80;
//                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
////                throw new AxiomException("Channel Setting is not configured properly!!!");
//                throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
//            }
//        }
//        if (channel == null) {
//
//            aStatus.errorcode = -102;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
////                throw new AxiomException("Channel Setting is not configured properly!!!");
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
////            throw new AxiomException("Channel is null!!!");
//        }
//
//        AxiomStatus as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
//        if (as.errorcode != 0) {
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
//            as.error = errmsg.getUsermessage();
//            throw new AxiomException(as.error + " ( " + as.errorcode + ")");
//        }
//
//        String userFlag = LoadSettings.g_sSettings.getProperty("check.user");
//        AuthUser authUser = null;
//        String USERID;
//        AxiomUser axiomUser = null;
//
//        AxiomCredentialDetails[] atDetails = null;
//
//        Otptokens[] tDetails = null;
//        List<AxiomCredentialDetails> AlDetails = new ArrayList<AxiomCredentialDetails>();
//
//        List<AuthUser> aUserList = new ArrayList<AuthUser>();
//        for (int i = 0; i < searchFor.length; i++) {
//            if (userFlag.equals("1")) {
//                authUser = uManagement.CheckUserByType(sessionid, session.getChannelid(), searchFor[i], type);
//                if (authUser != null) {
//                    aUserList.add(authUser);
//                }
//            } else {
//                authUser = uManagement.CheckUserByType(sessionid, session.getChannelid(), searchFor[i], type);
//                if (authUser != null) {
//                    aUserList.add(authUser);
//                }
//            }
//        }
//
//        if (aUserList.size() == 0) {
//            String resultStr = "No Users Found!!!";
//            int retValue = -11;
//
//            String cip = req.getRemoteAddr();
//            if (clientip == null || clientip.isEmpty() == true) {
//            } else {
//                cip = clientip;
//            }
//            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                    cip, channel.getName(),
//                    session.getLoginid(), session.getLoginid(), new Date(),
//                    "GET USER", "ERROR", retValue,
//                    "User Management", "", resultStr,
//                    "USERPASSWORD", session.getLoginid());
//
//        }
//
//        for (int j = 0; j < aUserList.size(); j++) {
//            if (aUserList.get(j) != null) {
//
//                axiomUser = new AxiomUser();
//                axiomUser.phoneNo = aUserList.get(j).phoneNo;
//                axiomUser.email = aUserList.get(j).email;
//                axiomUser.userName = aUserList.get(j).userName;
//                axiomUser.userId = aUserList.get(j).userId;
//                axiomUser.lLastAccessOn = aUserList.get(j).lLastAccessOn;
//                axiomUser.lCreatedOn = aUserList.get(j).lCreatedOn;
//                axiomUser.iAttempts = aUserList.get(j).iAttempts;
//                axiomUser.statePassword = aUserList.get(j).statePassword;
//                axiomUser.organisation = aUserList.get(j).organisation;
//                axiomUser.organisationUnit = aUserList.get(j).organisationUnit;
//                axiomUser.country = aUserList.get(j).country;
//                axiomUser.location = aUserList.get(j).location;
//                axiomUser.street = aUserList.get(j).street;
//                USERID = aUserList.get(j).userId;
//
//                if (USERID != null && USERID != "") {
//                    AxiomCredentialDetails PassDetails = new AxiomCredentialDetails();
//                    PassDetails.category = AxiomCredentialDetails.PASSWORD;
//                    PassDetails.subcategory = 0;
//                    PassDetails.Password = aUserList.get(j).getPassword();
//                    PassDetails.attempts = aUserList.get(j).iAttempts;
//                    PassDetails.createOn = aUserList.get(j).lCreatedOn;
//                    PassDetails.lastaccessOn = aUserList.get(j).lLastAccessOn;
//                    PassDetails.status = aUserList.get(j).statePassword;
//                    AlDetails.add(PassDetails);
//
//                }
//
//                if (USERID != null && USERID != "") {
//
//                    OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//
////                        tDetails = oManagement.getTokenList(sessionid, session.getChannelid(), USERID);//nilesh
//                    tDetails = oManagement.getTokenListv2(sessionid, session.getChannelid(), USERID);
//                    Object settings = setManagement.getSetting(sessionid, channelid, SettingsManagement.Token, SettingsManagement.PREFERENCE_ONE);
//                    TokenSettings tokenObj = null;
//                    if (settings != null) {
//                        tokenObj = (TokenSettings) settings;
//                    }
//                    if (tDetails != null) {
//                        for (int i = 0; i < tDetails.length; i++) {
//                            AxiomCredentialDetails TokenDetail = new AxiomCredentialDetails();
//                            int res = -1;
//                            if (tDetails[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
////                                    res = oManagement.unlockTokenAfter(channelid, USERID, tokenObj, tDetails[i]);//nilesh
//                                res = oManagement.unlockTokenAfterv2(channelid, USERID, tokenObj, tDetails[i]);
//                            }
//
//                            if (tDetails[i].getCategory() == OTP_TOKEN_SOFTWARE) {
//                                TokenDetail.category = AxiomCredentialDetails.SOFTWARE_TOKEN;
//                            } else if (tDetails[i].getCategory() == OTP_TOKEN_HARDWARE) {
//                                TokenDetail.category = AxiomCredentialDetails.HARDWARE_TOKEN;
//                            } else if (tDetails[i].getCategory() == OTP_TOKEN_OUTOFBAND) {
//                                TokenDetail.category = AxiomCredentialDetails.OUTOFBOUND_TOKEN;
//                            }
//
//                            TokenDetail.subcategory = tDetails[i].getSubcategory();
//                            TokenDetail.Password = null;
//                            TokenDetail.attempts = tDetails[i].getAttempts();
//                            TokenDetail.createOn = tDetails[i].getCreationdatetime().getTime();
//                            TokenDetail.lastaccessOn = tDetails[i].getLastaccessdatetime().getTime();
//                            if (res == 0) {
//                                TokenDetail.status = OTPTokenManagement.TOKEN_STATUS_ACTIVE;
//                            } else {
//                                TokenDetail.status = tDetails[i].getStatus();
//                            }
//                            TokenDetail.serialnumber = tDetails[i].getSrno();
//                            AlDetails.add(TokenDetail);
//
//                        }
//                    }
//
//                }
//
//                TokenStatusDetails[] tSDetails = null;
//                if (USERID != null && USERID != "") {
//
//                    PKITokenManagement pManagement = new PKITokenManagement();
//
//                    tSDetails = pManagement.getTokenList(sessionid, session.getChannelid(), USERID);
//
//                    if (tSDetails != null) {
//                        for (int i = 0; i < tDetails.length; i++) {
//                            AxiomCredentialDetails TokenDetail = new AxiomCredentialDetails();
//                            if (tDetails[i].getCategory() == PKITokenManagement.SOFTWARE_TOKEN) {
//                                TokenDetail.category = AxiomCredentialDetails.PKI_SOFTWARE_TOKEN;
//                            } else if (tDetails[i].getCategory() == PKITokenManagement.HARDWARE_TOKEN) {
//                                TokenDetail.category = AxiomCredentialDetails.PKI_HARDWARE_TOKEN;
//                            }
//
//                            TokenDetail.subcategory = tDetails[i].getSubcategory();
//                            TokenDetail.Password = null;
//                            TokenDetail.attempts = tDetails[i].getAttempts();
//                            TokenDetail.createOn = tDetails[i].getCreationdatetime().getTime();
//                            TokenDetail.lastaccessOn = tDetails[i].getLastaccessdatetime().getTime();
//                            TokenDetail.status = tDetails[i].getStatus();
//                            TokenDetail.serialnumber = tDetails[i].getSrno();
//                            AlDetails.add(TokenDetail);
//
//                        }
//                    }
//
//                }
//                if (!"".equals(USERID)) {
//
//                    CertificateManagement certManagement = new CertificateManagement();
//                    Certificates cert = certManagement.getCertificate(sessionid, session.getChannelid(), USERID);
//
//                    if (cert != null) {
//
//                        AxiomCredentialDetails CertDetails = new AxiomCredentialDetails();
//                        CertDetails.category = AxiomCredentialDetails.CERTIFICATE;
//                        CertDetails.Password = cert.getPfxpassword();
//                        CertDetails.createOn = cert.getCreationdatetime().getTime();
//                        CertDetails.serialnumber = cert.getSrno();
//                        CertDetails.status = cert.getStatus();
//                        CertDetails.base64Cert = cert.getCertificate();
//                        AlDetails.add(CertDetails);
//
//                    }
//                }
//                if (!"".equals(USERID)) {
//
//                    AxiomCredentialDetails ChallengeResponseDetails = new AxiomCredentialDetails();
//                    ChallengeResponsemanagement ChalResp = new ChallengeResponsemanagement();
//                    AxiomQuestionsAndAnswers qanda = ChalResp.getUserQuestionsandAnswers(sessionid, session.getChannelid(), USERID);
//
//                    if (qanda != null) {
//                        ChallengeResponseDetails.qas = new AxiomChallengeResponse();
//                        ChallengeResponseDetails.qas.webQAndA = qanda.webQAndA;
//                        AlDetails.add(ChallengeResponseDetails);
//                    }
//                }
//
//                atDetails = new AxiomCredentialDetails[AlDetails.size()];
//                for (int i = 0; i < AlDetails.size(); i++) {
//                    atDetails[i] = AlDetails.get(i);
//                }
//
//                AxiomUCred = new AxiomUserCerdentials();
//                AxiomUCred.axiomUser = axiomUser;
//                AxiomUCred.tokenDetails = atDetails;
//            }
//            if (AxiomUCred != null) {
//                axiomUserList.add(AxiomUCred);
//            }
//        }
//
//        if (null != axiomUserList || axiomUserList.size() > 0) {
//            aUCredentials = new AxiomUserCerdentials[axiomUserList.size()];
//            for (int i = 0; i < axiomUserList.size(); i++) {
//                aUCredentials[i] = axiomUserList.get(i);
//            }
//
//            int retValue = 0;
//            String resultStr = "SUCCESS";
//            String cip = req.getRemoteAddr();
//            if (clientip == null || clientip.isEmpty() == true) {
//            } else {
//                cip = clientip;
//            }
//            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                    cip, channel.getName(),
//                    session.getLoginid(), session.getLoginid(), new Date(),
//                    "GET USER", resultStr, retValue,
//                    "User Management",
//                    "",
//                    "GET USER RETURNING COUNT:" + axiomUserList.size(),
//                    "USERPASSWORD", session.getLoginid());
//
//        }
//
//        log.info("Exit GetUsers()");
//
//        return aUCredentials;
//
//    }
//
//    @Override
//    public AxiomCredentialDetails[] GetTokens(String sessionid, String userid, String clientip) throws AxiomException {
//
//        log.info("entered GetTokens()");
//
//        String strDebug = null;
//        AxiomTokenDetails[] tokenDetailstoRet = null;
//        AxiomUserCerdentials AxiomUCred = new AxiomUserCerdentials();
//        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.debug("GetTokens::sessionId::" + sessionid);
//                log.debug("GetTokens::userid::" + userid);
//
//            }
//        } catch (Exception ex) {
//        }
//
//        int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
////            AxiomUCred.errorMsg = aStatus.error = "Licence is invalid";
//            AxiomUCred.errorCode = aStatus.errorcode = -100;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
//
////            throw new AxiomException("Licence is invalid");
//        }
//
//        AxiomStatus aStatus = new AxiomStatus();
//        String resultStr = "Failure";
//        int retValue = -1;
//
//        UserManagement uManagement = new UserManagement();
//        SessionManagement sManagement = new SessionManagement();
//        AuditManagement audit = new AuditManagement();
//
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        Sessions session = sManagement.getSessionById(sessionid);
////            AxiomUser aUser = null;
//        if (sessionid == null || session == null) {
//            aStatus.errorcode = -113;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
////            throw new AxiomException("Session ID is empty!!!");
//            throw new AxiomException(aStatus.error + " ( " + aStatus.errorcode + ")");
//
////            throw new AxiomException("Session ID is empty!!!");
//        }
//
//        ChannelManagement cManagement = new ChannelManagement();
//        Channels channel = cManagement.getChannelByID(session.getChannelid());
//
//        SettingsManagement setManagement = new SettingsManagement();
//        String channelid = session.getChannelid();
//
//        ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
//        if (channelProfileObj != null) {
//            int retVal = sManagement.getServerStatus(channelProfileObj);
//            if (retVal != 0) {
//                aStatus.errorcode = -80;
//                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
////                throw new AxiomException("Server Channel is not configured properly!!!");
//            }
//        }
//        if (channel == null) {
//            aStatus.errorcode = -102;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
////            throw new AxiomException("Channel is null!!!");
//        }
//
//        AxiomStatus as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
//        if (as.errorcode != 0) {
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
//            as.error = errmsg.getUsermessage();
//            throw new AxiomException(as.error + " ( " + as.errorcode + ")");
//        }
//
//        //String userFlag = LoadSettings.g_sSettings.getProperty("check.user");
//        AuthUser authUser = null;
//        String USERID;
//        AxiomUser axiomUser = null;
//        AxiomCredentialDetails[] atDetails = null;
//        Otptokens[] tDetails = null;
//        List<AxiomCredentialDetails> AlDetails = new ArrayList<AxiomCredentialDetails>();
//        if (userid != null && userid != "") {
//
//            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//
////                        tDetails = oManagement.getTokenList(sessionid, session.getChannelid(), USERID);//nilesh
//            Object settings = setManagement.getSetting(sessionid, channelid, SettingsManagement.Token, SettingsManagement.PREFERENCE_ONE);
//            TokenSettings tokenObj = null;
//            if (settings != null) {
//                tokenObj = (TokenSettings) settings;
//            } else {
//                aStatus.errorcode = -80;
//                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
////                throw new AxiomException("Channel Setting is not configured properly!!!");
//                throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
////                throw new AxiomException("Channel Token Settings is not properly configured!!!");
//            }
//
//            tDetails = oManagement.getTokenListv2(sessionid, session.getChannelid(), userid);
//            if (tDetails == null) {
//                aStatus.errorcode = -38;
//                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
//
////                throw new AxiomException("No OTP tokens are associated with this user!!!");
//            }
//
//            if (tDetails.length == 0) {
//
//                aStatus.errorcode = -38;
//                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
////                throw new AxiomException("No OTP tokens are associated with this user!!!");
//            }
//
//            for (int i = 0; i < tDetails.length; i++) {
//                AxiomCredentialDetails TokenDetail = new AxiomCredentialDetails();
//                int res = -1;
//                if (tDetails[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
//                    res = oManagement.unlockTokenAfterv2(channelid, userid, tokenObj, tDetails[i]);
//                }
//
//                if (tDetails[i].getCategory() == OTP_TOKEN_SOFTWARE) {
//                    TokenDetail.category = (AxiomCredentialDetails.SOFTWARE_TOKEN - 1);
//                } else if (tDetails[i].getCategory() == OTP_TOKEN_HARDWARE) {
//                    TokenDetail.category = (AxiomCredentialDetails.HARDWARE_TOKEN - 1);
//                } else if (tDetails[i].getCategory() == OTP_TOKEN_OUTOFBAND) {
//                    TokenDetail.category = (AxiomCredentialDetails.OUTOFBOUND_TOKEN - 1);
//                }
//
//                TokenDetail.subcategory = tDetails[i].getSubcategory();
//                TokenDetail.Password = null;
//                TokenDetail.attempts = tDetails[i].getAttempts();
//                TokenDetail.createOn = tDetails[i].getCreationdatetime().getTime();
//                TokenDetail.lastaccessOn = tDetails[i].getLastaccessdatetime().getTime();
//                if (res == 0) {
//                    TokenDetail.status = OTPTokenManagement.TOKEN_STATUS_ACTIVE;
//                } else {
//                    TokenDetail.status = tDetails[i].getStatus();
//                }
//                TokenDetail.serialnumber = tDetails[i].getSrno();
//                AlDetails.add(TokenDetail);
//
//            }
//
//        }
//
//        if (AlDetails.size() != 0) {
//            retValue = 0;
//            resultStr = "SUCCESS";
//            String cip = req.getRemoteAddr();
//            if (clientip == null || clientip.isEmpty() == true) {
//            } else {
//                cip = clientip;
//            }
//            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                    cip, channel.getName(),
//                    session.getLoginid(), session.getLoginid(), new Date(),
//                    "GET Tokens", resultStr, retValue,
//                    "Token Management",
//                    "",
//                    "Tokens count=" + AlDetails.size(), "OTPTOKENS", userid);
//        } else {
//            retValue = -101;
//            resultStr = "ERROR";
//            String cip = req.getRemoteAddr();
//            if (clientip == null || clientip.isEmpty() == true) {
//            } else {
//                cip = clientip;
//            }
//            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                    cip, channel.getName(),
//                    session.getLoginid(), session.getLoginid(), new Date(),
//                    "GET Tokens", resultStr, retValue,
//                    "Token Management", "",
//                    "No Tokens Found", "OTPTOKENS", userid);
//        }
//
//        AxiomCredentialDetails[] array = AlDetails.toArray(new AxiomCredentialDetails[AlDetails.size()]);
//
//        log.info("Exiting GetTokens()");
//
//        return array;
//
//    }
//
//    @Override
//    public AxiomStatus ResyncToken(String sessionid, String userid, int type, String otp1, String otp2, String clientip) {
//        try {
//
//            log.info("Entered ResyncToken()");
//
//            String strDebug = null;
//
//            try {
//                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                    log.debug("CloseSession::sessionid::" + sessionid);
//                }
//            } catch (Exception ex) {
//
//            }
//
//            int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                AxiomStatus aStatus = new AxiomStatus();
//                ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////                aStatus.error = "Licence is invalid";
//                aStatus.errorcode = -100;
//                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                return aStatus;
//            }
//
//            SessionManagement sManagement = new SessionManagement();
//            Sessions session = sManagement.getSessionById(sessionid);
//            MessageContext mc = wsContext.getMessageContext();
//            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "ERROR";
//            aStatus.errorcode = -2;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//
//            if (session == null) {
//                //AxiomStatus aStatus = new AxiomStatus();
////                aStatus.error = "session is invalid";
//                aStatus.errorcode = -114;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                return aStatus;
//            }
//
//            SettingsManagement setManagement = new SettingsManagement();
//            String channelid = session.getChannelid();
//            ChannelManagement cManagement = new ChannelManagement();
//            Channels channel = cManagement.getChannelByID(channelid);
//            if (channel == null) {
////                aStatus.error = "channel is null";
//
//                aStatus.errorcode = -102;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                return aStatus;
//            }
//
//            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//            if (ipobj == null) {
////                aStatus.error = "Channel Global Setting is null";
//
//                aStatus.errorcode = -80;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//
//                return aStatus;
//            }
//
//            AxiomStatus as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
//            if (as.errorcode != 0) {
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
//                as.error = errmsg.getUsermessage();
//                throw new AxiomException(as.error);
//            }
//
//            OTPTokenManagement otptkmgmt = new OTPTokenManagement(channelid);
//            int result = otptkmgmt.resyncOTP(sessionid, channelid, userid, type, otp1, otp2);
//            if (result == 0) {
//                aStatus.errorcode = 3;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
////                aStatus.error = "Token has been Synchronized successfully";
//                try {
//                    AuditManagement audit = new AuditManagement();
//                    String cip = req.getRemoteAddr();
//                    if (clientip == null || clientip.isEmpty() == true) {
//                    } else {
//                        cip = clientip;
//                    }
//                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                            cip, channel.getName(),
//                            session.getLoginid(), session.getLoginid(), new Date(),
//                            "RESYNC", "SUCCESS", aStatus.errorcode,
//                            "Token Management",
//                            "",
//                            errmsg.getErrorMessage() + " for userid=" + userid + " of token type=" + type,
//                            "OTPTOKENS", userid);
//                    audit = null;
//                } catch (Exception e) {
//                }
//                return aStatus;
//            } else {
//                aStatus.errorcode = -201;
////                aStatus.error = "Token synchronization failed";
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                try {
//                    AuditManagement audit = new AuditManagement();
//                    String cip = req.getRemoteAddr();
//                    if (clientip == null || clientip.isEmpty() == true) {
//                    } else {
//                        cip = clientip;
//                    }
//                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                            cip, channel.getName(),
//                            session.getLoginid(), session.getLoginid(), new Date(),
//                            "RESYNC", "ERROR", aStatus.errorcode,
//                            "Token Management",
//                            "",
//                            errmsg.getErrorMessage() + " for userid=" + userid + " of token type=" + type,
//                            "OTPTOKENS", userid);
//                    audit = null;
//
//                } catch (Exception e) {
//                }
//
//                return aStatus;
//
//            }
//
//        } catch (Exception ex) {
//            log.error(ex);
//        }
//
//        log.info("Exited ResyncToken()");
//
//        return null;
//
//    }
//
//    @Override
//    public AxiomData VerifyCredentialsAndSignTransaction(String sessionid, String userid, String SOTP, String challenge, String[] Data, int type, String plusFactor, String clientip) {
//
//        log.info("Entered VerifyCredentialsAndSignTransaction()");
//
//        String strDebug = null;
//        String strException = null;
//        String ratio = null;
//
//        //try {
//        strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//
//        if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//            log.debug("VerifyCredentialsAndSignTransaction::sessionid::" + sessionid);
//            log.debug("VerifyCredentialsAndSignTransaction::userid::" + userid);
//            log.debug("VerifyCredentialsAndSignTransaction::SOTP::" + SOTP);
//            log.debug("VerifyCredentialsAndSignTransaction::Data::" + Data);
//            log.debug("VerifyCredentialsAndSignTransaction::Data.length::" + Data.length);
//            log.debug("VerifyCredentialsAndSignTransaction::challenge::" + challenge);
//            log.debug("VerifyCredentialsAndSignTransaction::typetoVerify::" + type);
//            log.debug("VerifyCredentialsAndSignTransaction::plusFactor::" + plusFactor);
//            log.debug("VerifyCredentialsAndSignTransaction::clientip::" + clientip);
//        }
//
//        int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            AxiomData aData = new AxiomData();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aData.sErrorMessage = "Licence is invalid";
////            aData.iErrorCode = -100;
//
//            aData.iErrorCode = -100;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//            aData.sErrorMessage = errmsg.getUsermessage();
//
//            return aData;
//        }
//
//        AxiomData aData = new AxiomData();
//        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//
//        byte[] SHA1hash = null;
//
//        SessionManagement sManagement = new SessionManagement();
//        UserManagement uManagement = new UserManagement();
//
//        AuditManagement audit = new AuditManagement();
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        Sessions session = sManagement.getSessionById(sessionid);
//
//        if (session == null) {
////            aData.sErrorMessage = "Sessio is  null!! ";
//            aData.iErrorCode = -113;
//
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//            aData.sErrorMessage = errmsg.getUsermessage();
//            return aData;
//        }
//
//        int retValue = -1;
////            aData.sErrorMessage = "ERROR";
////            aData.iErrorCode = retValue;
////            if (session == null) {
////                return aData;
////            }
//        ChannelManagement cManagement = new ChannelManagement();
//        Channels channel = cManagement.getChannelByID(session.getChannelid());
//        if (channel == null) {
////            aData.sErrorMessage = "Channel is not configured or null!! ";
//            aData.iErrorCode = -102;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//            aData.sErrorMessage = errmsg.getUsermessage();
//
//            return aData;
//        }
//
//        SettingsManagement setManagement = new SettingsManagement();
//        String channelid = session.getChannelid();
//        ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
//        if (channelProfileObj == null) {
////            aData.sErrorMessage = "Channel Profile is not properly configured !!!";
//            aData.iErrorCode = -80;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//            aData.sErrorMessage = errmsg.getUsermessage();
//
//            return aData;
//        }
//
//        int retVal = sManagement.getServerStatus(channelProfileObj);
//        if (retVal != 0) {
////            aData.sErrorMessage = "Acces is not available as per channel profile restriction!!!";
//
//            aData.iErrorCode = -84;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//            aData.sErrorMessage = errmsg.getUsermessage();
//            return aData;
//        }
//
//        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//
//        AuthUser aUser = uManagement.getUser(sessionid, channel.getChannelid(), userid);
//        if (aUser == null) {
////            aData.sErrorMessage = "INVALID USER";
//            aData.iErrorCode = -12;
//
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//            aData.sErrorMessage = errmsg.getUsermessage();
//            return aData;
//        }
//
//        Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//        if (ipobj == null) {
////            aData.sErrorMessage = "Channel Global Settings is not properly configured !!!";
//            aData.iErrorCode = -83;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//            aData.sErrorMessage = errmsg.getUsermessage();
//            return aData;
//        }
//
//        AxiomStatus as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
//        if (as.errorcode != 0) {
//            AxiomData aStatus = new AxiomData();
////            aStatus.sErrorMessage = as.error;
//            aStatus.iErrorCode = as.errorcode;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
//            aData.sErrorMessage = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        //we need to check the Data with the chanlen is the data is same or not for challenge
//        String allData = "";
//        if (Data != null && Data.length != 0) {
//
//            for (int i = 0; i < Data.length; i++) {
//                allData += Data[i];
//            }
//
//            if (challenge != null && challenge.isEmpty() == false) {
//                RSSUtils rs = new RSSUtils();
//                long lChallenge = rs.CRC32CheckSum(allData.getBytes());
//                String calculatedChallenge = "" + lChallenge;
//
//                //System.out.println(calculatedChallenge);
//                //System.out.println(challenge);
//                if (calculatedChallenge.compareTo(challenge) != 0) {
//                    //challenge is not from this data so error 
//                    AxiomData aStatus = new AxiomData();
////                aStatus.sErrorMessage = "Challenge does not represent the data provided!!!";
//                    aStatus.iErrorCode = -18;
//                    Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.iErrorCode);
//                    aStatus.sErrorMessage = errmsg.getUsermessage();
//                    return aStatus;
//                }
//
//            }
//
//        } else {
////            AxiomData aStatus = new AxiomData();
////            aStatus.sErrorMessage = "Data is empty. It cannot be empty. Please recheck.";
////            aStatus.iErrorCode = -19;
////            return aStatus;
//            allData = challenge;
//        }
//
//        AxiomData aStatus = new AxiomData();
//        String cip = req.getRemoteAddr();
//        if (clientip == null || clientip.isEmpty() == true) {
//        } else {
//            cip = clientip;
//        }
//
//        if (plusFactor != null && plusFactor.isEmpty() == false) {
//            AxiomStatus aStatus1 = VerifyPlusFactor(sessionid, channel, userid, SOTP, plusFactor);
//            if (aStatus1.errorcode != 0) {
////                aStatus.sErrorMessage = aStatus1.error;
//                aStatus.iErrorCode = aStatus1.errorcode;
//                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.iErrorCode);
//                aStatus.sErrorMessage = errmsg.getUsermessage();
//                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), cip, channel.getName(),
//                        session.getLoginid(), session.getLoginid(),
//                        new Date(),
//                        "Verify Plus Component",
//                        "ERROR",
//                        aData.iErrorCode,
//                        "Mobile Trust Management",
//                        "",
//                        errmsg.getErrorMessage(),
//                        "MOBILETRUST",
//                        userid);
//
//                log.error("VerifyPlusFactor failed = " + aStatus1.errorcode);
//                log.info("Exiting VerifyCredentialsAndSignTransaction");
//                return aStatus;
//            }
//        }
//
//        if (challenge == null || challenge.isEmpty() == true) { //OTP
//            if (type != 0) // check specific type of OTP token
//            {
//                retValue = oManagement.VerifyOTPByType(channelid, userid, sessionid, SOTP, type);
//            } else //check all OTP tokens.
//            {
//                retValue = oManagement.VerifyOTP(channelid, userid, sessionid, SOTP);
//            }
//        }else{ //SOTP
//            String[] challengeData = new String[1];
//            challengeData[0] = challenge;
//
//            if (type != 0) // check specific type of OTP token
//            {
//                retValue = oManagement.VerifySignatureOTPByType(session.getChannelid(), userid, sessionid, challengeData, SOTP, type);
//            } else //check all OTP tokens.
//            {
//                retValue = oManagement.VerifySignatureOTP(session.getChannelid(), userid, sessionid, challengeData, SOTP);
//            }
//        }
//
////        if (retValue == 0) {
////                aStatus.error = "SUCCESS";
////                aStatus.errorcode = 0;
////            } else if (retValue == -22) {
////                aStatus.error = "Signature OTP Token is locked";
////                aStatus.errorcode = -22;
////            } else if (retValue == -8) {
////                aStatus.error = "Signature OTP is already consumed";
////                aStatus.errorcode = -8;
////            } else if (retValue == -17) {
////                aStatus.error = "User/Token is not found";
////                aStatus.errorcode = -17;
////            } else if (retValue == -9) {
////                aStatus.error = "Signature OTP is expired";
////                aStatus.errorcode = -9;
////            } else if (retValue == -2) {
////                aStatus.error = "Session has expired";
////                aStatus.errorcode = -2;
////            } else if (retValue == -1) {
////                aStatus.error = "General Error";
////                aStatus.errorcode = -1;
////            }
//        if (retValue == 0) {
////            aData.sErrorMessage = "SUCCESS";
//            aData.iErrorCode = 0;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//            aData.sErrorMessage = errmsg.getUsermessage();
//        } else if (retValue == -9) {
////            aData.sErrorMessage = "One Time Password/Signature One Time Password was expired";
//            aData.iErrorCode = -9;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//            aData.sErrorMessage = errmsg.getUsermessage();
//        } else if (retValue == -2) {
////            aData.sErrorMessage = "Internal Error";
//            aData.iErrorCode = -2;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//            aData.sErrorMessage = errmsg.getUsermessage();
//        } else if (retValue == -14) {
//            aData.sErrorMessage = "Invalid Session";
//            aData.iErrorCode = -114;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//            aData.sErrorMessage = errmsg.getUsermessage();
//        } else if (retValue == -8) {
////            aData.sErrorMessage = "One Time Password/Signature One Time Password was expired";
//            aData.iErrorCode = -9;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//            aData.sErrorMessage = errmsg.getUsermessage();
//        } else if (retValue == -17) {
////            aData.sErrorMessage = "User/Token is not found!!!";
//
//            aData.iErrorCode = -17;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//            aData.sErrorMessage = errmsg.getUsermessage();
//        } else if (retValue == -22) {
////            aData.sErrorMessage = "Token is locked!!!";
//            aData.iErrorCode = -22;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//            aData.sErrorMessage = errmsg.getUsermessage();
//        } else if (retValue == -1) {
//            aData.iErrorCode = -97;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//            aData.sErrorMessage = errmsg.getUsermessage();
//        }
//
//        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), cip,
//                channel.getName(),
//                session.getLoginid(), session.getLoginid(),
//                new Date(),
//                "VERIFY OTP/SIGNATURE OTP", "ERROR", aData.iErrorCode,
//                "Token Management",
//                "",
//                "OTP/Siganture OTP=" + SOTP + " for challenge =" + challenge + " failed with error=" + errmsg.getErrorMessage(),
//                "OTPTOKENS", userid);
//
//        if (retValue != 0) {
//            log.error("VerifySignatureOTP/VerifyOTP failed = " + retValue);
//            log.info("Exiting VerifyCredentialsAndSignTransaction");
//            return aData;
//        }
//
//        RemoteSigningInfo rsInfo = new RemoteSigningInfo();
//        int result = -1;
//
//        CertificateManagement certManagement = new CertificateManagement();
//        Certificates cert = certManagement.getCertificate(sessionid, session.getChannelid(), userid);
//
//        if (cert == null) {
////            aData.sErrorMessage = "Certificate is not issued to the user!!";
//            aData.iErrorCode = -7;
//
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//            aData.sErrorMessage = errmsg.getUsermessage();
//            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), cip, channel.getName(),
//                    session.getLoginid(), session.getLoginid(),
//                    new Date(),
//                    "Sign Transaction",
//                    "ERROR",
//                    aData.iErrorCode,
//                    "Certificate Management",
//                    "",
//                    errmsg.getErrorMessage(),
//                    "PKITOKEN", userid);
//
//            log.error("getCertificate failed = -7");
//            log.info("Exiting VerifyCredentialsAndSignTransaction");
//
//            return aData;
//        }
//
//        String pfxFile = cert.getPfx();
//        String pfxPassword = cert.getPfxpassword();
//
//        if (pfxFile == null || pfxPassword == null) {
////            aData.sErrorMessage = "Certificate is not present for this user!!";
//            aData.iErrorCode = -6;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//            aData.sErrorMessage = errmsg.getUsermessage();
//
//            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), cip, channel.getName(),
//                    session.getLoginid(), session.getLoginid(),
//                    new Date(),
//                    "Sign Transaction",
//                    "ERROR",
//                    aData.iErrorCode,
//                    "Certificate Management",
//                    "",
//                    errmsg.getErrorMessage(),
//                    "PKITOKEN", userid);
//
//            log.error("getPfx failed = -6");
//            log.info("Exiting VerifyCredentialsAndSignTransaction");
//
//            return aData;
//
//        }
//
//        CryptoManagement cyManagement = new CryptoManagement();
//        //boolean sResult = false;
//        String signature = null;
//
//        //this is raw signing...
//        rsInfo.type = RemoteSigningInfo.RAW_DATA;
//        rsInfo.dataToSign = allData;
//        rsInfo.clientLocation = null;
//        rsInfo.reason = null;
//        rsInfo.name = null;
//        rsInfo.designation = null;
//        rsInfo.qrCodeData = null;
//        rsInfo.bNotifyUser = false;
//        rsInfo.bReturnSignedDocument = false;
//        rsInfo.bAddTimestamp = false;
//        rsInfo.referenceId = null;
//        String referenceid = null;
//        String plain = userid + session.getChannelid() + new Date();
//        String archiveid = UtilityFunctions.Bas64SHA1(plain);
//
//        String refData = plain + archiveid + new Date();
//        referenceid = UtilityFunctions.Bas64SHA1(refData);
//
//        if (rsInfo.type == RemoteSigningInfo.RAW_DATA) {
//            String strSignApproach = LoadSettings.g_sSettings.getProperty("pkcs.signature.version");
//            if (strSignApproach != null && strSignApproach.isEmpty() == false) {
//                if (strSignApproach.compareToIgnoreCase("pkcs7") == 0) {
//                    //PKCS7 enveloped sgnature with data and certificate and signature
//                    signature = cyManagement.SignDataPKCS7(rsInfo.dataToSign, pfxFile, pfxPassword);
//                } else {
//
//                    signature = cyManagement.SignData(rsInfo.dataToSign, pfxFile, pfxPassword); // signData();
//                }
//            } else {
//                //default do PKCS 1
//                signature = cyManagement.SignData(rsInfo.dataToSign, pfxFile, pfxPassword); // signData();
//            }
//
//            if (signature != null) {
//                result = 0;
//                aData.archiveid = archiveid;
//                aData.signature = signature;
//                aData.referenceid = referenceid;
////                aData.sErrorMessage = "SUCCESS";
//                aData.iErrorCode = 0;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//                aData.sErrorMessage = errmsg.getUsermessage();
//
//                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                        cip, channel.getName(),
//                        session.getLoginid(), session.getLoginid(),
//                        new Date(),
//                        "Sign Transaction",
//                        "SUCCESS",
//                        aData.iErrorCode,
//                        "Certificate Management",
//                        "",
//                        "Challenge=" + challenge + ",Archiveid=" + aData.archiveid + ", Referenceid="
//                        + aData.referenceid,
//                        "PKITOKEN", userid);
//
//                //added for dumping into remote service  signature 
//                PDFSigningManagement pManagement = new PDFSigningManagement();
//                retValue = pManagement.addRemoteSignature(sessionid, session.getChannelid(),
//                        userid,
//                        archiveid,
//                        SOTP,
//                        0,
//                        new Date(),
//                        rsInfo.type,
//                        rsInfo.dataToSign,
//                        signature,
//                        0, //success 
//                        new Date(),
//                        referenceid);
//                //end of addition
//
//                log.debug("archiveid = " + archiveid + " and referenceid=" + referenceid + " and result is " + result);
//
//                log.info("Exiting VerifyCredentialsAndSignTransaction");
//
//                return aData;
//
//            } else {
//                result = -11;
//                aData.archiveid = archiveid;
//                aData.signature = "";
//                aData.referenceid = referenceid;
////                aData.sErrorMessage = "ERROR";
//                aData.iErrorCode = -2;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//                aData.sErrorMessage = errmsg.getUsermessage();
//
//                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                        cip, channel.getName(),
//                        session.getLoginid(), session.getLoginid(),
//                        new Date(),
//                        "Sign Transaction",
//                        "ERROR",
//                        aData.iErrorCode,
//                        "Certificate Management",
//                        "",
//                        "Challenge=" + challenge + ",Archiveid=" + aData.archiveid + ", Referenceid="
//                        + aData.referenceid,
//                        "PKITOKEN", userid);
//
//                //added for dumping into remote service  signature 
//                PDFSigningManagement pManagement = new PDFSigningManagement();
//                retValue = pManagement.addRemoteSignature(sessionid, session.getChannelid(),
//                        userid, archiveid,
//                        SOTP,
//                        0,
//                        new Date(),
//                        rsInfo.type,
//                        rsInfo.dataToSign,
//                        signature,
//                        result, //success 
//                        new Date(),
//                        referenceid);
//                //end of addition
//
//                log.debug("archiveid = " + archiveid + " and referenceid=" + referenceid + " and error  result is " + result);
//
//                log.info("Exiting VerifyCredentialsAndSignTransaction");
//
//                return aData;
//
//            }
//        } else {
//            aData.iErrorCode = -13;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//            aData.sErrorMessage = errmsg.getUsermessage();
//            log.info("Exiting VerifyCredentialsAndSignTransaction");
//            return aData;
//        }
//    }
//
//    @Override
//    public String GenerateSignatureCode(String sessionid, String userid,
//            String[] Data, String clientip) throws AxiomException {
//
//        log.info("Exiting VerifyCredentialsAndSignTransaction");
//
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.debug("GenerateSignatureCode::sessionid::" + sessionid);
//                log.debug("GenerateSignatureCode::userid::" + userid);
//            }
//        } catch (Exception ex) {
//        }
//
//        int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "Licence invalid";
//            aStatus.errorcode = -100;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
//        }
//
//        if (sessionid == null) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//            aStatus.errorcode = -113;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
////            throw new AxiomException("Invalid Parameters!!!");
//        }
//
//        if (Data == null) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//            aStatus.errorcode = -67;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
////            throw new AxiomException("Data is empty.");
//        }
//
//        if (Data.length == 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//            aStatus.errorcode = -68;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
////            throw new AxiomException("Data Arrray is empty.");
//        }
//
//        AxiomStatus aStatus = new AxiomStatus();
//        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//        int retValue = -10;
////        aStatus.error = "Invalid Parameters!!!";
//        aStatus.errorcode = -66;
//
//        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//        aStatus.error = errmsg.getUsermessage();
//
//        String strData = "";
//        for (int i = 0; i < Data.length; i++) {
//            strData += Data[i];
//        }
//
//        SessionManagement sManagement = new SessionManagement();
//        AuditManagement audit = new AuditManagement();
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        Sessions session = sManagement.getSessionById(sessionid);
////        aStatus.error = "ERROR";
//        aStatus.errorcode = -2;
//        errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//        aStatus.error = errmsg.getUsermessage();
//        if (session == null) {
////            aStatus.error = "Invalid Session";
//            aStatus.errorcode = -114;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
//        }
//        ChannelManagement cManagement = new ChannelManagement();
//        Channels channel = cManagement.getChannelByID(session.getChannelid());
//        if (channel == null) {
////            aStatus.error = "Invalid Channel";
//            aStatus.errorcode = -102;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
////            throw new AxiomException("Invalid Channel");
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
//        }
//
//        SettingsManagement setManagement = new SettingsManagement();
//        String channelid = session.getChannelid();
//        ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
//        if (channelProfileObj != null) {
//            int retVal = sManagement.getServerStatus(channelProfileObj);
//            if (retVal != 0) {
//
//                aStatus = new AxiomStatus();
////                aStatus.error = "Channel Profile is not properly configured.";
//
//                aStatus.errorcode = -82;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
//
//            }
//        }
//
//        Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//        if (ipobj == null) {
////            throw new AxiomException("Channel Global Setting is not configured properly");
//            aStatus.errorcode = -83;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
////                throw new AxiomException("Channel Setting is not configured properly!!!");
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
//        }
//
//        AxiomStatus as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
//        if (as.errorcode != 0) {
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
//            as.error = errmsg.getUsermessage();
//            throw new AxiomException(as.error);
//        }
//
//        String concatData = "";
//        for (int i = 0; i < Data.length; i++) {
//            concatData += Data[i];
//        }
//
//        long signaturecode = 00;
//        RSSUtils rUtils = new RSSUtils();
//        signaturecode = rUtils.CRC32CheckSum(concatData.getBytes());
//
//        String signature = "" + signaturecode;
//
//        String cip = req.getRemoteAddr();
//        if (clientip == null || clientip.isEmpty() == true) {
//        } else {
//            cip = clientip;
//        }
//
//        aStatus.errorcode = 0;
//
//        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                cip, channel.getName(),
//                session.getLoginid(),
//                session.getLoginid(),
//                new Date(), "Generate Signature Code",
//                "SUCCESS", aStatus.errorcode,
//                "Security Management",
//                "",
//                errmsg.getErrorMessage(),
//                "PKITOKEN", userid);
//
//        return signature;
//    }
//
//    @Override
//    public AxiomStatus ResendActivationCode(String sessionid, String userid, int Category, int SubCategory, int type, String clientip) {
//
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.debug("sendNotification::sessionid::" + sessionid);
//                log.debug("sendNotification::userid::" + userid);
//                log.debug("sendNotification::type::" + type);
//
//            }
//        } catch (Exception ex) {
//        }
//
//        int iResult = AxiomProtect.ValidateLicense();
//
//        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "Licence is invalid";
//            aStatus.errorcode = -100;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        AxiomStatus aStatus = new AxiomStatus();
//        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//
//        SessionManagement sManagement = new SessionManagement();
//        UserManagement uManagement = new UserManagement();
//        CryptoManagement cyManagement = new CryptoManagement();
//        AuditManagement audit = new AuditManagement();
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        Sessions session = sManagement.getSessionById(sessionid);
//
//        int retValue = -1;
////        aStatus.error = "ERROR";
//        aStatus.errorcode = -2;
//        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//        aStatus.error = errmsg.getUsermessage();
//        if (session == null) {
////            aStatus.error = "Invalid Session";
//            aStatus.errorcode = -114;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        ChannelManagement cManagement = new ChannelManagement();
//        Channels channel = cManagement.getChannelByID(session.getChannelid());
//        if (channel == null) {
////            aStatus.error = "Invalid Channel";
//            aStatus.errorcode = -101;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        SettingsManagement setManagement = new SettingsManagement();
//        String channelid = session.getChannelid();
//        ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
//        if (channelProfileObj != null) {
//            int retVal = sManagement.getServerStatus(channelProfileObj);
//            if (retVal != 0) {
////                aStatus.error = "Channel Profile is not properly configured";
//                aStatus.errorcode = -82;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                return aStatus;
//            }
//        }
//
//        AuthUser aUser = uManagement.getUser(sessionid, channel.getChannelid(), userid);
//        if (aUser == null) {
////            aStatus.error = "User is invalid";
//            aStatus.errorcode = -12;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        AxiomStatus as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
//        if (as.errorcode != 0) {
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
//            as.error = errmsg.getUsermessage();
//            return as;
//        }
//
//        as = SendRegistrationCode(session, channel, userid, OTP_TOKEN, req, clientip);
//        return as;
//
//    }
//
//    @Override
//    public AxiomStatus GetPUKCode(String sessionid, String serialnumber, String unlockcodeFromToken, String clientip) {
//
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.debug("GetPUKCode::sessionid::" + sessionid);
//                log.debug("GetPUKCode::serialnumber::" + serialnumber);
//                log.debug("GetPUKCode::unlockcodeFromToken::" + unlockcodeFromToken);
//                log.debug("GetPUKCode::clientip::" + clientip);
//
//            }
//        } catch (Exception ex) {
//        }
//
//        int iResult = AxiomProtect.ValidateLicense();
//        iResult = 0;
//        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "Licence is invalid";
//            aStatus.errorcode = -100;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//            //throw new AxiomException("Licence is invalid");
//        }
//
//        if (sessionid == null) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "Empty Session ID";
//            aStatus.errorcode = -114;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        AxiomStatus aStatus = new AxiomStatus();
//        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//        String pukcode = null;
//
//        try {
//
//            SessionManagement sManagement = new SessionManagement();
//            UserManagement uManagement = new UserManagement();
//            CryptoManagement cyManagement = new CryptoManagement();
//
//            AuditManagement audit = new AuditManagement();
//            MessageContext mc = wsContext.getMessageContext();
//            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//            Sessions session = sManagement.getSessionById(sessionid);
//
//            int retValue = -1;
////            aStatus.error = "ERROR";
//            aStatus.errorcode = -2;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//
//            if (session == null) {
////                aStatus.error = "Invalid Session";
//                aStatus.errorcode = -114;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                return aStatus;
//            }
//
//            ChannelManagement cManagement = new ChannelManagement();
//            Channels channel = cManagement.getChannelByID(session.getChannelid());
//            if (channel == null) {
////                aStatus.error = "Invalid Channel";
//                aStatus.errorcode = -102;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                return aStatus;
//            }
//
//            SettingsManagement setManagement = new SettingsManagement();
//            String channelid = session.getChannelid();
//
//            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
//            if (channelProfileObj != null) {
//                int retVal = sManagement.getServerStatus(channelProfileObj);
//                if (retVal != 0) {
////                    aStatus.error = "Server Down for maintainance,Please try later!!!";
//                    aStatus.errorcode = -48;
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                    aStatus.error = errmsg.getUsermessage();
//                    return aStatus;
//                }
//            }
//
//            AxiomStatus as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
//            if (as.errorcode != 0) {
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
//                as.error = errmsg.getUsermessage();
//                return as;
//            }
//
//            OTPTokenManagement otptkmgmt = new OTPTokenManagement(channelid);
//            SettingsManagement setObj = new SettingsManagement();
//            Object settingsObj = setObj.getSetting(sessionid, channelid, SettingsManagement.Token, SettingsManagement.PREFERENCE_ONE);
//
//            TokenSettings tokenObj = null;
//            if (settingsObj != null) {
//                tokenObj = (TokenSettings) settingsObj;
//            }
//
//            Otptokens otpObj = otptkmgmt.getOtpObjBySerialNo(sessionid, serialnumber);
//            if (otpObj.getCategory() == OTP_TOKEN_HARDWARE) {
//                pukcode = otptkmgmt.generatePUK(otpObj.getSecret(), tokenObj.getPukCodeLengthHW(), tokenObj.getPukCodeValidityHW());
//            } else if (otpObj.getCategory() == OTP_TOKEN_SOFTWARE) {
//                pukcode = otptkmgmt.generatePUKSW(serialnumber, tokenObj, otpObj);
//            }
//
//            if (pukcode != null) {
//                aStatus.regcode = pukcode;
////                aStatus.error = "SUCCESS";
//                aStatus.errorcode = 0;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//
//                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                        session.getLoginid(), session.getLoginid(), new Date(), "PUK Code generation Success ", aStatus.error, aStatus.errorcode,
//                        "OTP Token Management", errmsg.getErrorMessage(), " ",
//                        "OTPTOKENS", "");
//
//            } else {
//
//                aStatus.regcode = null;
////                aStatus.error = "ERROR";
//                aStatus.errorcode = -1;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//
//                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                        session.getLoginid(), session.getLoginid(), new Date(), "PUK Code generation failed", aStatus.error, aStatus.errorcode,
//                        "OTP Token Management", errmsg.getErrorMessage(), " ",
//                        "OTPTOKENS", "");
//
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//
//            aStatus.regcode = null;
////            aStatus.error = "ERROR";
//            aStatus.errorcode = -2;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        }
//        return aStatus;
//        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    private AxiomStatus SendRegistrationCode(Sessions session, Channels channel, String userid, int type, HttpServletRequest req, String clientip) {
//        String strDebug = null;
//
//        if (type == OTP_TOKEN) {
//
//            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
//                AxiomStatus aStatus = new AxiomStatus();
//                ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////                aStatus.error = "OTP Token feature is not available in this license!!!";
//                aStatus.errorcode = -104;
//                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//
//                return aStatus;
//            }
//        } else if (type == PKI_TOKEN) {
//
//            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_SOFTWARE) != 0) {
//                AxiomStatus aStatus = new AxiomStatus();
//                ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////                aStatus.error = "PKI Token feature is not available in this license!!!";
//                aStatus.errorcode = -105;
//                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                return aStatus;
//            }
//        }
//
//        AuditManagement audit = new AuditManagement();
//
//        AxiomStatus aStatus = new AxiomStatus();
//        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//        int retValue = -1;
////        aStatus.error = "ERROR";
//        aStatus.errorcode = -2;
//        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//        aStatus.error = errmsg.getUsermessage();
//
//        if (session == null) {
////            aStatus.error = "Session Invalid!!!";
//            aStatus.errorcode = -114;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//
//            return aStatus;
//        }
//
//        String strCategory = "SOFTWARE_TOKEN";
//
//        if (channel == null) {
////            aStatus.error = "Invalid Channel";
//            aStatus.errorcode = -102;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//
//            return aStatus;
//        }
//
//        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//
//        SettingsManagement setManagement = new SettingsManagement();
//        //if (tSettings.isbSilentCall() == false) {
//        String regCode = null;
//        Otptokens oToken = null;
//        Pkitokens pToken = null;
//        Securephrase sPhrase = null;
//        int subtype = 0;
//        PKITokenManagement pManagement = new PKITokenManagement();
//        SecurePhraseManagment sManagment = new SecurePhraseManagment();
//        TokenSettings token = (TokenSettings) setManagement.getSetting(session.getSessionid(), session.getChannelid(), SettingsManagement.Token, SettingsManagement.PREFERENCE_ONE);
//        if (token == null) {
////            aStatus.error = "OTP Token Settings is not configured!!!";
//            aStatus.errorcode = -85;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//
//            return aStatus;
//        }
//        SimpleDateFormat sdfExpiry = new SimpleDateFormat("hh:mm");
//        Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.MINUTE, token.getRegistrationValidity());
//        Date expiry = cal.getTime();
//        if (type == OTP_TOKEN) {
//
//            oToken = oManagement.getOtpObjByUserId(session.getSessionid(), session.getChannelid(), userid, OTPTokenManagement.SOFTWARE_TOKEN);
//            if (oToken == null) {
////                aStatus.error = "Desired Software Token is not assigned!!!";
//                aStatus.errorcode = -32;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//
//                String cip = req.getRemoteAddr();
//                if (clientip == null || clientip.isEmpty() == true) {
//                } else {
//                    cip = clientip;
//                }
//
//                audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(), cip, channel.getName(),
//                        session.getLoginid(), session.getLoginid(), new Date(), "Check OTP Token", "ERROR", aStatus.errorcode,
//                        "Token Management", "",
//                        errmsg.getErrorMessage(),
//                        "OTPTOKENS", userid);
//                return aStatus;
//            }
//
//            subtype = oToken.getSubcategory();
//            regCode = oManagement.generateRegistrationCode(session.getSessionid(), session.getChannelid(), userid, OTP_TOKEN_SOFTWARE);
//
//            if (regCode == null) {
////                aStatus.error = "Registration Code cannot be generate";
//                aStatus.errorcode = -23;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//
//                String cip = req.getRemoteAddr();
//                if (clientip == null || clientip.isEmpty() == true) {
//                } else {
//                    cip = clientip;
//                }
//
//                audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(), cip, channel.getName(),
//                        session.getLoginid(), session.getLoginid(), new Date(), "Generate Registration Code", "ERROR", aStatus.errorcode,
//                        "Token Management", "",
//                        errmsg.getErrorMessage(),
//                        "OTPTOKENS", userid);
//                return aStatus;
//            }
//
//            String strSWOTPType = LoadSettings.g_sSettings.getProperty("sw.otp.type");
//            if (strSWOTPType != null && strSWOTPType.compareToIgnoreCase("simple") == 0) { // we are forcing it to be time based for mobile token.
//                regCode = "1" + regCode;
//            }
//            //log.debug("REGCODE =" + regCode);
//
//        } else if (type == PKI_TOKEN) {
//            pToken = pManagement.getPkiObjByUserid(session.getChannelid(), userid, PKITokenManagement.SOFTWARE_TOKEN);
//            if (pToken == null) {
////                aStatus.error = "Desired Software Token is not assigned!!!";
//                aStatus.errorcode = -32;
//
//                audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                        session.getLoginid(), session.getLoginid(), new Date(), "Send Registration Code ", errmsg.getErrorMessage(), aStatus.errorcode,
//                        "PKI Token Management", "", "Category = " + strCategory + "regcode =" + "******",
//                        "PKITOKEN", userid);
//                return aStatus;
//
//            }
//            subtype = pToken.getCategory();
//            regCode = pManagement.generateRegistrationCode(session.getSessionid(), session.getChannelid(), userid, PKITokenManagement.SOFTWARE_TOKEN);
//        }
//
//        if (regCode == null) {
//            String cip = req.getRemoteAddr();
//            if (clientip == null || clientip.isEmpty() == true) {
//            } else {
//                cip = clientip;
//            }
//            aStatus.errorcode = -24;
//            audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(), clientip, channel.getName(),
//                    session.getLoginid(), session.getLoginid(), new Date(), "Send Registration Code", "ERROR", aStatus.errorcode,
//                    "Image Authentication Management",
//                    "",
//                    "Image Auth Reg Code is null",
//                    "IMAGEAUTH", userid);
//        } else {
//
//            UserManagement userObj = new UserManagement();
//            AuthUser user = null;
//            user = userObj.getUser(session.getSessionid(), channel.getChannelid(), userid);
//
//            Templates temp = null;
//            TemplateManagement tObj = new TemplateManagement();
//            if (type == OTP_TOKEN || type == PKI_TOKEN) {
//                temp = tObj.LoadbyName(session.getSessionid(), channel.getChannelid(), TemplateNames.MOBILE_SOFTWARE_TOKEN_REGISTER_TEMPLATE);
//            } else if (type == IMG_AUTH) {
//                temp = tObj.LoadbyName(session.getSessionid(), channel.getChannelid(), TemplateNames.IMAGE_AUTH_REGISTER_TEMPLATE);
//            }
//            ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//            Date d = new Date();
//            String tmessage = (String) UtilityFunctions.deserializeFromObject(bais);
//            tmessage = tmessage.replaceAll("#name#", user.getUserName());
//            tmessage = tmessage.replaceAll("#channel#", channel.getName());
//            tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
//            tmessage = tmessage.replaceAll("#regcode#", regCode);
//            tmessage = tmessage.replaceAll("#expiry#", sdfExpiry.format(expiry));
//
//            if (subtype == OTPTokenManagement.SW_WEB_TOKEN) {
//                tmessage = tmessage.replaceAll("#tokentype#", "WEB");
//            }
//
//            if (type == OTP_TOKEN || type == PKI_TOKEN) {
//                if (subtype == OTPTokenManagement.SW_MOBILE_TOKEN) {
//                    tmessage = tmessage.replaceAll("#tokentype#", "MOBILE");
//
//                }
//                if (subtype == OTPTokenManagement.SW_PC_TOKEN) {
//                    tmessage = tmessage.replaceAll("#tokentype#", "PC");
//
//                }
//            }
//            if (type == IMG_AUTH) {
//                if (subtype == OTPTokenManagement.SW_MOBILE_TOKEN) {
//                    tmessage = tmessage.replaceAll("#tokentype#", "MOBILE");
//
//                }
//                if (subtype == OTPTokenManagement.SW_PC_TOKEN) {
//                    tmessage = tmessage.replaceAll("#tokentype#", "PC");
//
//                }
//            } else {
//                tmessage = tmessage.replaceAll("#tokentype#", "");
//            }
//            SendNotification send = new SendNotification();
//            AXIOMStatus status = new AXIOMStatus();
//            if (temp.getStatus() == tObj.ACTIVE_STATUS) {
//                aStatus.errorcode = SEND_MESSAGE_PENDING_STATE;
////                aStatus.error = "PENDING";
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                aStatus.regCodeMessage = tmessage;
//                aStatus.regcode = regCode;
//
//                status = send.SendOnMobileNoWaiting(channel.getChannelid(), user.phoneNo, tmessage, 1, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                //ashish
//                send.SendEmail(channel.getChannelid(), user.getEmail(), "REGISTRATION CODE", tmessage, null, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                //end ashish
//            }
//
//            if (aStatus != null) {
//                if (aStatus.errorcode == SEND_MESSAGE_PENDING_STATE) {
//                    RegistrationCodeTrailManagement rManagement = new RegistrationCodeTrailManagement();
//                    String registrationcode = UtilityFunctions.Bas64SHA1(regCode);
//                    rManagement.addRegCodeTrail(session.getChannelid(), userid, registrationcode, RegistrationCodeTrailManagement.SENT, RegistrationCodeTrailManagement.OTPTOKEN);
//                }
//            }
//
//            retValue = 0;
////            aStatus.error = "SUCCESS";
//            aStatus.errorcode = 0;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//
//            aStatus.regCodeMessage = tmessage;
//            aStatus.regcode = regCode;
//            if (type == IMG_AUTH) {
//                audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getChannelid(),
//                        req.getRemoteAddr(), channel.getName(),
//                        session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code",
//                        "success", 0, "IMAGE AUTHENTICATION", "", "Registration Code = ******" /*strRegCode*/, "IMAGAUTH", userid);
//            } else {
//                audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getChannelid(),
//                        req.getRemoteAddr(), channel.getName(),
//                        session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code",
//                        "success", 0, "Token Management", "", "Registration Code = ******" /*strRegCode*/, itemtype, userid);
//            }
//        }
//        if (type == IMG_AUTH) {
//            audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getChannelid(),
//                    req.getRemoteAddr(), channel.getName(),
//                    session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code",
//                    errmsg.getErrorMessage(), aStatus.errorcode, "IMAGE AUTHENTICATION", "", "Registration Code = ******" /*strRegCode*/, "IMAGAUTH", userid);
//        } else {
//            audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                    session.getLoginid(), session.getLoginid(), new Date(), "Send Registration Code", errmsg.getErrorMessage(), aStatus.errorcode,
//                    "Token Management", "", "Category = " + strCategory + "regcode =" + "******",
//                    itemtype, userid);
//        }
//        return aStatus;
//    }
//
//    @Override
//    public AxiomStatus ping() {
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                Date d = new Date();
//                //log.debug(d + ">>" + "ping::" + new Date());
//            }
//        } catch (Exception ex) {
//        }
//        AxiomStatus response = new AxiomStatus();
//        response.error = "SUCCESS";
//        response.errorcode = 0;
//        return response;
//    }
//
//    private byte[] SHA1(String message) {
//        try {
//            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
//            byte[] array = sha1.digest(message.getBytes());
//            return array;
//        } catch (Exception e) {
//            return null;
//        }
//    }
//
//    @Override
//    public MobileTrustStatus ActivateSDKToken(String sessionid, String regCode, String SwTokenData, String clientip) {
//        String certPass = null;
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.debug("Activate::sessionid::" + sessionid);
//                log.debug("Activate::regCode::" + regCode);
//                log.debug("Activate::SwTokenData::" + SwTokenData);
//            }
//        } catch (Exception ex) {
//        }
//
//        int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            MobileTrustStatus aStatus = new MobileTrustStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "Licence Invalid";
//            aStatus.errorCode = -100;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//            aStatus.error = errmsg.getUsermessage();
//            aStatus.data = null;
//            return aStatus;
//        }
//
//        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.MOBILETRUST) != 0) {
//            MobileTrustStatus aStatus = new MobileTrustStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "OTP Token feature is not available in this license!!!";
//            aStatus.errorCode = -104;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        MobileTrustStatus aStatus = new MobileTrustStatus();
//        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////        aStatus.error = "ERROR";
//        aStatus.errorCode = -2;
//        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//        aStatus.error = errmsg.getUsermessage();
//
//        SessionManagement sManagement = new SessionManagement();
//        Sessions session = sManagement.getSessionById(sessionid);
//
//        if (session == null) {
////            aStatus.error = "Session Invalid!!!";
//            aStatus.errorCode = -114;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        String signData = null;
//
//        String oldValue = "";
//        String strValue = "";
//        //String otp = null;
//        String cert = null;
//
//        if (session != null) {
//            try {
//                SettingsManagement setManagement = new SettingsManagement();
//
////                int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
////                if (result != 1) {
////                    aStatus.error = "INVALID IP REQUEST";
////                    aStatus.errorCode = -8;
////                    aStatus.data = signData;
////                    return aStatus;   //why return statement is commented
////                }
//                String channelid = session.getChannelid();
//                ChannelManagement cManagement = new ChannelManagement();
//                Channels channel = cManagement.getChannelByID(channelid);
//                if (channel == null) {
////                    aStatus.error = "Invalid Channel!!!";
//                    aStatus.errorCode = -102;
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//                    aStatus.error = errmsg.getUsermessage();
//                    return aStatus;
//                }
//                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//                if (ipobj != null) {
//                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                    int checkIp = 1;
//                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                    } else {
//                        checkIp = 1;
//                    }
//                    if (iObj.ipstatus == 0 && checkIp != 1) {
//                        if (iObj.ipalertstatus == 0) {
//                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                            Session sTemplate = suTemplate.openSession();
//                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                            OperatorsManagement oManagement = new OperatorsManagement();
//                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                            if (aOperator != null) {
//                                String[] emailList = new String[aOperator.length - 1];
//                                for (int i = 1; i < aOperator.length; i++) {
//                                    emailList[i - 1] = aOperator[i].getEmailid();
//                                }
//                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                    String strsubject = templatesObj.getSubject();
//                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                    if (strmessageBody != null) {
//                                        // Date date = new Date();
//                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                    }
//
//                                    SendNotification send = new SendNotification();
//                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                }
//
//                                suTemplate.close();
//                                sTemplate.close();
////                                aStatus.error = "INVALID IP";
//                                aStatus.errorCode = -8;
//                                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//                                aStatus.error = errmsg.getUsermessage();
//
//                                return aStatus;
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
////                            aStatus.error = "INVALID IP";
//                            aStatus.errorCode = -8;
//                            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//                            aStatus.error = errmsg.getUsermessage();
//                            return aStatus;
//                        }
////                        aStatus.error = "INVALID IP";
//                        aStatus.errorCode = -8;
//                        errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//                        aStatus.error = errmsg.getUsermessage();
//                        return aStatus;
//                    }
//                }
//
//                RegistrationCodeTrailManagement rManagement = new RegistrationCodeTrailManagement();
//                regCode = UtilityFunctions.Bas64SHA1(regCode);
//                Registrationcodetrail regObj = rManagement.getRegistrationcodetrail(channelid, regCode);
//
//                String userid = "";
//
//                if (regObj != null) {
//                    userid = regObj.getUserid();
//                } else {
//                    aStatus.errorCode = -25;
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//                    aStatus.error = errmsg.getUsermessage();
////                    aStatus.error = "Registration code not found";
//                    aStatus.data = signData;
//                    return aStatus;
//                }
//
//                OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//
//                TokenStatusDetails tokenSelected = oManagement.getToken(sessionid, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE);
////
//                if (tokenSelected == null) {
//                    aStatus.errorCode = -62;
////                    aStatus.error = "Desired Token is not assigned";
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//                    aStatus.error = errmsg.getUsermessage();
//                    aStatus.data = signData;
//                    return aStatus;
//                }
//
//                if (tokenSelected.Status == TOKEN_STATUS_ACTIVE) {
//                    oldValue = strACTIVE;
//                } else if (tokenSelected.Status == TOKEN_STATUS_SUSPENDED) {
//                    oldValue = strSUSPENDED;
//                } else if (tokenSelected.Status == TOKEN_STATUS_UNASSIGNED) {
//                    oldValue = strUNASSIGNED;
//                }
//                //CryptoManagement cryManagement = new CryptoManagement();
//                strValue = strACTIVE;
//                //decrypt data here SwTokenData
//                //decrypt SwTokenData
//                String dSWData = SwTokenData;
//                // dSWData
//
//                //String serverpublicKey = channel.getVisiblekey();
//                String serverPrivateKey = channel.getHiddenkey();
//                CryptoManager cm = new CryptoManager();
//                //which variable is userd for verifyneeded?
//                JSONObject jsonObj = new JSONObject(dSWData);
//                String device = jsonObj.getString("_deviceType");
//                int devicetype = 1;
//                if (device.equals("ANDROID")) {
//                    devicetype = MobileTrustManagment.ANDROID;
//                } else if (device.equals("IOS")) {
//                    devicetype = MobileTrustManagment.IOS;
//                }
//                String jsonData = cm.ConsumeMobileTrust(dSWData, serverPrivateKey, true, CryptoManager.ACTIVATE);
//                //  String jsonData = dSWData;
//                JSONObject json = new JSONObject(jsonData);
////                String _mtkey = json.getString("_mtkey");
////                byte[]  datatoSign =Base64.decode(_mtkey);
////                String serverprivateKey = LoadSettings.g_sSettings.getProperty("spkey");
////                String Ss= new String(cryManagement.decryptRSA(datatoSign,serverprivateKey));
//                String ip = json.getString("ip");
//                //String con = json.getString("con");
//                String longi = json.getString("longi");
//                String latti = json.getString("latti");
//                String mac = json.getString("macaddress");
//                String did = json.getString("did");
//                String vKeyCLIENT = json.getString("vKey");
//                String pKeyCLIENT = json.getString("pKey");
//                String os = json.getString("os");
//                String regcode = json.getString("regcode");
//
//                Otptokens oToken = oManagement.getOtpObjByUserId(sessionid, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE);
//                if (oToken == null) {
////                    aStatus.error = "Token Not found";
//                    aStatus.errorCode = -65;
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//                    aStatus.error = errmsg.getUsermessage();
//                    aStatus.data = signData;
//                }
//
//                if (oToken.getStatus() == TOKEN_STATUS_ACTIVE) {
////                    aStatus.error = "Token is Already Active";
//                    aStatus.errorCode = -63;
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//                    aStatus.error = errmsg.getUsermessage();
//                    aStatus.data = signData;
//                    return aStatus;
//                }
//
//                MobileTrustSettings mSettings = null;
//                int retValue = -1;
//                Object obj = setManagement.getSetting(sessionid, session.getChannelid(), SettingsManagement.MOBILETRUST_SETTING, 1);
//                if (obj != null) {
//                    if (obj instanceof MobileTrustSettings) {
//                        mSettings = (MobileTrustSettings) obj;
//                    }
//                }
//
//                RootCertificateSettings rcaSettings = (RootCertificateSettings) setManagement.getSetting(sessionid, session.getChannelid(), SettingsManagement.RootConfiguration, 1);
//                if (rcaSettings == null) {
////                    aStatus.error = "Certificate Setting is not configured!!!";
//                    aStatus.errorCode = -86;
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//                    aStatus.error = errmsg.getUsermessage();
//                    aStatus.data = signData;
//                    return aStatus;
//                }
//
//                if (mSettings == null) {
////                    aStatus.error = "Mobile Trust Setting is not configured!!!";
//                    aStatus.errorCode = -87;
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//                    aStatus.error = errmsg.getUsermessage();
//                    aStatus.data = signData;
//                    return aStatus;
//                }
//
//                TokenSettings tSettings = (TokenSettings) setManagement.getSetting(sessionid, session.getChannelid(), TOKEN, 1);
//                if (tSettings == null) {
////                    aStatus.error = "OTP Token Setting is not configured!!!";
//                    aStatus.errorCode = -85;
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//                    aStatus.error = errmsg.getUsermessage();
//                    aStatus.data = signData;
//                    return aStatus;
//                }
//
//                if (mSettings.bSilentCall == false) {
//
//                    if (regcode.compareTo(oToken.getRegcode()) != 0) {
////                        aStatus.error = "Invalid Registration Code";
//                        aStatus.errorCode = -26;
//                        errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//                        aStatus.error = errmsg.getUsermessage();
//                        aStatus.data = signData;
//                        return aStatus;
//                    }
//
//                    Date dExpiryTime = oToken.getExpirytime();
//                    Date dCurrent = new Date();
//
//                    if (dExpiryTime.before(dCurrent) == true) {
////                        aStatus.error = "Regitration code has expired.";
//                        aStatus.errorCode = -28;
//                        errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//                        aStatus.error = errmsg.getUsermessage();
//                        aStatus.data = signData;
//                        return aStatus;
//                    }
//                }
//
////                if (mSettings.bSilentCall == false ) {
////                    if (regcode.compareTo(oToken.getRegcode()) != 0) {
////                        aStatus.error = "Invalid Regitration Code";
////                        aStatus.errorCode = -17;
////                        aStatus.data = signData;
////                        return aStatus;
////                    }
////                }
////                Date dExpiryTime = oToken.getExpirytime();
////                Date dCurrent = new Date();
////
////                if (dExpiryTime.before(dCurrent) == true) {
////                    aStatus.error = "Regitration code has expired.";
////                    aStatus.errorCode = -18;
////                    aStatus.data = signData;
////                    return aStatus;
////                }
//                MobileTrustManagment mManagment = new MobileTrustManagment();
//                String strAxiomid = userid + mac + did + os + vKeyCLIENT; //add all device info and get new unique number
//
//                String axiomid = new String(Base64.encode(SHA1(strAxiomid)));
//
//                String con = "";
//                String city = "";
//                String state = "";
//                String country = "";
//                String zipcode = "";
//
//                Location srvLoc = mManagment.getLocationByLongAndLat(longi, latti);
//                if (srvLoc != null) {
//                    con = srvLoc.country;
//                    city = srvLoc.city;
//                    state = srvLoc.state;
//                    country = con;
//                    zipcode = srvLoc.zipcode;
//                } else {
//                    aStatus.error = "Geo Address could not found for (" + longi + "," + latti + ").";
//                    aStatus.errorCode = -27;
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//                    aStatus.error = errmsg.getUsermessage();
//                    //aStatus.data = signData;
//                }
//
//                int result = -1;
//                LocationManagement gLocation = new LocationManagement();
//                Countrylist clist = gLocation.getCountriesByid(Integer.parseInt(mSettings.homeCountryCode));
//                String countryName = null;
//                if (clist != null) {
//                    countryName = clist.getCountyName();
//                }
//
//                if (srvLoc.country.equalsIgnoreCase(countryName)) {
//
//                    result = 0;
//                } else {
////                    aStatus.error = "Home Country Is Different";
//                    aStatus.errorCode = -98;
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//                    aStatus.error = errmsg.getUsermessage();
//                    aStatus.data = signData;
//                    result = -21;
//                    AuditManagement audit = new AuditManagement();
//
//                    String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
//                    audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
//                            session.getLoginid(), new Date(), "Change Token Status", errmsg.getErrorMessage(), aStatus.errorCode,
//                            "Token Management", errorLocation, null, itemtype,
//                            session.getLoginid());
//
//                    return aStatus;
//                }
////                int result = -1;
////                if (con.equalsIgnoreCase(mSettings.homeCountryCode)) {
////                    result = 0;
////                } else {
////                    aStatus.error = "Home Country Is Different";
////                    aStatus.errorCode = -21;
////                    aStatus.data = signData;
////                    result = -21;
////                    AuditManagement audit = new AuditManagement();
////
////                    String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
////                    audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
////                            session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorCode,
////                            "Token Management", errorLocation, null, itemtype,
////                            session.getLoginid());
////
////                    return aStatus;
////                }
//
//                String prKeySERVER_GEN_CLIENT = null;
//                String vKeySERVER_GEN_CLIENT = null;
//
//                boolean bServerKEYGeneration = false;
//
//                if (result == 0) {
//
//                    CertificateManagement cerManagement = new CertificateManagement();
//                    Certificates usercert = cerManagement.getCertificate1(channelid, userid);
//
//                    if (usercert != null) {
//                        String filepath = cerManagement.getPFXFile(sessionid, channelid, userid);
//                        log.debug("#ActivateMobileTrust: certificate#filepath" + filepath);
//                        byte[] pfxAsBytes = Base64.decode(usercert.getPfx());
//                        ByteArrayInputStream fis = new ByteArrayInputStream(pfxAsBytes);
//                        //certPass = usercert.getPfxpassword();
//
//                        try {
//                            // supported KeyStore types  (JDK1.4): PKCS12 and JKS (native Sun)
//                            KeyStore ks = KeyStore.getInstance("PKCS12");
//                            ks.load(fis, usercert.getPfxpassword().toCharArray());
//                            for (Enumeration num = ks.aliases(); num.hasMoreElements();) {
//                                String alias = (String) num.nextElement();
//                                if (ks.isKeyEntry(alias)) {
//                                    PrivateKey priKey = (PrivateKey) ks.getKey(alias, usercert.getPfxpassword().toCharArray());
//                                    prKeySERVER_GEN_CLIENT = new String(Base64.encode(priKey.getEncoded()));
//
//                                    RSAPrivateCrtKey privk = (RSAPrivateCrtKey) priKey;
//
//                                    RSAPublicKeySpec publicKeySpec = new java.security.spec.RSAPublicKeySpec(privk.getModulus(), privk.getPublicExponent());
//
//                                    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
//                                    PublicKey myPublicKey = keyFactory.generatePublic(publicKeySpec);
//                                    vKeySERVER_GEN_CLIENT = new String(Base64.encode(myPublicKey.getEncoded()));
//                                }
//                            }
//                        } catch (Exception e) {
//                            log.debug("#ActivateForSDKToken: certificate checking" + e.getMessage());
//                        }
//
//                        cert = usercert.getCertificate();
//                        bServerKEYGeneration = true;
//
//                    } else {
//
//                        if (devicetype == MobileTrustManagment.IOS) {
//                            KeyPair kp = CryptoManagement.getAxiomKeyPair(pKeyCLIENT);
//                            PrivateKey pk = kp.getPrivate();
//                            PublicKey pubk = kp.getPublic();
//
//                            byte[] s = Base64.encode(pubk.getEncoded());
//                            vKeyCLIENT = new String(s);
//
//                            s = Base64.encode(pk.getEncoded());
//                            pKeyCLIENT = new String(s);
//                            cert = mManagment.AddCertificate(sessionid, session.getChannelid(), userid, oToken.getSrno(), vKeyCLIENT, pKeyCLIENT);
//                        } else {
//                            cert = mManagment.AddCertificate(sessionid, session.getChannelid(), userid, oToken.getSrno(), vKeyCLIENT, pKeyCLIENT);
//                        }
//                        if (cert == null) {
////                            aStatus.error = "Digital Certificate Issuance Failed";
//                            aStatus.errorCode = -75;
//                            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//                            aStatus.error = errmsg.getUsermessage();
//                            aStatus.data = signData;
//                        }
//                    }
//                }
//
//                certPass = "";
//
//                if (result == 0 && cert != null) {
//
//                    if (bServerKEYGeneration == true) {
//                        retValue = mManagment.addTrustedDevice(sessionid, session.getChannelid(), userid, axiomid, did, mac, os, true, vKeySERVER_GEN_CLIENT, new Date(), new Date());
//                    } else {
//                        retValue = mManagment.addTrustedDevice(sessionid, session.getChannelid(), userid, axiomid, did, mac, os, true, vKeyCLIENT, new Date(), new Date());
//                    }
//
//                    if (retValue == 0) {
//                        retValue = mManagment.addGeoFence(sessionid, session.getChannelid(), userid, null, null, mSettings.homeCountryCode, MobileTrustManagment.ROAMING_DISABLE, null, null);
//                        if (retValue == 0) {
//                            retValue = mManagment.addGeoTrack(sessionid, session.getChannelid(), userid, longi, ip, latti,
//                                    //con, 
//                                    city, state, mSettings.homeCountryCode, zipcode,
//                                    MobileTrustManagment.REGISTER, new Date());
//                        }
//                        if (retValue == 0) {
//                            retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), userid, TOKEN_STATUS_ACTIVE, OTP_TOKEN_SOFTWARE, tokenSelected.SubCategory);
//                        }
//                        if (retValue == 0) {
//                            oToken = oManagement.getOtpObjByUserId(sessionid, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE);
//                            if (oToken != null) {
//
//                                retValue = mManagment.AddPKIToken(sessionid, session.getChannelid(), userid, oToken.getSrno(), oToken.getCategory(), oToken.getRegcode());
//
//                                if (retValue == 0) {
//
//                                    String secret = oToken.getSecret();
//                                    signData = mManagment.confirmation(session.getChannelid(), userid, did, secret, cert, certPass, mac, vKeyCLIENT, serverPrivateKey, devicetype, oToken.getSrno());
//                                    if (signData == null) {
////                                        aStatus.error = "Confirmation Message Could Not Be Generated.";
//                                        aStatus.errorCode = -77;
//                                        errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//                                        aStatus.error = errmsg.getUsermessage();
//                                        //aStatus.data = signData;
//                                    }
//                                } else {
//                                    mManagment.eraseMobileTrustCredential(sessionid, channel.getChannelid(), userid);
////                                    aStatus.error = "PKI Token Assignment Failed!!!";
//                                    aStatus.errorCode = -91;
//                                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//                                    aStatus.error = errmsg.getUsermessage();
//                                    aStatus.data = null;
//                                    //end
//                                }
//                                // String cert = cManagement.g
//                            }
//                        }
//                    }
//
//                }
//
//                if (retValue == 0) {
////                    aStatus.error = "SUCCESS";
//                    aStatus.errorCode = 0;
//                    errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//                    aStatus.error = errmsg.getUsermessage();
//                    aStatus.data = signData;
//
//                }
//
//                // return aStatus;
//            } catch (Exception ex) {
//                ex.printStackTrace();
////                aStatus.error = "General Exception::" + ex.getMessage();
//                aStatus.errorCode = -999;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorCode);
//                aStatus.error = errmsg.getUsermessage();
//                aStatus.data = signData;
//            }
//        }
//        ChannelManagement cManagement = new ChannelManagement();
//        Channels channel = cManagement.getChannelByID(session.getChannelid());
//        AuditManagement audit = new AuditManagement();
//        audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
//                session.getLoginid(), new Date(), "Change Token Status", errmsg.getErrorMessage(), aStatus.errorCode,
//                "Token Management", "Old Status :" + oldValue, "New Status :" + strValue, itemtype,
//                session.getLoginid());
//
//        return aStatus;
//
//    }
//
//    @Override
//    public String GetTimeStamp(String sessionid, String userid, String clientip) throws AxiomException {
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.debug("GetTimeStamp::sessionid::" + sessionid);
//                log.debug("GetTimeStamp::userid::" + userid);
//            }
//        } catch (Exception ex) {
//
//        }
//
//        if (sessionid == null) {
////            throw new AxiomException("Session is empty!!!");
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//            aStatus.errorcode = -113;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
////            throw new AxiomException("Session ID is empty!!!");
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
//        }
//
//        if (userid == null) {
//
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//            aStatus.errorcode = -69;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
////throw new AxiomException("Userid is empty!!!");
//        }
//
//        int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//            aStatus.errorcode = -100;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
////            throw new AxiomException("Invalid License!!!");
//        }
//
//        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.MOBILETRUST) != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//            aStatus.errorcode = -106;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
////            throw new AxiomException("Mobile Trust Feature is not present in this license!!!");
//        }
//
//        SessionManagement sManagement = new SessionManagement();
//        Sessions session = sManagement.getSessionById(sessionid);
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        AxiomStatus aStatus = new AxiomStatus();
//        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////        aStatus.error = "ERROR";
//        aStatus.errorcode = -2;
//        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//        aStatus.error = errmsg.getUsermessage();
//        String oldValue = "";
//        String strValue = "";
//
//        if (session == null) {
//            aStatus.errorcode = -114;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
////            throw new AxiomException("Session is invlaid/null!!!");
//        }
//
//        SettingsManagement setManagement = new SettingsManagement();
//
//        String channelid = session.getChannelid();
//        ChannelManagement cManagement = new ChannelManagement();
//        Channels channel = cManagement.getChannelByID(channelid);
//        if (channel == null) {
//            aStatus.errorcode = -102;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
////            throw new AxiomException("Invalid Channel!!!");
//        }
//
//        AxiomStatus as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
//        if (as.errorcode != 0) {
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
//            as.error = errmsg.getUsermessage();
//            throw new AxiomException(as.error + " (" + as.errorcode + ")");
//        }
//
//        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//        TokenStatusDetails tokenSelected = oManagement.getToken(sessionid, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE);
//
//        if (tokenSelected == null) {
//            aStatus.errorcode = -62;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
////            throw new AxiomException("Desired Token is not assigned!!!");
//
//        }
//
//        if (tokenSelected.Status == TOKEN_STATUS_ACTIVE) {
//            oldValue = strACTIVE;
//        } else if (tokenSelected.Status == TOKEN_STATUS_SUSPENDED) {
//            oldValue = strSUSPENDED;
//        } else if (tokenSelected.Status == TOKEN_STATUS_UNASSIGNED) {
//            oldValue = strUNASSIGNED;
//        }
//
//        strValue = strACTIVE;
//
//        MobileTrustManagment mManagment = new MobileTrustManagment();
//        String timeStamp = mManagment.GetTimeStamp(sessionid, session.getChannelid(), userid);
//
//        if (timeStamp != null) {
////            aStatus.error = "SUCCESS";
//            aStatus.errorcode = 0;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        }
//
//        AuditManagement audit = new AuditManagement();
//        String cip = req.getRemoteAddr();
//        if (clientip == null || clientip.isEmpty() == true) {
//        } else {
//            cip = clientip;
//        }
//
//        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                cip, channel.getName(),
//                session.getLoginid(), session.getLoginid(), new Date(),
//                "Get TimeStamp",
//                "ERROR",
//                aStatus.errorcode,
//                "",
//                "",
//                errmsg.getErrorMessage(),
//                "MOBILETRUST",
//                userid);
//
////        audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
////                session.getLoginid(), new Date(), "Get TimeStamp", aStatus.error, aStatus.errorcode,
////                "MobileTrust Management", "", "New Session :" + strValue, itemtype,
////                session.getLoginid());
//        if (timeStamp == null) {
//            aStatus.errorcode = -76;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
////            throw new AxiomException("Failed to generate desired timestamp for this user!!!");
//        }
//
//        return timeStamp;
//        // return aStatus;
//
//    }
//
//    @Override
//    public AxiomStatus ALertEvent(String sessionid, String signData, String clientip) {
//
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.debug("ALertEvent::sessionid::" + sessionid);
//                log.debug("ALertEvent::signData::" + signData);
//            }
//        } catch (Exception ex) {
//        }
//
//        if (sessionid == null || sessionid.isEmpty() == true) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//            aStatus.errorcode = -113;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
////            aStatus.error = "Session is empty";
//            return aStatus;
//        }
//
//        int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "Licence is invalid";
//            aStatus.errorcode = -100;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.MOBILETRUST) != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "Mobile Trust feature is not available in this license!!!";
//            aStatus.errorcode = -106;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        SessionManagement sManagement = new SessionManagement();
//        AuditManagement audit = new AuditManagement();
//        Sessions session = sManagement.getSessionById(sessionid);
//        if (session == null) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "Session is invalid/wrong";
//            aStatus.errorcode = -114;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        AxiomStatus aStatus = new AxiomStatus();
//        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//        int retValue = -1;
////        aStatus.error = "ERROR";
//        aStatus.errorcode = -2;
//        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//        aStatus.error = errmsg.getUsermessage();
//
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//
//        ChannelManagement cManagement = new ChannelManagement();
//        Channels channel = cManagement.getChannelByID(session.getChannelid());
//        if (channel == null) {
////            aStatus.error = "Invalid Channel";
//            aStatus.errorcode = -102;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        SettingsManagement setManagement = new SettingsManagement();
//        String channelid = channel.getChannelid();
//
//        AxiomStatus as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
//        if (as.errorcode != 0) {
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
//            as.error = errmsg.getUsermessage();
//            return as;
//        }
//
//        MobileTrustManagment mManagment = new MobileTrustManagment();
//
//        retValue = mManagment.CheckForAlert(channel, sessionid, signData);
//
//        if (retValue == 0) {
//
//            String cip = req.getRemoteAddr();
//            if (clientip == null || clientip.isEmpty() == true) {
//            } else {
//                cip = clientip;
//            }
////            aStatus.error = "SUCCESS";
//            aStatus.errorcode = 0;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), cip, channel.getName(),
//                    session.getLoginid(), session.getLoginid(), new Date(), "Wrong Attempt Alert", "SUCCESS", aStatus.errorcode,
//                    "Alert Management", "",
//                    "Successfully Destroy",
//                    "MOBILETRUST", mManagment.m_userid);
//        } else {
//
//            aStatus.errorcode = retValue;
//            if (aStatus.errorcode == -2) {
////                aStatus.error = "Session has expired";
//                aStatus.errorcode = -115;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//            } else if (aStatus.errorcode == -6) {
//                aStatus.errorcode = -92;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
////                aStatus.error = "Trusted Device not found!!!";
//            } else if (aStatus.errorcode == -17) {
//                aStatus.errorcode = -93;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
////                aStatus.error = "Trusted Device is marked suspended";
//            } else if (aStatus.errorcode == -110) {
//                aStatus.errorcode = -78;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
////                aStatus.error = "Adding Abuse Alert Failed!!!";
//            } else if (aStatus.errorcode == -60) {
//                aStatus.errorcode = -12;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
////                aStatus.error = "User not Found!!!";
//            } else if (aStatus.errorcode == -30) {
//                aStatus.errorcode = -94;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
////                aStatus.error = "Trust Device mismatch!!!";
//            }
//
//            String cip = req.getRemoteAddr();
//            if (clientip == null || clientip.isEmpty() == true) {
//            } else {
//                cip = clientip;
//            }
//
//            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), cip, channel.getName(),
//                    session.getLoginid(), session.getLoginid(), new Date(), "Wrong Attempt Alert",
//                    "ERROR", aStatus.errorcode,
//                    "Abuse Management",
//                    "",
//                    errmsg.getErrorMessage(),
//                    "MOBILETRUST",
//                    mManagment.m_userid);
//        }
//
//        return aStatus;
//    }
//
//    private AxiomStatus VerifyPlusFactor(String sessionid, Channels channel, String userid, String sotp, String sotpPlus) {
//
//        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.MOBILETRUST) != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "Mobile Trust feature is not available in this license!!!";
//            aStatus.errorcode = -106;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//
//            return aStatus;
//        }
//
//        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_OOB) != 0
//                && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_HARDWARE) != 0
//                && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "OTP Token feature is not available in this license!!!";
//            aStatus.errorcode = -104;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        int retValue = -10;
//        AxiomStatus aStatus = new AxiomStatus();
//        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////        aStatus.error = "Invalid Parameters!!!";
//        aStatus.errorcode = -66;
//        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//        aStatus.error = errmsg.getUsermessage();
//
//        MobileTrustManagment mManagment = new MobileTrustManagment();
//        int check = -9;
//        JSONObject jsonObj = null;
//        int devicetype = 0;
//        try {
//            jsonObj = new JSONObject(sotpPlus);
//            String device = jsonObj.getString("_deviceType");
//            if (device.equals("ANDROID")) {
//                devicetype = MobileTrustManagment.ANDROID;
//            } else if (device.equals("IOS")) {
//                devicetype = MobileTrustManagment.IOS;
//            } else {
////                aStatus.error = "Invalid Device Type (Android and iOS) is only supported";
//                aStatus.errorcode = -95;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
//                return aStatus;
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
////            aStatus.error = "Mobile Trust Plus Payload is invalid!!!";
//            aStatus.errorcode = -96;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            return aStatus;
//        }
//
//        check = mManagment.CheckPlusComponent(channel, userid, sessionid, sotp, sotpPlus, devicetype);
//        if (retValue == -6) {
////            aStatus.error = "Trusted device not found";
//            aStatus.errorcode = -92;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        } else if (retValue == -4) {
////            aStatus.error = "OTP is invalid";
//            aStatus.errorcode = -97;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        } else if (retValue == -5) {
////            aStatus.error = "Trusted Device mismatch";
//
//            aStatus.errorcode = -94;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        } else if (retValue == -7) {
////            aStatus.error = "Invalid Location, out of home country!!!";
//            aStatus.errorcode = -98;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        } else if (retValue == -8) {
////            aStatus.error = "Mobile Trust Setting is not configured!!!";
//            aStatus.errorcode = -87;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        } else if (retValue == -9) {
////            aStatus.error = "Timestamp is null";
//            aStatus.errorcode = -141;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        } else if (retValue == -10) {
////            aStatus.error = "TimeStamp mistmatch!!!";
//            aStatus.errorcode = -142;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        } else if (retValue == -11) {
////            aStatus.error = "TimeStamp has expired!!!";
//            aStatus.errorcode = -143;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        } else if (retValue == -12) {
////            aStatus.error = "Invalid Location, out of home country!!!";
//            aStatus.errorcode = -98;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        } else if (retValue == -13) {
////            aStatus.error = "Invalid Location, roaming country is not allowed!!!";
//            aStatus.errorcode = -99;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        } else if (retValue == -14) {
////            aStatus.error = "Roaming duration is over";
//            aStatus.errorcode = -10;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        } else if (retValue == -15) {
//            // //aStatus.error = "TimeStamp Updated failed";
////            aStatus.error = "SUCCESS";
//            aStatus.errorcode = 0;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        } else if (retValue == -2) {
////            aStatus.error = "Session has expired";
//            aStatus.errorcode = -115;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        } else if (retValue == -1) {
////            aStatus.error = "General Exception";
//            aStatus.errorcode = -999;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        } else if (retValue == 0) {
////            aStatus.error = "SUCCESS";
//            aStatus.errorcode = 0;
//            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//        }
//        return aStatus;
//    }
//
//}
