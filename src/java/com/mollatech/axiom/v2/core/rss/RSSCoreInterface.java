package com.mollatech.axiom.v2.core.rss;

import com.mollatech.axiom.common.utils.InitTransactionPackage;
import com.mollatech.axiom.common.utils.SSOData;
import com.mollatech.axiom.common.utils.SSOResponse;
import com.mollatech.axiom.common.utils.TransactionStatus;
import com.mollatech.axiom.connector.communication.RemoteSigningInfo;
import com.mollatech.axiom.connector.user.AxiomChallengeResponse;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
//import com.mollatech.axiom.nucleus.db.ApEasylogin;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Remotesignature;
import com.mollatech.axiom.nucleus.db.Securephrase;
import com.mollatech.axiom.nucleus.db.Srtracking;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.settings.PasswordPolicySetting;
import com.mollatech.axiom.v2.core.utils.AuthenticationPackage;
import com.mollatech.axiom.v2.core.utils.AxiomData;
import com.mollatech.axiom.v2.core.utils.AxiomException;
import com.mollatech.axiom.v2.core.utils.AxiomMessage;
import com.mollatech.axiom.v2.core.utils.AxiomStatus;
import com.mollatech.axiom.v2.core.utils.AxiomTokenData;
import com.mollatech.axiom.v2.core.utils.EasyCheckIn;
import com.mollatech.axiom.v2.core.utils.EasyCheckInSessions;
import com.mollatech.axiom.v2.core.utils.RSSUserCerdentials;
import com.mollatech.axiom.v2.core.utils.RSSUserDetails;
import com.mollatech.axiom.v2.core.utils.VerifyRequest;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

//Service Endpoint Interface
//@WebService
@WebService(name = "rss")
public interface RSSCoreInterface {

    @WebMethod
    public String OpenSession(@WebParam(name = "channelid") String channelid, @WebParam(name = "loginid") String loginid, @WebParam(name = "loginpassword") String loginpassword, @WebParam(name = "integrityCheckString") String integrityCheckString);

    @WebMethod
    public AxiomStatus CloseSession(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "integrityCheckString") String integrityCheckString);

    @WebMethod
    public AxiomStatus CreateRSSUser(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "rssUser") RSSUserDetails rssUser, @WebParam(name = "integrityCheckString") String integrityCheckString, @WebParam(name = "trustPayLoad") String trustPayLoad);

    @WebMethod
    public AxiomStatus ChangeRSSUserDetails(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "rssUserID") String rssUserID, @WebParam(name = "rssUser") RSSUserDetails rssUser, @WebParam(name = "integrityCheckString") String integrityCheckString, @WebParam(name = "trustPayLoad") String trustPayLoad);

    @WebMethod
    public RSSUserCerdentials GetRSSUser(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "keyword") String keyword, @WebParam(name = "type") int type, @WebParam(name = "integrityCheckString") String integrityCheckString, @WebParam(name = "trustPayLoad") String trustPayLoad);

    @WebMethod
    public AxiomStatus ResetCredential(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "credentials") RSSUserCerdentials credential, @WebParam(name = "integrityCheckString") String integrityCheckString, @WebParam(name = "trustPayLoad") String trustPayLoad);

    @WebMethod
    public AxiomStatus ChangeCredential(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "credentials") RSSUserCerdentials credentials, @WebParam(name = "integrityCheckString") String integrityCheckString, @WebParam(name = "trustPayLoad") String trustPayLoad);

    @WebMethod
    public AxiomStatus AssignCredential(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "credentials") RSSUserCerdentials credentials, @WebParam(name = "integrityCheckString") String integrityCheckString, @WebParam(name = "trustPayLoad") String trustPayLoad);

    @WebMethod
    public AxiomChallengeResponse getQuestionsForRegistration(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "noOfQuestion") int noOfQuestion, @WebParam(name = "integrityCheckString") String integrityCheckString) throws AxiomException;

    @WebMethod
    public String GenerateSignatureCode(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "data") String[] data, @WebParam(name = "integrityCheckString") String integrityCheckString) throws AxiomException;

    @WebMethod
    public String EnforceSecurity(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "data") String data, @WebParam(name = "integrityCheckString") String integrityCheckString, @WebParam(name = "trustPayLoad") String trustPayLoad) throws AxiomException;

    @WebMethod
    public String ConsumeSecurity(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "data") String data, @WebParam(name = "integrityCheckString") String integrityCheckString, @WebParam(name = "trustPayLoad") String trustPayLoad) throws AxiomException;

    @WebMethod
    public AxiomStatus VerifyCredential(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "verifyRequest") VerifyRequest verifyRequest, @WebParam(name = "integrityCheckString") String integrityCheckString, @WebParam(name = "trustPayLoad") String trustPayLoad) throws AxiomException;

    @WebMethod
    public AxiomData VerifySignature(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "dataToSign") String dataToSign, @WebParam(name = "envelopedData") String envelopedData, @WebParam(name = "docType") int docType, @WebParam(name = "integrityCheckString") String integrityCheckString, @WebParam(name = "trustPayLoad") String trustPayLoad);

    @WebMethod
    public AxiomData ValidateUserAndSignTransaction(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "verifyRequest") VerifyRequest verifyRequest, @WebParam(name = "rsInfo") RemoteSigningInfo rsInfo, @WebParam(name = "bNotifyUser") boolean bNotifyUser, @WebParam(name = "integrityCheckString") String integrityCheckString, @WebParam(name = "trustPayLoad") String trustPayLoad) throws AxiomException;

    @WebMethod
    public AxiomStatus sendNotification(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type, @WebParam(name = "data") String[] data, @WebParam(name = "integrityCheckString") String integrityCheckString, @WebParam(name = "trustPayLoad") String trustPayLoad);

    @WebMethod
    public AxiomMessage[] SendMessages(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "msgs") AxiomMessage[] msgs, @WebParam(name = "bCheckContent") boolean bCheckContent, @WebParam(name = "speed") int speed, @WebParam(name = "type") int type) throws AxiomException;

//    @WebMethod
//    public AxiomStatus SendRegistrationCode(@WebParam(name = "sessionid")  String sessionid, @WebParam(name = "userid")  String userid, @WebParam(name = "type")  int type,@WebParam(name = "integrityCheckString")String integrityCheckString, @WebParam(name = "trustPayLoad")String trustPayLoad);
//
//     @WebMethod
//     public AxiomStatus SendNotifications(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "credentials") RSSUserCerdentials credentials, @WebParam(name = "integrityCheckString")String integrityCheckString, @WebParam(name = "trustPayLoad")String trustPayLoad);
    //nilesh Image auth
    @WebMethod
    public AxiomStatus IsRegistered(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "credentials") RSSUserCerdentials credentials, @WebParam(name = "integrityCheckString") String integrityCheckString, @WebParam(name = "trustPayLoad") String trustPayLoad);

    @WebMethod
    public String CheckUser(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "credentials") RSSUserCerdentials credentials, @WebParam(name = "cookieString") String cookieString, @WebParam(name = "integrityCheckString") String integrityCheckString, @WebParam(name = "trustPayLoad") String trustPayLoad) throws AxiomException;

    ;
@WebMethod
    public Securephrase GetImage(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "credentials") RSSUserCerdentials credentials, @WebParam(name = "cookieString") String cookieString, @WebParam(name = "authToken") String authToken, @WebParam(name = "integrityCheckString") String integrityCheckString, @WebParam(name = "trustPayLoad") String trustPayLoad) throws AxiomException;

    @WebMethod
    public AuthenticationPackage GenerateAuthenticationPackage(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "credentials") RSSUserCerdentials credentials, @WebParam(name = "cookieString") String cookieString, @WebParam(name = "authToken") String authToken, @WebParam(name = "integrityCheckString") String integrityCheckString, @WebParam(name = "trustPayLoad") String trustPayLoad) throws AxiomException;

    ;

    
    
//    @WebMethod
//    public String ActivateToken(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "credentials") RSSUserCerdentials credentials,  @WebParam(name = "regcode") String regcode,@WebParam(name = "integrityCheckString")String integrityCheckString, @WebParam(name = "trustPayLoad")String trustPayLoad)throws AxiomException;

    
    @WebMethod
    public AxiomTokenData ActivateToken(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "credentials") RSSUserCerdentials credentials, @WebParam(name = "regcode") String regcode, @WebParam(name = "integrityCheckString") String integrityCheckString, @WebParam(name = "trustPayLoad") String trustPayLoad) throws AxiomException;

    //manoj
    @WebMethod
    public AxiomStatus ALertEvent(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "signData") String signData, @WebParam(name = "type") int type, @WebParam(name = "integrityCheckString") String integrityCheckString) throws AxiomException;

    //two way msg
    @WebMethod
    public AxiomStatus initTransaction(@WebParam(name = "InitTransactionPackage") InitTransactionPackage package1, @WebParam(name = "integrityCheckString") String integrityCheckString, @WebParam(name = "trustPayLoad") String trustPayLoad) throws AxiomException;

    @WebMethod
    public TransactionStatus getStatus(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "txId") String txId, @WebParam(name = "integrityCheckString") String integrityCheckString, @WebParam(name = "trustPayLoad") String trustPayLoad) throws AxiomException;

    @WebMethod
    public AxiomStatus changeAuthStatus(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type, @WebParam(name = "status") int status, @WebParam(name = "integrityCheckString") String integrityCheckString, @WebParam(name = "trustPayLoad") String trustPayLoad) throws AxiomException;

    @WebMethod
    public SSOData getSSODetails(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "userid") String userid, @WebParam(name = "integrityCheckString") String integrityCheckString) throws AxiomException;

    @WebMethod
    public SSOResponse validateSSODetails(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "ssoId") String ssoId, @WebParam(name = "apId") String apId, @WebParam(name = "integrityCheckString") String integrityCheckString) throws AxiomException;

    //start amol 20-10-15 addition for partner portal
    @WebMethod
    public RSSUserDetails[] getUserByOrganisationName(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "oraganization") String oraganization) throws AxiomException;

    @WebMethod
    public String GetPassword(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "userid") String userid) throws AxiomException;

//    
    @WebMethod
    public RSSUserDetails getUser(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "userid") String userid) throws AxiomException;

    @WebMethod
    public String GeneratePassword(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "strPassword") String strPassword) throws AxiomException;

    @WebMethod
    public PasswordPolicySetting getSettingInner(String sessionId) throws AxiomException;

    @WebMethod
    public ChannelProfile getSetting(@WebParam(name = "sessionId") String sessionId) throws AxiomException;

    @WebMethod
    public Operators getOperatorById(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "id") String id) throws AxiomException;

    @WebMethod
    public int AssignPassword(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "userid") String userid, @WebParam(name = "strPassword") String strPassword) throws AxiomException;

    @WebMethod
    public Templates LoadbyName(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "templateType") int templateType) throws AxiomException;

    @WebMethod
    public int SendEmail(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "emailId") String emailId, @WebParam(name = "subject") String subject, @WebParam(name = "message") String message, @WebParam(name = "arrCC") String[] arrCC, @WebParam(name = "arrBCC") String[] arrBCC, @WebParam(name = "files") String[] files, @WebParam(name = "mimetypes") String[] mimetypes, @WebParam(name = "productType") int productType) throws AxiomException;

    @WebMethod
    public int SendEmailOperator(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "emailId") String emailId, @WebParam(name = "subject") String subject, @WebParam(name = "message") String message, @WebParam(name = "arrCC") String[] arrCC, @WebParam(name = "arrBCC") String[] arrBCC, @WebParam(name = "files") String[] files, @WebParam(name = "mimetypes") String[] mimetypes, @WebParam(name = "productType") int productType) throws AxiomException;

    @WebMethod
    public String MD5HashPassword(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "passwordToHash") String passwordToHash) throws AxiomException;

    @WebMethod
    public void AddPasswordTrail(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "operatorid") String operatorid, @WebParam(name = "password") String password) throws AxiomException;

    @WebMethod
    public int ValidatePassword(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "strPassword") String strPassword, @WebParam(name = "passwordSetting") PasswordPolicySetting passwordSetting) throws AxiomException;
    //end amol 20-10-15 addition for partner portal

    @WebMethod
    public Srtracking[] getAllEsignerTrackingDetails(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "userId") String userId, @WebParam(name = "status") int status, @WebParam(name = "type") int type, @WebParam(name = "startdate") String startdate, @WebParam(name = "enddate") String enddate) throws AxiomException;

    @WebMethod
    public AxiomStatus SignPdf(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "filename") String filename, @WebParam(name = "useremailId") String useremailId, @WebParam(name = "emailId") String emailId,
            @WebParam(name = "otp") String otp, @WebParam(name = "referenceId") String referenceId, @WebParam(name = "latitude") String latitude, @WebParam(name = "longitude") String longitude) throws AxiomException;

    @WebMethod
    public Remotesignature[] GetAllSignaturebyUserId(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "userid") String userid, int type);

    @WebMethod
    public AxiomStatus UpdateRemoteSignaturesByRequest(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "refid") String refid, @WebParam(name = "deviceid") String deviceid, @WebParam(name = "AuthorisationDetailsforPdf") String deviceDetails);

    @WebMethod
    public AxiomStatus SignPdfWithQR(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "filename") String filename, @WebParam(name = "emailId") String emailId, @WebParam(name = "otp") String otp, @WebParam(name = "referenceId") String referenceId, @WebParam(name = "latitude") String latitude, @WebParam(name = "longitude") String longitude, @WebParam(name = "qrCodeData") String qrCodeData, @WebParam(name = "allowedPhoneAndCountry") String allowedPhoneAndCountry) throws AxiomException;

    @WebMethod
    public AxiomData VerifyCredentialsAndSignTransactionRSS(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "SOTP") String SOTP, @WebParam(name = "challenge") String challenge, @WebParam(name = "Data") String[] Data, @WebParam(name = "type") int type, @WebParam(name = "PlusFactor") String PlusFactor, @WebParam(name = "clientip") String clientip);

    @WebMethod
    public String GenerateSignatureCodeRSS(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "Data") String[] Data, @WebParam(name = "clientip") String clientip) throws AxiomException;

    @WebMethod
    public AxiomStatus AddDownloadRequest(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "refid") String refid, @WebParam(name = "deviceid") String deviceid, @WebParam(name = "AuthorisationDetailsforPdf") String deviceDetails);

    @WebMethod
    public EasyCheckInSessions addEasyLoginRequest(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "easycheckinsession") EasyCheckInSessions easycheckinsession);

    @WebMethod
    public EasyCheckInSessions getEasyLoginRequest(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "userid") String userid);

    @WebMethod
    public int editEasyLoginRequest(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "easycheckinsession") EasyCheckInSessions easycheckinsession);

    @WebMethod
    public AxiomStatus verifyEasyLoginRequest(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "userid") String userid, @WebParam(name = "lattitude") String lattitude, @WebParam(name = "longitude") String longitude);

    @WebMethod
    public EasyCheckIn getEasyLogin(@WebParam(name = "sessionId") String sessionId, @WebParam(name = "EasyLoginId") String EasyLoginId);

 @WebMethod
    public AxiomStatus getQrImageWithRegCode(@WebParam(name="sessionId") String var1, @WebParam(name="userid") String var2, @WebParam(name="type") int var3);

  @WebMethod
    public AxiomStatus ping();
    
    
     @WebMethod
    public AxiomStatus GetGoogleAuthToken(@WebParam(name="sessionId") String sessionid,@WebParam(name="userid") String userid,@WebParam(name="bSendEmail") boolean bSendEmail,@WebParam(name="bReturnB64QRCode") boolean bReturnB64QRCode);
    

}
