/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.core.all;

import com.mollatech.axiom.connector.communication.RemoteSigningInfo;
import com.mollatech.axiom.connector.epin.QuestionsAndAnswers;
import com.mollatech.axiom.v2.core.utils.AxiomData;
import com.mollatech.axiom.v2.core.utils.AxiomException;
import com.mollatech.axiom.v2.core.utils.AxiomMessage;
import com.mollatech.axiom.v2.core.utils.AxiomStatus;
import com.mollatech.axiom.v2.core.utils.AxiomTokenDetails;
import com.mollatech.axiom.v2.core.utils.AxiomUser;
import com.mollatech.axiom.v2.core.utils.ConnectorStatus;
import com.mollatech.axiom.v2.core.utils.MessageReport;
import com.mollatech.axiom.v2.core.utils.MobileTrustStatus;

import com.mollatech.ftress.mapper.AuthenticationResponse;
import com.mollatech.ftress.mapper.Response;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author Human
 */
@WebService(name = "core")
public interface CoreInterface {
    
     final String itemtype = "MOBILETRUST";
     final static int ANDROID = 1;
    public final static int IOS = 2;
    public int GETUSER_BY_USERID = 1;
    public int GETUSER_BY_PHONENUMBER = 2;
    public int GETUSER_BY_EMAILID = 3;
    public int GETUSER_BY_FULLNAME = 4;
    public int RESET_USER_PASSWORD = 1;
    public int RESET_USER_TOKEN_OOB = 2;
    public int RESET_USER_TOKEN_SOFTWARE = 3;
    public int RESET_USER_TOKEN_HARDWARE = 4;
    public int RESET_USER_CERT = 5;
    public int RESET_USER_PKI_TOKEN_SOFTWARE = 6;
    public int RESET_USER_PKI_TOKEN_HARDWARE = 7;

    public int OTP_TOKEN_OUTOFBAND = 3;
    public int OTP_TOKEN_SOFTWARE = 1;
    public int OTP_TOKEN_HARDWARE = 2;
    public int OTP_TOKEN_OUTOFBAND_SMS = 1;
    public int OTP_TOKEN_OUTOFBAND_VOICE = 2;
    public int OTP_TOKEN_OUTOFBAND_USSD = 3;
    public int OTP_TOKEN_OUTOFBAND_EMAIL = 4;
    public int OTP_TOKEN_SOFTWARE_MOBILE = 2;
    public int OTP_TOKEN_SOFTWARE_PC = 3;
    public int OTP_TOKEN_SOFTWARE_WEB = 1;
    public int OTP_TOKEN_HARDWARE_MINI = 1;
    public int OTP_TOKEN_HARDWARE_CR = 2;
//    private int MESSAGE_AS_SMS = 1;
//    private int MESSAGE_AS_VOICE = 2;
//    private int MESSAGE_AS_USSD = 3;
//    private int MESSAGE_AS_MAIL = 4;
    public int TOKEN_STATUS_UNASSIGNED = -10;
    public int TOKEN_STATUS_ACTIVE = 1;
    public int TOKEN_STATUS_SUSPENDED = -2;
    //private int TOKEN_STATUS_PENDING = 0;
    //public int MESSAGES_BLOCKED= -5;
    public String strACTIVE = "ACTIVE";
    public String strSUSPENDED = "SUSPENDED";
    //private String strPENDING = "PENDING";
    //public String strBLOCKED = "BLOCKED";    
    public String strUNASSIGNED = "UNASSIGNED";
    //private String USER_ASSIGN_PASSWORD_TEMPLATE_NAME = "User Password";
    public int SEND_MESSAGE_PENDING_STATE = 2;
    public String SEND_MESSAGE_PENDING_STATE_STRING = "PENDING";
    public int SEND_MESSAGE_BLOCKED_STATE = -5;
    public String SEND_MESSAGE_BLOCKED_STATE_STRING = "BLOCKED";
    public int SEND_MESSAGE_INVALID_DATA = -4;
    public String SEND_MESSAGE_INVALID_DATA_STRING = "INVALIDDATA";
    public int INVALID_IP = -3;
    public int INVALID_REMOTEUSER = -4;
    public int INVALID_LICENSE = -5;
    public int EPINSMS = 1;
    public int EPINIVR = 2;
    
    

//    public int GETUSER_BY_USERID = 1;
//    public int GETUSER_BY_PHONENUMBER = 2;
//    public int GETUSER_BY_EMAILID = 3;
//    public int GETUSER_BY_FULLNAME = 4;
//
//    public int RESET_USER_PASSWORD = 1;
//    public int RESET_USER_TOKEN_OOB = 2;
//    public int RESET_USER_TOKEN_SOFTWARE = 3;
//    public int RESET_USER_TOKEN_HARDWARE = 4;
//
//    public int OTP_TOKEN_SOFTWARE = 1;
//    public int OTP_TOKEN_SOFTWARE_WEB = 1;
//    public int OTP_TOKEN_SOFTWARE_MOBILE = 2;
//    public int OTP_TOKEN_SOFTWARE_PC = 3;
//
//    public int TOKEN_STATUS_UNASSIGNED = -10;
//    public int TOKEN_STATUS_ACTIVE = 1;
//    public int TOKEN_STATUS_SUSPENDED = -2;
//    public int TOKEN_STATUS_PENDING = 0;
//
//    public String strACTIVE = "ACTIVE";
//    public String strSUSPENDED = "SUSPENDED";
//    public String strPENDING = "PENDING";
//    public String strUNASSIGNED = "UNASSIGNED";
//
//    public int WRONG_ATTEMPT_DESTROY = 1;
//    public int INTENTIONALLY_DESTROY = 2;
//    public int FORCE_DESTROY = 3;
//
//    public String USER_ASSIGN_PASSWORD_TEMPLATE_NAME = "User Password";
//
//    public int SEND_MESSAGE_PENDING_STATE = 0;
//    public String SEND_MESSAGE_PENDING_STATE_STRING = "PENDING";
//
//    public int SEND_MESSAGE_BLOCKED_STATE = -5;
//    public String SEND_MESSAGE_BLOCKED_STATE_STRING = "BLOCKED";
//    public int SEND_MESSAGE_INVALID_DATA = -4;
//    public String SEND_MESSAGE_INVALID_DATA_STRING = "INVALIDDATA";
//    public final int LOGIN = 2;
//    // AUTHENTICATE = 1
//    public final int TRANSACTION = 3;
//    // AUTHORIZE = 2
//    
//    public int RESET_USER_MOBILETRUST = 5;
//    public int OTP_TOKEN_OUTOFBAND_SMS = 1;
//    public int OTP_TOKEN_OUTOFBAND_VOICE = 2;
//    public int OTP_TOKEN_OUTOFBAND_USSD = 3;
//    public int OTP_TOKEN_OUTOFBAND_EMAIL = 4;
//    public int OTP_TOKEN_HARDWARE_MINI = 1;
//    public int OTP_TOKEN_HARDWARE_CR = 2;

    @WebMethod
    public String OpenSessionAP(@WebParam(name = "channelid") String channelid, @WebParam(name = "loginid") String loginid, @WebParam(name = "loginpassword") String loginpassword);
    //public String OpenSession(@WebParam(name = "channelid") String channelid, @WebParam(name = "loginid") String loginid, @WebParam(name = "loginpassword") String loginpassword);

    @WebMethod
    public String OpenSessionMT(@WebParam(name = "channelid") String channelid, @WebParam(name = "loginid") String loginid, @WebParam(name = "loginpassword") String loginpassword);
    
    
    @WebMethod
    public AxiomStatus CloseSessionAP(@WebParam(name = "sessionid") String sessionid);
    //public AxiomStatus CloseSession(@WebParam(name = "sessionid") String sessionid);
    
    @WebMethod
    public AxiomStatus CloseSessionMT(@WebParam(name = "sessionid") String sessionid);

    @WebMethod
    public AxiomStatus CreateUserAP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "fullname") String fullname, @WebParam(name = "phone") String phone, @WebParam(name = "email") String email, @WebParam(name = "userid") String userid);
    //public AxiomStatus CreateUserA(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "fullname") String fullname, @WebParam(name = "phone") String phone, @WebParam(name = "email") String email, @WebParam(name = "userid") String userid);
    
    @WebMethod
    public AxiomStatus CreateUserMT(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "fullname") String fullname, @WebParam(name = "phone") String phone, @WebParam(name = "email") String email, @WebParam(name = "userid") String userid);
    

    @WebMethod
    public AxiomUser GetUserByAP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "type") int type, @WebParam(name = "searchFor") String searchFor) throws AxiomException;
    //public AxiomUser GetUserBy(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "type") int type, @WebParam(name = "searchFor") String searchFor) throws AxiomException;
    
    @WebMethod
    public AxiomUser GetUserByMT(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "type") int type, @WebParam(name = "searchFor") String searchFor) throws AxiomException;
    

    @WebMethod
    public AxiomStatus ResetUserAP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type);
    //public AxiomStatus ResetUserA(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type);
    
    @WebMethod
    public AxiomStatus ResetUserMT(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type);
    

    @WebMethod
    public AxiomStatus AssignPasswordAP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "password") String password, @WebParam(name = "bSentToUser") boolean bSentToUser, @WebParam(name = "subcategory") int subcategory);
    //public AxiomStatus AssignPassword(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "password") String password, @WebParam(name = "bSentToUser") boolean bSentToUser, @WebParam(name = "subcategory") int subcategory);

    @WebMethod
    public AxiomStatus VerifyPasswordAP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "password") String password);
    //public AxiomStatus VerifyPassword(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "password") String password);

    @WebMethod
    public AxiomStatus AssignTokenAP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type, @WebParam(name = "subtype") int subtype);
    //public AxiomStatus AssignToken(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type, @WebParam(name = "subtype") int subtype);

    @WebMethod
    public AxiomTokenDetails[] GetUserTokensAP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid) throws AxiomException;
    //public AxiomTokenDetails[] GetUserTokens(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid) throws AxiomException;

    @WebMethod
    public AxiomStatus SendOTPAP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid);
    //public AxiomStatus SendOTP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid);

    @WebMethod
    public AxiomStatus VerifyOTPAP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "otp") String otp);
    //public AxiomStatus VerifyOTP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "otp") String otp);

    @WebMethod
    public AxiomStatus SendSignatureOTPAP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "data") String[] data);
    //public AxiomStatus SendSignatureOTP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "data") String[] data);

    @WebMethod
    public AxiomStatus VerifySignatureOTPAP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "data") String[] data, @WebParam(name = "otp") String otp);
    //public AxiomStatus VerifySignatureOTP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "data") String[] data, @WebParam(name = "otp") String otp);

    @WebMethod
    public AxiomMessage[] SendMessagesAP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "msgs") AxiomMessage[] msgs, @WebParam(name = "bCheckContent") boolean bCheckContent, @WebParam(name = "speed") int speed, @WebParam(name = "type") int type) throws AxiomException;
    //public AxiomMessage[] SendMessages(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "msgs") AxiomMessage[] msgs, @WebParam(name = "bCheckContent") boolean bCheckContent, @WebParam(name = "speed") int speed, @WebParam(name = "type") int type) throws AxiomException;

    @WebMethod
    public AxiomStatus ChangeOTPTokenStatusAP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "category") int category, @WebParam(name = "value") int value);
    //public AxiomStatus ChangeOTPTokenStatus(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "category") int category, @WebParam(name = "value") int value);

    @WebMethod
    public ConnectorStatus GetConnectorsStatusAP(@WebParam(name = "channelid") String channelid, @WebParam(name = "loginid") String loginid, @WebParam(name = "password") String password);
    //public ConnectorStatus GetConnectorsStatus(@WebParam(name = "channelid") String channelid, @WebParam(name = "loginid") String loginid, @WebParam(name = "password") String password);

    @WebMethod
    public AxiomStatus AssignTokenAndGenerateCertificateAP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type, @WebParam(name = "subtype") int subtype, @WebParam(name = "srno") String srno);
    //public AxiomStatus AssignTokenAndGenerateCertificate(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type, @WebParam(name = "subtype") int subtype, @WebParam(name = "srno") String srno);

    @WebMethod
    public AxiomData VerifyOTPAndSignTransactionAP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "otp") String otp, @WebParam(name = "rsInfo") RemoteSigningInfo rsInfo, @WebParam(name = "bNotifyUser") boolean bNotifyUser);
    //public AxiomData VerifyOTPAndSignTransaction(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "otp") String otp, @WebParam(name = "rsInfo") RemoteSigningInfo rsInfo, @WebParam(name = "bNotifyUser") boolean bNotifyUser);
    
    //public AxiomData VerifyOTPAndSignTransaction(String sessionid, String userid, String otp, String dataToSign, int docType, boolean bNotifyUser);
    

    @WebMethod
    public AxiomData VerifySignatureAP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "dataToSign") String dataToSign, @WebParam(name = "envelopedData") String envelopedData, @WebParam(name = "docType") int docType);
    //public AxiomData VerifySignature(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "dataToSign") String dataToSign, @WebParam(name = "envelopedData") String envelopedData, @WebParam(name = "docType") int docType);

    //For software token >> regcode = challenge and response is Hex(2 6digit OTPs). 
    @WebMethod
    public AxiomStatus ValidateRegistrationAP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "challenge") String challenge, @WebParam(name = "response") String response);
    //public AxiomStatus ValidateRegistration(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "challenge") String challenge, @WebParam(name = "response") String response);

    @WebMethod
    public MessageReport[] FetchStatusAP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "phonNo") String phonNo, @WebParam(name = "startDate") String startDate, @WebParam(name = "endDate") String endDate) throws AxiomException;
    //public MessageReport[] FetchStatus(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "phonNo") String phonNo, @WebParam(name = "startDate") String startDate, @WebParam(name = "endDate") String endDate) throws AxiomException;

//    @WebMethod
//    public AxiomStatus ResetUser(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid);
    
    @WebMethod
    public AxiomStatus AssignMT(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "subtype") int subtype);
    //public AxiomStatus Assign(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "subtype") int subtype);

    @WebMethod
    public MobileTrustStatus ActivateMT(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "SwTokenData") String SwTokenData);
    //public MobileTrustStatus Activate(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "SwTokenData") String SwTokenData);

    @WebMethod
    public String GetTimeStampMT(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid); // client encrypt(server sign(timestamp, expiry time, session id,channel id, client hardware profile))
    //public String GetTimeStamp(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid); // client encrypt(server sign(timestamp, expiry time, session id,channel id, client hardware profile))

    @WebMethod
    public AxiomStatus VerifyOTPPlusMT(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type, @WebParam(name = "otp") String otp, @WebParam(name = "otpPlus") String otpPlus);
    //public AxiomStatus VerifyOTPPlus(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type, @WebParam(name = "otp") String otp, @WebParam(name = "otpPlus") String otpPlus);

    @WebMethod
    public AxiomStatus VerifySignatureOTPPlusMT(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type, @WebParam(name = "data") String[] data, @WebParam(name = "sotp") String sotp, @WebParam(name = "sotpPlus") String sotpPlus);
    //public AxiomStatus VerifySignatureOTPPlus(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type, @WebParam(name = "data") String[] data, @WebParam(name = "sotp") String sotp, @WebParam(name = "sotpPlus") String sotpPlus);

    @WebMethod
    public AxiomStatus VerifyDigitalSignatureMT(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "data") String data, @WebParam(name = "type") int type, @WebParam(name = "signature") String signature);
    //public AxiomStatus VerifyDigitalSignature(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "data") String data, @WebParam(name = "type") int type, @WebParam(name = "signature") String signature);

    @WebMethod
    public AxiomStatus VerifyDigitalSignaturePlusMT(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "data") String data, @WebParam(name = "type") int type, @WebParam(name = "signature") String signature, @WebParam(name = "signaturePlus") String signaturePlus);
    //public AxiomStatus VerifyDigitalSignaturePlus(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "data") String data, @WebParam(name = "type") int type, @WebParam(name = "signature") String signature, @WebParam(name = "signaturePlus") String signaturePlus);

    @WebMethod
    public AxiomStatus ALertEventMT(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "signData") String signData);
    //public AxiomStatus ALertEvent(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "signData") String signData);

    @WebMethod
    public AuthenticationResponse verifyEBankingPassword(@WebParam(name = "systemSessionId") String systemSessionId, @WebParam(name = "userid") String userid, @WebParam(name = "password") String password, @WebParam(name = "fds_options") int fds_options, @WebParam(name = "channel") String channel, @WebParam(name = "domain") String domain);

    @WebMethod
    public Response resetUser(@WebParam(name = "systemSessionId") String systemSessionId, @WebParam(name = "authenticationType") String authenticationType, @WebParam(name = "userid") String userid, @WebParam(name = "channel") String channel, @WebParam(name = "domain") String domain);

    @WebMethod
    public AuthenticationResponse changeEbankingPassword(@WebParam(name = "systemSessionId") String systemSessionId, @WebParam(name = "userid") String userid, @WebParam(name = "oldpassword") String oldpassword, @WebParam(name = "newpassword") String newpassword, @WebParam(name = "channel") String channel, @WebParam(name = "domain") String domain);

    @WebMethod
    public Response assignPwdAuthenticationType(@WebParam(name = "systemSessionId") String systemSessionId, @WebParam(name = "userid") String userid, @WebParam(name = "password") String password, @WebParam(name = "channel") String channel, @WebParam(name = "domain") String domain);

    @WebMethod
    public AuthenticationResponse verifyEBankingOTP(@WebParam(name = "systemSessionId") String systemSessionId, @WebParam(name = "userid") String userid, @WebParam(name = "otptoken") String otptoken, @WebParam(name = "password") String password, @WebParam(name = "channel") String channel, @WebParam(name = "domain") String domain);

    @WebMethod
    public Response createUser(@WebParam(name = "systemSessionId") String systemSessionId, @WebParam(name = "userid") String userid, @WebParam(name = "firstname") String firstname, @WebParam(name = "lastname") String lastname, @WebParam(name = "postcode") String postcode, @WebParam(name = "address1") String address1, @WebParam(name = "address2") String address2, @WebParam(name = "city") String city, @WebParam(name = "mobileno") String mobileno, @WebParam(name = "channel") String channel, @WebParam(name = "domain") String domain);

    @WebMethod
    public Response systemLogout(@WebParam(name = "systemSessionId") String systemSessionId, @WebParam(name = "channel") String channel, @WebParam(name = "domain") String domain);

    @WebMethod
    public AuthenticationResponse systemLogin(@WebParam(name = "userid") String userid, @WebParam(name = "password") String password, @WebParam(name = "channel") String channel, @WebParam(name = "domain") String domain);

    @WebMethod
    public Response assignOtpAuthenticationType(@WebParam(name = "systemSessionId") String systemSessionId, @WebParam(name = "userid") String userid, @WebParam(name = "otptoken") String otptoken, @WebParam(name = "channel") String channel, @WebParam(name = "domain") String domain);

    @WebMethod
    public Response ping();

    @WebMethod
    //public AxiomStatus initiateEpinRequest(@WebParam(name = "sessionid")String sessionid,@WebParam(name = "phoneNumber")String phoneNumber);
    public AxiomStatus initiateRequestEPIN(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "inChannel") int inChannel);

    @WebMethod
    public void AddTrailRA(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "source") String source, @WebParam(name = "type") int type, @WebParam(name = "txDetails") String[][] txDetails, @WebParam(name = "ip") String ip, @WebParam(name = "requestheaders") String[][] requestheaders, @WebParam(name = "extendsDetails") String[][] extendsDetails);

    @WebMethod
    public int IsAdditionalAuthenticationNeededRA(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid);

    @WebMethod
    public QuestionsAndAnswers GetQuestionsEPIN(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid);

    @WebMethod
    public AxiomStatus verifyAnswersAndSendEPIN(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "QandA") QuestionsAndAnswers QandA);

    @WebMethod
    public AxiomStatus DeleteUserAP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid);
    //public AxiomStatus DeleteUser(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid);

    @WebMethod
    public AxiomStatus ChangeUserDetailsAP(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "fullname") String fullname, @WebParam(name = "phone") String phone, @WebParam(name = "email") String email);
    //public AxiomStatus ChangeUserDetails(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "fullname") String fullname, @WebParam(name = "phone") String phone, @WebParam(name = "email") String email);

    @WebMethod
    public String EnforceSecurityMT(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "data") String data);
    //public String EnforceSecurity(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "data") String data);

    @WebMethod
    public String ConsumeSecurityMT(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "data") String data);
   
    
    @WebMethod
    public AxiomStatus ResyncToken(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "catgory") int catgory, @WebParam(name = "otp1") String otp1, @WebParam(name = "otp2") String otp2);
    
    
     
    //@WebMethod
    //public AxiomStatus IsUserRegistered(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid);

//    @WebMethod
//    public AxiomChallengeResponse getQuestionsForRegistration(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "noOfQuestion") int noOfQuestion) throws AxiomException;

//    @WebMethod
//    public AxiomStatus registerAnswersForUser(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "QAndA") AxiomChallengeResponse QAndA);

//    @WebMethod
//    public AxiomStatus registerAnswersForUserAndGenerateCertificate(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "QAndA") AxiomChallengeResponse QAndA);

//    @WebMethod
//    public AxiomChallengeResponse getQuestionsForValidation(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid) throws AxiomException;

    //@WebMethod
    //public AxiomStatus ValidateUserAnswers(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "QAndA") AxiomChallengeResponse QAndA);

//    @WebMethod
//    public AxiomData ValidateUserAnswersAndSignTransaction(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "QAndA") AxiomChallengeResponse QAndA, @WebParam(name = "rsInfo") RemoteSigningInfo rsInfo, @WebParam(name = "bNotifyUser") boolean bNotifyUser) throws AxiomException;

//    @WebMethod
//    public AxiomStatus changeAnswersForUser(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "QAndA") AxiomChallengeResponse QAndA);

//    @WebMethod
//    public AxiomStatus ValidateUserAnswersAndResetUser(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "QAndA") AxiomChallengeResponse QAndA, @WebParam(name = "type") int type);

//    @WebMethod
//    public AxiomStatus ValidateUserAnswersAndChangePassword(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "password") String password, @WebParam(name = "QAndA") AxiomChallengeResponse QAndA, @WebParam(name = "bSendOnMobile") boolean bSendOnMobile, @WebParam(name = "type") int type);

//    @WebMethod
//    public AxiomStatus validateRegistrationCode(String sessionid, String userid, String regCode, String deviceid);
}
