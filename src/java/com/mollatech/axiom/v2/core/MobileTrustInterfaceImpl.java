/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.core;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.mobiletrust.Location;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.connector.user.TokenStatusDetails;
import com.mollatech.axiom.mobiletrust.crypto.CryptoManager;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Countrylist;
import com.mollatech.axiom.nucleus.db.Geofence;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Otptokens;
import com.mollatech.axiom.nucleus.db.Registrationcodetrail;
import com.mollatech.axiom.nucleus.db.Sessions;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.GeoFenceUtils;
import com.mollatech.axiom.nucleus.db.connector.GeoTrackingUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import com.mollatech.axiom.nucleus.db.connector.management.CryptoManagement;
import com.mollatech.axiom.nucleus.db.connector.management.LocationManagement;
import com.mollatech.axiom.nucleus.db.connector.management.MobileTrustManagment;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import static com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement.TOKEN;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.RegistrationCodeTrailManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import com.mollatech.axiom.nucleus.settings.MobileTrustSettings;
import com.mollatech.axiom.nucleus.settings.RootCertificateSettings;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import com.mollatech.axiom.nucleus.settings.TokenSettings;
import com.mollatech.axiom.v2.core.utils.AxiomException;
import com.mollatech.axiom.v2.core.utils.AxiomStatus;
import com.mollatech.axiom.v2.core.utils.AxiomUser;
import com.mollatech.axiom.v2.core.utils.MobileTrustStatus;
import java.io.ByteArrayInputStream;
import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import org.bouncycastle.util.encoders.Base64;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author mollatech2
 */
@WebService(endpointInterface = "com.mollatech.axiom.v2.core.MobileTrustInterface")
public class MobileTrustInterfaceImpl implements MobileTrustInterface {
    
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(MobileTrustInterfaceImpl.class.getName());

    private final static int ANDROID = 1;
    private final static int IOS = 2;

    final String itemtype = "MOBILETRUST";
    @Resource
    WebServiceContext wsContext;

    @Override
    public String OpenSession(String channelid, String loginid, String loginpassword) throws AxiomException {
        try {

            String strDebug = null;
            SettingsManagement setManagement = new SettingsManagement();
            try {

                ChannelProfile channelprofileObj = null;
                Object channelpobj = setManagement.getSettingInner(channelid, SettingsManagement.CHANNELPROFILE_SETTING, 1);

                if (channelpobj == null) {
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                } else {
                    channelprofileObj = (ChannelProfile) channelpobj;
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                }
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    log.debug("OpenSession::loginpassword::" + loginpassword);
                    log.debug("OpenSession::channelid::" + channelid);
                    log.debug("OpenSession::loginid::" + loginid);
                }
            } catch (Exception ex) {
                log.error(ex);
                throw new AxiomException("Invalid Credentials!!!");
            }

            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                log.error("License Error::" + iResult);                
                throw new AxiomException("Invalid License (" + iResult + ")");
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(channelid);
            if (channel == null) {
                log.error("ChannelManagement returned empty channel for channel id " + channelid);                
                throw new AxiomException("Channel ID not found (" + iResult + ")");
            }

            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            //log.debug(("Client IP = " + req.getRemoteAddr());
            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        try {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            SessionFactoryUtil suOperators = new SessionFactoryUtil(SessionFactoryUtil.operators);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                return null;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            return null;
                        } catch (Exception e) {
                            log.error(e);
                            throw e;
                        }
                    }
                    
                    throw new AxiomException("Invalid IP");
                }
            }

//            int result = setManagement.checkIP(channelid, req.getRemoteAddr());
//            if (checkIp != 1) {
//                return null;
//            }
            String resultStr = "Failure";
            int retValue = -1;
            SessionManagement sManagement = new SessionManagement();
            String sessionId = sManagement.OpenSessionForWS(channelid, loginid, loginpassword, req.getSession().getId());

            AuditManagement audit = new AuditManagement();
            if (sessionId != null) {
                retValue = 0;
                resultStr = "Success";
                audit.AddAuditTrail(sessionId, channelid, loginid,
                        req.getRemoteAddr(),
                        channel.getName(), loginid,
                        loginid, new Date(), "Open Session", resultStr, retValue,
                        "Login", "", "Open Session successfully with Session Id =" + sessionId, "SESSION",
                        loginid);

            } else if (sessionId == null) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), loginid, req.getRemoteAddr(), channel.getName(), loginid,
                        loginid, new Date(), "Open Session", resultStr, retValue,
                        "Login", "", "Failed To Open Session", "SESSION",
                        loginid);

            }

            return sessionId;
        } catch (Exception e) {
            log.error(e);
            throw new AxiomException(e.getMessage());
        }
    }

    @Override
    public AxiomStatus CloseSession(String sessionid) {
        try {

            String strDebug = null;
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    log.debug("CloseSession::sessionid::" + sessionid);
                }
            } catch (Exception ex) {
                log.error(ex);
            }

            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                log.error("ValidateLicense failed=" + iResult);
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Licence invalid";
                aStatus.errorcode = -100;
                return aStatus;
            }

            SessionManagement sManagement = new SessionManagement();
            Sessions session = sManagement.getSessionById(sessionid);
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;

            if (session != null) {

                //log.debug(("Client IP = " + req.getRemoteAddr());        
                SettingsManagement setManagement = new SettingsManagement();
//                int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
//                if (result != 1) {
//                    aStatus.error = "INVALID IP REQUEST";
//                    aStatus.errorcode = -8;
//                    //  return aStatus;
//                }
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    log.error("get channel failed=" + channelid);
                    aStatus.error = "Invalid Channel";
                    aStatus.errorcode = -3;
                    return aStatus;
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                log.error("invalid ip");
                                aStatus.error = "INVALID IP";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            log.error("invalid ip");
                            aStatus.error = "INVALID IP";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        log.error("invalid ip");
                        aStatus.error = "INVALID IP";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }
                int retValue = sManagement.CloseSession(sessionid);

                aStatus.error = "ERROR";
                aStatus.errorcode = retValue;
                if (retValue == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                }

            } else if (session == null) {
                log.error("session is null");
                return aStatus;
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                log.error("channel is empty for id=" + session.getChannelid());
                return aStatus;
            }
            AuditManagement audit = new AuditManagement();
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                    session.getLoginid(), new Date(), "Close Session", aStatus.error, aStatus.errorcode,
                    "Login", "", "Close Session successfully with Session Id =" + sessionid, "SESSION",
                    session.getLoginid());
            return aStatus;
        } catch (Exception e) {
            log.error(e);
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "General Exception::" + e.getMessage();
            aStatus.errorcode = -99;
            return aStatus;
        }
    }

    @Override
    public AxiomStatus CreateUser(String sessionId, String fullname, String phone, String email, String userid) {

        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug("CreateUser::sessionId::" + sessionId);
                log.debug("CreateUser::fullname::" + fullname);
                log.debug("CreateUser::phone::" + phone);
                log.debug("CreateUser::email::" + email);
                log.debug("CreateUser::userid::" + userid);
            }
        } catch (Exception ex) {
            log.error(ex);
        }

        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            log.error("license is invalid with code as "  +iResult);
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Licence invalid";
            aStatus.errorcode = -100;
            return aStatus;
        }

        try {

            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            AxiomStatus aStatus = new AxiomStatus();
            if (session == null) {
                log.error("Invalid Session for id as " + sessionId);
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                return aStatus;
            }

            if (sessionId == null || fullname == null || phone == null
                    || sessionId.isEmpty() == true || fullname.isEmpty() == true || phone.isEmpty() == true
                    || email == null || email.isEmpty() == true) {
                log.error("Invalid Data");
                aStatus.error = "Invalid Data";
                aStatus.errorcode = -11;
                return aStatus;
            }

            if (userid != null && userid.isEmpty() == true) {
                //AxiomStatus aStatus = new AxiomStatus();
                
                aStatus.error = "User ID cannot be empty/null!!!";
                log.error(aStatus.error);
                aStatus.errorcode = -12;
                return aStatus;
            }

//            int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                AxiomStatus aStatus = new AxiomStatus();
//                aStatus.error = "Invalid License";
//                aStatus.errorcode = -10;
//                return aStatus;
//            }
            UserManagement uManagement = new UserManagement();
            //SessionManagement sManagement = new SessionManagement();
            //MessageContext mc = wsContext.getMessageContext();
            //HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            //Sessions session = sManagement.getSessionById(sessionId);
            //AxiomStatus aStatus = new AxiomStatus();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;

            if (session != null) {

                //log.debug(("Client IP = " + req.getRemoteAddr());        
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return null;
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                
                                aStatus.error = "Invalid IP";
                                aStatus.errorcode = -8;
                                log.error(aStatus.error);
                                return aStatus;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "Invalid IP";
                            aStatus.errorcode = -8;
                            log.error(aStatus.error);
                            return aStatus;
                        }
                        aStatus.error = "Invalid IP";
                        aStatus.errorcode = -8;
                        log.error(aStatus.error);
                        return aStatus;
                    }
                }

                //addition for license check enforcement
                if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_PASSWORD) != 0) {
                    
                    aStatus.error = "This feature is not available in this license!!!";
                    log.error(aStatus.error);
                    aStatus.errorcode = -100;
                    return aStatus;
                }

                int iUserCount = uManagement.getCountOfLicenseUser(channel.getChannelid());
                int licensecount = AxiomProtect.GetUsersAllowed(); //AxiomProtect Function here now default 0. 
                if (licensecount == -998) {
                    //unlimited licensing 
                } else if (iUserCount >= licensecount) {
                    aStatus.error = "User Addition already reached its limit as per this license!!!";
                    log.error(aStatus.error);
                    aStatus.errorcode = -100;
                    return aStatus;
                }
                //end of addition

                retValue = uManagement.CreateUser(sessionId, session.getChannelid(), fullname, phone, email, userid, 0, null, null, null, null, null, null, null, null);
                if (retValue == 0) {
                    aStatus.error = "SUCCESS";
                    
                    aStatus.errorcode = 0;
                } else if (retValue == 1) {
                    //aStatus.error = "SUCCESS";
                    //aStatus.errorcode = 0;
                    aStatus.error = "User created succesfully but message could not be sent!!!";
                    log.info(aStatus.error);
                    aStatus.errorcode = 1;

                } else if (retValue == -1) {
                    aStatus.error = "Duplicate Entry!!!";
                    log.error(aStatus.error);
                    aStatus.errorcode = -2;
                }
                //return aStatus;
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            //AuditManagement audit = new AuditManagement();
            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
                    req.getRemoteAddr(), channel.getName(),
                    session.getLoginid(), session.getLoginid(),
                    new Date(), "Create User",
                    aStatus.error, aStatus.errorcode,
                    "User Management", "",
                    "Name=" + fullname + ",Phone=" + phone + ",Email=" + email + ",State=Inactive",
                    "USER", "");
            return aStatus;
        } catch (Exception e) {
            log.error(e);
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "General Exception::" + e.getMessage();
            aStatus.errorcode = -99;
            return aStatus;
        }
    }

    @Override
    public AxiomUser GetUserBy(String sessionId, int type, String searchFor) throws AxiomException {

        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug("GetUserBy::sessionId::" + sessionId);
                log.debug("GetUserBy::type::" + type);
                log.debug("GetUserBy::searchFor::" + searchFor);
            }
        } catch (Exception ex) {
            log.error(ex);
        }

        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            log.error("Licence invalid");
            throw new AxiomException("Licence invalid");
        }

        try {
//            int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                return null;
//            }                        

            UserManagement uManagement = new UserManagement();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            String resultStr = "Failure";
            int retValue = -1;
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            AxiomUser aUser = null;

            if (sessionId == null) {
                log.error("Session ID is empty!!!");
                throw new AxiomException("Session ID is empty!!!");
            }

            if (session == null) {
                log.error("Session ID is invalid/expired!!!");
                throw new AxiomException("Session ID is invalid/expired!!!");
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
//        if (channel == null) {
//            return null;
//        }
            if (session != null) {

                SettingsManagement setManagement = new SettingsManagement();
//                int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
//                if (result != 1) {
//                    throw new Exception("IP is not allowed!!!");
//                }

                String channelid = session.getChannelid();

                if (channel == null) {
                    log.error("Channel ID not found");
                    throw new AxiomException("Channel ID not found");
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                log.error("Invalid IP");
                                throw new AxiomException("Invalid IP");
                            }

                            suTemplate.close();
                            sTemplate.close();
                            log.error("Invalid IP");
                            throw new AxiomException("Invalid IP");
                        }
                        log.error("Invalid IP");
                        throw new AxiomException("Invalid IP");
                    }
                }

                String userFlag = LoadSettings.g_sSettings.getProperty("check.user");
                AuthUser authUser = null;
                if (userFlag.equals("1")) {
                    authUser = uManagement.CheckUserByType(sessionId, session.getChannelid(), searchFor, 4); // search by name
                } else if (type == 5) {
                    RegistrationCodeTrailManagement rManagement = new RegistrationCodeTrailManagement();
                    searchFor = UtilityFunctions.Bas64SHA1(searchFor);
                    Registrationcodetrail regObj = rManagement.getRegistrationcodetrail(channelid, searchFor);
                    String userid = null;
                    if (regObj != null) {
                        userid = regObj.getUserid();
                        authUser = uManagement.CheckUserByType(sessionId, session.getChannelid(), userid, 1); // search by type as passed
                    } else {
                        log.error("Registration code not found");
                        throw new AxiomException("Registration code not found");

                    }
                } else {
                    authUser = uManagement.CheckUserByType(sessionId, session.getChannelid(), searchFor, type); // search by type as passed
                }
                if (authUser != null) {
                    aUser = new AxiomUser();
                    aUser.email = authUser.email;
                    aUser.iAttempts = authUser.iAttempts;
                    aUser.lCreatedOn = authUser.lCreatedOn;
                    aUser.lLastAccessOn = authUser.lLastAccessOn;
                    aUser.phoneNo = authUser.phoneNo;
                    aUser.statePassword = authUser.statePassword;
                    aUser.userId = authUser.userId;
                    aUser.userName = authUser.userName;
                }
            }

            if (aUser != null) {
                retValue = 0;
                resultStr = "Success";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
                        req.getRemoteAddr(), channel.getName(),
                        session.getLoginid(), session.getLoginid(), new Date(),
                        "GET USER", resultStr, retValue,
                        "User Management",
                        "Name=" + aUser.userName + ",Phone=" + aUser.phoneNo + ",Email=" + aUser.email,
                        "", "USER", aUser.userId);
            } else if (aUser == null) {
//                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
//                        req.getRemoteAddr(), channel.getName(),
//                        session.getLoginid(), session.getLoginid(), new Date(),
//                        "GET USER", resultStr, retValue,
//                        "User Management", "", "", "USER", "");
                log.error("User not found!!!");
                throw new AxiomException("User not found!!!");
            }

            return aUser;
        } catch (Exception e) {
            log.error(e);
            throw new AxiomException(e.getMessage());
        }
    }

    @Override
    public AxiomStatus ResetUserM(String sessionId, String userid, int type) {
        //int type = OTP_TOKEN_SOFTWARE;
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug("ResetUser::sessionId::" + sessionId);
                log.debug("ResetUser::userid::" + userid);
                log.debug("ResetUser::type::" + type);
            }
        } catch (Exception ex) {
            log.error(ex);
        }

        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Licence invalid";
            log.error(aStatus.error);
            aStatus.errorcode = -100;
            return aStatus;
        }

        try {
            SessionManagement sManagement = new SessionManagement();
            Sessions session = sManagement.getSessionById(sessionId);
            AuditManagement audit = new AuditManagement();
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "ERROR";
            aStatus.errorcode = -9;

            int retValue = -1;
            String strCategory = "";
            if (type == RESET_USER_TOKEN_SOFTWARE) {
                strCategory = "SOFTWARE_TOKEN";
            } else if (type == RESET_USER_TOKEN_HARDWARE) {
                strCategory = "HARDWARE_TOKEN";
            } else if (type == RESET_USER_TOKEN_OOB) {
                strCategory = "OOB_TOKEN";
            }
            String strSubCategory = "";
            if (session == null) {
                aStatus.error = "Invalid Session";
                log.error(aStatus.error);
                aStatus.errorcode = -14;
                return aStatus;
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                aStatus.error = "Invalid Channel";
                log.error(aStatus.error);
                aStatus.errorcode = -3;
                return aStatus;
            }
            if (session != null) {
                MessageContext mc = wsContext.getMessageContext();
                HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
                //log.debug(("Client IP = " + req.getRemoteAddr());        
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP";
                                aStatus.errorcode = -8;
                                log.error(aStatus.error);
                                return aStatus;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP";
                            aStatus.errorcode = -8;
                            log.error(aStatus.error);
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP";
                        aStatus.errorcode = -8;
                        log.error(aStatus.error);
                        return aStatus;
                    }
                }
                if (type == RESET_USER_PASSWORD) {
                    UserManagement uManagement = new UserManagement();
                    retValue = uManagement.resetPassword(sessionId, session.getChannelid(), userid);

                    if (retValue == 0) {
                        aStatus.error = "SUCCESS";
                        aStatus.errorcode = 0;
                        //return aStatus;
                    } else {
                        aStatus.errorcode = -20;
                        aStatus.error = "Password Reset Failed (" + retValue + ")";
                        log.error(aStatus.error);
                        // return aStatus;
                    }

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(), "RESET",
                            aStatus.error, aStatus.errorcode,
                            "User Management",
                            "Old Password = ******",
                            "New Password = ******",
                            "PASSWORD", userid);

                    return aStatus;
                } else if (type == RESET_USER_TOKEN_OOB) {

                    OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                    TokenStatusDetails[] tokens = oManagement.getTokenList(sessionId, session.getChannelid(), userid);
                    TokenStatusDetails tokenSelected = null;
                    for (int i = 0; i < tokens.length; i++) {
                        if (tokens[i].Catrgory == OTPTokenManagement.OOB_TOKEN) {
                            tokenSelected = tokens[i];
                            break;
                        }
                    }

                    if (tokenSelected == null) {
                        aStatus.errorcode = -2;
                        aStatus.error = "Desired OOB Token is not assigned";
                        log.error(aStatus.error);
                        return aStatus;
                    }

                    if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_SMS) {
                        strSubCategory = "OOB__SMS_TOKEN";
                    } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_USSD) {
                        strSubCategory = "OOB__USSD_TOKEN";
                    } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_VOICE) {
                        strSubCategory = "OOB__VOICE_TOKEN";
                    } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_EMAIL) {
                        strSubCategory = "OOB__EMAIL_TOKEN";
                    }

                    retValue = oManagement.ChangeStatus(sessionId, session.getChannelid(), userid, TOKEN_STATUS_UNASSIGNED, OTPTokenManagement.OOB_TOKEN, tokenSelected.SubCategory);

                    if (retValue == 0) {
                        aStatus.error = "SUCCESS";
                        aStatus.errorcode = 0;
                    } else {
                        aStatus.errorcode = -21;
                        aStatus.error = "OOB Token Reset Failed (" + retValue + ")";
                        log.error(aStatus.error);
                        //   return aStatus;
                    }

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(),
                            "RESET", aStatus.error, aStatus.errorcode,
                            "Token Management",
                            "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                            "New Status =" + strUNASSIGNED,
                            "OOBTOKEN", userid);

                    return aStatus;

                } else if (type == RESET_USER_TOKEN_SOFTWARE) {

                    OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                    TokenStatusDetails[] tokens = oManagement.getTokenList(sessionId, session.getChannelid(), userid);
                    TokenStatusDetails tokenSelected = null;
                    for (int i = 0; i < tokens.length; i++) {
                        if (tokens[i].Catrgory == OTPTokenManagement.SOFTWARE_TOKEN) {
                            tokenSelected = tokens[i];
                            break;
                        }
                    }

                    if (tokenSelected == null) {
                        //return ERROR;
                        aStatus.errorcode = -22;
                        aStatus.error = "Desired Software Token is not assigned";
                        log.error(aStatus.error);
                        return aStatus;
                    }

                    if (tokenSelected.SubCategory == OTP_TOKEN_SOFTWARE_WEB) {
                        strSubCategory = "SW_WEB_TOKEN";
                    } else if (tokenSelected.SubCategory == OTP_TOKEN_SOFTWARE_MOBILE) {
                        strSubCategory = "SW_MOBILE_TOKEN";
                    } else if (tokenSelected.SubCategory == OTP_TOKEN_SOFTWARE_PC) {
                        strSubCategory = "SW_PC_TOKEN";
                    }

                    retValue = oManagement.ChangeStatus(sessionId, session.getChannelid(), userid, TOKEN_STATUS_UNASSIGNED, OTPTokenManagement.SOFTWARE_TOKEN, tokenSelected.SubCategory);

                    if (retValue == 0) {
                        aStatus.error = "SUCCESS";
                        aStatus.errorcode = 0;
                    } else {
                        aStatus.errorcode = -23;
                        aStatus.error = "Software Token Reset Failed (" + retValue + ")";
                        log.error(aStatus.error);
                        //  return aStatus;
                    }

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(),
                            "RESET", aStatus.error, aStatus.errorcode,
                            "Token Management",
                            "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                            "New Status =" + strUNASSIGNED,
                            "SOFTWARETOKEN", userid);

//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                            session.getLoginid(), session.getLoginid(), new Date(), "Change OTP Status", aStatus.error, aStatus.errorcode,
//                            "Token Management", "Category =" + strCategory + "SubCategory =" + strSubCategory + "Old Status =" + tokenSelected.Status,
//                            "Category =" + strCategory + "New Status =" + strUNASSIGNED,
//                            "RESET TOKEN", userid);
                    return aStatus;
                } else if (type == RESET_USER_TOKEN_HARDWARE) {

                    OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                    TokenStatusDetails[] tokens = oManagement.getTokenList(sessionId, session.getChannelid(), userid);
                    TokenStatusDetails tokenSelected = null;
                    for (int i = 0; i < tokens.length; i++) {
                        if (tokens[i].Catrgory == OTPTokenManagement.HARDWARE_TOKEN) {
                            tokenSelected = tokens[i];
                            break;
                        }
                    }

                    if (tokenSelected == null) {
                        //return ERROR;
                        aStatus.errorcode = -24;
                        aStatus.error = "Desired Hardware Token is not assigned";
                        log.error(aStatus.error);
                        return aStatus;
                    }

                    if (tokenSelected.SubCategory == OTP_TOKEN_HARDWARE_CR) {
                        strSubCategory = "HW_CR_TOKEN";
                    } else if (tokenSelected.SubCategory == OTP_TOKEN_HARDWARE_MINI) {
                        strSubCategory = "HW_MINI_TOKEN";
                    }
                    retValue = oManagement.ChangeStatus(sessionId, session.getChannelid(), userid, TOKEN_STATUS_UNASSIGNED, OTPTokenManagement.HARDWARE_TOKEN, tokenSelected.SubCategory);

                    if (retValue == 0) {
                        aStatus.error = "SUCCESS";
                        aStatus.errorcode = 0;
                    } else {
                        aStatus.errorcode = -25;
                        aStatus.error = "Hardware Token Reset Failed (" + retValue + ")";
                        log.error(aStatus.error);
                        //  return aStatus;
                    }

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(),
                            "RESET", aStatus.error, aStatus.errorcode,
                            "Token Management",
                            "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                            "New Status =" + strUNASSIGNED,
                            "HARDWARETOKEN", userid);

//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                            session.getLoginid(), session.getLoginid(), new Date(), "Change OTP Status", aStatus.error, aStatus.errorcode,
//                            "Token Management", "Category =" + strCategory + "SubCategory =" + strSubCategory + "Old Status =" + tokenSelected.Status,
//                            "Category =" + strCategory + "New Status =" + strUNASSIGNED,
//                            "RESET TOKEN", userid);
                    return aStatus;

                } else if (type == RESET_USER_MOBILETRUST) {

                    MobileTrustManagment mManagment = new MobileTrustManagment();

                    retValue = mManagment.eraseMobileTrustCredential(sessionId, channel.getChannelid(), userid);
                    if (retValue == 0) {
                        aStatus.error = "SUCCESS";
                        aStatus.errorcode = 0;
                    } else {

                        aStatus.errorcode = -26;
                        aStatus.error = "Mobile Trust Acccount Reset Failed (" + retValue + ")";
                        log.error(aStatus.error);
                        if (retValue == -3) {
                            aStatus.error = "Mobile Trust Acccount was not present/active";
                            log.error(aStatus.error);
                            aStatus.errorcode = -27;
                        }

                        //  return aStatus;
                    }

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(),
                            "RESET", aStatus.error, aStatus.errorcode,
                            "MOBILETRUST",
                            "ALL TOKENS AND CERTIFICATE",
                            "New Status =" + strUNASSIGNED,
                            "MOBILETRUST", userid);

                    return aStatus;

                }
            }

            return aStatus;
        } catch (Exception e) {
            log.error(e);
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "General Exception::" + e.getMessage();
            aStatus.errorcode = -99;
            return aStatus;
        }
    }

    @Override
    public AxiomStatus Assign(String sessionId, String userid, int subtype) {

        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug("Assign::sessionId::" + sessionId);
                log.debug("Assign::userid::" + userid);
                log.debug("Assign::subtype::" + subtype);
            }
        } catch (Exception ex) {
            log.error(ex);
        }

        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Licence invalid";
            aStatus.errorcode = -100;
            log.error(aStatus.error);
            return aStatus;
        }

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "OTP Token feature is not available in this license!!!";
            aStatus.errorcode = -101;
            log.error(aStatus.error);
            return aStatus;
        }

        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionId);
        AxiomStatus aStatus = new AxiomStatus();
        int retValue = -1;
        aStatus.error = "ERROR";
        aStatus.errorcode = retValue;

        if (session == null) {
            aStatus.error = "Session Invalid!!!";
            aStatus.errorcode = -14;
            log.error(aStatus.error);
            return aStatus;
        }

        String strCategory = "SOFTWARE_TOKEN";

        String strSubCategory = "";
        if (subtype == OTP_TOKEN_SOFTWARE_WEB) {
            strSubCategory = "SW_WEB_TOKEN";
        } else if (subtype == OTP_TOKEN_SOFTWARE_MOBILE) {
            strSubCategory = "SW_MOBILE_TOKEN";
        } else if (subtype == OTP_TOKEN_SOFTWARE_PC) {
            strSubCategory = "SW_PC_TOKEN";
        }

        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(session.getChannelid());
        if (channel == null) {
            aStatus.error = "Invalid Channel";
            aStatus.errorcode = -3;
            log.error(aStatus.error);
            return aStatus;
        }

        if (session != null) {

            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();

            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagementObj = new OperatorsManagement();
                        Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP";
                            aStatus.errorcode = -8;
                            log.error(aStatus.error);
                            return aStatus;
                        }

                        suTemplate.close();
                        sTemplate.close();
                        aStatus.error = "INVALID IP";
                        aStatus.errorcode = -8;
                        log.error(aStatus.error);
                        return aStatus;
                    }
                    aStatus.error = "INVALID IP";
                    aStatus.errorcode = -8;
                    log.error(aStatus.error);
                    return aStatus;
                }
            }

            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());

            //new addtion for license enforcement 
            int tokenCountCurrentDBState = oManagement.getTokenCountForLicenseCheck(sessionId, channel.getChannelid(), OTPTokenManagement.SOFTWARE_TOKEN);
            int licensecount = 0; // Axiom Protect fuction call here 

            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
                aStatus.error = "Software Token is not available in this license!!!";
                aStatus.errorcode = -100;
                log.error(aStatus.error);
                return aStatus;
            }
            licensecount = AxiomProtect.GetSoftwareTokensAllowed();
            if (licensecount == -998) {
                //unlimited licensing 
            } else {
                if (tokenCountCurrentDBState >= licensecount) {
                    aStatus.error = "Software Token already reached its limit as per this license!!!";
                    aStatus.errorcode = -100;
                    log.error(aStatus.error);
                    return aStatus;
                }
            }

            //end of new addition
            retValue = oManagement.AssignToken(sessionId, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE, subtype, userid);

            if (retValue == 0) {
                SessionFactoryUtil suSettings = new SessionFactoryUtil(SessionFactoryUtil.settings);
                Session sSettings = suSettings.openSession();
                MobileTrustSettings mSettings = null;
                Object obj = setManagement.getSetting(sessionId, session.getChannelid(), SettingsManagement.MOBILETRUST_SETTING, 1);
                if (obj != null) {
                    if (obj instanceof MobileTrustSettings) {
                        mSettings = (MobileTrustSettings) obj;
                    }
                }

                sSettings.close();
                suSettings.close();

                if (mSettings == null) {
                    aStatus.error = "Mobile Trust Setting is not configured!!!";
                    aStatus.errorcode = -23;
                    log.error(aStatus.error);
                    //aStatus.data = signData;
                    return aStatus;
                }

                if (mSettings.bSilentCall == false) {

                    //if (tSettings.isbSilentCall() == false) {
                    String regCode = oManagement.generateRegistrationCode(sessionId, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE);
                    SimpleDateFormat sdfExpiry = new SimpleDateFormat("hh:mm");
                    if (regCode != null) {

                        UserManagement userObj = new UserManagement();
                        AuthUser user = null;
                        user = userObj.getUser(sessionId, channel.getChannelid(), userid);

                        Templates temp = null;
                        TemplateManagement tObj = new TemplateManagement();
                        temp = tObj.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.MOBILE_SOFTWARE_TOKEN_REGISTER_TEMPLATE);
                        ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        Date d = new Date();
                        String tmessage = (String) UtilityFunctions.deserializeFromObject(bais);
                        tmessage = tmessage.replaceAll("#name#", user.getUserName());
                        tmessage = tmessage.replaceAll("#channel#", channel.getName());
                        tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                        tmessage = tmessage.replaceAll("#regcode#", regCode);
                        tmessage = tmessage.replaceAll("#expiry#", "3 Mins");
                        if (subtype == OTPTokenManagement.SW_WEB_TOKEN) {
                            tmessage = tmessage.replaceAll("#tokentype#", "WEB");

                        }
                        if (subtype == OTPTokenManagement.SW_MOBILE_TOKEN) {
                            tmessage = tmessage.replaceAll("#tokentype#", "MOBILE");

                        }
                        if (subtype == OTPTokenManagement.SW_PC_TOKEN) {
                            tmessage = tmessage.replaceAll("#tokentype#", "PC");

                        }

                        SendNotification send = new SendNotification();
                        //  AxiomStatus status = null;
                        if (temp.getStatus() == tObj.ACTIVE_STATUS) {
                            AXIOMStatus status = send.SendOnMobileNoWaiting(channel.getChannelid(), user.getPhoneNo(), tmessage, 1, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                        }

                        retValue = 0;
                        aStatus.error = "SUCCESS";
                        aStatus.errorcode = 0;
                        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getChannelid(),
                                req.getRemoteAddr(), channel.getName(),
                                session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code",
                                "success", 0, "Token Management", "", "Registration Code = ******" /*strRegCode*/, itemtype, userid);
                    } else if (regCode == null) {
                        aStatus.error = "Failed To generate registration code";
                        aStatus.errorcode = -24;
                        log.error(aStatus.error);
                        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
                                req.getRemoteAddr(), channel.getName(),
                                session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code",
                                "failed", -1, "Token Management", "", "Failed To generate Registration Code", itemtype, userid);
                    }

                } else {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                }
            }
        }
        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                session.getLoginid(), session.getLoginid(), new Date(), "Assign Token", aStatus.error, aStatus.errorcode,
                "Token Management", "", "Category = " + strCategory + " Sub Category =" + strSubCategory,
                itemtype, userid);
        
        return aStatus;
    }

    @Override
    public MobileTrustStatus Activate(String sessionid, String regCode, String SwTokenData) {
        String certPass=null;
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug("Activate::sessionid::" + sessionid);
                log.debug("Activate::userid::" + regCode);
                log.debug("Activate::SwTokenData::" + SwTokenData);
            }
        } catch (Exception ex) {
            log.error(ex);
        }

        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
        MobileTrustStatus aStatus = new MobileTrustStatus();
            aStatus.error = "Licence Invalid";
            aStatus.errorCode = -100;
            aStatus.data = null;
            log.error(aStatus.error);
            return aStatus;
        }

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.MOBILETRUST) != 0) {
            MobileTrustStatus aStatus = new MobileTrustStatus();
            aStatus.error = "OTP Token feature is not available in this license!!!";
            aStatus.errorCode = -101;
            log.error(aStatus.error);
            return aStatus;
        }

//        int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "Licence Not Valid";
//            aStatus.errorcode = -10;
//            return aStatus;
//        }
        
        
        MobileTrustStatus aStatus = new MobileTrustStatus();
        aStatus.error = "ERROR";
        aStatus.errorCode = -2;

        SessionManagement sManagement = new SessionManagement();
        Sessions session = sManagement.getSessionById(sessionid);

        if (session == null) {
            aStatus.error = "Session Invalid!!!";
            aStatus.errorCode = -14;
            log.error(aStatus.error);
            return aStatus;
        }

        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        String signData = null;

        String oldValue = "";
        String strValue = "";
        //String otp = null;
        String cert = null;

        if (session != null) {
            try {
                //log.debug(("Client IP = " + req.getRemoteAddr());
                SettingsManagement setManagement = new SettingsManagement();
//                int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
//                if (result != 1) {
//                    aStatus.error = "INVALID IP REQUEST";
//                    aStatus.errorCode = -8;
//                    aStatus.data = signData;
//                    return aStatus;   //why return statement is commented
//                }

                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    aStatus.error = "Invalid Channel!!!";
                    aStatus.errorCode = -3;
                    log.error(aStatus.error);
                    return aStatus;
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP";
                                aStatus.errorCode = -8;
                                log.error(aStatus.error);
                                return aStatus;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP";
                            aStatus.errorCode = -8;
                            log.error(aStatus.error);
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP";
                        aStatus.errorCode = -8;
                        log.error(aStatus.error);
                        return aStatus;
                    }
                }

                RegistrationCodeTrailManagement rManagement = new RegistrationCodeTrailManagement();
                regCode = UtilityFunctions.Bas64SHA1(regCode);
                Registrationcodetrail regObj = rManagement.getRegistrationcodetrail(channelid, regCode);
                String userid = null;
                if (regObj != null) {
                    userid = regObj.getUserid();
                } else {
                    aStatus.errorCode = -95;
                    aStatus.error = "Registration code not found";
                    aStatus.data = signData;
                    return aStatus;
                }

                OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());

                TokenStatusDetails tokenSelected = oManagement.getToken(sessionid, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE);
//
                if (tokenSelected == null) {
                    aStatus.errorCode = -20;
                    aStatus.error = "Desired Token is not assigned";
                    log.error(aStatus.error);
                    aStatus.data = signData;
                    return aStatus;
                }

                if (tokenSelected.Status == TOKEN_STATUS_ACTIVE) {
                    oldValue = strACTIVE;
                } else if (tokenSelected.Status == TOKEN_STATUS_SUSPENDED) {
                    oldValue = strSUSPENDED;
                } else if (tokenSelected.Status == TOKEN_STATUS_UNASSIGNED) {
                    oldValue = strUNASSIGNED;
                }
                //CryptoManagement cryManagement = new CryptoManagement();
                strValue = strACTIVE;
                //decrypt data here SwTokenData
                //decrypt SwTokenData
                String dSWData = SwTokenData;
                // dSWData

                //String serverpublicKey = channel.getVisiblekey();
                String serverPrivateKey = channel.getHiddenkey();
                CryptoManager cm = new CryptoManager();
                //which variable is userd for verifyneeded?
                JSONObject jsonObj = new JSONObject(dSWData);
                String device = jsonObj.getString("_deviceType");
                int devicetype = 1;
                if (device.equals("ANDROID")) {
                    devicetype = ANDROID;
                } else if (device.equals("IOS")) {
                    devicetype = IOS;
                }
                String jsonData = cm.ConsumeMobileTrust(dSWData, serverPrivateKey,true ,CryptoManager.ACTIVATE,"");
                //  String jsonData = dSWData;
                JSONObject json = new JSONObject(jsonData);
//                String _mtkey = json.getString("_mtkey");
//                byte[]  datatoSign =Base64.decode(_mtkey);
//                String serverprivateKey = LoadSettings.g_sSettings.getProperty("spkey");
//                String Ss= new String(cryManagement.decryptRSA(datatoSign,serverprivateKey));
                String ip = json.getString("ip");
                //String con = json.getString("con");
                String longi = json.getString("longi");
                String latti = json.getString("latti");
                String mac = json.getString("macaddress");
                String did = json.getString("did");
                String vKey = json.getString("vKey");
                String pKey = json.getString("pKey");
                String os = json.getString("os");
                String regcode = json.getString("regcode");

                Otptokens oToken = oManagement.getOtpObjByUserId(sessionid, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE);
                if (oToken == null) {
                    aStatus.error = "Token Not found";
                    aStatus.errorCode = -13;
                    aStatus.data = signData;
                    log.error(aStatus.error);
                }

                if (oToken.getStatus() == TOKEN_STATUS_ACTIVE) {
                    aStatus.error = "Token is Already Active";
                    log.error(aStatus.error);
                    aStatus.errorCode = -6;
                    aStatus.data = signData;
                    return aStatus;
                }

                MobileTrustSettings mSettings = null;
                int retValue = -1;
                Object obj = setManagement.getSetting(sessionid, session.getChannelid(), SettingsManagement.MOBILETRUST_SETTING, 1);
                if (obj != null) {
                    if (obj instanceof MobileTrustSettings) {
                        mSettings = (MobileTrustSettings) obj;
                    }
                }

                RootCertificateSettings rcaSettings = (RootCertificateSettings) setManagement.getSetting(sessionid, session.getChannelid(), SettingsManagement.RootConfiguration, 1);
                if (rcaSettings == null) {
                    aStatus.error = "Certificate Setting is not configured!!!";
                    log.error(aStatus.error);
                    aStatus.errorCode = -24;
                    aStatus.data = signData;
                    return aStatus;
                }

                if (mSettings == null) {
                    aStatus.error = "Mobile Trust Setting is not configured!!!";
                    log.error(aStatus.error);
                    aStatus.errorCode = -23;
                    aStatus.data = signData;
                    return aStatus;
                }

                TokenSettings tSettings = (TokenSettings) setManagement.getSetting(sessionid, session.getChannelid(), TOKEN, 1);
                if (tSettings == null) {
                    aStatus.error = "OTP Token Setting is not configured!!!";
                    log.error(aStatus.error);
                    aStatus.errorCode = -25;
                    aStatus.data = signData;
                    return aStatus;
                }

                if (mSettings.bSilentCall == false) {

                    if (regcode.compareTo(oToken.getRegcode()) != 0) {
                        aStatus.error = "Invalid Registration Code";
                        log.error(aStatus.error);
                        aStatus.errorCode = -17;
                        aStatus.data = signData;
                        return aStatus;
                    }

                    Date dExpiryTime = oToken.getExpirytime();
                    Date dCurrent = new Date();

                    if (dExpiryTime.before(dCurrent) == true) {
                        aStatus.error = "Regitration code has expired.";
                        
                        log.error(aStatus.error);
                        aStatus.errorCode = -18;
                        aStatus.data = signData;
                        return aStatus;
                    }
                }

//                if (mSettings.bSilentCall == false ) {
//                    if (regcode.compareTo(oToken.getRegcode()) != 0) {
//                        aStatus.error = "Invalid Regitration Code";
//                        aStatus.errorCode = -17;
//                        aStatus.data = signData;
//                        return aStatus;
//                    }
//                }
//                Date dExpiryTime = oToken.getExpirytime();
//                Date dCurrent = new Date();
//
//                if (dExpiryTime.before(dCurrent) == true) {
//                    aStatus.error = "Regitration code has expired.";
//                    aStatus.errorCode = -18;
//                    aStatus.data = signData;
//                    return aStatus;
//                }
                MobileTrustManagment mManagment = new MobileTrustManagment();
                String strAxiomid = userid + mac + did + os + vKey; //add all device info and get new unique number
                String axiomid = new String(Base64.encode(SHA1(strAxiomid)));

                String con = "";
                String city = "";
                String state = "";
                String country = "";
                String zipcode = "";

                Location srvLoc = mManagment.getLocationByLongAndLat(longi, latti);
                if (srvLoc != null) {
                    con = srvLoc.country;
                    city = srvLoc.city;
                    state = srvLoc.state;
                    country = con;
                    zipcode = srvLoc.zipcode;
                } else {
                    aStatus.error = "Geo Address could not found for (" + longi + "," + latti + ").";
                    aStatus.errorCode = -30;
                    log.error(aStatus.error);
                    //aStatus.data = signData;
                }

                int result = -1;
                LocationManagement gLocation = new LocationManagement();
                Countrylist clist = gLocation.getCountriesByid(Integer.parseInt(mSettings.homeCountryCode));
                String countryName = null;
                if (clist != null) {
                    countryName = clist.getCountyName();
                }

                if (srvLoc.country.equalsIgnoreCase(countryName)) {

                    result = 0;
                } else {
                    aStatus.error = "Home Country Is Different";
                    aStatus.errorCode = -21;
                    log.error(aStatus.error);
                    aStatus.data = signData;
                    result = -21;
                    AuditManagement audit = new AuditManagement();

                    String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
                    audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                            session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorCode,
                            "Token Management", errorLocation, null, itemtype,
                            session.getLoginid());

                    return aStatus;
                }
//                int result = -1;
//                if (con.equalsIgnoreCase(mSettings.homeCountryCode)) {
//                    result = 0;
//                } else {
//                    aStatus.error = "Home Country Is Different";
//                    aStatus.errorCode = -21;
//                    aStatus.data = signData;
//                    result = -21;
//                    AuditManagement audit = new AuditManagement();
//
//                    String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
//                    audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
//                            session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorCode,
//                            "Token Management", errorLocation, null, itemtype,
//                            session.getLoginid());
//
//                    return aStatus;
//                }

                if (result == 0) {
                    if (devicetype == IOS) {
                        KeyPair kp = CryptoManagement.getAxiomKeyPair(pKey);
                        PrivateKey pk = kp.getPrivate();
                        PublicKey pubk = kp.getPublic();

                        byte[] s = Base64.encode(pubk.getEncoded());
                        vKey = new String(s);

                        s = Base64.encode(pk.getEncoded());
                        pKey = new String(s);
                        cert = mManagment.AddCertificate(sessionid, session.getChannelid(), userid, oToken.getSrno(), vKey, pKey);
                    } else {
                        cert = mManagment.AddCertificate(sessionid, session.getChannelid(), userid, oToken.getSrno(), vKey, pKey);
                    }
                }

                if (cert == null) {
                    aStatus.error = "Digital Certificate Issuance Failed";
                    aStatus.errorCode = -22;
                    log.error(aStatus.error);
                    aStatus.data = signData;
                }

                if (result == 0 && cert != null) {
                    retValue = mManagment.addTrustedDevice(sessionid, session.getChannelid(), userid, axiomid, did, mac, os, true, vKey, new Date(), new Date());
                    if (retValue == 0) {
                        retValue = mManagment.addGeoFence(sessionid, session.getChannelid(), userid, null, null, mSettings.homeCountryCode, MobileTrustManagment.ROAMING_DISABLE, null, null);
                        if (retValue == 0) {
                            retValue = mManagment.addGeoTrack(sessionid, session.getChannelid(), userid, longi, ip, latti,
                                    //con, 
                                    city, state, mSettings.homeCountryCode, zipcode,
                                    MobileTrustManagment.REGISTER, new Date());
                        }
                        if (retValue == 0) {
                            retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), userid, TOKEN_STATUS_ACTIVE, OTP_TOKEN_SOFTWARE, tokenSelected.SubCategory);
                        }
                        if (retValue == 0) {
                            oToken = oManagement.getOtpObjByUserId(sessionid, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE);
                            if (oToken != null) {

                                retValue = mManagment.AddPKIToken(sessionid, session.getChannelid(), userid, oToken.getSrno(), oToken.getCategory(), oToken.getRegcode());

                                if (retValue == 0) {

                                    String secret = oToken.getSecret();
                                    signData = mManagment.confirmation(session.getChannelid(), userid, did, secret, cert,certPass, mac, vKey, serverPrivateKey, devicetype,oToken.getSrno());
                                    if (signData == null) {
                                        aStatus.error = "Confirmation Message Could Not Be Generated.";
                                        aStatus.errorCode = -7;
                                        log.error(aStatus.error);
                                        //aStatus.data = signData;
                                    }
                                } else {
                                    mManagment.eraseMobileTrustCredential(sessionid, channel.getChannelid(), userid);
                                    aStatus.error = "PKI Token Assignment Failed!!!";
                                    log.error(aStatus.error);
                                    aStatus.errorCode = -26;
                                    aStatus.data = null;
                                    //end
                                }
                                // String cert = cManagement.g
                            }
                        }
                    }

                }

                if (retValue == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorCode = 0;
                    aStatus.data = signData;
                }

                // return aStatus;
            } catch (Exception ex) {
                log.error(ex);
                aStatus.error = "General Exception::" + ex.getMessage();
                aStatus.errorCode = -99;
                aStatus.data = signData;
            }
        }
        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(session.getChannelid());
        AuditManagement audit = new AuditManagement();
        audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorCode,
                "Token Management", "Old Status :" + oldValue, "New Status :" + strValue, itemtype,
                session.getLoginid());

        return aStatus;
    }

    @Override
    public String GetTimeStamp(String sessionid, String userid) throws AxiomException {

        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug("GetTimeStamp::sessionid::" + sessionid);
                log.debug("GetTimeStamp::userid::" + userid);
            }
        } catch (Exception ex) {
            log.error(ex);

        }

        if (sessionid == null) {
            log.error("Session ID is empty!!!");
            throw new AxiomException("Session ID is empty!!!");
        }

        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            log.error("Invalid License with code as " + iResult);
            throw new AxiomException("Invalid License!!!");
        }

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.MOBILETRUST) != 0) {
            log.error("Mobile Trust Feature is not present in this license!!!");
            throw new AxiomException("Mobile Trust Feature is not present in this license!!!");
        }

        SessionManagement sManagement = new SessionManagement();
        Sessions session = sManagement.getSessionById(sessionid);
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        AxiomStatus aStatus = new AxiomStatus();
        aStatus.error = "ERROR";
        aStatus.errorcode = -2;
        String oldValue = "";
        String strValue = "";

        if (session == null) {
            log.error("Session is invlaid/null!!!");
            throw new AxiomException("Session is invlaid/null!!!");
        }

        try {
            SettingsManagement setManagement = new SettingsManagement();

            String channelid = session.getChannelid();
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(channelid);
            if (channel == null) {
                log.error("Invalid Channel!!!");
                throw new AxiomException("Invalid Channel!!!");
            }
            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        suTemplate.close();
                        sTemplate.close();

                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }
                        }
                    }
                    throw new AxiomException("Invalid IP!!!");
                }
            }
            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
            TokenStatusDetails tokenSelected = oManagement.getToken(sessionid, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE);

            if (tokenSelected == null) {
//                    aStatus.errorcode = -2;
//              aStatus.error = "Desired Token is not assigned";
                log.error("Desired Token is not assigned!!!");
                throw new AxiomException("Desired Token is not assigned!!!");
                //return null;
            }

            if (tokenSelected.Status == TOKEN_STATUS_ACTIVE) {
                oldValue = strACTIVE;
            } else if (tokenSelected.Status == TOKEN_STATUS_SUSPENDED) {
                oldValue = strSUSPENDED;
            } else if (tokenSelected.Status == TOKEN_STATUS_UNASSIGNED) {
                oldValue = strUNASSIGNED;
            }

            strValue = strACTIVE;

            MobileTrustManagment mManagment = new MobileTrustManagment();

            String timeStamp = mManagment.GetTimeStamp(sessionid, session.getChannelid(), userid);

            if (timeStamp != null) {
                aStatus.error = "SUCCESS";
                aStatus.errorcode = 0;
            }

            //ChannelManagement cManagement = new ChannelManagement();
            //Channels channel = cManagement.getChannelByID(session.getChannelid());
            AuditManagement audit = new AuditManagement();
            audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                    session.getLoginid(), new Date(), "Get TimeStamp", aStatus.error, aStatus.errorcode,
                    "MobileTrust Management", "", "New Session :" + strValue, itemtype,
                    session.getLoginid());

            if (timeStamp == null) {
                log.error("Failed to generate desired timestamp for this user!!!");
                throw new AxiomException("Failed to generate desired timestamp for this user!!!");
            }

            return timeStamp;
            // return aStatus;
        } catch (AxiomException ex) {
            log.error(ex);
            //throw new AxiomException("Failed to generate desired timestamp for this user!!!");
            throw ex;
        } catch (Exception ex) {
            log.error(ex);
            //throw new AxiomException("Failed to generate desired timestamp for this user!!!");
            throw new AxiomException("General Exception::" + ex.getMessage());
        }
    }

    @Override
    public AxiomStatus VerifyDigitalSignaturePlus(String sessionid, String userid, String data, int type, String signature, String signaturePlus) {

        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug("VerifyDigitalSignaturePlus::sessionid::" + sessionid);
                log.debug("VerifyDigitalSignaturePlus::userid::" + userid);
                log.debug("VerifyDigitalSignaturePlus::data::" + data);
                log.debug("VerifyDigitalSignaturePlus::type::" + type);
                log.debug("VerifyDigitalSignaturePlus::signature::" + signature);
                log.debug("VerifyDigitalSignaturePlus::signaturePlus::" + signaturePlus);
            }
        } catch (Exception ex) {
            log.error(ex);
        }

        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Licence Invalid";
            aStatus.errorcode = -100;
            log.error(aStatus.error);
            return aStatus;
        }

        //OTPTokenManagement oManagement = new OTPTokenManagement();
        CryptoManager crypto = new CryptoManager();
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);
        AxiomStatus aStatus = new AxiomStatus();
        int retValue = -1;
        aStatus.error = "ERROR";
        aStatus.errorcode = retValue;
        if (session == null) {
            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
            log.error(aStatus.error);
            return aStatus;
        }
        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(session.getChannelid());
        if (channel == null) {
            aStatus.error = "Invalid Channel";
            aStatus.errorcode = -3;
            log.error(aStatus.error);
            return aStatus;
        }
        if (session != null) {

            //log.debug(("Client IP = " + req.getRemoteAddr());        
            SettingsManagement setManagement = new SettingsManagement();
//            int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
//            if (result != 1) {
//                aStatus.error = "INVALID IP REQUEST";
//                aStatus.errorcode = -8;
//                return aStatus;
//            }

            String channelid = session.getChannelid();

            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP";
                            aStatus.errorcode = -8;
                            log.error(aStatus.error);
                            return aStatus;
                        }

                        suTemplate.close();
                        sTemplate.close();
                        aStatus.error = "INVALID IP";
                        aStatus.errorcode = -8;
                        log.error(aStatus.error);
                        return aStatus;
                    }
                    aStatus.error = "INVALID IP";
                    aStatus.errorcode = -8;
                    log.error(aStatus.error);
                    return aStatus;
                }
            }
            int devicetype = 1;
            JSONObject jsonObj = null;
            try {
                jsonObj = new JSONObject(signaturePlus);
                String device = jsonObj.getString("_deviceType");
                if (device.equals("ANDROID")) {
                    devicetype = ANDROID;
                } else if (device.equals("IOS")) {
                    devicetype = IOS;
                }
            } catch (Exception ex) {
                log.error(ex);
            }
            MobileTrustManagment mManagment = new MobileTrustManagment();
            retValue = mManagment.CheckPlusComponentForSign(channel, userid, sessionid, data, signature, signaturePlus, devicetype);

            if (retValue == 0) {
                aStatus.error = "SUCCESS";
                aStatus.errorcode = 0;
            } else {
                aStatus.errorcode = retValue;
                if (aStatus.errorcode == -14) {
                    aStatus.error = "Mobile Trust Setting is not configured!!!";
                } else if (aStatus.errorcode == -6) {
                    aStatus.error = "Trusted Device not found!!!";
                } else if (aStatus.errorcode == -17) {
                    aStatus.error = "Trusted Device is marked suspended";
                } else if (aStatus.errorcode == -5) {
                    aStatus.error = "Trust Device mismatch!!!";
                } else if (aStatus.errorcode == -4) {
                    aStatus.error = "Verification failed!!!";
                } else if (aStatus.errorcode == -23) {
                    aStatus.error = "Digtial Signature mismatch!!!";
                } else if (aStatus.errorcode == -7) {
                    aStatus.error = "Home Country is not defined!!!";
                } else if (aStatus.errorcode == -28) {
                    aStatus.error = "TimeStamp is not issued!!";
                } else if (aStatus.errorcode == -27) {
                    aStatus.error = "TimeStamp mistmatch!!!";
                } else if (aStatus.errorcode == -29) {
                    aStatus.error = "TimeStamp has expired!!!";
                } else if (aStatus.errorcode == -10) {
                    aStatus.error = "Invalid Location, out of home country!!!";
                } else if (aStatus.errorcode == -11) {
                    aStatus.error = "Invalid Location, roaming country is not allowed!!!";
                } else if (aStatus.errorcode == -12) {
                    aStatus.error = "Invalid Location, roaming is not configured properly!!!";
                }
                log.error(aStatus.error);
            }

        }
        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                session.getLoginid(), session.getLoginid(), new Date(), "Verify Digital Signature ", aStatus.error, aStatus.errorcode,
                "Verify Digital Signature", "Signature = ******", " Signature = ******",
                itemtype, userid);

        return aStatus;
    }

    @Override
    public AxiomStatus ALertEvent(String sessionid, String signData) {

        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug("ALertEvent::sessionid::" + sessionid);
                log.debug("ALertEvent::signData::" + signData);
            }
        } catch (Exception ex) {
            log.error(ex);
        }

        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Licence invalid";
            aStatus.errorcode = -100;
            log.error(aStatus.error);
            return aStatus;
        }

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.MOBILETRUST) != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Mobile Trust feature is not available in this license!!!";
            aStatus.errorcode = -101;
            log.error(aStatus.error);
            return aStatus;
        }

        // int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "Licence Not Valid";
//            aStatus.errorcode = -10;
//            return aStatus;
//        }
        //OTPTokenManagement oManagement = new OTPTokenManagement();
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);
        AxiomStatus aStatus = new AxiomStatus();
        int retValue = -1;
        aStatus.error = "ERROR";
        aStatus.errorcode = retValue;
        if (session == null) {
            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
            log.error(aStatus.error);
            return aStatus;
        }
        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(session.getChannelid());
        if (channel == null) {
            aStatus.error = "Invalid Channel";
            aStatus.errorcode = -3;
            log.error(aStatus.error);
            return aStatus;
        }
        if (session != null) {
            try {

                SettingsManagement setManagement = new SettingsManagement();

                String channelid = session.getChannelid();

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP";
                                aStatus.errorcode = -8;
                                log.error(aStatus.error);
                                return aStatus;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP";
                            aStatus.errorcode = -8;
                            log.error(aStatus.error);
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP";
                        aStatus.errorcode = -8;
                        log.error(aStatus.error);
                        return aStatus;
                    }
                }
                
                
                MobileTrustManagment mManagment = new MobileTrustManagment();
                retValue = mManagment.CheckForAlert(channel, sessionid, signData);
                if (retValue == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                } else {
                    if (aStatus.errorcode == -2) {
                        aStatus.error = "Session is not active";
                    } else if (aStatus.errorcode == -6) {
                        aStatus.error = "Trusted Device not found!!!";
                    } else if (aStatus.errorcode == -17) {
                        aStatus.error = "Trusted Device is marked suspended";
                    } else if (aStatus.errorcode == -110) {
                        aStatus.error = "Adding Abuse Alert Failed!!!";
                    } else if (aStatus.errorcode == -60) {
                        aStatus.error = "User not Found!!!";
                    } else if (aStatus.errorcode == -30) {
                        aStatus.error = "Trust Device mismatch!!!";
                    }
                    log.error(aStatus.error);
                }
            } catch (Exception ex) {
                log.error(ex);
                aStatus.error = "General Exception::" + ex.getMessage();
                aStatus.errorcode = -99;
            }

        }
        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                session.getLoginid(), session.getLoginid(), new Date(), "Alert User ", aStatus.error, aStatus.errorcode,
                "Alert Management", "", " Successfully Destroy",
                itemtype, session.getLoginid());

        return aStatus;
    }

    @Override
    public AxiomStatus VerifyOTPPlus(String sessionid, String userid, int type, String otp, String plusDetails) {

        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug("VerifyOTPPlus::sessionid::" + sessionid);
                log.debug("VerifyOTPPlus::userid::" + userid);
                log.debug("VerifyOTPPlus::type::" + type);
                log.debug("VerifyOTPPlus::otp::" + otp);
                log.debug("VerifyOTPPlus::plusDetails::" + plusDetails);
            }
        } catch (Exception ex) {
            log.error(ex);
        }

        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Licence invalid";
            aStatus.errorcode = -100;
            log.error(aStatus.error);
            return aStatus;
        }

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.MOBILETRUST) != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Mobile Trust feature is not available in this license!!!";
            aStatus.errorcode = -101;
            log.error(aStatus.error);
            return aStatus;
        }

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_OOB) != 0
                && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_HARDWARE) != 0
                && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "OTP Token feature is not available in this license!!!";
            aStatus.errorcode = -102;
            log.error(aStatus.error);
            return aStatus;
        }

        try {

            int retValue = -10;
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Invalid Parameters!!!";
            aStatus.errorcode = retValue;

            if (sessionid == null || userid == null || otp == null) {
                log.error(aStatus.error);
                return aStatus;
            }

            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionid);
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            if (session == null) {
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -104;
                log.error(aStatus.error);
                return aStatus;
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                aStatus.error = "Invalid Channel";
                aStatus.errorcode = -3;
                log.error(aStatus.error);
                return aStatus;
            }

            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
            if (session != null) {

                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagementObj = new OperatorsManagement();
                            Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP";
                                aStatus.errorcode = -108;
                                log.error(aStatus.error);
                                return aStatus;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP";
                            aStatus.errorcode = -108;
                            log.error(aStatus.error);
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP";
                        aStatus.errorcode = -108;
                        log.error(aStatus.error);
                        return aStatus;
                    }
                }

                MobileTrustManagment mManagment = new MobileTrustManagment();
                JSONObject jsonObj = new JSONObject(plusDetails);
                String device = jsonObj.getString("_deviceType");
                int devicetype = 1;
                if (device.equals("ANDROID")) {
                    devicetype = ANDROID;
                } else if (device.equals("IOS")) {
                    devicetype = IOS;
                }
                if (plusDetails != null && !otp.contains(":")) {
                    retValue = mManagment.CheckPlusComponent(channel, userid, sessionid, otp, plusDetails, devicetype);

                    log.debug("CheckPlusComponent returned::" + retValue);
                    //added for error handling 
                    if (retValue == -6) {
                        aStatus.error = "Trusted device not found";
                        aStatus.errorcode = -6;
                    } else if (retValue == -4) {
                        aStatus.error = "OTP is invalid";
                        aStatus.errorcode = -4;
                    } else if (retValue == -5) {
                        aStatus.error = "Trusted Device mismatch";
                        aStatus.errorcode = -5;
                    } else if (retValue == -7) {
                        aStatus.error = "Invalid Location, out of home country!!!";
                        aStatus.errorcode = -7;
                    } else if (retValue == -8) {
                        aStatus.error = "Mobile Trust Setting is not configured!!!";
                        aStatus.errorcode = -118;
                    } else if (retValue == -9) {
                        aStatus.error = "Timestamp is null";
                        aStatus.errorcode = -9;
                    } else if (retValue == -10) {
                        aStatus.error = "TimeStamp mistmatch!!!";
                        aStatus.errorcode = -10;
                    } else if (retValue == -11) {
                        aStatus.error = "TimeStamp has expired!!!";
                        aStatus.errorcode = -11;
                    } else if (retValue == -12) {
                        aStatus.error = "Invalid Location, out of home country!!!";
                        aStatus.errorcode = -12;
                    } else if (retValue == -13) {
                        aStatus.error = "Invalid Location, roaming country is not allowed!!!";
                        aStatus.errorcode = -13;
                    } else if (retValue == -14) {
                        aStatus.error = "Roaming duration is over";
                        aStatus.errorcode = -15;
                    } else if (retValue == -15) {
                        //aStatus.error = "TimeStamp Updated failed";
                        aStatus.error = "SUCCESS";
                        aStatus.errorcode = 0;
                    } else if (retValue == -2) {
                        aStatus.error = "Session has expired";
                        aStatus.errorcode = -2;
                    } else if (retValue == -1) {
                        aStatus.error = "General Exception";
                        aStatus.errorcode = -16;
                    }
                    log.error(aStatus.error);
                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(), "Mobile Trust PLUS Check", aStatus.error, aStatus.errorcode,
                            "Token Management", aStatus.error, " ",
                            itemtype, userid);

                    //return aStatus;
                    //end of addition
                } else if (otp.contains(":")) {
                    retValue = 0;
                }
                if (retValue == 0) {
                    if (otp.contains(":")) {
                        UtilityFunctions utility = new UtilityFunctions();
                        String oldLatiLong = otp.substring(0, otp.indexOf(":"));
                        String[] latilong = utility.DecodeGEOCode(oldLatiLong);
                        String newOtp = otp.substring(otp.indexOf(":") + 1, otp.length());
                        Location srvLoc = mManagment.getLocationByLongAndLat(latilong[1], latilong[0]);
                        SessionFactoryUtil geouSession = new SessionFactoryUtil(SessionFactoryUtil.geotrack);
                        Session geoSession = geouSession.openSession();
                        GeoTrackingUtils geoUtil = new GeoTrackingUtils(geouSession, geoSession);
                        SessionFactoryUtil guSession = new SessionFactoryUtil(SessionFactoryUtil.geofence);
                        Session gSession = guSession.openSession();
                        GeoFenceUtils gUtil = new GeoFenceUtils(guSession, gSession);
                        try {
                            //Location srvLocip = getLocationByIpAddress(ip);
                            int val = geoUtil.addGeotrack(channel.getChannelid(), userid, latilong[1], latilong[0], req.getRemoteAddr(), srvLoc.city, srvLoc.state, srvLoc.country, srvLoc.zipcode, LOGIN, new Date());
                            Geofence gFence = gUtil.getGeofence(userid, channel.getChannelid());
                            if (gFence == null) {
                                aStatus.errorcode = -7;
                            }

                            boolean b3434 = false;
                            String locationUser = null;
                            if (gFence != null) {
                                //b3434 = gFence.getRoamingAllowed();
                                b3434 = (gFence.getRoamingAllowed() != 0);
                                locationUser = gFence.getHomeCountry();
                            }

                            if (b3434 == false) {
                                if (srvLoc.country.equalsIgnoreCase(locationUser)) {
                                    retValue = 0;
                                } else {
                                    retValue = -12;
                                }
                            } else {

                                String[] foreignCountryList = null;
                                if (gFence.getForeignCountry() != null) {
                                    foreignCountryList = gFence.getForeignCountry().split(",");
                                }
                                if (foreignCountryList != null && foreignCountryList.length > 0) {
                                    for (int i = 0; i < foreignCountryList.length; i++) {
                                        if (srvLoc.country.equalsIgnoreCase(foreignCountryList[i])) {

                                            long nowDate = new Date().getTime();
                                            //log.debug(("Current Time::"+nowDate);
                                            //log.debug(("Start Time::"+gFence.getStartDateForRoming().getTime() );
                                            //log.debug(("End Time::"+gFence.getEndDateForRoming().getTime() );

                                            Long startDiff = nowDate - gFence.getStartDateForRoming().getTime();
                                            Long endDiff = gFence.getEndDateForRoming().getTime() - nowDate;
                                            if (startDiff < 0 || endDiff < 0) {
                                                retValue = -11;
                                            }
                                            retValue = 0;
                                        }
                                    }
                                } else {
                                    retValue = -13;
                                }

//                                if (gFence.getForeignCountry().equalsIgnoreCase(srvLoc.country)) {
//                                    Long startDiff = new Date().getTime() - gFence.getStartDateForRoming().getTime();
//                                    Long endDiff = gFence.getEndDateForRoming().getTime() - new Date().getTime();
//                                    if (startDiff < 0 || endDiff < 0) {
//                                        retValue = -11;
//                                    }
//                                    retValue = 0;
//                                } else {
//                                    retValue = -12;
//                                }
                            }
                        } finally {
                            geoSession.close();
                            geouSession.close();
                            gSession.close();
                            guSession.close();
                        }
                        if (retValue == 0) {
                            retValue = oManagement.VerifyOTP(session.getChannelid(), userid, sessionid, newOtp);
                        }
                    } else {
                        retValue = oManagement.VerifyOTP(session.getChannelid(), userid, sessionid, otp);
                    }
                }

                if (retValue == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                } else if (retValue == -22) {
                    aStatus.error = "OTP Token is locked";
                    aStatus.errorcode = -22;
                } else if (retValue == -8) {
                    aStatus.error = "OTP is already consumed";
                    aStatus.errorcode = -8;
                } else if (retValue == -17) {
                    aStatus.error = "OTP Token not found ";
                    aStatus.errorcode = -17;
                } else if (retValue == -9) {
                    aStatus.error = "OTP is expired";
                    aStatus.errorcode = -9;
                } else if (retValue == -2) {
                    aStatus.error = "Session has expired";
                    aStatus.errorcode = -2;
                } else if (retValue == -1) {
                    aStatus.error = "General Error";
                    aStatus.errorcode = -1;
                }
                
                if (retValue != 0) { 
                    log.error(aStatus.error);
                }
            }
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                    session.getLoginid(), session.getLoginid(), new Date(), "Verify OTP ", aStatus.error, aStatus.errorcode,
                    "Token Management", "OTP = ******", " OTP = ******",
                    itemtype, userid);
            return aStatus;

        } catch (Exception e) {
            log.error(e);
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "General Exception::" + e.getMessage();
            aStatus.errorcode = -99;
            return aStatus;
        }
    }

    private byte[] SHA1(String message) {
        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            byte[] array = sha1.digest(message.getBytes());
            return array;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public AxiomStatus ChangeUserDetails(String sessionId, String userid, String fullname, String phone, String email) {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug("ChangeUserDetails::sessionId::" + sessionId);
                log.debug("ChangeUserDetails::userid::" + userid);
                log.debug("ChangeUserDetails::fullname::" + fullname);
                log.debug("ChangeUserDetails::phone::" + phone);
                log.debug("ChangeUserDetails::email::" + email);
            }
        } catch (Exception ex) {
            log.error(ex);
        }

        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Licence Invalid";
            aStatus.errorcode = -100;
            log.error(aStatus.error);
            return aStatus;
        }

        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionId);
        if (session == null) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
            log.error(aStatus.error);
            return aStatus;
            //throw  new AxiomException("");
        }

        try {
            UserManagement uManagement = new UserManagement();
            AxiomStatus aStatus = new AxiomStatus();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            AuthUser oldUser = null;
            if (session != null) {

                //log.debug(("Client IP = " + req.getRemoteAddr());        
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    aStatus.error = "Invalid Channel";
                    log.error(aStatus.error);
                    aStatus.errorcode = -3;
                    return aStatus;
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP";
                                aStatus.errorcode = -8;
                                log.error(aStatus.error);
                                return aStatus;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP";
                            aStatus.errorcode = -8;
                            log.error(aStatus.error);
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP";
                        aStatus.errorcode = -8;
                        log.error(aStatus.error);
                        return aStatus;
                    }
                }

                retValue = uManagement.EditUser(sessionId, session.getChannelid(), userid, fullname, phone, email, 0, null, null, null, null, null, null, null, null);
                if (retValue == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                } else if (retValue == 1) {
                    aStatus.error = "User Details Changed Succesfully but email could not be sent!!!";
                    aStatus.errorcode = 0;
                    log.error(aStatus.error);
                } else if (retValue == -5 || retValue == -1 || retValue == -2) {
                    aStatus.error = "General Error (" + retValue + ")";
                    aStatus.errorcode = -5;
                    log.error(aStatus.error);
                }

                //return aStatus;
                oldUser = uManagement.getUser(sessionId, session.getChannelid(), userid);
            }

            String oldUserName = "";
            String oldPhoneNo = "";
            String oldEmailID = "";
            String oldPasswordState = "";

            if (oldUser != null) {
                oldUserName = oldUser.getUserName();
                oldPhoneNo = oldUser.getPhoneNo();
                oldEmailID = oldUser.getEmail();
                oldPasswordState = "" + oldUser.getStatePassword();
            }

            String itemtype = "USERPASSWORD";

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            //AuditManagement audit = new AuditManagement();
            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                    session.getLoginid(), session.getLoginid(), new Date(), "Edit User", aStatus.error, aStatus.errorcode,
                    "User Management", "User Name = " + oldUserName + "User Phone =" + oldPhoneNo + "User Email =" + oldEmailID + "State Of Password =" + oldPasswordState,
                    "User Name = " + fullname + "User Phone =" + phone + "User Email =" + email + "State Of Password =" + -1,
                    itemtype, "");

            return aStatus;
        } catch (Exception e) {
            log.error(e);
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "General Exception::" + e.getMessage();
            aStatus.errorcode = -99;
            return aStatus;
        }
    }

    @Override
    public AxiomStatus DeleteUser(String sessionId, String userid) {
        try {

            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Licence Invalid";
                aStatus.errorcode = -100;
                log.error(aStatus.error);
                return aStatus;
            }

            String strDebug = null;
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    log.debug("DeleteUser::sessionId::" + sessionId);
                    log.debug("DeleteUser::userid::" + userid);

                }
            } catch (Exception ex) {
                log.error(ex);
            }

             iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
                log.error(aStatus.error);
                return aStatus;
            }
            
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                log.error(aStatus.error);
                return aStatus;
                //throw  new AxiomException("");
            }

            UserManagement uManagement = new UserManagement();
//            SessionManagement sManagement = new SessionManagement();
//            MessageContext mc = wsContext.getMessageContext();
//            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//            Sessions session = sManagement.getSessionById(sessionId);
            AxiomStatus aStatus = new AxiomStatus();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            AuthUser oldUser = null;
            if (session != null) {

                //log.debug(("Client IP = " + req.getRemoteAddr());        
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    aStatus.error = "Invalid Channel";
                    aStatus.errorcode = -3;
                    log.error(aStatus.error);
                    return aStatus;
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP";
                                aStatus.errorcode = -8;
                                log.error(aStatus.error);
                                return aStatus;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP";
                            aStatus.errorcode = -8;
                            log.error(aStatus.error);
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP";
                        aStatus.errorcode = -8;
                        log.error(aStatus.error);
                        return aStatus;
                    }
                }

                retValue = uManagement.DeleteUser(sessionId, session.getChannelid(), userid);
                if (retValue == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                }
                //return aStatus;
                oldUser = uManagement.getUser(sessionId, session.getChannelid(), userid);
            }
            String oldUserName = "";
            String oldPhoneNo = "";
            String oldEmailID = "";
            String oldPasswordState = "";

            if (oldUser != null) {
                oldUserName = oldUser.getUserName();
                oldPhoneNo = oldUser.getPhoneNo();
                oldEmailID = oldUser.getEmail();
                oldPasswordState = "" + oldUser.getStatePassword();
            }

            String itemtype = "USERPASSWORD";
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
//            AuditManagement audit = new AuditManagement();
            if (retValue == 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                        session.getLoginid(), session.getLoginid(), new Date(), "Delete User", aStatus.error, aStatus.errorcode,
                        "User Management", "User Name = " + oldUserName + "User Phone =" + oldPhoneNo + "User Email =" + oldEmailID + "State Of Password =" + oldPasswordState,
                        "User Deleted Successfully",
                        itemtype, "");
                return aStatus;
            } else {
                String reason = "User Deletion Failed";
                if (retValue == -10) {
                    reason += "User is assigned token/certificate, therefore cannot be removed!!!";
                    aStatus.error = reason;
                    aStatus.errorcode = -10;
                    log.error(aStatus.error);
                } else {
                    reason += "!!!";
                    aStatus.error = reason;
                    aStatus.errorcode = -11;
                    log.error(aStatus.error);
                }
                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                        session.getLoginid(), session.getLoginid(), new Date(), "Delete User", aStatus.error, aStatus.errorcode,
                        "User Management", "User Name = " + oldUserName + "User Phone =" + oldPhoneNo + "User Email =" + oldEmailID + "State Of Password =" + oldPasswordState,
                        reason,
                        itemtype, "");

                return aStatus;
            }
        } catch (Exception e) {
            log.error(e);
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "General Exception::" + e.getMessage();
            aStatus.errorcode = -99;
            return aStatus;

        }
    }

    @Override
    public AxiomStatus VerifySignatureOTPPlus(String sessionid, String userid, int type, String[] data, String sotp, String sotpPlus) {

        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug("VerifySignatureOTPPlus::sessionid::" + sessionid);
                log.debug("VerifySignatureOTPPlus::userid::" + userid);
                log.debug("VerifySignatureOTPPlus::type::" + type);
                log.debug("VerifySignatureOTPPlus::sotp::" + sotp);
                log.debug("VerifySignatureOTPPlus::sotpPlus::" + sotpPlus);
                log.debug("VerifySignatureOTPPlus::data.length::" + data.length);
                for (int i = 0; i < data.length; i++) {
                    log.debug("VerifySignatureOTPPlus::data[" + i + "]::" + data[i]);
                }
            }
        } catch (Exception ex) {
            log.error(ex);
        }

        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Licence invalid";
            aStatus.errorcode = -100;
            log.error(aStatus.error);
            return aStatus;
        }

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.MOBILETRUST) != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Mobile Trust feature is not available in this license!!!";
            aStatus.errorcode = -101;
            log.error(aStatus.error);
            return aStatus;
        }

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_OOB) != 0
                && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_HARDWARE) != 0
                && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "OTP Token feature is not available in this license!!!";
            aStatus.errorcode = -102;
            log.error(aStatus.error);
            return aStatus;
        }

        // int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "Licence Not Valid";
//            aStatus.errorcode = -10;
//            return aStatus;
//        }
        int retValue = -10;
        AxiomStatus aStatus = new AxiomStatus();
        aStatus.error = "Invalid Parameters!!!";
        aStatus.errorcode = retValue;

        if (sessionid == null || userid == null || sotp == null) {
            log.error(aStatus.error);
            return aStatus;
        }
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);

        if (session == null) {
            aStatus.error = "Invalid Session";
            aStatus.errorcode = -104;
            log.error(aStatus.error);
            return aStatus;
        }
        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(session.getChannelid());
        if (channel == null) {
            aStatus.error = "Invalid Channel";
            aStatus.errorcode = -3;
            log.error(aStatus.error);
            return aStatus;
        }

        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
        if (session != null) {

            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagementObj = new OperatorsManagement();
                        Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP";
                            aStatus.errorcode = -108;
                            log.error(aStatus.error);
                            return aStatus;
                        }

                        suTemplate.close();
                        sTemplate.close();
                        aStatus.error = "INVALID IP";
                        aStatus.errorcode = -108;
                        log.error(aStatus.error);
                        return aStatus;
                    }
                    aStatus.error = "INVALID IP";
                    aStatus.errorcode = -108;
                    log.error(aStatus.error);
                    return aStatus;
                }
            }
            
            MobileTrustManagment mManagment = new MobileTrustManagment();
            int check = -9;
            JSONObject jsonObj = null;
            int devicetype = 1;
            try {
                jsonObj = new JSONObject(sotpPlus);

                String device = jsonObj.getString("_deviceType");

                if (device.equals("ANDROID")) {
                    devicetype = ANDROID;
                } else if (device.equals("IOS")) {
                    devicetype = IOS;
                }
            } catch (Exception ex) {
                log.error(ex);
            }

            if (sotpPlus != null && !sotp.contains(":")) {
                check = mManagment.CheckPlusComponent(channel, userid, sessionid, sotp, sotpPlus, devicetype);
                log.debug("CheckPlusComponent returned::" + check);
                //added for error handling
                if (retValue == -6) {
                    aStatus.error = "Trusted device not found";
                    aStatus.errorcode = -6;
                } else if (retValue == -4) {
                    aStatus.error = "OTP is invalid";
                    aStatus.errorcode = -4;
                } else if (retValue == -5) {
                    aStatus.error = "Trusted Device mismatch";
                    aStatus.errorcode = -5;
                } else if (retValue == -7) {
                    aStatus.error = "Invalid Location, out of home country!!!";
                    aStatus.errorcode = -7;
                } else if (retValue == -8) {
                    aStatus.error = "Mobile Trust Setting is not configured!!!";
                    aStatus.errorcode = -118;
                } else if (retValue == -9) {
                    aStatus.error = "Timestamp is null";
                    aStatus.errorcode = -9;
                } else if (retValue == -10) {
                    aStatus.error = "TimeStamp mistmatch!!!";
                    aStatus.errorcode = -10;
                } else if (retValue == -11) {
                    aStatus.error = "TimeStamp has expired!!!";
                    aStatus.errorcode = -11;
                } else if (retValue == -12) {
                    aStatus.error = "Invalid Location, out of home country!!!";
                    aStatus.errorcode = -12;
                } else if (retValue == -13) {
                    aStatus.error = "Invalid Location, roaming country is not allowed!!!";
                    aStatus.errorcode = -13;
                } else if (retValue == -14) {
                    aStatus.error = "Roaming duration is over";
                    aStatus.errorcode = -15;
                } else if (retValue == -15) {
                    //aStatus.error = "TimeStamp Updated failed";
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                } else if (retValue == -2) {
                    aStatus.error = "Session has expired";
                    aStatus.errorcode = -2;
                } else if (retValue == -1) {
                    aStatus.error = "General Exception";
                    aStatus.errorcode = -16;
                }
                log.error(aStatus.error);
                log.debug(aStatus.error);
                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                        session.getLoginid(), session.getLoginid(), new Date(), "Mobile Trust PLUS Check", aStatus.error, aStatus.errorcode,
                        "Token Management", aStatus.error, " ",
                        itemtype, userid);

                //return aStatus;
                //end of addition
            } else if (sotp.contains(":")) {
                check = 0;
            }
            if (check == 0) {
                if (sotp.contains(":")) {
                    UtilityFunctions utility = new UtilityFunctions();
                    String oldLatiLong = sotp.substring(0, sotp.indexOf(":"));
                    String[] latilong = utility.DecodeGEOCode(oldLatiLong);
                    String newOtp = sotp.substring(sotp.indexOf(":") + 1, sotp.length());
                    Location srvLoc = mManagment.getLocationByLongAndLat(latilong[1], latilong[0]);
                    SessionFactoryUtil geouSession = new SessionFactoryUtil(SessionFactoryUtil.geotrack);
                    Session geoSession = geouSession.openSession();
                    GeoTrackingUtils geoUtil = new GeoTrackingUtils(geouSession, geoSession);
                    SessionFactoryUtil guSession = new SessionFactoryUtil(SessionFactoryUtil.geofence);
                    Session gSession = guSession.openSession();
                    GeoFenceUtils gUtil = new GeoFenceUtils(guSession, gSession);
                    try {
                        //Location srvLocip = getLocationByIpAddress(ip);
                        int val = geoUtil.addGeotrack(channel.getChannelid(), userid, latilong[1], latilong[0], req.getRemoteAddr(), srvLoc.city, srvLoc.state, srvLoc.country, srvLoc.zipcode, LOGIN, new Date());
                        Geofence gFence = gUtil.getGeofence(userid, channel.getChannelid());
                        if (gFence == null) {
                            aStatus.errorcode = -7;
                        }
                        boolean b3434 = false;
                        String locationUser = null;
                        if (gFence != null) {
                            //b3434 = gFence.getRoamingAllowed();
                            b3434 = (gFence.getRoamingAllowed() != 0);
                            locationUser = gFence.getHomeCountry();
                        }

                        if (b3434 == false) {
                            if (srvLoc.country.equalsIgnoreCase(locationUser)) {
                                retValue = 0;
                            } else {
                                retValue = -12;
                            }
                        } else {
                            String[] foreignCountryList = null;
                            if (gFence.getForeignCountry() != null) {
                                foreignCountryList = gFence.getForeignCountry().split(",");
                            }
                            if (foreignCountryList != null && foreignCountryList.length > 0) {
                                for (int i = 0; i < foreignCountryList.length; i++) {
                                    if (srvLoc.country.equalsIgnoreCase(foreignCountryList[i])) {

                                        long nowDate = new Date().getTime();
                                        //log.debug(("Current Time::"+nowDate);
                                        //log.debug(("Start Time::"+gFence.getStartDateForRoming().getTime() );
                                        //log.debug(("End Time::"+gFence.getEndDateForRoming().getTime() );

                                        Long startDiff = nowDate - gFence.getStartDateForRoming().getTime();
                                        Long endDiff = gFence.getEndDateForRoming().getTime() - nowDate;
                                        if (startDiff < 0 || endDiff < 0) {
                                            retValue = -11;
                                        }
                                        retValue = 0;
                                    }
                                }
                            } else {
                                retValue = -13;
                            }
//                            if (gFence.getForeignCountry().equalsIgnoreCase(srvLoc.country)) {
//                                Long startDiff = new Date().getTime() - gFence.getStartDateForRoming().getTime();
//                                Long endDiff = gFence.getEndDateForRoming().getTime() - new Date().getTime();
//                                if (startDiff < 0 || endDiff < 0) {
//                                    retValue = -11;
//                                }
//                                retValue = 0;
//                            } else {
//                                retValue = -12;
//                            }
                        }
                    } finally {
                        geoSession.close();
                        geouSession.close();
                        gSession.close();
                        guSession.close();
                    }
                    if (retValue == 0) {
                        retValue = oManagement.VerifySignatureOTP(session.getChannelid(), userid, sessionid, data, newOtp);
                    }
                } else {
                    retValue = oManagement.VerifySignatureOTP(session.getChannelid(), userid, sessionid, data, sotp);
                }
            }

            if (retValue == 0) {
                aStatus.error = "SUCCESS";
                aStatus.errorcode = 0;
            } else if (retValue == -22) {
                aStatus.error = "Signature OTP Token is locked";
                aStatus.errorcode = -22;
            } else if (retValue == -8) {
                aStatus.error = "Signature OTP is already consumed";
                aStatus.errorcode = -8;
            } else if (retValue == -17) {
                aStatus.error = "User/Token is not found";
                aStatus.errorcode = -17;
            } else if (retValue == -9) {
                aStatus.error = "Signature OTP is expired";
                aStatus.errorcode = -9;
            } else if (retValue == -2) {
                aStatus.error = "Session has expired";
                aStatus.errorcode = -2;
            } else if (retValue == -1) {
                aStatus.error = "General Error";
                aStatus.errorcode = -1;
            }
            
            
            if ( retValue == 0) { 
                log.error(aStatus.error);
            }

        }
        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                session.getLoginid(), session.getLoginid(), new Date(), "Verify SOTP ", aStatus.error, aStatus.errorcode,
                "Token Management", "SOTP = ******", " SOTP = ******",
                itemtype, userid);
        return aStatus;
    }

}
