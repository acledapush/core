/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.core;

import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.SettingsUtil;
import com.mollatech.axiom.nucleus.db.connector.management.ConnectorStatusAuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.settings.ConnectorStatusInternal;
import com.mollatech.axiom.nucleus.settings.OOBEmailChannelSettings;
import com.mollatech.axiom.nucleus.settings.OOBMobileChannelSettings;
import com.mollatech.axiom.nucleus.settings.RootCertificateSettings;
import com.mollatech.axiom.nucleus.settings.AxiomRadiusConfiguration;
import com.mollatech.axiom.nucleus.settings.IphonePushNotificationSettings;
import com.mollatech.axiom.nucleus.settings.PushNotificationSettings;
import com.mollatech.axiom.radius.AxiomRadiusInterfaceImpl;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.hibernate.Session;

public class ConnectorStatusListener implements ServletContextListener {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ConnectorStatusListener.class.getName());

    private static OOBEmailChannelSettings OOBEMailPrimary;
    private static OOBEmailChannelSettings OOBEMailSecondary;
    private static OOBMobileChannelSettings OOBSMSPrimary;
    private static OOBMobileChannelSettings OOBSMSSecondary;
    private static OOBMobileChannelSettings OOBUSSDPrimary;
    private static OOBMobileChannelSettings OOBUSSDSecondary;
    private static OOBMobileChannelSettings OOBVoicePrimary;    
    private static OOBMobileChannelSettings OOBVoiceSecondary;
    private static AxiomRadiusConfiguration RadiusSetting;
    private static PushNotificationSettings ANDROIDPUSHSetting;
    private static IphonePushNotificationSettings IPHONEPUSHSetting;
    private static RootCertificateSettings rCert;
    
    
    
    private static int intervalCall;
    private static Timer timer;
    private static ConnectorStatusInternal cStatus;
    private static String channelid;
    private static Channels channel;
    private final int NOT_CONFIGURED = -2;
    private final int SUSPENDED = -1;
    private final int RUNNING = 0;

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
        ServletContext ch = sce.getServletContext();
        String _channelName = ch.getContextPath();
        log.info("Channel Core Name >>" + _channelName);
        //_channelName = "/ibank2core";
        _channelName = _channelName.replaceAll("/", "");
        if (_channelName.compareTo("core") != 0) {
            _channelName = _channelName.replaceAll("core", "");
        } else {
            _channelName = "face";
        }

        channel = cUtil.getChannel(_channelName);
        if (channel == null) {
          //  log.error("Channel Details could not be found>>" + _channelName);
            return;
        }

        SettingsManagement sMngt = new SettingsManagement();
        //new addition
        ChannelProfile channelprofileObj = null;
        Object channelpobj = sMngt.getSettingInner(channel.getChannelid(), SettingsManagement.CHANNELPROFILE_SETTING, 1);

        if (channelpobj == null) {
            LoadSettings.LoadChannelProfile(channelprofileObj);
        } else {
            channelprofileObj = (ChannelProfile) channelpobj;
            LoadSettings.LoadChannelProfile(channelprofileObj);
        }

        String interval = LoadSettings.g_sSettings.getProperty("connector.status.check.time");
        if (interval != null) {
            intervalCall = new Integer(interval).intValue();
        } else {
            intervalCall = 300;
        }
        
        try {

            cStatus = new ConnectorStatusInternal();

            timer = new Timer();

            timer.schedule(new TimerTask() {
                public void run() {

                    String active = LoadSettings.g_sSettings.getProperty("reserved.1");

                    if (active == null || active.isEmpty() == true) {
                      //  log.error("system is not active yet!!!");
                    } else {

                        try {
                            SessionFactoryUtil suSettings = new SessionFactoryUtil(SessionFactoryUtil.settings);
                            Session sSettings = suSettings.openSession();
                            SettingsUtil sUtil = new SettingsUtil(suSettings, sSettings);
                            //SettingsManagement sManagement = new SettingsManagement();
                            SettingsManagement sMngt = new SettingsManagement();

                            channelid = channel.getChannelid();
                            OOBEMailPrimary = (OOBEmailChannelSettings) sUtil.getSetting(channel.getChannelid(), SettingsManagement.EMAIL, 1);
                            OOBEMailSecondary = (OOBEmailChannelSettings) sUtil.getSetting(channel.getChannelid(), SettingsManagement.EMAIL, 2);
                            OOBSMSPrimary = (OOBMobileChannelSettings) sUtil.getSetting(channel.getChannelid(), SettingsManagement.SMS, 1);
                            OOBSMSSecondary = (OOBMobileChannelSettings) sUtil.getSetting(channel.getChannelid(), SettingsManagement.SMS, 2);
                            OOBVoicePrimary = (OOBMobileChannelSettings) sUtil.getSetting(channel.getChannelid(), SettingsManagement.VOICE, 1);
                            OOBVoiceSecondary = (OOBMobileChannelSettings) sUtil.getSetting(channel.getChannelid(), SettingsManagement.VOICE, 2);
                            OOBUSSDPrimary = (OOBMobileChannelSettings) sUtil.getSetting(channel.getChannelid(), SettingsManagement.USSD, 1);
                            OOBUSSDSecondary = (OOBMobileChannelSettings) sUtil.getSetting(channel.getChannelid(), SettingsManagement.USSD, 2);
                            rCert = (RootCertificateSettings) sUtil.getSetting(channel.getChannelid(), SettingsManagement.RootConfiguration, 1);
                            RadiusSetting = (AxiomRadiusConfiguration) sUtil.getSetting(channel.getChannelid(), SettingsManagement.Radius, 1);
                            ANDROIDPUSHSetting = (PushNotificationSettings) sUtil.getSetting(channel.getChannelid(), SettingsManagement.ANDROID_PUSH_NOTIFICATION_SETTING, 1);
                            IPHONEPUSHSetting = (IphonePushNotificationSettings) sUtil.getSetting(channel.getChannelid(), SettingsManagement.IOS_PUSH_NOTIFICATION_SETTING, 1);

                            //SettingsManagement sManagement = new SettingsManagement();
                            // do stuff
                            if (OOBEMailPrimary == null) {
                                cStatus.EMAILPrimary = NOT_CONFIGURED;
                            } else {
                                if (OOBEMailPrimary.getStatus() == SettingsManagement.SUSPENDED_STATUS) {
                                    cStatus.EMAILPrimary = SUSPENDED;
                                } else {
                                    int status = sMngt.GetServiceStatus(channelid, OOBEMailPrimary, SettingsManagement.EMAIL, 1);
                                    if (status == -1 || status == -2) {
                                        cStatus.EMAILPrimary = -5;
                                    } else {
                                        cStatus.EMAILPrimary = status;
                                    }

                                }
                            }
                            if (OOBEMailSecondary == null) {
                                cStatus.EMAILSecondary = NOT_CONFIGURED;
                            } else {
                                if (OOBEMailSecondary.getStatus() == SettingsManagement.SUSPENDED_STATUS) {
                                    cStatus.EMAILSecondary = SUSPENDED;
                                }
                                int status = sMngt.GetServiceStatus(channelid, OOBEMailSecondary, SettingsManagement.EMAIL, 2);
                                if (status == -1 || status == -2) {
                                    cStatus.EMAILSecondary = -5;
                                } else {
                                    cStatus.EMAILSecondary = status;
                                }
                            }
                            if (OOBSMSPrimary == null) {
                                cStatus.SMSPrimary = NOT_CONFIGURED;
                            } else {
                                if (OOBSMSPrimary.getStatus() == SettingsManagement.SUSPENDED_STATUS) {
                                    cStatus.SMSPrimary = SUSPENDED;
                                } else {
                                    try {
                                        int status = sMngt.GetServiceStatus(channelid, OOBSMSPrimary, SettingsManagement.SMS, 1);

                                        if (status == -1 || status == -2) {
                                            cStatus.SMSPrimary = -5;
                                        } else {
                                            cStatus.SMSPrimary = status;
                                        }
                                    } catch (Exception e) {
                                       // e.printStackTrace();
                                    }
                                }
                            }
                            if (OOBSMSSecondary == null) {
                                cStatus.SMSSecondary = NOT_CONFIGURED;
                            } else {
                                if (OOBSMSSecondary.getStatus() == SettingsManagement.SUSPENDED_STATUS) {
                                    cStatus.SMSSecondary = SUSPENDED;
                                } else {
                                    int status = sMngt.GetServiceStatus(channelid, OOBSMSSecondary, SettingsManagement.SMS, 2);
                                    if (status == -1 || status == -2) {
                                        cStatus.SMSSecondary = -5;
                                    } else {
                                        cStatus.SMSSecondary = status;
                                    }
                                }
                            }
                            if (OOBUSSDPrimary == null) {
                                cStatus.USSDSecondary = NOT_CONFIGURED;
                            } else {
                                if (OOBUSSDPrimary.getStatus() == SettingsManagement.SUSPENDED_STATUS) {
                                    cStatus.USSDSecondary = SUSPENDED;
                                } else {
                                    int status = sMngt.GetServiceStatus(channelid, OOBUSSDPrimary, SettingsManagement.USSD, 1);
                                    if (status == -1 || status == -2) {
                                        cStatus.USSDPrimary = -5;
                                    } else {
                                        cStatus.USSDPrimary = status;
                                    }
                                }
                            }
                            if (OOBUSSDSecondary == null) {
                                cStatus.USSDSecondary = NOT_CONFIGURED;
                            } else {
                                if (OOBUSSDSecondary.getStatus() == SettingsManagement.SUSPENDED_STATUS) {
                                    cStatus.USSDSecondary = SUSPENDED;
                                } else {
                                    int status = sMngt.GetServiceStatus(channelid, OOBUSSDSecondary, SettingsManagement.USSD, 2);
                                    if (status == -1 || status == -2) {
                                        cStatus.USSDSecondary = -5;
                                    } else {
                                        cStatus.USSDSecondary = status;
                                    }
                                }
                            }
                            if (OOBVoicePrimary == null) {
                                cStatus.VOICEPrimary = NOT_CONFIGURED;
                            } else {
                                if (OOBVoicePrimary.getStatus() == SettingsManagement.SUSPENDED_STATUS) {
                                    cStatus.VOICEPrimary = SUSPENDED;
                                } else {
                                    int status = sMngt.GetServiceStatus(channelid, OOBVoicePrimary, SettingsManagement.VOICE, 1);
                                    if (status == -1 || status == -2) {
                                        cStatus.VOICEPrimary = -5;
                                    } else {
                                        cStatus.VOICEPrimary = status;
                                    }
                                }
                            }
                            if (OOBVoiceSecondary == null) {
                                cStatus.VOICESecondary = NOT_CONFIGURED;
                            } else {
                                if (OOBVoiceSecondary.getStatus() == SettingsManagement.SUSPENDED_STATUS) {
                                    cStatus.VOICESecondary = SUSPENDED;
                                } else {
                                    int status = sMngt.GetServiceStatus(channelid, OOBVoiceSecondary, SettingsManagement.VOICE, 2);
                                    if (status == -1 || status == -2) {
                                        cStatus.VOICESecondary = -5;
                                    } else {
                                        cStatus.VOICESecondary = status;
                                    }
                                }
                            }
                            if (rCert == null) {
                                cStatus.CertificateConnector = NOT_CONFIGURED;
                            } else {
                                if (rCert.getStatus() == SettingsManagement.SUSPENDED_STATUS) {
                                    cStatus.CertificateConnector = SUSPENDED;
                                } else {
                                    int status = sMngt.GetServiceStatus(channelid, rCert, SettingsManagement.RootConfiguration, 1);
                                    if (status == -1 || status == -2) {
                                        cStatus.CertificateConnector = -5;
                                    } else {
                                        cStatus.CertificateConnector = status;
                                    }
                                }
                            }
                            if (RadiusSetting == null) {
                                cStatus.AxiomRadius = NOT_CONFIGURED;
                            } else {

                                int status = AxiomRadiusInterfaceImpl.AxiomRadiusServerStatus;
                                if (status == 1) {
                                    cStatus.AxiomRadius = 0;
                                } else {
                                    cStatus.AxiomRadius = -5;
                                }

                            }
                            if (ANDROIDPUSHSetting == null) {
                                cStatus.AndroidPUSH = NOT_CONFIGURED;
                            } else {

                                int status = sMngt.GetServiceStatus(channelid, ANDROIDPUSHSetting, SettingsManagement.ANDROID_PUSH_NOTIFICATION_SETTING, 1);
                                if (status == 1) {
                                    cStatus.AndroidPUSH = 0;
                                } else {
                                    cStatus.AndroidPUSH = -5;
                                }

                            }
                            if (IPHONEPUSHSetting == null) {
                                cStatus.IphonePUSH = NOT_CONFIGURED;
                            } else {
                                int status = sMngt.GetServiceStatus(channelid, IPHONEPUSHSetting, SettingsManagement.IOS_PUSH_NOTIFICATION_SETTING, 1);
                                if (status == 1) {
                                    cStatus.IphonePUSH = 0;
                                } else {
                                    cStatus.IphonePUSH = -5;
                                }

                            }
                            cStatus.channelid = channelid;
                            cStatus.statusDateTime = new Date();

                            AxiomRadiusInterfaceImpl.AxiomRadiusServerStatus = -1;

                            ConnectorStatusAuditManagement cManagement = new ConnectorStatusAuditManagement();

                            cManagement.addConnectorStatusConnectorstatusaudit(channelid, cStatus.SMSPrimary, SettingsManagement.SMS, SettingsManagement.PREFERENCE_ONE, new Date());
                            cManagement.addConnectorStatusConnectorstatusaudit(channelid, cStatus.SMSSecondary, SettingsManagement.SMS, SettingsManagement.PREFERENCE_TWO, new Date());

                            cManagement.addConnectorStatusConnectorstatusaudit(channelid, cStatus.VOICEPrimary, SettingsManagement.VOICE, SettingsManagement.PREFERENCE_ONE, new Date());
                            cManagement.addConnectorStatusConnectorstatusaudit(channelid, cStatus.VOICESecondary, SettingsManagement.VOICE, SettingsManagement.PREFERENCE_TWO, new Date());

                            cManagement.addConnectorStatusConnectorstatusaudit(channelid, cStatus.USSDPrimary, SettingsManagement.USSD, SettingsManagement.PREFERENCE_ONE, new Date());
                            cManagement.addConnectorStatusConnectorstatusaudit(channelid, cStatus.USSDSecondary, SettingsManagement.USSD, SettingsManagement.PREFERENCE_TWO, new Date());

                            cManagement.addConnectorStatusConnectorstatusaudit(channelid, cStatus.EMAILPrimary, SettingsManagement.EMAIL, SettingsManagement.PREFERENCE_ONE, new Date());
                            cManagement.addConnectorStatusConnectorstatusaudit(channelid, cStatus.EMAILSecondary, SettingsManagement.EMAIL, SettingsManagement.PREFERENCE_TWO, new Date());

                            cManagement.addConnectorStatusConnectorstatusaudit(channelid, cStatus.CertificateConnector, SettingsManagement.RootConfiguration, SettingsManagement.PREFERENCE_ONE, new Date());

                            cManagement.addConnectorStatusConnectorstatusaudit(channelid, cStatus.AndroidPUSH, SettingsManagement.ANDROID_PUSH_NOTIFICATION_SETTING, SettingsManagement.PREFERENCE_ONE, new Date());
                            cManagement.addConnectorStatusConnectorstatusaudit(channelid, cStatus.AndroidPUSH, SettingsManagement.IOS_PUSH_NOTIFICATION_SETTING, SettingsManagement.PREFERENCE_ONE, new Date());

                            cManagement.addConnectorStatusConnectorstatusaudit(channelid, cStatus.AxiomRadius, SettingsManagement.Radius, SettingsManagement.PREFERENCE_ONE, new Date());
                            Object obj = sMngt.getSetting(channel.getChannelid(), SettingsManagement.CONNECTORS_STATUS, SettingsManagement.PREFERENCE_ONE);
                            ConnectorStatusInternal cStatusOld = null;

                            if (obj != null) {
                                cStatusOld = (ConnectorStatusInternal) obj;
                            }
                            if (cStatusOld == null) {
                                sMngt.addSetting(channelid, SettingsManagement.CONNECTORS_STATUS, SettingsManagement.PREFERENCE_ONE, cStatus);
                            } else {
                                sMngt.updateSetting(channelid, SettingsManagement.CONNECTORS_STATUS, SettingsManagement.PREFERENCE_ONE, cStatusOld, cStatus);
                            }

                            sSettings.close();
                            suSettings.close();

                        } catch (Exception e) {
                            //e.printStackTrace();
                            //log.error(e);
                        }

                    }

                }
            }, (60000), (intervalCall * 1000));
        } catch (Exception ex) {
           // log.error(ex);
        } finally {
            sChannel.close();
            suChannel.close();
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        try {
            timer.purge();
            timer.cancel();
        } catch (Exception ex) {
           // log.error(ex);
        }

    }

    public ConnectorStatusInternal CheckConnectorStatus(String channelid) {
        try {
            return ConnectorStatusListener.cStatus;
        } catch (Exception ex) {
           // log.error(ex);
        }
        return null;
    }
}
