/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.core.certificateDicsovery;

import certdiscovery.CertificatesReport;
import com.mollatech.axiom.v2.core.utils.AxiomStatus;
import com.mollatech.axiom.v2.core.utils.CertificateManagerDelete;
import com.mollatech.axiom.v2.core.utils.CertificateManagerStatus;
import com.mollatech.axiom.v2.core.utils.CertificateReport;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author bluebricks
 */
@WebService
public interface CertDiscovery {

    @WebMethod
    public String OpenSession(@WebParam(name = "channelid") String channelid, @WebParam(name = "loginid") String loginid, @WebParam(name = "loginpassword") String loginpassword);

    @WebMethod
    public AxiomStatus CloseSession(@WebParam(name = "sessionid") String sessionid);

    @WebMethod
    public CertificateManagerStatus ScanAllNodes(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "ip") String ip, @WebParam(name = "port") String[] port);
    
    @WebMethod
    public List<CertificatesReport> getReports(@WebParam(name = "sessionid") String sessionid,@WebParam(name = "archiveid") String archiveid,@WebParam(name = "reportType") String reportType,@WebParam(name = "reportCategory") String repoortCategory);
    
    @WebMethod
    public CertificateManagerDelete deleteRecords(@WebParam(name = "sessionid") String sessionid,@WebParam(name = "archiveid") String archiveid);
}
