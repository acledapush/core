package com.mollatech.ftress.mapper;


import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(name = "ftressOSB")
public interface FtressAcledaInterface {
     @WebMethod
    public AuthenticationResponse verifyEBankingPassword(@WebParam(name = "header") HeaderIn header, @WebParam(name = "request") request request,@WebParam(name = "spare") spare spare);

    @WebMethod
    public Response resetUser(@WebParam(name = "header") HeaderIn header, @WebParam(name = "request") request request,@WebParam(name = "spare") spare spare);

    @WebMethod
    public AuthenticationResponse changeEBankingPassword(@WebParam(name = "header") HeaderIn header, @WebParam(name = "request") request request,@WebParam(name = "spare") spare spare);

    @WebMethod
    public Response assignPwdAuthenticationType(@WebParam(name = "header") HeaderIn header, @WebParam(name = "request") request request,@WebParam(name = "spare") spare spare);

    @WebMethod
    public AuthenticationResponse verifyEBankingOTP(  @WebParam(name = "header") HeaderIn header, @WebParam(name = "request") request request,@WebParam(name = "spare") spare spare);

    @WebMethod
    public Response createUser(  @WebParam(name = "header") HeaderIn header, @WebParam(name = "request") request request,@WebParam(name = "spare") spare spare);

    @WebMethod
    public Response systemLogout(  @WebParam(name = "header") HeaderIn header, @WebParam(name = "request") request request,@WebParam(name = "spare") spare spare);

    @WebMethod
    public AuthenticationResponse systemLogin(  @WebParam(name = "header") HeaderIn header, @WebParam(name = "request") request request,@WebParam(name = "spare") spare spare);

    @WebMethod
    public Response assignOtpAuthenticationType(@WebParam(name = "header") HeaderIn header, @WebParam(name = "request") request request,@WebParam(name = "spare") spare spare);

    @WebMethod
    public Response ping(  @WebParam(name = "header") HeaderIn header, @WebParam(name = "request") request request,@WebParam(name = "spare") spare spare);
}
