/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ibanking.acleda.proj.apac.ps.hid.com;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author vikramsareen
 */
@WebService(name="ftress")
public interface FtressWrapperInterface {
  
    @WebMethod
    public AuthenticationResponse verifyEBankingPassword(@WebParam(name = "systemSessionId") String systemSessionId, @WebParam(name = "userid") String userid, @WebParam(name = "password") String password,@WebParam(name = "fds_options") int fds_options,@WebParam(name = "channel") String channel,@WebParam(name = "domain") String domain);

    @WebMethod
    public Response resetUser(@WebParam(name = "systemSessionId") String systemSessionId, @WebParam(name = "authenticationType") String authenticationType, @WebParam(name = "userid") String userid, @WebParam(name = "channel") String channel, @WebParam(name = "domain") String domain);

    @WebMethod
    public AuthenticationResponse changeEBankingPassword(@WebParam(name = "systemSessionId") String systemSessionId, @WebParam(name = "userid") String userid, @WebParam(name = "oldpassword") String oldpassword, @WebParam(name = "newpassword") String newpassword, @WebParam(name = "channel") String channel, @WebParam(name = "domain") String domain);

    @WebMethod
    public Response assignPwdAuthenticationType(@WebParam(name = "systemSessionId") String systemSessionId, @WebParam(name = "userid") String userid, @WebParam(name = "password") String password, @WebParam(name = "channel") String channel, @WebParam(name = "domain") String domain);

    @WebMethod
    public AuthenticationResponse verifyEBankingOTP(@WebParam(name = "systemSessionId") String systemSessionId, @WebParam(name = "userid") String userid, @WebParam(name = "otptoken") String otptoken, @WebParam(name = "password") String password, @WebParam(name = "channel") String channel, @WebParam(name = "domain") String domain);

    @WebMethod
    public Response createUser(@WebParam(name = "systemSessionId") String systemSessionId, @WebParam(name = "userid") String userid, @WebParam(name = "firstname") String firstname, @WebParam(name = "lastname") String lastname, @WebParam(name = "postcode") String postcode, @WebParam(name = "address1") String address1, @WebParam(name = "address2") String address2, @WebParam(name = "city") String city, @WebParam(name = "mobileno") String mobileno, @WebParam(name = "channel") String channel, @WebParam(name = "domain") String domain);

    @WebMethod
    public Response systemLogout(@WebParam(name = "systemSessionId") String systemSessionId, @WebParam(name = "channel") String channel, @WebParam(name = "domain") String domain);

    @WebMethod
    public AuthenticationResponse systemLogin(@WebParam(name = "userid") String userid, @WebParam(name = "password") String password, @WebParam(name = "channel") String channel, @WebParam(name = "domain") String domain);

    @WebMethod
    public Response assignOtpAuthenticationType(@WebParam(name = "systemSessionId") String systemSessionId, @WebParam(name = "userid") String userid, @WebParam(name = "otptoken") String otptoken, @WebParam(name = "channel") String channel, @WebParam(name = "domain") String domain);

    @WebMethod
    public Response ping();

    
}
